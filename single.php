<?php get_header(); ?>

<?php the_post(); ?>

<?php anemos_eutf_print_header_title( 'post' ); ?>
<?php anemos_eutf_print_header_breadcrumbs( 'post' ); ?>
<?php anemos_eutf_print_anchor_menu( 'post' ); ?>

<div class="eut-single-wrapper">
	<!-- CONTENT -->
	<div id="eut-content" class="clearfix <?php echo anemos_eutf_sidebar_class(); ?>">
		<div class="eut-content-wrapper">
			<!-- MAIN CONTENT -->
			<div id="eut-main-content">
				<div class="eut-main-content-wrapper clearfix">

					<?php

						//Get Post template
						get_template_part( 'content', get_post_format() );

					?>
					<div class="eut-container">
					<?php

						// Print Post Meta
						anemos_eutf_print_post_meta();

						//Post Pagination
						wp_link_pages();

						//Print Comments
						if ( anemos_eutf_visibility( 'post_comments_visibility' ) ) {
							comments_template();
						}

					?>
					</div>

				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<?php anemos_eutf_set_current_view( 'post' ); ?>
			<?php get_sidebar(); ?>
		</div>

		<?php
			// Print Post Meta
			anemos_eutf_print_post_meta_sticky();

			//Print Post Navigation
			anemos_eutf_print_post_navigation( 'sticky' );
		?>

	</div>
	<!-- END CONTENT -->

	<?php

		//Related Posts
		if ( anemos_eutf_visibility( 'post_related_visibility' ) ) {
			anemos_eutf_print_related_posts();
		}
	?>
</div>

<?php
	if ( anemos_eutf_visibility( 'post_author_visibility' ) ) {
?>
	<div id="eut-post-author-modal" class="eut-modal">
		<div class="eut-modal-wrapper">
			<div class="eut-modal-content">
				<div class="eut-modal-item">
					<?php anemos_eutf_print_post_about_author( 'modal' ); ?>
				</div>
			</div>
		</div>
	</div>
<?php
	}
?>

<?php get_footer();

//Omit closing PHP tag to avoid accidental whitespace output errors.
