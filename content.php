<?php
/**
 * The default post template
 */
?>

<?php
if ( is_singular() ) {
	$anemos_eutf_disable_media = anemos_eutf_post_meta( '_anemos_eutf_disable_media' );
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('eut-single-post'); ?> itemscope itemType="http://schema.org/BlogPosting">

		<?php
			if ( anemos_eutf_visibility( 'post_feature_image_visibility', '1' ) && 'yes' != $anemos_eutf_disable_media && has_post_thumbnail() ) {
		?>
		<div id="eut-single-media">
			<div class="eut-container">
				<div class="eut-media clearfix">
					<?php the_post_thumbnail( 'anemos-eutf-fullscreen' ); ?>
				</div>
			</div>
		</div>
		<?php
			}
		?>

		<div id="eut-post-content">
			<?php anemos_eutf_print_post_simple_title(); ?>
			<?php anemos_eutf_print_post_structured_data(); ?>
			<div itemprop="articleBody">
				<?php the_content(); ?>
			</div>
		</div>
	</article>

<?php
} else {

	$blog_style = anemos_eutf_option( 'blog_style', 'large' );

	$post_style = anemos_eutf_post_meta( '_anemos_eutf_post_standard_style' );
	$bg_mode = false;

	if ( ( 'masonry' == $blog_style || 'grid' == $blog_style ) && 'anemos' == $post_style ) {
		$bg_mode = true;
	}
	if ( $bg_mode ) {
		$anemos_eutf_post_class = anemos_eutf_get_post_class("eut-style-2");
		$bg_color = anemos_eutf_post_meta( '_anemos_eutf_post_standard_bg_color', 'black' );
		$bg_opacity = anemos_eutf_post_meta( '_anemos_eutf_post_standard_bg_opacity', '70' );
		$bg_options = array(
			'bg_color' => $bg_color,
			'bg_opacity' => $bg_opacity,
		);
	} else {
		$anemos_eutf_post_class = anemos_eutf_get_post_class();
	}

?>
	<!-- Article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class( $anemos_eutf_post_class ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<?php do_action( 'anemos_eutf_inner_post_loop_item_before' ); ?>
		<?php if ( $bg_mode ) { ?>
		<?php anemos_eutf_print_post_bg_image_container( $bg_options ); ?>
		<?php } else { ?>
		<?php anemos_eutf_print_post_feature_media( 'image' ); ?>
		<?php } ?>
		<div class="eut-post-content-wrapper">
			<div class="eut-post-content">
				<?php anemos_eutf_print_post_meta_top(); ?>
				<?php anemos_eutf_print_post_structured_data(); ?>
				<div itemprop="articleBody">
					<?php anemos_eutf_print_post_excerpt(); ?>
				</div>
			</div>
			<?php anemos_eutf_print_post_meta_bottom(); ?>
		</div>
		<?php do_action( 'anemos_eutf_inner_post_loop_item_after' ); ?>
	</article>
	<!-- End Article -->

<?php

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
