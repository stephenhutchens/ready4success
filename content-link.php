<?php
/**
 * The Link Post Type Template
 */
?>

<?php
if ( is_singular() ) {

	$anemos_eutf_link = get_post_meta( get_the_ID(), '_anemos_eutf_post_link_url', true );
	$new_window = get_post_meta( get_the_ID(), '_anemos_eutf_post_link_new_window', true );

	if( empty( $anemos_eutf_link ) ) {
		$anemos_eutf_link = get_permalink();
	}

	$anemos_eutf_target = '_self';
	if( !empty( $new_window ) ) {
		$anemos_eutf_target = '_blank';
	}

	$bg_color = anemos_eutf_post_meta( '_anemos_eutf_post_link_bg_color', 'primary-1' );
	$bg_hover_color = anemos_eutf_post_meta( '_anemos_eutf_post_link_bg_hover_color', 'black' );
	$bg_opacity = anemos_eutf_post_meta( '_anemos_eutf_post_link_bg_opacity', '70' );
	$bg_options = array(
		'bg_color' => $bg_color,
		'bg_hover_color' => $bg_hover_color,
		'bg_opacity' => $bg_opacity,
	);

?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'eut-single-post' ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<div id="eut-single-link">
			<div class="eut-container">
				<a href="<?php echo esc_url( $anemos_eutf_link ); ?>" target="<?php echo esc_attr( $anemos_eutf_target ); ?>" rel="bookmark">
					<?php anemos_eutf_print_post_bg_image_container( $bg_options ); ?>
					<div class="eut-post-content">
						<h2 class="eut-post-title eut-h3" itemprop="name headline"><?php the_title(); ?></h2>
						<div class="eut-post-url eut-small-text eut-circle-arrow"><?php echo esc_url( $anemos_eutf_link ); ?></div>
					</div>
				</a>
			</div>
		</div>
		<div id="eut-post-content">
			<?php anemos_eutf_print_post_simple_title(); ?>
			<?php anemos_eutf_print_post_structured_data(); ?>
			<div itemprop="articleBody">
				<?php the_content(); ?>
			</div>
		</div>
	</article>

<?php
} else {

	$anemos_eutf_post_class = anemos_eutf_get_post_class( 'eut-style-2' );
	$anemos_eutf_link = get_post_meta( get_the_ID(), '_anemos_eutf_post_link_url', true );
	$new_window = get_post_meta( get_the_ID(), '_anemos_eutf_post_link_new_window', true );

	if( empty( $anemos_eutf_link ) ) {
		$anemos_eutf_link = get_permalink();
	}

	$anemos_eutf_target = '_self';
	if( !empty( $new_window ) ) {
		$anemos_eutf_target = '_blank';
	}

	$bg_color = anemos_eutf_post_meta( '_anemos_eutf_post_link_bg_color', 'primary-1' );
	$bg_hover_color = anemos_eutf_post_meta( '_anemos_eutf_post_link_bg_hover_color', 'black' );
	$bg_opacity = anemos_eutf_post_meta( '_anemos_eutf_post_link_bg_opacity', '70' );
	$bg_options = array(
		'bg_color' => $bg_color,
		'bg_hover_color' => $bg_hover_color,
		'bg_opacity' => $bg_opacity,
	);

?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( $anemos_eutf_post_class ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<?php do_action( 'anemos_eutf_inner_post_loop_item_before' ); ?>
		<a href="<?php echo esc_url( $anemos_eutf_link ); ?>" target="<?php echo esc_attr( $anemos_eutf_target ); ?>" rel="bookmark">
			<?php anemos_eutf_print_post_bg_image_container( $bg_options ); ?>
			<div class="eut-post-content">
				<?php anemos_eutf_loop_post_title(); ?>
				<?php anemos_eutf_print_post_structured_data(); ?>
				<div itemprop="articleBody">
				<?php anemos_eutf_print_post_excerpt('link'); ?>
				</div>
				<div class="eut-post-url eut-small-text eut-circle-arrow"><?php echo esc_url( $anemos_eutf_link ); ?></div>
			</div>
		</a>
		<?php do_action( 'anemos_eutf_inner_post_loop_item_after' ); ?>

	</article>

<?php

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
