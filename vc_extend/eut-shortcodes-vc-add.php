<?php
/*
 *	Euthemians Visual Composer Shortcode Extentions
 *
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */


if ( function_exists( 'vc_add_param' ) ) {

	//Generic css aniation for elements

	$anemos_eutf_add_animation = array(
		"type" => "dropdown",
		"heading" => esc_html__("CSS Animation", 'anemos' ),
		"param_name" => "animation",
		"admin_label" => true,
		"value" => array(
			esc_html__( "No", "anemos" ) => '',
			esc_html__( "Fade In", "anemos" ) => "eut-fade-in",
			esc_html__( "Fade In Up", "anemos" ) => "eut-fade-in-up",
			esc_html__( "Fade In Down", "anemos" ) => "eut-fade-in-down",
			esc_html__( "Fade In Left", "anemos" ) => "eut-fade-in-left",
			esc_html__( "Fade In Right", "anemos" ) => "eut-fade-in-right",
			esc_html__( "Zoom In", "anemos" ) => "eut-zoom-in",
		),
		"description" => esc_html__("Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", 'anemos' ),
	);

	$anemos_eutf_add_animation_delay = array(
		"type" => "textfield",
		"heading" => esc_html__( 'Css Animation Delay', 'anemos' ),
		"param_name" => "animation_delay",
		"value" => '200',
		"description" => esc_html__( "Add delay in milliseconds.", 'anemos' ),
	);

	$anemos_eutf_add_margin_bottom = array(
		"type" => "textfield",
		"heading" => esc_html__( 'Bottom margin', 'anemos' ),
		"param_name" => "margin_bottom",
		"description" => esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'anemos' ),
	);

	$anemos_eutf_add_el_class = array(
		"type" => "textfield",
		"heading" => esc_html__("Extra class name", 'anemos' ),
		"param_name" => "el_class",
		"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'anemos' ),
	);

	$anemos_eutf_column_width_list = array(
		esc_html__( '1 column - 1/12', 'anemos' ) => '1/12',
		esc_html__( '2 columns - 1/6', 'anemos' ) => '1/6',
		esc_html__( '3 columns - 1/4', 'anemos' ) => '1/4',
		esc_html__( '4 columns - 1/3', 'anemos' ) => '1/3',
		esc_html__( '5 columns - 5/12', 'anemos' ) => '5/12',
		esc_html__( '6 columns - 1/2', 'anemos' ) => '1/2',
		esc_html__( '7 columns - 7/12', 'anemos' ) => '7/12',
		esc_html__( '8 columns - 2/3', 'anemos' ) => '2/3',
		esc_html__( '9 columns - 3/4', 'anemos' ) => '3/4',
		esc_html__( '10 columns - 5/6', 'anemos' ) => '5/6',
		esc_html__( '11 columns - 11/12', 'anemos' ) => '11/12',
		esc_html__( '12 columns - 1/1', 'anemos' ) => '1/1'
	);

	$anemos_eutf_column_desktop_hide_list = array(
		esc_html__( 'Default value from width attribute', 'anemos') => '',
		esc_html__( 'Hide', 'anemos' ) => 'hide',
	);

	$anemos_eutf_column_width_tablet_list = array(
		esc_html__( 'Default value from width attribute', 'anemos') => '',
		esc_html__( 'Hide', 'anemos' ) => 'hide',
		esc_html__( '1 column - 1/12', 'anemos' ) => '1-12',
		esc_html__( '2 columns - 1/6', 'anemos' ) => '1-6',
		esc_html__( '3 columns - 1/4', 'anemos' ) => '1-4',
		esc_html__( '4 columns - 1/3', 'anemos' ) => '1-3',
		esc_html__( '5 columns - 5/12', 'anemos' ) => '5-12',
		esc_html__( '6 columns - 1/2', 'anemos' ) => '1-2',
		esc_html__( '7 columns - 7/12', 'anemos' ) => '7-12',
		esc_html__( '8 columns - 2/3', 'anemos' ) => '2-3',
		esc_html__( '9 columns - 3/4', 'anemos' ) => '3-4',
		esc_html__( '10 columns - 5/6', 'anemos' ) => '5-6',
		esc_html__( '11 columns - 11/12', 'anemos' ) => '11-12',
		esc_html__( '12 columns - 1/1', 'anemos' ) => '1',
	);

	$anemos_eutf_column_width_tablet_sm_list = array(
		esc_html__( 'Inherit from Tablet Landscape', 'anemos') => '',
		esc_html__( 'Hide', 'anemos' ) => 'hide',
		esc_html__( '1 column - 1/12', 'anemos' ) => '1-12',
		esc_html__( '2 columns - 1/6', 'anemos' ) => '1-6',
		esc_html__( '3 columns - 1/4', 'anemos' ) => '1-4',
		esc_html__( '4 columns - 1/3', 'anemos' ) => '1-3',
		esc_html__( '5 columns - 5/12', 'anemos' ) => '5-12',
		esc_html__( '6 columns - 1/2', 'anemos' ) => '1-2',
		esc_html__( '7 columns - 7/12', 'anemos' ) => '7-12',
		esc_html__( '8 columns - 2/3', 'anemos' ) => '2-3',
		esc_html__( '9 columns - 3/4', 'anemos' ) => '3-4',
		esc_html__( '10 columns - 5/6', 'anemos' ) => '5-6',
		esc_html__( '11 columns - 11/12', 'anemos' ) => '11-12',
		esc_html__( '12 columns - 1/1', 'anemos' ) => '1',
	);
	$anemos_eutf_column_mobile_width_list = array(
		esc_html__( '12 columns - 1/1', 'anemos' ) => '',
		esc_html__( 'Hide', 'anemos' ) => 'hide',
	);

	$anemos_eutf_column_gap_list = array(
		esc_html__( 'No Gap', 'anemos' ) => 'none',
		esc_html__( '5px', 'anemos' ) => '5',
		esc_html__( '10px', 'anemos' ) => '10',
		esc_html__( '15px', 'anemos' ) => '15',
		esc_html__( '20px', 'anemos' ) => '20',
		esc_html__( '25px', 'anemos' ) => '25',
		esc_html__( '30px', 'anemos' ) => '30',
		esc_html__( '35px', 'anemos' ) => '35',
		esc_html__( '40px', 'anemos' ) => '40',
		esc_html__( '45px', 'anemos' ) => '45',
		esc_html__( '50px', 'anemos' ) => '50',
		esc_html__( '55px', 'anemos' ) => '55',
		esc_html__( '60px', 'anemos' ) => '60',
	);

	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__('Section ID', 'anemos' ),
			"param_name" => "section_id",
			"description" => esc_html__("If you wish you can type an id to use it as bookmark.", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Font Color', 'anemos' ),
			"param_name" => "font_color",
			"description" => esc_html__("Select font color", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Heading Color", 'anemos' ),
			"param_name" => "heading_color",
			"value" => array(
				esc_html__( "Default", 'anemos' ) => '',
				esc_html__( "Dark", 'anemos' ) => 'dark',
				esc_html__( "Light", 'anemos' ) => 'light',
				esc_html__( "Primary 1", 'anemos' ) => 'primary-1',
				esc_html__( "Primary 2", 'anemos' ) => 'primary-2',
				esc_html__( "Primary 3", 'anemos' ) => 'primary-3',
				esc_html__( "Primary 4", 'anemos' ) => 'primary-4',
				esc_html__( "Primary 5", 'anemos' ) => 'primary-5',
			),
			"description" => esc_html__( "Select heading color", 'anemos' ),
		)
	);

	vc_add_param( "vc_row", $anemos_eutf_add_el_class );

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Section Type", 'anemos' ),
			"param_name" => "section_type",
			"value" => array(
				esc_html__( "Full Width Background", 'anemos' ) => 'fullwidth-background',
				esc_html__( "Full Width Element", 'anemos' ) => 'fullwidth',
			),
			"description" => esc_html__( "Select section type", 'anemos' ),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Section Window Height", 'anemos' ),
			"param_name" => "section_full_height",
			"value" => array(
				esc_html__( "No", 'anemos' ) => 'no',
				esc_html__( "Yes", 'anemos' ) => 'fullheight',
			),
			"description" => esc_html__( "Select if you want your section height to be equal with the window height", 'anemos' ),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => 'dropdown',
			"heading" => esc_html__( "Background Type", 'anemos' ),
			"param_name" => "bg_type",
			"description" => esc_html__( "Select Background type", 'anemos' ),
			"value" => array(
				esc_html__( "None", 'anemos' ) => '',
				esc_html__( "Color", 'anemos' ) => 'color',
				esc_html__( "Image", 'anemos' ) => 'image',
				esc_html__( "Hosted Video", 'anemos' ) => 'hosted_video',
				esc_html__( "YouTube Video", 'anemos' ) => 'video',
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'YouTube link', 'anemos' ),
			'param_name' => 'bg_video_url',
			'value' => 'https://www.youtube.com/watch?v=lMJXxhRFO1k',
			// default video url
			'description' => esc_html__( 'Add YouTube link.', 'anemos' ),
			'dependency' => array(
				'element' => 'bg_type',
				'value' => 'video',
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Video Popup Button", 'anemos' ),
			"param_name" => "bg_video_button",
			"value" => array(
				esc_html__( 'None', 'anemos' ) => '',
				esc_html__( 'Devices only', 'anemos' ) => 'device',
				esc_html__( 'Always visible', 'anemos' ) => 'all',
			),
			"description" => esc_html__( "Select video popup button behavior", 'anemos' ),
			'dependency' => array(
				'element' => 'bg_type',
				'value' => 'video',
			),
			"std" => 'center-center',
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Video Button Position", 'anemos' ),
			"param_name" => "bg_video_button_position",
			"value" => array(
				esc_html__( 'Left Top', 'anemos' ) => 'left-top',
				esc_html__( 'Left Bottom', 'anemos' ) => 'left-bottom',
				esc_html__( 'Center Center', 'anemos' ) => 'center-center',
				esc_html__( 'Right Top', 'anemos' ) => 'right-top',
				esc_html__( 'Right Bottom', 'anemos' ) => 'right-bottom',
			),
			"description" => esc_html__( "Select position for video popup", 'anemos' ),
			'dependency' => array(
				'element' => 'bg_video_button',
				'value_not_equal_to' => array( '' )
			),
			"std" => 'center-center',
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__( "Custom Background Color", 'anemos' ),
			"param_name" => "bg_color",
			"description" => esc_html__( "Select background color for your row", 'anemos' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'color' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "attach_image",
			"heading" => esc_html__('Background Image', 'anemos' ),
			"param_name" => "bg_image",
			"value" => '',
			"description" => esc_html__("Select background image for your row. Used also as fallback for video.", 'anemos' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video', 'video' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Background Image Type", 'anemos' ),
			"param_name" => "bg_image_type",
			"value" => array(
				esc_html__( "Default", 'anemos' ) => '',
				esc_html__( "Parallax", 'anemos' ) => 'parallax',
				esc_html__( "Horizontal Parallax Left to Right", 'anemos' ) => 'horizontal-parallax-lr',
				esc_html__( "Horizontal Parallax Right to Left", 'anemos' ) => 'horizontal-parallax-rl',
				esc_html__( "Animated", 'anemos' ) => 'animated',
				esc_html__( "Horizontal Animation", 'anemos' ) => 'horizontal',
				esc_html__( "Image usage as Pattern", 'anemos' ) => 'pattern'
			),
			"description" => esc_html__( "Select how a background image will be displayed", 'anemos' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Background Image Vertical Position", 'anemos' ),
			"param_name" => "bg_image_vertical_position",
			"value" => array(
				esc_html__( "Top", 'anemos' ) => 'top',
				esc_html__( "Center", 'anemos' ) => 'center',
				esc_html__( "Bottom", 'anemos' ) => 'bottom',
			),
			"description" => esc_html__( "Select vertical position for background image", 'anemos' ),
			"dependency" => array(
				'element' => 'bg_image_type',
				'value' => array( 'horizontal-parallax-lr', 'horizontal-parallax-rl' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Parallax Sensor", 'anemos' ),
			"param_name" => "parallax_sensor",
			"value" => array(
				esc_html__( "Low", 'anemos' ) => '150',
				esc_html__( "Normal", 'anemos' ) => '250',
				esc_html__( "High", 'anemos' ) => '350',
				esc_html__( "Max", 'anemos' ) => '550',
			),
			"description" => esc_html__( "Define the appearance for the parallax effect. Note that you get greater image zoom when you increase the parallax sensor.", 'anemos' ),
			"dependency" => array(
				'element' => 'bg_image_type',
				'value' => array( 'parallax', 'horizontal-parallax-lr', 'horizontal-parallax-rl' )
			),
			"std" => '250',
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__("WebM File URL", 'anemos'),
			"param_name" => "bg_video_webm",
			"description" => esc_html__( "Fill WebM and mp4 format for browser compatibility", 'anemos' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__( "MP4 File URL", 'anemos' ),
			"param_name" => "bg_video_mp4",
			"description" => esc_html__( "Fill mp4 format URL", 'anemos' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__( "OGV File URL", 'anemos' ),
			"param_name" => "bg_video_ogv",
			"description" => esc_html__( "Fill OGV format URL ( optional )", 'anemos' ),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Loop", 'anemos' ),
			"param_name" => "bg_video_loop",
			"value" => array(
				esc_html__( "Yes", 'anemos' ) => 'yes',
				esc_html__( "No", 'anemos' ) => 'no',
			),
			"std" => 'yes',
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'hosted_video' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Pattern overlay", 'anemos'),
			"param_name" => "pattern_overlay",
			"description" => esc_html__( "If selected, a pattern will be added.", 'anemos' ),
			"value" => Array(esc_html__( "Add pattern", 'anemos' ) => 'yes'),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video', 'video' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Color overlay", 'anemos' ),
			"param_name" => "color_overlay",
			"value" => array(
				esc_html__( "None", 'anemos' ) => '',
				esc_html__( "Dark", 'anemos' ) => 'dark',
				esc_html__( "Light", 'anemos' ) => 'light',
				esc_html__( "Primary 1", 'anemos' ) => 'primary-1',
				esc_html__( "Primary 2", 'anemos' ) => 'primary-2',
				esc_html__( "Primary 3", 'anemos' ) => 'primary-3',
				esc_html__( "Primary 4", 'anemos' ) => 'primary-4',
				esc_html__( "Primary 5", 'anemos' ) => 'primary-5',
				esc_html__( "Custom", 'anemos' ) => 'custom',
			),
			"dependency" => array(
				'element' => 'bg_type',
				'value' => array( 'image', 'hosted_video', 'video' )
			),
			"description" => esc_html__( "A color overlay for the media", 'anemos' ),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Custom Color Overlay', 'anemos' ),
			"param_name" => "color_overlay_custom",
			"dependency" => array(
				'element' => 'color_overlay',
				'value' => array( 'custom' )
			),
			"std" => '#ffffff',
			"description" => esc_html__("Select custom color overlay", 'anemos' ),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Opacity overlay", 'anemos' ),
			"param_name" => "opacity_overlay",
			"value" => array( 10, 20, 30 ,40, 50, 60, 70, 80 ,90 ),
			"description" => esc_html__( "Opacity of the overlay", 'anemos' ),
			"dependency" => array(
				'element' => 'color_overlay',
				'value' => array( 'dark', 'light', 'primary-1', 'primary-2', 'primary-3', 'primary-4', 'primary-5' )
			),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);


	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Top padding", 'anemos' ),
			"param_name" => "padding_top_multiplier",
			"value" => array(
				esc_html__( "None", 'anemos' ) => '',
				esc_html__( "1x", 'anemos' ) => '1x',
				esc_html__( "2x", 'anemos' ) => '2x',
				esc_html__( "3x", 'anemos' ) => '3x',
				esc_html__( "4x", 'anemos' ) => '4x',
				esc_html__( "5x", 'anemos' ) => '5x',
				esc_html__( "6x", 'anemos' ) => '6x',
				esc_html__( "Custom", 'anemos' ) => 'custom',
			),
			"dependency" => array(
				'element' => 'section_full_height',
				'value' => array( 'no' )
			),
			"std" => '1x',
			"description" => esc_html__( "Select padding top for your section.", 'anemos' ),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__( "Custom Top padding", 'anemos' ),
			"param_name" => "padding_top",
			"dependency" => array(
				'element' => 'padding_top_multiplier',
				'value' => array( 'custom' )
			),
			"description" => esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'anemos' ),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Bottom padding", 'anemos' ),
			"param_name" => "padding_bottom_multiplier",
			"value" => array(
				esc_html__( "None", 'anemos' ) => '',
				esc_html__( "1x", 'anemos' ) => '1x',
				esc_html__( "2x", 'anemos' ) => '2x',
				esc_html__( "3x", 'anemos' ) => '3x',
				esc_html__( "4x", 'anemos' ) => '4x',
				esc_html__( "5x", 'anemos' ) => '5x',
				esc_html__( "6x", 'anemos' ) => '6x',
				esc_html__( "Custom", 'anemos' ) => 'custom',
			),
			"dependency" => array(
				'element' => 'section_full_height',
				'value' => array( 'no' )
			),
			"std" => '1x',
			"description" => esc_html__( "Select padding bottom for your section.", 'anemos' ),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__( "Custom Bottom padding", 'anemos' ),
			"param_name" => "padding_bottom",
			"dependency" => array(
				'element' => 'padding_bottom_multiplier',
				'value' => array( 'custom' )
			),
			"description" => esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'anemos' ),
			"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
		"type" => "textfield",
		"heading" => esc_html__( 'Bottom margin', 'anemos' ),
		"param_name" => "margin_bottom",
		"description" => esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'anemos' ),
		"group" => esc_html__( "Section Options", 'anemos' ),
		)
	);
	
	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Columns Gap", 'anemos' ),
			"param_name" => "columns_gap",
			'value' => $anemos_eutf_column_gap_list,
			"description" => esc_html__( "Select gap between columns in row.", 'anemos' ),
			"group" => esc_html__( "Inner Columns", 'anemos' ),
			"std" => '30',
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Equal Column Height", 'anemos' ),
			"param_name" => "equal_column_height",
			"value" => array(
				esc_html__( "None", 'anemos' ) => 'none',
				esc_html__( "Equal Height Columns", 'anemos' ) => 'equal-column',
				esc_html__( "Equal Height Columns and Middle Content", 'anemos' ) => 'middle-content',
			),
			"description" => esc_html__( "Recommended for multiple columns with different background colors. Additionally you can set your columns content in middle. If you need some paddings in your columns, please place them only in the column with the largest content.", 'anemos' ),
			"group" => esc_html__( "Inner Columns", 'anemos' ),
		)
	);
	
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Desktop Visibility", 'anemos'),
			"param_name" => "desktop_visibility",
			"description" => esc_html__( "If selected, row will be hidden on desktops/laptops.", 'anemos' ),
			"value" => Array(esc_html__( "Hide", 'anemos' ) => 'hide'),
			'group' => esc_html__( "Responsiveness", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Tablet Landscape Visibility", 'anemos'),
			"param_name" => "tablet_visibility",
			"description" => esc_html__( "If selected, row will be hidden on tablet devices with landscape orientation.", 'anemos' ),
			"value" => Array(esc_html__( "Hide", 'anemos' ) => 'hide'),
			'group' => esc_html__( "Responsiveness", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Tablet Portrait Visibility", 'anemos'),
			"param_name" => "tablet_sm_visibility",
			"description" => esc_html__( "If selected, row will be hidden on tablet devices with portrait orientation.", 'anemos' ),
			"value" => Array(esc_html__( "Hide", 'anemos' ) => 'hide'),
			'group' => esc_html__( "Responsiveness", 'anemos' ),
		)
	);
	vc_add_param( "vc_row",
		array(
			"type" => 'checkbox',
			"heading" => esc_html__( "Mobile Visibility", 'anemos'),
			"param_name" => "mobile_visibility",
			"description" => esc_html__( "If selected, row will be hidden on mobile devices.", 'anemos' ),
			"value" => Array(esc_html__( "Hide", 'anemos' ) => 'hide'),
			'group' => esc_html__( "Responsiveness", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Header Style", 'anemos' ),
			"param_name" => "scroll_header_style",
			"value" => array(
				esc_html__( "Dark", 'anemos' ) => 'dark',
				esc_html__( "Light", 'anemos' ) => 'light',
			),
			"std" => 'dark',
			"description" => esc_html__( "Select header style", 'anemos' ),
			"group" => esc_html__( "Scrolling Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_row",
		array(
			"type" => "textfield",
			"heading" => esc_html__('Scrolling Section Title', 'anemos' ),
			"param_name" => "scroll_section_title",
			"description" => esc_html__("If you wish you can type a title for the side dot navigation.", 'anemos' ),
			"group" => esc_html__( "Scrolling Section Options", 'anemos' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( "Width", 'anemos' ),
			'param_name' => 'width',
			'value' => $anemos_eutf_column_width_list,
			'group' => esc_html__( "Width & Responsiveness", 'anemos' ),
			'description' => esc_html__( "Select column width.", 'anemos' ),
			'std' => '1/1'
		)
	);
	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Desktop", 'anemos' ),
			"param_name" => "desktop_hide",
			"value" => $anemos_eutf_column_desktop_hide_list,
			"description" => esc_html__( "Responsive column on desktops/laptops.", 'anemos' ),
			'group' => esc_html__( 'Width & Responsiveness', 'anemos' ),
		)
	);
	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Landscape", 'anemos' ),
			"param_name" => "tablet_width",
			"value" => $anemos_eutf_column_width_tablet_list,
			"description" => esc_html__( "Responsive column on tablet devices with landscape orientation.", 'anemos' ),
			'group' => esc_html__( 'Width & Responsiveness', 'anemos' ),
		)
	);
	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Portrait", 'anemos' ),
			"param_name" => "tablet_sm_width",
			"value" => $anemos_eutf_column_width_tablet_sm_list,
			"description" => esc_html__( "Responsive column on tablet devices with portrait orientation.", 'anemos' ),
			'group' => esc_html__( 'Width & Responsiveness', 'anemos' ),
		)
	);
	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Mobile", 'anemos' ),
			"param_name" => "mobile_width",
			"value" => $anemos_eutf_column_mobile_width_list,
			"description" => esc_html__( "Responsive column on mobile devices.", 'anemos' ),
			'group' => esc_html__( 'Width & Responsiveness', 'anemos' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "colorpicker",
			"heading" => esc_html__('Font Color', 'anemos' ),
			"param_name" => "font_color",
			"description" => esc_html__("Select font color", 'anemos' ),
		)
	);

	vc_add_param( "vc_column",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Heading Color", 'anemos' ),
			"param_name" => "heading_color",
			"value" => array(
				esc_html__( "Default", 'anemos' ) => '',
				esc_html__( "Dark", 'anemos' ) => 'dark',
				esc_html__( "Light", 'anemos' ) => 'light',
				esc_html__( "Primary 1", 'anemos' ) => 'primary-1',
				esc_html__( "Primary 2", 'anemos' ) => 'primary-2',
				esc_html__( "Primary 3", 'anemos' ) => 'primary-3',
				esc_html__( "Primary 4", 'anemos' ) => 'primary-4',
				esc_html__( "Primary 5", 'anemos' ) => 'primary-5',
			),
			"description" => esc_html__( "Select heading color", 'anemos' ),
		)
	);

	vc_add_param( "vc_column_inner",
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( "Width", 'anemos' ),
			'param_name' => 'width',
			'value' => $anemos_eutf_column_width_list,
			'group' => esc_html__( "Width & Responsiveness", 'anemos' ),
			'description' => esc_html__( "Select column width.", 'anemos' ),
			'std' => '1/1'
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Desktop", 'anemos' ),
			"param_name" => "desktop_hide",
			"value" => $anemos_eutf_column_desktop_hide_list,
			"description" => esc_html__( "Responsive column on desktops/laptops.", 'anemos' ),
			'group' => esc_html__( 'Width & Responsiveness', 'anemos' ),
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Landscape", 'anemos' ),
			"param_name" => "tablet_width",
			"value" => $anemos_eutf_column_width_tablet_list,
			"description" => esc_html__( "Responsive column on tablet devices with landscape orientation.", 'anemos' ),
			'group' => esc_html__( 'Width & Responsiveness', 'anemos' ),
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Tablet Portrait", 'anemos' ),
			"param_name" => "tablet_sm_width",
			"value" => $anemos_eutf_column_width_tablet_sm_list,
			"description" => esc_html__( "Responsive column on tablet devices with portrait orientation.", 'anemos' ),
			'group' => esc_html__( 'Width & Responsiveness', 'anemos' ),
		)
	);
	vc_add_param( "vc_column_inner",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Mobile", 'anemos' ),
			"param_name" => "mobile_width",
			"value" => $anemos_eutf_column_mobile_width_list,
			"description" => esc_html__( "Responsive column on mobile devices.", 'anemos' ),
			'group' => esc_html__( 'Width & Responsiveness', 'anemos' ),
		)
	);

	vc_add_param( "vc_widget_sidebar",
		array(
			'type' => 'hidden',
			'param_name' => 'title',
			'value' => '',
		)
	);

	if ( defined( 'WPB_VC_VERSION' ) && version_compare( WPB_VC_VERSION, '4.6', '>=') ) {

		vc_add_param( "vc_tta_tabs",
			array(
				'type' => 'hidden',
				'param_name' => 'no_fill_content_area',
				'value' => '',
			)
		);

		vc_add_param( "vc_tta_tabs",
			array(
				'type' => 'hidden',
				'param_name' => 'tab_position',
				'value' => 'top',
			)
		);

		vc_add_param( "vc_tta_accordion",
			array(
				'type' => 'hidden',
				'param_name' => 'no_fill',
				'value' => '',
			)
		);

		vc_add_param( "vc_tta_tour",
			array(
				'type' => 'hidden',
				'param_name' => 'no_fill_content_area',
				'value' => '',
			)
		);
	}

	vc_add_param( "vc_column_text",
		array(
			"type" => "dropdown",
			"heading" => esc_html__( "Text Style", 'anemos' ),
			"param_name" => "text_style",
			"value" => array(
				esc_html__( "None", 'anemos' ) => '',
				esc_html__( "Leader", 'anemos' ) => 'leader-text',
				esc_html__( "Subtitle", 'anemos' ) => 'subtitle',
			),
			"description" => esc_html__( "Select your text style", 'anemos' ),
		)
	);
	vc_add_param( "vc_column_text", $anemos_eutf_add_animation );
	vc_add_param( "vc_column_text", $anemos_eutf_add_animation_delay );


}

//Omit closing PHP tag to avoid accidental whitespace output errors.
