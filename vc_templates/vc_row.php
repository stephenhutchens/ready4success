<?php

	$output = $out_pattern = $out_overlay = $out_image_bg = $out_video_bg = '';

	extract(
		shortcode_atts(
			array(
				'section_id' => '',
				'font_color' => '',
				'heading_color' => '',
				'section_type' => 'fullwidth-background',
				'columns_gap' => '30',
				'section_full_height' => 'no',
				'equal_column_height' => 'none',
				'desktop_visibility' => '',
				'tablet_visibility' => '',
				'tablet_sm_visibility' => '',
				'mobile_visibility' => '',
				'bg_color' => '',
				'bg_type' => '',
				'bg_image' => '',
				'bg_image_type' => 'none',
				'bg_image_vertical_position' => 'top',
				'parallax_sensor' => '250',
				'pattern_overlay' => '',
				'color_overlay' => '',
				'color_overlay_custom' => '#ffffff',
				'opacity_overlay' => '',
				'bg_video_webm' => '',
				'bg_video_mp4' => '',
				'bg_video_ogv' => '',
				'bg_video_loop' => 'yes',
				'bg_video_url' => 'https://www.youtube.com/watch?v=lMJXxhRFO1k',
				'bg_video_button' => '',
				'bg_video_button_position' => 'center-center',
				'padding_top_multiplier' => '1x',
				'padding_bottom_multiplier' => '1x',
				'padding_top' => '',
				'padding_bottom' => '',
				'margin_bottom' => '',
				'el_class' => '',
				'el_id' => '',
				'css' => '',
				'scroll_section_title' => '',
				'scroll_header_style' => 'dark',
			),
			$atts
		)
	);

	if ( 'image' == $bg_type || 'hosted_video' == $bg_type ) {

		if ( !empty ( $color_overlay ) && 'custom' != $color_overlay ) {

			//Overlay Classes
			$overlay_classes = array();
			$overlay_classes[] = 'eut-bg-overlay eut-bg-' . $color_overlay;
			if ( !empty ( $opacity_overlay ) ) {
				$overlay_classes[] = 'eut-opacity-' . $opacity_overlay;
			}
			$overlay_string = implode( ' ', $overlay_classes );
			$out_overlay .= '  <div class="' . esc_attr( $overlay_string ) .'"></div>';
		}

		if ( 'custom' == $color_overlay ) {
			$out_overlay .= '  <div class="eut-bg-overlay" style="background-color:' . esc_attr( $color_overlay_custom ) . '"></div>';
		}
	}

	// Pattern Overlay
	if ( !empty ( $pattern_overlay ) ) {
		$out_pattern .= '  <div class="eut-pattern"></div>';
	}

	//Background Image Classses
	$bg_image_classes = array( 'eut-bg-image' );

	if( 'horizontal-parallax-lr' == $bg_image_type || 'horizontal-parallax-rl' == $bg_image_type ){
		$bg_image_classes[] = 'eut-bg-center-' . $bg_image_vertical_position;
	}

	$bg_image_string = implode( ' ', $bg_image_classes );

	//Background Image
	$img_style = anemos_eutf_build_shortcode_img_style( $bg_image ,$bg_image_type );

	if ( ( 'image' == $bg_type || 'hosted_video' == $bg_type ) && !empty ( $bg_image ) && ('parallax' !== $bg_image_type ) ) {
		$out_image_bg .= '  <div class="' . esc_attr( $bg_image_string ) . '"  ' . $img_style . '></div>';
	}

	if ( ( 'image' == $bg_type || 'hosted_video' == $bg_type ) && !empty ( $bg_image ) && ('parallax' == $bg_image_type ) ) {
		$out_image_bg .= '  <div class="' . esc_attr( $bg_image_string ) . '" ' . $img_style . '></div>';
	}

	//Background Video
	if ( 'hosted_video' == $bg_type && ( !empty ( $bg_video_webm ) || !empty ( $bg_video_mp4 ) || !empty ( $bg_video_ogv ) ) ) {

		$video_settings = array(
			'preload' => 'auto',
			'autoplay' => 'yes',
			'loop' => $bg_video_loop,
			'muted' => 'yes',
		);

		$out_video_bg .= '<div class="eut-bg-video">';
		$out_video_bg .=  '<video ' . anemos_eutf_print_media_video_settings( $video_settings ) . '>';
		if ( !empty ( $bg_video_webm ) ) {
			$out_video_bg .=  '<source src="' . esc_url( $bg_video_webm ) . '" type="video/webm">';
		}
		if ( !empty ( $bg_video_mp4 ) ) {
			$out_video_bg .=  '<source src="' . esc_url( $bg_video_mp4 ) . '" type="video/mp4">';
		}
		if ( !empty ( $bg_video_ogv ) ) {
			$out_video_bg .=  '<source src="' . esc_url( $bg_video_ogv ) . '" type="video/ogg">';
		}
		$out_video_bg .=  '</video>';
		$out_video_bg .= '</div>';
	}
	
	if ( wp_is_mobile() ) {
		$out_video_bg = '';
	}

	//YouTube Video
	$out_video_bg_url = '';
	$has_video_bg = ( 'video' == $bg_type && ! empty( $bg_video_url ) && vc_extract_youtube_id( $bg_video_url ) );
	if ( $has_video_bg ) {
		wp_enqueue_script( 'vc_youtube_iframe_api_js' );
		$out_video_bg_url .= '<div class="eut-bg-video-wrapper" data-vc-video-bg="' . esc_attr( $bg_video_url ) . '"></div>';
		if ( !empty( $bg_video_button ) ) {
			$out_video_bg_url .= '<a class="eut-video-popup eut-bg-video-button-' . esc_attr( $bg_video_button ) . '" href="' . esc_url( $bg_video_url ) . '">';
			$out_video_bg_url .= '<div class="eut-video-icon eut-icon-' . esc_attr( $bg_video_button_position )  . ' eut-icon-video eut-bg-primary-1"></div>';
			$out_video_bg_url .= '</a>';
		}
	}

	//Section Classses
	$section_classes = array( 'eut-section' );

	$section_classes[] = 'eut-' . $section_type ;

	if ( 'yes' != $section_full_height ) {
		if ( !empty ( $padding_top_multiplier ) && 'custom' != $padding_top_multiplier  ) {
			$section_classes[] = 'eut-padding-top-' . $padding_top_multiplier;
		} else if( 'custom' != $padding_top_multiplier ) {
			$padding_top ="";
		}

		if ( !empty ( $padding_bottom_multiplier ) && 'custom' != $padding_bottom_multiplier ) {
			$section_classes[] = 'eut-padding-bottom-' . $padding_bottom_multiplier;
		} else if( 'custom' != $padding_bottom_multiplier ) {
			$padding_bottom ="";
		}
	} else {
		$padding_top = $padding_bottom = "";
	}

	if( 'horizontal-parallax-lr' == $bg_image_type || 'horizontal-parallax-rl' == $bg_image_type ){
		$section_classes[] = 'eut-' . $bg_image_type;
		$section_classes[] = 'eut-bg-parallax';
	} else {
		$section_classes[] = 'eut-bg-' . $bg_image_type;
	}

	if ( !empty ( $heading_color ) ) {
		$section_classes[] = 'eut-headings-' . $heading_color;
	}
	if( 'none' != $equal_column_height ) {
		$section_classes[] = 'eut-' . $equal_column_height;
	}
	if( 'no' != $section_full_height ) {
		$section_classes[] = 'eut-' . $section_full_height;
	}
	if ( !empty ( $el_class ) ) {
		$section_classes[] = $el_class;
	}

	if( vc_settings()->get( 'not_responsive_css' ) != '1') {
		if ( !empty( $desktop_visibility ) ) {
			$section_classes[] = 'eut-desktop-row-hide';
		}
		if ( !empty( $tablet_visibility ) ) {
			$section_classes[] = 'eut-tablet-row-hide';
		}
		if ( !empty( $tablet_sm_visibility ) ) {
			$section_classes[] = 'eut-tablet-sm-row-hide';
		}
		if ( !empty( $mobile_visibility ) ) {
			$section_classes[] = 'eut-mobile-row-hide';
		}
	}

	$section_string = implode( ' ', $section_classes );

	$wrapper_attributes = array();

	if ( !empty ( $section_id ) ) {
		$wrapper_attributes[] = 'id="' . esc_attr( $section_id ) . '"';
	}

	$wrapper_attributes[] = 'class="' . esc_attr( $section_string ) . '"';

	if ( is_page_template( 'page-templates/template-full-page.php' ) ) {
		$section_uniqid = uniqid('eut-scrolling-section-');
		$wrapper_attributes[] = 'data-anchor="' . esc_attr( $section_uniqid ) . '"';
		$wrapper_attributes[] = 'data-anchor-tooltip="' . esc_attr( $scroll_section_title ) . '"';
		$wrapper_attributes[] = 'data-header-color="' . esc_attr( $scroll_header_style ) . '"';
	}

	if( 'parallax' == $bg_image_type || 'horizontal-parallax-lr' == $bg_image_type || 'horizontal-parallax-rl' == $bg_image_type ){
		$wrapper_attributes[] = 'data-parallax-sensor="' . esc_attr( $parallax_sensor ) . '"';
	}

	$style = anemos_eutf_build_shortcode_style(
		array(
			'bg_color' => $bg_color,
			'font_color' => $font_color,
			'padding_top' => $padding_top,
			'padding_bottom' => $padding_bottom,
			'margin_bottom' => $margin_bottom,
		)
	);

	if( !empty( $style ) ) {
		$wrapper_attributes[] = $style;
	}

	$row_classes = array( 'eut-row', 'eut-bookmark' );
	if( !empty( $columns_gap ) ) {
		$row_classes[] = 'eut-columns-gap-' . $columns_gap;
	}
	$row_css_string = implode( ' ', $row_classes );

	//Section Output
	$output .= '<div ' . implode( ' ', $wrapper_attributes ) . '>';
	$output .= '  <div class="eut-container">';
	$output	.= '    <div class="' . esc_attr( $row_css_string ) . '">';
	$output	.= do_shortcode( $content );
	$output	.= '    </div>';
	$output	.= '  </div>';
	$output .= '  <div class="eut-background-wrapper">';
	$output .= $out_video_bg_url;
	$output .= $out_image_bg;
	$output .= $out_video_bg;
	$output .= $out_overlay;
	$output .= $out_pattern;
	$output	.= '  </div>';

	$output	.= '</div>';

	print $output;

//Omit closing PHP tag to avoid accidental whitespace output errors.
