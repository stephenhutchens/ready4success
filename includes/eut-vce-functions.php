<?php

/*
*	Visual Composer Extension Plugin Hooks
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

/**
 * Translation function returning the theme translations
 */

/* All */
function anemos_eutf_theme_vce_get_string_all() {
    return esc_html__( 'All', 'anemos' );
}
/* Read more */
function anemos_eutf_theme_vce_get_string_read_more() {
    return esc_html__( 'read more', 'anemos' );
}
/* In Categories */
function anemos_eutf_theme_vce_get_string_categories_in() {
    return esc_html__( 'in', 'anemos' );
}

/* Author By */
function anemos_eutf_theme_vce_get_string_by_author() {
    return esc_html__( 'By:', 'anemos' );
}

/* E-mail */
function anemos_eutf_theme_vce_get_string_email() {
    return esc_html__( 'E-mail', 'anemos' );
}

/**
 * Hooks for portfolio translations
 */

add_filter( 'anemos_eutf_vce_portfolio_string_all_categories', 'anemos_eutf_theme_vce_get_string_all' );

 /**
 * Hooks for blog translations
 */

add_filter( 'anemos_eutf_vce_string_read_more', 'anemos_eutf_theme_vce_get_string_read_more' );
add_filter( 'anemos_eutf_vce_blog_string_all_categories', 'anemos_eutf_theme_vce_get_string_all' );
add_filter( 'anemos_eutf_vce_blog_string_categories_in', 'anemos_eutf_theme_vce_get_string_categories_in' );
add_filter( 'anemos_eutf_vce_blog_string_by_author', 'anemos_eutf_theme_vce_get_string_by_author' );

 /**
 * Hooks for general translations
 */
 
 add_filter( 'anemos_eutf_vce_string_email', 'anemos_eutf_theme_vce_get_string_email' );

//Omit closing PHP tag to avoid accidental whitespace output errors.
