<?php
/*
*	Helper Functions for meta options ( Post / Page / Portfolio / Product )
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


/**
 * Functions to print global metaboxes
 */
add_action( 'add_meta_boxes', 'anemos_eutf_generic_options_add_custom_boxes' );
add_action( 'save_post', 'anemos_eutf_generic_options_save_postdata', 10, 2 );

function anemos_eutf_generic_options_add_custom_boxes() {

	if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
		return;
	}

	//General Page Options
	add_meta_box(
		'eut-page-options',
		esc_html__( 'Page Options', 'anemos' ),
		'anemos_eutf_page_options_box',
		'page'
	);
	add_meta_box(
		'eut-page-options',
		esc_html__( 'Post Options', 'anemos' ),
		'anemos_eutf_page_options_box',
		'post'
	);

	$feature_section_post_types = anemos_eutf_option( 'feature_section_post_types');

	if ( !empty( $feature_section_post_types ) ) {

		foreach ( $feature_section_post_types as $key => $value ) {

			if ( 'attachment' != $value ) {

				add_meta_box(
					'_anemos_eutf_page_feature_section',
					esc_html__( 'Feature Section', 'anemos' ),
					'anemos_eutf_page_feature_section_box',
					$value,
					'advanced',
					'low'
				);

			}

		}
	}

}

/**
 * Page Options Metabox
 */
function anemos_eutf_page_options_box( $post ) {

	$post_type = get_post_type( $post->ID );
	$anemos_eutf_page_title_selection = array(
		'' => esc_html__( '-- Inherit --', 'anemos' ),
		'custom' => esc_html__( 'Custom Advanced Title', 'anemos' ),
	);
	switch( $post_type ) {
		case 'portfolio':
			$anemos_eutf_theme_options_info = esc_html__( 'Inherit : Theme Options - Portfolio Options.', 'anemos' );
			$anemos_eutf_theme_options_info_text_empty =  esc_html__('If empty, text is configured from: Theme Options - Portfolio Options.', 'anemos' );
			$anemos_eutf_desc_info = esc_html__( 'Enter your portfolio description.', 'anemos' );
		break;
		case 'post':
			$anemos_eutf_theme_options_info = esc_html__( 'Inherit : Theme Options - Blog Options - Single Post.', 'anemos' );
			$anemos_eutf_theme_options_info_text_empty =  esc_html__('If empty, text is configured from: Theme Options - Blog Options - Single Post.', 'anemos' );
			$anemos_eutf_desc_info = esc_html__( 'Enter your post description( Not available in Simple Title ).', 'anemos' );
			$anemos_eutf_page_title_selection = array(
				'' => esc_html__( '-- Inherit --', 'anemos' ),
				'custom' => esc_html__( 'Custom Advanced Title', 'anemos' ),
				'simple' => esc_html__( 'Simple Title', 'anemos' ),
			);
		break;
		case 'product':
			$anemos_eutf_theme_options_info = esc_html__( 'Inherit : Theme Options - WooCommerce Options - Single Product.', 'anemos' );
			$anemos_eutf_theme_options_info_text_empty =  esc_html__('If empty, text is configured from: Theme Options - WooCommerce Options - Single Product.', 'anemos' );
			$anemos_eutf_desc_info = esc_html__( 'Enter your product description( Not available in Simple Title ).', 'anemos' );
			$anemos_eutf_page_title_selection = array(
				'' => esc_html__( '-- Inherit --', 'anemos' ),
				'custom' => esc_html__( 'Custom Advanced Title', 'anemos' ),
				'simple' => esc_html__( 'Simple Title', 'anemos' ),
			);
		break;
		case 'page':
		default:
			$anemos_eutf_theme_options_info = esc_html__( 'Inherit : Theme Options - Page Options.', 'anemos' );
			$anemos_eutf_theme_options_info_text_empty =  esc_html__('If empty, text is configured from: Theme Options - Page Options.', 'anemos' );
			$anemos_eutf_desc_info = esc_html__( 'Enter your page description.', 'anemos' );
		break;
	}

	wp_nonce_field( 'anemos_eutf_nonce_page_save', '_anemos_eutf_nonce_page_save' );


	$anemos_eutf_custom_title_options = get_post_meta( $post->ID, '_anemos_eutf_custom_title_options', true );
	$anemos_eutf_description = get_post_meta( $post->ID, '_anemos_eutf_description', true );

	//Layout Fields
	$anemos_eutf_padding_top = get_post_meta( $post->ID, '_anemos_eutf_padding_top', true );
	$anemos_eutf_padding_bottom = get_post_meta( $post->ID, '_anemos_eutf_padding_bottom', true );
	$anemos_eutf_layout = get_post_meta( $post->ID, '_anemos_eutf_layout', true );
	$anemos_eutf_sidebar = get_post_meta( $post->ID, '_anemos_eutf_sidebar', true );
	$anemos_eutf_fixed_sidebar = get_post_meta( $post->ID, '_anemos_eutf_fixed_sidebar', true );
	$anemos_eutf_post_content_width = get_post_meta( $post->ID, '_anemos_eutf_post_content_width', true ); //Post Only

	//Sliding Area
	$anemos_eutf_sidearea_visibility = get_post_meta( $post->ID, '_anemos_eutf_sidearea_visibility', true );
	$anemos_eutf_sidearea_sidebar = get_post_meta( $post->ID, '_anemos_eutf_sidearea_sidebar', true );

	//Header - Main Menu Fields
	$anemos_eutf_header_overlapping = get_post_meta( $post->ID, '_anemos_eutf_header_overlapping', true );
	$anemos_eutf_header_style = get_post_meta( $post->ID, '_anemos_eutf_header_style', true );
	$anemos_eutf_main_navigation_menu = get_post_meta( $post->ID, '_anemos_eutf_main_navigation_menu', true );
	$anemos_eutf_sticky_header_type = get_post_meta( $post->ID, '_anemos_eutf_sticky_header_type', true );
	$anemos_eutf_menu_type = get_post_meta( $post->ID, '_anemos_eutf_menu_type', true );

	//Extras
	$anemos_eutf_details_title = get_post_meta( $post->ID, '_anemos_eutf_details_title', true ); //Portfolio Only
	$anemos_eutf_details = get_post_meta( $post->ID, '_anemos_eutf_details', true ); //Portfolio Only
	$anemos_eutf_backlink_id = get_post_meta( $post->ID, '_anemos_eutf_backlink_id', true ); //Portfolio Only

	$anemos_eutf_anchor_navigation_menu = get_post_meta( $post->ID, '_anemos_eutf_anchor_navigation_menu', true );
	$anemos_eutf_theme_loader = get_post_meta( $post->ID, '_anemos_eutf_theme_loader', true );
	$anemos_eutf_responsive_scrolling = get_post_meta( $post->ID, 'anemos_eutf_responsive_scrolling', true );

	//Visibility Fields
	$anemos_eutf_disable_top_bar = get_post_meta( $post->ID, '_anemos_eutf_disable_top_bar', true );
	$anemos_eutf_disable_sticky = get_post_meta( $post->ID, '_anemos_eutf_disable_sticky', true );
	$anemos_eutf_disable_logo = get_post_meta( $post->ID, '_anemos_eutf_disable_logo', true );
	$anemos_eutf_disable_menu = get_post_meta( $post->ID, '_anemos_eutf_disable_menu', true );
	$anemos_eutf_disable_menu_items = get_post_meta( $post->ID, '_anemos_eutf_disable_menu_items', true );
	$anemos_eutf_disable_breadcrumbs = get_post_meta( $post->ID, '_anemos_eutf_disable_breadcrumbs', true );
	$anemos_eutf_disable_title = get_post_meta( $post->ID, '_anemos_eutf_disable_title', true );
	$anemos_eutf_disable_media = get_post_meta( $post->ID, '_anemos_eutf_disable_media', true ); //Post Only
	$anemos_eutf_disable_content = get_post_meta( $post->ID, '_anemos_eutf_disable_content', true ); //Page Only
	$anemos_eutf_disable_recent_entries = get_post_meta( $post->ID, '_anemos_eutf_disable_recent_entries', true );//Portfolio Only
	$anemos_eutf_disable_bottom_bar = get_post_meta( $post->ID, '_anemos_eutf_disable_bottom_bar', true );
	$anemos_eutf_disable_footer = get_post_meta( $post->ID, '_anemos_eutf_disable_footer', true );
	$anemos_eutf_disable_copyright = get_post_meta( $post->ID, '_anemos_eutf_disable_copyright', true );
	$anemos_eutf_disable_back_to_top = get_post_meta( $post->ID, '_anemos_eutf_disable_back_to_top', true );

?>

	<!--  METABOXES -->
	<div class="eut-metabox-content">

		<!-- TABS -->
		<div class="eut-tabs">

			<ul class="eut-tab-links">
				<li class="active"><a href="#eut-page-option-tab-header"><?php esc_html_e( 'Header / Main Menu', 'anemos' ); ?></a></li>
				<li><a href="#eut-page-option-tab-title"><?php esc_html_e( 'Title / Description', 'anemos' ); ?></a></li>
				<li><a href="#eut-page-option-tab-layout"><?php esc_html_e( 'Content / Sidebars', 'anemos' ); ?></a></li>
				<li><a href="#eut-page-option-tab-sliding-area"><?php esc_html_e( 'Sliding Area', 'anemos' ); ?></a></li>
				<li><a href="#eut-page-option-tab-extras"><?php esc_html_e( 'Extras', 'anemos' ); ?></a></li>
				<li><a href="#eut-page-option-tab-visibility"><?php esc_html_e( 'Visibility', 'anemos' ); ?></a></li>
			</ul>
			<div class="eut-tab-content">

				<div id="eut-page-option-tab-header" class="eut-tab-item active">
					<?php

						//Header Overlapping Option
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_header_overlapping',
								'id' => '_anemos_eutf_header_overlapping',
								'value' => $anemos_eutf_header_overlapping,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'anemos' ),
									'yes' => esc_html__( 'Yes', 'anemos' ),
									'no' => esc_html__( 'No', 'anemos' ),
								),
								'label' => array(
									"title" => esc_html__( 'Header Overlapping', 'anemos' ),
									"desc" => esc_html__( 'Select if you want to overlap your page header', 'anemos' ),
									"info" => $anemos_eutf_theme_options_info,
								),
							)
						);

						//Header Style Option
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_header_style',
								'id' => '_anemos_eutf_header_style',
								'value' => $anemos_eutf_header_style,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'anemos' ),
									'default' => esc_html__( 'Default', 'anemos' ),
									'dark' => esc_html__( 'Dark', 'anemos' ),
									'light' => esc_html__( 'Light', 'anemos' ),
								),
								'label' => array(
									"title" => esc_html__( 'Header Style', 'anemos' ),
									"desc" => esc_html__( 'With this option you can change the coloring of your header. In case that you use Slider in Feature Section, select the header style per slide/image.', 'anemos' ),
									"info" => $anemos_eutf_theme_options_info,
								),
							)
						);

						//Main Navigation Menu Option
						anemos_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Main Navigation Menu', 'anemos' ),
									"desc" => esc_html__( 'Select alternative main navigation menu.', 'anemos' ),
									"info" => esc_html__( 'Inherit : Menus - Theme Locations - Header Menu.', 'anemos' ),
								),
							)
						);
						anemos_eutf_print_menu_selection( $anemos_eutf_main_navigation_menu, 'eut-main-navigation-menu', '_anemos_eutf_main_navigation_menu', 'default' );
						anemos_eutf_print_admin_option_wrapper_end();

						//Menu Type
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_menu_type',
								'id' => '_anemos_eutf_menu_type',
								'value' => $anemos_eutf_menu_type,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'anemos' ),
									'classic' => esc_html__( 'Classic', 'anemos' ),
									'button' => esc_html__( 'Button Style', 'anemos' ),
									'underline' => esc_html__( 'Underline', 'anemos' ),
									'hidden' => esc_html__( 'Hidden', 'anemos' ),
								),
								'label' => array(
									"title" => esc_html__( 'Menu Type', 'anemos' ),
									"desc" => esc_html__( 'With this option you can select the type of the menu ( Not available for Side Header Mode ).', 'anemos' ),
									"info" => esc_html__( 'Inherit : Theme Options - Header Options.', 'anemos' ),
								),
							)
						);

						//Sticky Header Type
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_sticky_header_type',
								'id' => '_anemos_eutf_sticky_header_type',
								'value' => $anemos_eutf_sticky_header_type,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'anemos' ),
									'simple' => esc_html__( 'Simple', 'anemos' ),
									'shrink' => esc_html__( 'Shrink', 'anemos' ),
									'advanced' => esc_html__( 'Advanced Shrink', 'anemos' ),
									'anemos' => esc_html__( 'Anemos Sticky', 'anemos' ),
								),
								'label' => array(
									"title" => esc_html__( 'Sticky Header Type', 'anemos' ),
									"desc" => esc_html__( 'With this option you can select the type of sticky header.', 'anemos' ),
									"info" => esc_html__( 'Inherit : Theme Options - Header Options - Sticky Header Options.', 'anemos' ),
								),
							)
						);
					?>
				</div>
				<div id="eut-page-option-tab-title" class="eut-tab-item">
					<?php

						echo '<div id="_anemos_eutf_page_title">';

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_disable_title',
								'id' => '_anemos_eutf_disable_title',
								'value' => $anemos_eutf_disable_title,
								'options' => array(
									'' => esc_html__( 'Visible', 'anemos' ),
									'yes' => esc_html__( 'Hidden', 'anemos' ),

								),
								'label' => array(
									"title" => esc_html__( 'Title/Description Visibility', 'anemos' ),
									"desc" => esc_html__( 'Select if you want to hide your title and decription .', 'anemos' ),
								),
								'group_id' => '_anemos_eutf_page_title',
							)
						);

						//Description Option
						anemos_eutf_print_admin_option(
							array(
								'type' => 'textfield',
								'name' => '_anemos_eutf_description',
								'id' => '_anemos_eutf_description',
								'value' => $anemos_eutf_description,
								'label' => array(
									'title' => esc_html__( 'Description', 'anemos' ),
									'desc' => $anemos_eutf_desc_info,
								),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						//Custom Title Option

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_page_title_custom',
								'id' => '_anemos_eutf_page_title_custom',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'custom' ),
								'options' => $anemos_eutf_page_title_selection,
								'label' => array(
									"title" => esc_html__( 'Title Options', 'anemos' ),
									"info" => $anemos_eutf_theme_options_info,
								),
								'group_id' => '_anemos_eutf_page_title',
								'highlight' => 'highlight',
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);



						global $anemos_eutf_area_height;
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'options' => $anemos_eutf_area_height,
								'name' => '_anemos_eutf_page_title_height',
								'id' => '_anemos_eutf_page_title_height',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'height', '40' ),
								'label' => array(
									"title" => esc_html__( 'Title Area Height in %', 'anemos' ),
								),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'textfield',
								'name' => '_anemos_eutf_page_title_min_height',
								'id' => '_anemos_eutf_page_title_min_height',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'min_height', '200' ),
								'label' => array(
									"title" => esc_html__( 'Title Area Minimum Height in px', 'anemos' ),
								),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_anemos_eutf_page_title_bg_color',
								'id' => '_anemos_eutf_page_title_bg_color',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_color', 'dark' ),
								'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_color_custom', '#000000' ),
								'label' => array(
									"title" => esc_html__( 'Background Color', 'anemos' ),
								),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
								'multiple' => 'multi',
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_anemos_eutf_page_title_content_bg_color',
								'id' => '_anemos_eutf_page_title_content_bg_color',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'content_bg_color', 'light' ),
								'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'content_bg_color_custom', '#ffffff' ),
								'label' => array(
									"title" => esc_html__( 'Content Background Color', 'anemos' ),
								),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
								'multiple' => 'multi',
								'type_usage' => 'title-content-bg',
							)
						);

						if( 'post' == $post_type ) {
							anemos_eutf_print_admin_option(
								array(
									'type' => 'select-colorpicker',
									'name' => '_anemos_eutf_page_title_subheading_color',
									'id' => '_anemos_eutf_page_title_subheading_color',
									'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'subheading_color', 'dark' ),
									'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'subheading_color_custom', '#000000' ),
									'label' => array(
										"title" => esc_html__( 'Categories/Meta Color', 'anemos' ),
									),
									'dependency' =>
									'[
										{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
										{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
									]',
									'multiple' => 'multi',
								)
							);
						}

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_anemos_eutf_page_title_title_color',
								'id' => '_anemos_eutf_page_title_title_color',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'title_color', 'dark' ),
								'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'title_color_custom', '#000000' ),
								'label' => array(
									"title" => esc_html__( 'Title Color', 'anemos' ),
								),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
								'multiple' => 'multi',
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_anemos_eutf_page_title_caption_color',
								'id' => '_anemos_eutf_page_title_caption_color',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'caption_color', 'dark' ),
								'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'caption_color_custom', '#000000' ),
								'label' => array(
									"title" => esc_html__( 'Description Color', 'anemos' ),
								),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
								'multiple' => 'multi',
							)
						);

						global $anemos_eutf_media_bg_position_selection;
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_page_title_content_position',
								'id' => '_anemos_eutf_page_title_content_position',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'content_position', 'center-center' ),
								'options' => $anemos_eutf_media_bg_position_selection,
								'label' => array(
									"title" => esc_html__( 'Content Position', 'anemos' ),
								),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-text-animation',
								'name' => '_anemos_eutf_page_title_content_animation',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'content_animation', 'fade-in' ),
								'label' => esc_html__( 'Content Animation', 'anemos' ),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);


						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_page_title_bg_mode',
								'id' => '_anemos_eutf_page_title_bg_mode',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_mode'),
								'options' => array(
									'' => esc_html__( 'Color Only', 'anemos' ),
									'featured' => esc_html__( 'Featured Image', 'anemos' ),
									'custom' => esc_html__( 'Custom Image', 'anemos' ),

								),
								'label' => array(
									"title" => esc_html__( 'Background', 'anemos' ),
								),
								'group_id' => '_anemos_eutf_page_title',
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }

								]',
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-image',
								'name' => '_anemos_eutf_page_title_bg_image_id',
								'id' => '_anemos_eutf_page_title_bg_image_id',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_image_id'),
								'label' => array(
									"title" => esc_html__( 'Background Image', 'anemos' ),
								),
								'width' => 'fullwidth',
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_page_title_bg_mode", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }

								]',
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-bg-position',
								'name' => '_anemos_eutf_page_title_bg_position',
								'id' => '_anemos_eutf_page_title_bg_position',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_position', 'center-center'),
								'label' => array(
									"title" => esc_html__( 'Background Position', 'anemos' ),
								),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_page_title_bg_mode", "values" : ["featured","custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-pattern-overlay',
								'name' => '_anemos_eutf_page_title_pattern_overlay',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'pattern_overlay'),
								'label' => esc_html__( 'Pattern Overlay', 'anemos' ),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_page_title_bg_mode", "values" : ["featured","custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-colorpicker',
								'name' => '_anemos_eutf_page_title_color_overlay',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'color_overlay', 'dark' ),
								'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'color_overlay_custom', '#000000' ),
								'label' => esc_html__( 'Color Overlay', 'anemos' ),
								'multiple' => 'multi',
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_page_title_bg_mode", "values" : ["featured","custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select-opacity',
								'name' => '_anemos_eutf_page_title_opacity_overlay',
								'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'opacity_overlay', '0' ),
								'label' => esc_html__( 'Opacity Overlay', 'anemos' ),
								'dependency' =>
								'[
									{ "id" : "_anemos_eutf_page_title_custom", "values" : ["custom"] },
									{ "id" : "_anemos_eutf_page_title_bg_mode", "values" : ["featured","custom"] },
									{ "id" : "_anemos_eutf_disable_title", "values" : [""] }
								]',
							)
						);

						echo '</div>';
					?>
				</div>
				<div id="eut-page-option-tab-layout" class="eut-tab-item">
					<?php

						anemos_eutf_print_admin_option(
							array(
								'type' => 'textfield',
								'name' => '_anemos_eutf_padding_top',
								'id' => '_anemos_eutf_padding_top',
								'value' => $anemos_eutf_padding_top,
								'label' => array(
									'title' => esc_html__( 'Top Padding', 'anemos' ),
									'desc' => esc_html__( "Define the space above the content area.", 'anemos' ) . " " . esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'anemos' ),
									'info' => esc_html__( "Leave this empty for the default value", 'anemos' ),
								),
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'textfield',
								'name' => '_anemos_eutf_padding_bottom',
								'id' => '_anemos_eutf_padding_bottom',
								'value' => $anemos_eutf_padding_bottom,
								'label' => array(
									'title' => esc_html__( 'Bottom Padding', 'anemos' ),
									'desc' => esc_html__( "Define the space below the content area.", 'anemos' ) . " " . esc_html__( "You can use px, em, %, etc. or enter just number and it will use pixels.", 'anemos' ),
									'info' => esc_html__( "Leave this empty for the default value", 'anemos' ),
								),
							)
						);

						//Layout Option
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_layout',
								'id' => '_anemos_eutf_layout',
								'value' => $anemos_eutf_layout,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'anemos' ),
									'none' => esc_html__( 'Full Width', 'anemos' ),
									'left' => esc_html__( 'Left Sidebar', 'anemos' ),
									'right' => esc_html__( 'Right Sidebar', 'anemos' ),
								),
								'label' => array(
									"title" => esc_html__( 'Layout', 'anemos' ),
									"desc" => esc_html__( 'Select page content and sidebar alignment.', 'anemos' ),
									"info" => $anemos_eutf_theme_options_info,
								),
							)
						);

						//Sidebar Option
						anemos_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Sidebar', 'anemos' ),
									"desc" => esc_html__( 'Select page sidebar.', 'anemos' ),
									"info" => $anemos_eutf_theme_options_info,
								),
							)
						);
						anemos_eutf_print_sidebar_selection( $anemos_eutf_sidebar, '_anemos_eutf_sidebar', '_anemos_eutf_sidebar' );
						anemos_eutf_print_admin_option_wrapper_end();

						//Fixed Sidebar Option
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_fixed_sidebar',
								'id' => '_anemos_eutf_fixed_sidebar',
								'value' => $anemos_eutf_fixed_sidebar,
								'options' => array(
									'no' => esc_html__( 'No', 'anemos' ),
									'yes' => esc_html__( 'Yes', 'anemos' ),
								),
								'label' => array(
									"title" => esc_html__( 'Fixed Sidebar', 'anemos' ),
									"desc" => esc_html__( 'If selected, sidebar will be fixed.', 'anemos' ),
								),
							)
						);

						//Posts Content Width
						if ( 'post' == $post_type ) {
							anemos_eutf_print_admin_option(
								array(
									'type' => 'select',
									'name' => '_anemos_eutf_post_content_width',
									'id' => '_anemos_eutf_post_content_width',
									'value' => $anemos_eutf_post_content_width,
									'options' => array(
										'' => esc_html__( '-- Inherit --', 'anemos' ),
										'1170' => esc_html__( 'Large' , 'anemos' ),
										'990' => esc_html__( 'Medium' , 'anemos' ),
										'760' => esc_html__( 'Small' , 'anemos' ),
									),
									'label' => array(
										"title" => esc_html__( 'Post Content Width', 'anemos' ),
										"desc" => esc_html__( 'Select the Single Post Content width (only for Full Width Post Layout)', 'anemos' ),
										"info" => $anemos_eutf_theme_options_info,
									),
								)
							);
						}

					?>
				</div>
				<div id="eut-page-option-tab-sliding-area" class="eut-tab-item">
					<?php
						//Sidearea Visibility
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_sidearea_visibility',
								'id' => '_anemos_eutf_sidearea_visibility',
								'value' => $anemos_eutf_sidearea_visibility,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'anemos' ),
									'no' => esc_html__( 'No', 'anemos' ),
									'yes' => esc_html__( 'Yes', 'anemos' ),
								),
								'label' => array(
									"title" => esc_html__( 'Sliding Area Visibility', 'anemos' ),
									"info" => $anemos_eutf_theme_options_info,
								),
							)
						);

						//Sidearea Sidebar Option
						anemos_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Sliding Area Sidebar', 'anemos' ),
									"desc" => esc_html__( 'Select sliding area sidebar.', 'anemos' ),
									"info" => $anemos_eutf_theme_options_info,
								),
							)
						);
						anemos_eutf_print_sidebar_selection( $anemos_eutf_sidearea_sidebar, '_anemos_eutf_sidearea_sidebar', '_anemos_eutf_sidearea_sidebar' );
						anemos_eutf_print_admin_option_wrapper_end();
					?>
				</div>
				<div id="eut-page-option-tab-extras" class="eut-tab-item">
					<?php

						//Details Option
						if ( 'portfolio' == $post_type) {
							anemos_eutf_print_admin_option(
								array(
									'type' => 'textfield',
									'name' => '_anemos_eutf_details_title',
									'id' => '_anemos_eutf_details_title',
									'value' => $anemos_eutf_details_title,
									'label' => array(
										'title' => esc_html__( 'Details Title', 'anemos' ),
										'desc' => esc_html__( 'Enter your details title.', 'anemos' ),
										'info' => $anemos_eutf_theme_options_info_text_empty,
									),
									'width' => 'fullwidth',
								)
							);
							anemos_eutf_print_admin_option(
								array(
									'type' => 'textarea',
									'name' => '_anemos_eutf_details',
									'id' => '_anemos_eutf_details',
									'value' => $anemos_eutf_details,
									'label' => array(
										'title' => esc_html__( 'Details Area', 'anemos' ),
										'desc' => esc_html__( 'Enter your details area.', 'anemos' ),
									),
									'width' => 'fullwidth',
								)
							);

							//Backlink page
							anemos_eutf_print_admin_option_wrapper_start(
								array(
									'type' => 'select',
									'label' => array(
										"title" => esc_html__( 'Backlink', 'anemos' ),
										"desc" => esc_html__( 'Select your backlink page.', 'anemos' ),
										"info" => $anemos_eutf_theme_options_info,
									),
								)
							);
							anemos_eutf_print_page_selection( $anemos_eutf_backlink_id, 'eut-backlink-id', '_anemos_eutf_backlink_id' );
							anemos_eutf_print_admin_option_wrapper_end();


						}

						//Anchor Navigation Menu Option

						anemos_eutf_print_admin_option_wrapper_start(
							array(
								'type' => 'select',
								'label' => array(
									"title" => esc_html__( 'Anchor Navigation Menu', 'anemos' ),
									"desc" => esc_html__( 'Select page anchor navigation menu.', 'anemos' ),
								),
							)
						);
						anemos_eutf_print_menu_selection( $anemos_eutf_anchor_navigation_menu, 'eut-page-navigation-menu', '_anemos_eutf_anchor_navigation_menu' );
						anemos_eutf_print_admin_option_wrapper_end();

						//Theme Loader
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_theme_loader',
								'id' => '_anemos_eutf_theme_loader',
								'value' => $anemos_eutf_theme_loader,
								'options' => array(
									'' => esc_html__( '-- Inherit --', 'anemos' ),
									'no' => esc_html__( 'No', 'anemos' ),
									'yes' => esc_html__( 'Yes', 'anemos' ),
								),
								'label' => array(
									"title" => esc_html__( 'Theme Loader Visibility', 'anemos' ),
									"info" => esc_html__( 'Inherit : Theme Options - General Settings.', 'anemos' ),
								),
							)
						);

						//Responsive Scrolling Option
						if ( 'page' == $post_type) {
							anemos_eutf_print_admin_option(
								array(
									'type' => 'select',
									'name' => '_anemos_eutf_responsive_scrolling',
									'id' => '_anemos_eutf_responsive_scrolling',
									'value' => $anemos_eutf_responsive_scrolling,
									'options' => array(
										'' => esc_html__( 'Yes', 'movedo' ),
										'no' => esc_html__( 'No', 'movedo' ),
									),
									'label' => array(
										'title' => esc_html__( 'Responsive Scrolling Full Sections', 'anemos' ),
										'desc' => esc_html__( 'Select if you want to maintain the scrolling feature on devices.', 'anemos' ),
										'info' => esc_html__( 'Note: This option is available only for Scrolling Full Screen Sections Template.', 'anemos' ),
									),
								)
							);
						}

					?>
				</div>
				<div id="eut-page-option-tab-visibility" class="eut-tab-item">
					<?php

						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_top_bar',
								'id' => '_anemos_eutf_disable_top_bar',
								'value' => $anemos_eutf_disable_top_bar,
								'label' => array(
									"title" => esc_html__( 'Disable Top Bar', 'anemos' ),
									"desc" => esc_html__( 'If selected, top bar will be hidden.', 'anemos' ),
								),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_sticky',
								'id' => '_anemos_eutf_disable_sticky',
								'value' => $anemos_eutf_disable_sticky,
								'label' => array(
									"title" => esc_html__( 'Disable Sticky Header', 'anemos' ),
									"desc" => esc_html__( 'If selected, sticky header will be disabled.', 'anemos' ),
								),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_logo',
								'id' => '_anemos_eutf_disable_logo',
								'value' => $anemos_eutf_disable_logo,
								'label' => array(
									"title" => esc_html__( 'Disable Logo', 'anemos' ),
									"desc" => esc_html__( 'If selected, logo will be disabled.', 'anemos' ),
								),
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_menu',
								'id' => '_anemos_eutf_disable_menu',
								'value' => $anemos_eutf_disable_menu,
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu', 'anemos' ),
									"desc" => esc_html__( 'If selected, main menu will be hidden.', 'anemos' ),
								),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_menu_item_search',
								'id' => '_anemos_eutf_disable_menu_item_search',
								'value' => anemos_eutf_array_value( $anemos_eutf_disable_menu_items, 'search'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Search', 'anemos' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'anemos' ),
								),
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_menu_item_form',
								'id' => '_anemos_eutf_disable_menu_item_form',
								'value' => anemos_eutf_array_value( $anemos_eutf_disable_menu_items, 'form'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Contact Form', 'anemos' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'anemos' ),
								),
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_menu_item_language',
								'id' => '_anemos_eutf_disable_menu_item_language',
								'value' => anemos_eutf_array_value( $anemos_eutf_disable_menu_items, 'language'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Language Selector', 'anemos' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'anemos' ),
								),
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_menu_item_login',
								'id' => '_anemos_eutf_disable_menu_item_login',
								'value' => anemos_eutf_array_value( $anemos_eutf_disable_menu_items, 'login'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Login', 'anemos' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'anemos' ),
								),
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_menu_item_social',
								'id' => '_anemos_eutf_disable_menu_item_social',
								'value' => anemos_eutf_array_value( $anemos_eutf_disable_menu_items, 'social'),
								'label' => array(
									"title" => esc_html__( 'Disable Main Menu Item Social Icons', 'anemos' ),
									"desc" => esc_html__( 'If selected, main menu item will be hidden.', 'anemos' ),
								),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_breadcrumbs',
								'id' => '_anemos_eutf_disable_breadcrumbs',
								'value' => $anemos_eutf_disable_breadcrumbs,
								'label' => array(
									"title" => esc_html__( 'Disable Breadcrumbs', 'anemos' ),
									"desc" => esc_html__( 'If selected, breadcrumbs items will be hidden.', 'anemos' ),
								),
							)
						);

						if ( 'page' == $post_type ) {
							if ( anemos_eutf_woocommerce_enabled() && $post->ID == wc_get_page_id( 'shop' ) ) {
								//Skip
							} else {
								anemos_eutf_print_admin_option(
									array(
										'type' => 'checkbox',
										'name' => '_anemos_eutf_disable_content',
										'id' => '_anemos_eutf_disable_content',
										'value' => $anemos_eutf_disable_content,
										'label' => array(
											"title" => esc_html__( 'Disable Content Area', 'anemos' ),
											"desc" => esc_html__( 'If selected, content area will be hidden (including sidebar and comments).', 'anemos' ),
										),
									)
								);
							}
						}

						if ( 'post' == $post_type ) {
							anemos_eutf_print_admin_option(
								array(
									'type' => 'checkbox',
									'name' => '_anemos_eutf_disable_media',
									'id' => '_anemos_eutf_disable_media',
									'value' => $anemos_eutf_disable_media,
									'label' => array(
										"title" => esc_html__( 'Disable Media Area', 'anemos' ),
										"desc" => esc_html__( 'If selected, media area will be hidden.', 'anemos' ),
									),
								)
							);
						}
						if ( 'portfolio' == $post_type ) {
							anemos_eutf_print_admin_option(
								array(
									'type' => 'checkbox',
									'name' => '_anemos_eutf_disable_recent_entries',
									'id' => '_anemos_eutf_disable_recent_entries',
									'value' => $anemos_eutf_disable_recent_entries,
									'label' => array(
										"title" => esc_html__( 'Disable Recent Entries', 'anemos' ),
										"desc" => esc_html__( 'If selected, recent entries area will be hidden.', 'anemos' ),
									),
								)
							);
						}

						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_bottom_bar',
								'id' => '_anemos_eutf_disable_bottom_bar',
								'value' => $anemos_eutf_disable_bottom_bar,
								'label' => array(
									"title" => esc_html__( 'Disable Bottom Bar', 'anemos' ),
									"desc" => esc_html__( 'If selected, bottom bar will be hidden.', 'anemos' ),
								),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_footer',
								'id' => '_anemos_eutf_disable_footer',
								'value' => $anemos_eutf_disable_footer,
								'label' => array(
									"title" => esc_html__( 'Disable Footer Widgets', 'anemos' ),
									"desc" => esc_html__( 'If selected, footer widgets will be hidden.', 'anemos' ),
								),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_copyright',
								'id' => '_anemos_eutf_disable_copyright',
								'value' => $anemos_eutf_disable_copyright,
								'label' => array(
									"title" => esc_html__( 'Disable Footer Copyright', 'anemos' ),
									"desc" => esc_html__( 'If selected, footer copyright area will be hidden.', 'anemos' ),
								),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'checkbox',
								'name' => '_anemos_eutf_disable_back_to_top',
								'id' => '_anemos_eutf_disable_back_to_top',
								'value' => $anemos_eutf_disable_back_to_top,
								'label' => array(
									"title" => esc_html__( 'Disable Back to Top', 'anemos' ),
									"desc" => esc_html__( 'If selected, Back to Top button will be hidden.', 'anemos' ),
								),
							)
						);

					?>
				</div>
			</div>
		</div>
	</div>

<?php
}

function anemos_eutf_page_feature_section_box( $post ) {

	wp_nonce_field( 'anemos_eutf_nonce_feature_save', '_anemos_eutf_nonce_feature_save' );

	$post_id = $post->ID;
	anemos_eutf_admin_get_feature_section( $post_id );

}

function anemos_eutf_generic_options_save_postdata( $post_id , $post ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $_POST['_anemos_eutf_nonce_feature_save'] ) && wp_verify_nonce( $_POST['_anemos_eutf_nonce_feature_save'], 'anemos_eutf_nonce_feature_save' ) ) {

		if ( anemos_eutf_check_permissions( $post_id ) ) {
			anemos_eutf_admin_save_feature_section( $post_id );
		}

	}

	if ( isset( $_POST['_anemos_eutf_nonce_page_save'] ) && wp_verify_nonce( $_POST['_anemos_eutf_nonce_page_save'], 'anemos_eutf_nonce_page_save' ) ) {

		if ( anemos_eutf_check_permissions( $post_id ) ) {

			$anemos_eutf_page_options = array (
				array(
					'name' => 'Description',
					'id' => '_anemos_eutf_description',
				),
				array(
					'name' => 'Details Title',
					'id' => '_anemos_eutf_details_title',
				),
				array(
					'name' => 'Details',
					'id' => '_anemos_eutf_details',
				),
				array(
					'name' => 'Backlink',
					'id' => '_anemos_eutf_backlink_id',
				),
				array(
					'name' => 'Top Padding',
					'id' => '_anemos_eutf_padding_top',
				),
				array(
					'name' => 'Bottom Padding',
					'id' => '_anemos_eutf_padding_bottom',
				),
				array(
					'name' => 'Layout',
					'id' => '_anemos_eutf_layout',
				),
				array(
					'name' => 'Sidebar',
					'id' => '_anemos_eutf_sidebar',
				),
				array(
					'name' => 'Post Content width',
					'id' => '_anemos_eutf_post_content_width',
				),
				array(
					'name' => 'Sidearea Area Visibility',
					'id' => '_anemos_eutf_sidearea_visibility',
				),
				array(
					'name' => 'Sidearea Sidebar',
					'id' => '_anemos_eutf_sidearea_sidebar',
				),
				array(
					'name' => 'Fixed Sidebar',
					'id' => '_anemos_eutf_fixed_sidebar',
				),
				array(
					'name' => 'Header Overlapping',
					'id' => '_anemos_eutf_header_overlapping',
				),
				array(
					'name' => 'Header Style',
					'id' => '_anemos_eutf_header_style',
				),
				array(
					'name' => 'Navigation Anchor Menu',
					'id' => '_anemos_eutf_anchor_navigation_menu',
				),
				array(
					'name' => 'Theme Loader',
					'id' => '_anemos_eutf_theme_loader',
				),
				array(
					'name' => 'Responsive Scrolling',
					'id' => '_anemos_eutf_responsive_scrolling',
				),
				array(
					'name' => 'Main Navigation Menu',
					'id' => '_anemos_eutf_main_navigation_menu',
				),
				array(
					'name' => 'Menu Type',
					'id' => '_anemos_eutf_menu_type',
				),
				array(
					'name' => 'Sticky Header Type',
					'id' => '_anemos_eutf_sticky_header_type',
				),
				array(
					'name' => 'Disable Sticky',
					'id' => '_anemos_eutf_disable_sticky',
				),
				array(
					'name' => 'Disable Top Bar',
					'id' => '_anemos_eutf_disable_top_bar',
				),
				array(
					'name' => 'Disable Logo',
					'id' => '_anemos_eutf_disable_logo',
				),
				array(
					'name' => 'Disable Menu',
					'id' => '_anemos_eutf_disable_menu',
				),
				array(
					'name' => 'Disable Menu Items',
					'id' => '_anemos_eutf_disable_menu_items',
				),
				array(
					'name' => 'disable Breadcrumbs',
					'id' => '_anemos_eutf_disable_breadcrumbs',
				),
				array(
					'name' => 'Disable Title',
					'id' => '_anemos_eutf_disable_title',
				),
				array(
					'name' => 'Disable Media',
					'id' => '_anemos_eutf_disable_media',
				),
				array(
					'name' => 'Disable Content',
					'id' => '_anemos_eutf_disable_content',
				),
				array(
					'name' => 'Disable Recent Entries',
					'id' => '_anemos_eutf_disable_recent_entries',
				),
				array(
					'name' => 'Disable Bottom Bar',
					'id' => '_anemos_eutf_disable_bottom_bar',
				),
				array(
					'name' => 'Disable Footer',
					'id' => '_anemos_eutf_disable_footer',
				),
				array(
					'name' => 'Disable Copyright',
					'id' => '_anemos_eutf_disable_copyright',
				),
				array(
					'name' => 'Disable Back to Top',
					'id' => '_anemos_eutf_disable_back_to_top',
				),
			);

			$anemos_eutf_disable_menu_items_options = array (
				array(
					'param_id' => 'search',
					'id' => '_anemos_eutf_disable_menu_item_search',
					'default' => '',
				),
				array(
					'param_id' => 'form',
					'id' => '_anemos_eutf_disable_menu_item_form',
					'default' => '',
				),
				array(
					'param_id' => 'language',
					'id' => '_anemos_eutf_disable_menu_item_language',
					'default' => '',
				),
				array(
					'param_id' => 'login',
					'id' => '_anemos_eutf_disable_menu_item_login',
					'default' => '',
				),
				array(
					'param_id' => 'social',
					'id' => '_anemos_eutf_disable_menu_item_social',
					'default' => '',
				),
			);

			$anemos_eutf_page_title_options = array (
				array(
					'param_id' => 'custom',
					'id' => '_anemos_eutf_page_title_custom',
					'default' => '',
				),
				array(
					'param_id' => 'height',
					'id' => '_anemos_eutf_page_title_height',
					'default' => '40',
				),
				array(
					'param_id' => 'min_height',
					'id' => '_anemos_eutf_page_title_min_height',
					'default' => '200',
				),
				array(
					'param_id' => 'bg_color',
					'id' => '_anemos_eutf_page_title_bg_color',
					'default' => 'light',
				),
				array(
					'param_id' => 'bg_color_custom',
					'id' => '_anemos_eutf_page_title_bg_color_custom',
					'default' => '#ffffff',
				),
				array(
					'param_id' => 'subheading_color',
					'id' => '_anemos_eutf_page_title_subheading_color',
					'default' => 'light',
				),
				array(
					'param_id' => 'subheading_color_custom',
					'id' => '_anemos_eutf_page_title_subheading_color_custom',
					'default' => '#ffffff',
				),
				array(
					'param_id' => 'title_color',
					'id' => '_anemos_eutf_page_title_title_color',
					'default' => 'dark',
				),
				array(
					'param_id' => 'title_color_custom',
					'id' => '_anemos_eutf_page_title_title_color_custom',
					'default' => '#000000',
				),
				array(
					'param_id' => 'caption_color',
					'id' => '_anemos_eutf_page_title_caption_color',
					'default' => 'dark',
				),
				array(
					'param_id' => 'caption_color_custom',
					'id' => '_anemos_eutf_page_title_caption_color_custom',
					'default' => '#000000',
				),
				array(
					'param_id' => 'content_bg_color',
					'id' => '_anemos_eutf_page_title_content_bg_color',
					'default' => 'none',
				),
				array(
					'param_id' => 'content_bg_color_custom',
					'id' => '_anemos_eutf_page_title_content_bg_color_custom',
					'default' => '#000000',
				),
				array(
					'param_id' => 'content_position',
					'id' => '_anemos_eutf_page_title_content_position',
					'default' => 'center-center',
				),
				array(
					'param_id' => 'content_animation',
					'id' => '_anemos_eutf_page_title_content_animation',
					'default' => 'fade-in',
				),
				array(
					'param_id' => 'bg_mode',
					'id' => '_anemos_eutf_page_title_bg_mode',
					'default' => '',
				),
				array(
					'param_id' => 'bg_image_id',
					'id' => '_anemos_eutf_page_title_bg_image_id',
					'default' => '0',
				),
				array(
					'param_id' => 'bg_position',
					'id' => '_anemos_eutf_page_title_bg_position',
					'default' => 'center-center',
				),
				array(
					'param_id' => 'pattern_overlay',
					'id' => '_anemos_eutf_page_title_pattern_overlay',
					'default' => '',
				),
				array(
					'param_id' => 'color_overlay',
					'id' => '_anemos_eutf_page_title_color_overlay',
					'default' => 'dark',
				),
				array(
					'param_id' => 'color_overlay_custom',
					'id' => '_anemos_eutf_page_title_color_overlay_custom',
					'default' => '#000000',
				),
				array(
					'param_id' => 'opacity_overlay',
					'id' => '_anemos_eutf_page_title_opacity_overlay',
					'default' => '0',
				),
			);

			//Update Single custom fields
			foreach ( $anemos_eutf_page_options as $value ) {
				$new_meta_value = ( isset( $_POST[$value['id']] ) ? $_POST[$value['id']] : '' );
				$meta_key = $value['id'];


				$meta_value = get_post_meta( $post_id, $meta_key, true );

				if ( '' != $new_meta_value  && '' == $meta_value ) {
					add_post_meta( $post_id, $meta_key, $new_meta_value, true );
				} elseif ( '' != $new_meta_value && $new_meta_value != $meta_value ) {
					update_post_meta( $post_id, $meta_key, $new_meta_value );
				} elseif ( '' == $new_meta_value && '' != $meta_value ) {
					delete_post_meta( $post_id, $meta_key );
				}
			}

			//Update Menu Items Visibility array
			anemos_eutf_update_meta_array( $post_id, '_anemos_eutf_disable_menu_items', $anemos_eutf_disable_menu_items_options );
			//Update Title Options array
			anemos_eutf_update_meta_array( $post_id, '_anemos_eutf_custom_title_options', $anemos_eutf_page_title_options );
		}
	}

}

/**
 * Function update meta array
 */
function anemos_eutf_update_meta_array( $post_id, $param_id, $param_array_options ) {

	$array_options = array();

	if( !empty( $param_array_options ) ) {

		foreach ( $param_array_options as $value ) {

			$meta_key = $value['param_id'];
			$meta_default = $value['default'];

			$new_meta_value = ( isset( $_POST[$value['id']] ) ? $_POST[$value['id']] : $meta_default );

			if( !empty( $new_meta_value ) ) {
				$array_options[$meta_key] = $new_meta_value;
			}
		}

	}

	if( !empty( $array_options ) ) {
		update_post_meta( $post_id, $param_id, $array_options );
	} else {
		delete_post_meta( $post_id, $param_id );
	}
}

/**
 * Function to check post type permissions
 */

function anemos_eutf_check_permissions( $post_id ) {

	if ( 'post' == $_POST['post_type'] ) {
		if ( !current_user_can( 'edit_post', $post_id ) ) {
			return false;
		}
	} else {
		if ( !current_user_can( 'edit_page', $post_id ) ) {
			return false;
		}
	}
	return true;
}

/**
 * Function to print menu selector
 */
function anemos_eutf_print_menu_selection( $menu_id, $id, $name, $default = 'none' ) {

	?>
	<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
		<option value="" <?php selected( '', $menu_id ); ?>>
			<?php
				if ( 'none' == $default ){
					esc_html_e( 'None', 'anemos' );
				} else {
					esc_html_e( '-- Inherit --', 'anemos' );
				}
			?>
		</option>
	<?php
		$menus = wp_get_nav_menus();
		if ( ! empty( $menus ) ) {
			foreach ( $menus as $item ) {
	?>
				<option value="<?php echo esc_attr( $item->term_id ); ?>" <?php selected( $item->term_id, $menu_id ); ?>>
					<?php echo esc_html( $item->name ); ?>
				</option>
	<?php
			}
		}
	?>
	</select>
	<?php
}

/**
 * Function to print layout selector
 */
function anemos_eutf_print_layout_selection( $layout, $id, $name ) {

	$layouts = array(
		'' => esc_html__( '-- Inherit --', 'anemos' ),
		'none' => esc_html__( 'Full Width', 'anemos' ),
		'left' => esc_html__( 'Left Sidebar', 'anemos' ),
		'right' => esc_html__( 'Right Sidebar', 'anemos' ),
	);

	?>
	<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
	<?php
		foreach ( $layouts as $key => $value ) {
			if ( $value ) {
	?>
				<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $layout ); ?>><?php echo esc_html( $value ); ?></option>
	<?php
			}
		}
	?>
	</select>
	<?php
}

/**
 * Function to print sidebar selector
 */
function anemos_eutf_print_sidebar_selection( $sidebar, $id, $name ) {
	global $wp_registered_sidebars;

	?>
	<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
		<option value="" <?php selected( '', $sidebar ); ?>><?php echo esc_html__( '-- Inherit --', 'anemos' ); ?></option>
	<?php
	foreach ( $wp_registered_sidebars as $key => $value ) {
		?>
		<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $sidebar ); ?>><?php echo esc_html( $value['name'] ); ?></option>
		<?php
	}
	?>
	</select>
	<?php
}

/**
 * Function to print page selector
 */
function anemos_eutf_print_page_selection( $page_id, $id, $name ) {

?>
	<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
		<option value="" <?php selected( '', $page_id ); ?>>
			<?php esc_html_e( '-- Inherit --', 'anemos' ); ?>
		</option>
<?php
		$pages = get_pages();
		foreach ( $pages as $page ) {
?>
			<option value="<?php echo esc_attr( $page->ID ); ?>" <?php selected( $page->ID, $page_id ); ?>>
				<?php echo esc_html( $page->post_title ); ?>
			</option>
<?php
		}
?>
	</select>
<?php

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
