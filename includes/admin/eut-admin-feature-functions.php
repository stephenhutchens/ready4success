<?php
/*
*	Collection of functions for admin feature section
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


/**
 * Get Feature Single Image with ajax
 */
function anemos_eutf_get_image_media() {


	if( isset( $_POST['attachment_id'] ) ) {

		$media_id  = $_POST['attachment_id'];

		if( !empty( $media_id  ) ) {

			$image_item = array (
				'bg_image_id' => $media_id,
			);

			anemos_eutf_print_admin_feature_image_item( $image_item, "new" );

		}
	}

	if( isset( $_POST['attachment_id'] ) ) { die(); }
}
add_action( 'wp_ajax_anemos_eutf_get_image_media', 'anemos_eutf_get_image_media' );


function anemos_eutf_post_select_lookup() {
	global $wpdb;

	$result = array();
	$search = $wpdb->esc_like( $_REQUEST['q'] );
	
	$post_type = apply_filters( 'anemos_eutf_feature_section_slider_post_type', 'post' );

	$args = array(
		'posts_per_page' => -1,
		'post_status' => array( 'publish' ),
		'post_type' => $post_type,
		'order' => 'ASC',
		'orderby' => 'title',
		'suppress_filters' => false,
		's' => $search,
	);

	$query = new WP_Query( $args );

	while ( $query->have_posts() ) : $query->the_post();
		$post_title = get_the_title();
		$id = get_the_ID();

		$result[] = array(
			'id' => $id,
			'title' => $post_title,
		);
	endwhile;

	wp_reset_postdata();

	echo json_encode($result);

	die();
}
add_action('wp_ajax_anemos_eutf_post_select_lookup', 'anemos_eutf_post_select_lookup');

function anemos_eutf_get_post_titles() {
	$result = array();
	$post_type = apply_filters( 'anemos_eutf_feature_section_slider_post_type', 'post' );

	if (isset($_REQUEST['post_ids'])) {
		$post_ids = $_REQUEST['post_ids'];
		if (strpos($post_ids, ',') === false) {
			$post_ids = array( $post_ids );
		} else {
			$post_ids = explode(',', $post_ids);
		}
	} else {
		$post_ids = array();
	}

	if (is_array($post_ids) && ! empty($post_ids)) {

		$args = array(
			'posts_per_page' => -1,
			'post_status' => array('publish'),
			'post__in' => $post_ids,
			'post_type' => $post_type,
		);

		$query = new WP_Query( $args );

		while ( $query->have_posts() ) : $query->the_post();
			$post_title = get_the_title();
			$id = get_the_ID();

			$result[] = array(
				'id' => $id,
				'title' => $post_title,
			);
		endwhile;

		wp_reset_postdata();
	}

	echo json_encode($result);

	die;
}
add_action('wp_ajax_anemos_eutf_get_post_titles', 'anemos_eutf_get_post_titles');

/**
 * Get Replaced Image with ajax
 */
function anemos_eutf_get_replaced_image() {


	if( isset( $_POST['attachment_id'] ) ) {

		if ( isset( $_POST['attachment_mode'] ) && !empty( $_POST['attachment_mode'] ) ) {
			$mode = $_POST['attachment_mode'];
			switch( $mode ) {
				case 'image':
					$input_name = '_anemos_eutf_single_item_bg_image_id';
				break;
				case 'custom-image':
					if ( isset( $_POST['field_name'] ) && !empty( $_POST['field_name'] ) ) {
						$input_name = $_POST['field_name'];
					}
				break;
				case 'full-slider':
				default:
					$input_name = '_anemos_eutf_slider_item_bg_image_id[]';
				break;
			}
		} else {
			$input_name = '_anemos_eutf_slider_item_bg_image_id[]';
		}

		$media_id  = $_POST['attachment_id'];
		$thumb_src = wp_get_attachment_image_src( $media_id, 'thumbnail' );
		$thumbnail_url = $thumb_src[0];
		$alt = get_post_meta( $media_id, '_wp_attachment_image_alt', true );
?>
		<input type="hidden" class="eut-upload-media-id" value="<?php echo esc_attr( $media_id ); ?>" name="<?php echo esc_attr( $input_name ); ?>">
		<?php echo '<img class="eut-thumb" src="' . esc_url( $thumbnail_url ) . '" attid="' . esc_attr( $media_id ) . '" alt="' . esc_attr( $alt ) . '" width="120" height="120"/>'; ?>
		<a class="eut-upload-remove-image eut-item-new" href="#"></a>
<?php

	}

	if( isset( $_POST['attachment_id'] ) ) { die(); }
}
add_action( 'wp_ajax_anemos_eutf_get_replaced_image', 'anemos_eutf_get_replaced_image' );

/**
 * Get Single Feature Slider Media with ajax
 */
function anemos_eutf_get_admin_feature_slider_media() {


	if( isset( $_POST['attachment_ids'] ) ) {

		$attachment_ids = $_POST['attachment_ids'];

		if( !empty( $attachment_ids ) ) {

			$media_ids = explode(",", $attachment_ids);

			foreach ( $media_ids as $media_id ) {
				$slider_item = array (
					'bg_image_id' => $media_id,
				);

				anemos_eutf_print_admin_feature_slider_item( $slider_item, "new" );
			}
		}
	}

	if( isset( $_POST['post_ids'] ) ) {

		$post_ids = $_POST['post_ids'];
		if( !empty( $post_ids ) ) {

			$all_post_ids = explode(",", $post_ids);

			foreach ( $all_post_ids as $post_id ) {
				$slider_item = array (
					'type' => 'post',
					'post_id' => $post_id,
				);
				anemos_eutf_print_admin_feature_slider_item( $slider_item, "new" );
			}
		} else {
			$slider_item = array (
				'type' => 'post',
				'post_id' => '0',
			);
			anemos_eutf_print_admin_feature_slider_item( $slider_item, "new" );
		}
	}

	if( isset( $_POST['attachment_ids'] ) || isset( $_POST['post_ids'] )  ) { die(); }
}
add_action( 'wp_ajax_anemos_eutf_get_admin_feature_slider_media', 'anemos_eutf_get_admin_feature_slider_media' );

/**
 * Get Single Feature Map Point with ajax
 */
function anemos_eutf_get_map_point() {
	if( isset( $_POST['map_mode'] ) ) {
		$mode = $_POST['map_mode'];
		anemos_eutf_print_admin_feature_map_point( array(), $mode );
	}
	if( isset( $_POST['map_mode'] ) ) { die(); }
}
add_action( 'wp_ajax_anemos_eutf_get_map_point', 'anemos_eutf_get_map_point' );

/**
 * Prints Feature Map Points
 */
function anemos_eutf_print_admin_feature_map_items( $map_items ) {

	if( !empty($map_items) ) {
		foreach ( $map_items as $map_item ) {
			anemos_eutf_print_admin_feature_map_point( $map_item );
		}
	}

}

/**
 * Prints Feature Single Map Point
 */
function anemos_eutf_print_admin_feature_map_point( $map_item, $mode = '' ) {


	$map_item_id = uniqid('_anemos_eutf_map_point_');
	$map_uniqid = uniqid('-');
	$map_id = anemos_eutf_array_value( $map_item, 'id', $map_item_id );

	$map_lat = anemos_eutf_array_value( $map_item, 'lat', '51.516221' );
	$map_lng = anemos_eutf_array_value( $map_item, 'lng', '-0.136986' );
	$map_marker = anemos_eutf_array_value( $map_item, 'marker' );

	$map_title = anemos_eutf_array_value( $map_item, 'title' );
	$map_infotext = anemos_eutf_array_value( $map_item, 'info_text','' );
	$map_infotext_open = anemos_eutf_array_value( $map_item, 'info_text_open','no' );

	$button_text = anemos_eutf_array_value( $map_item, 'button_text' );
	$button_url = anemos_eutf_array_value( $map_item, 'button_url' );
	$button_target = anemos_eutf_array_value( $map_item, 'button_target', '_self' );
	$button_class = anemos_eutf_array_value( $map_item, 'button_class' );
	$anemos_eutf_closed_class = 'closed';
	$anemos_eutf_item_new = '';
	if( "new" == $mode ) {
		$anemos_eutf_item_new = " eut-item-new";
		$anemos_eutf_closed_class = "eut-item-new";
	}
?>
	<div class="eut-map-item postbox <?php echo esc_attr( $anemos_eutf_closed_class ); ?>">
		<button class="handlediv button-link" type="button">
			<span class="screen-reader-text"><?php esc_attr_e( 'Toggle panel: Feature Section Map Point', 'anemos' ); ?></span>
			<span class="toggle-indicator"></span>
		</button>
		<input class="eut-map-item-delete-button button<?php echo esc_attr( $anemos_eutf_item_new ); ?>" type="button" value="<?php esc_attr_e( 'Delete', 'anemos' ); ?>" />
		<span class="eut-button-spacer">&nbsp;</span>
		<span class="eut-modal-spinner"></span>
		<h3 class="eut-title">
			<span><?php esc_html_e( 'Map Point', 'anemos' ); ?>: </span><span id="<?php echo esc_attr( $map_id ); ?>_title_admin_label"><?php if ( !empty ($map_title) ) { echo esc_html( $map_title ); } ?></span>
		</h3>
		<div class="inside">

			<!--  METABOXES -->
			<div class="eut-metabox-content">

				<!-- TABS -->
				<div class="eut-tabs<?php echo esc_attr( $anemos_eutf_item_new ); ?>">

					<ul class="eut-tab-links">
						<li class="active"><a href="#eut-feature-single-map-tab-marker<?php echo esc_attr( $map_uniqid ); ?>"><?php esc_html_e( 'Marker', 'anemos' ); ?></a></li>
						<li><a href="#eut-feature-single-map-tab-infobox<?php echo esc_attr( $map_uniqid ); ?>"><?php esc_html_e( 'Info Box', 'anemos' ); ?></a></li>
						<li><a href="#eut-feature-single-map-tab-button<?php echo esc_attr( $map_uniqid ); ?>"><?php esc_html_e( 'Link', 'anemos' ); ?></a></li>
					</ul>

					<div class="eut-tab-content">

						<div id="eut-feature-single-map-tab-marker<?php echo esc_attr( $map_uniqid ); ?>" class="eut-tab-item active">
							<input type="hidden" name="_anemos_eutf_map_item_point_id[]" value="<?php echo esc_attr( $map_id ); ?>"/>
							<div class="eut-fields-wrapper">
								<div class="eut-label">
									<label for="eut-page-feature-element">
										<span class="eut-title"><?php esc_html_e( 'Marker', 'anemos' ); ?></span>
									</label>
								</div>
								<div class="eut-field-items-wrapper">
									<div class="eut-field-item eut-field-item-fullwidth">
										<input type="text" name="_anemos_eutf_map_item_point_marker[]" class="eut-upload-simple-media-field" value="<?php echo esc_attr( $map_marker ); ?>"/>
										<label></label>
										<input type="button" data-media-type="image" class="eut-upload-simple-media-button button-primary<?php echo esc_attr( $anemos_eutf_item_new ); ?>" value="<?php esc_attr_e( 'Insert Marker', 'anemos' ); ?>"/>
										<input type="button" class="eut-remove-simple-media-button button<?php echo esc_attr( $anemos_eutf_item_new ); ?>" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>
									</div>
								</div>
							</div>
							<?php
								anemos_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_anemos_eutf_map_item_point_lat[]',
										'value' => $map_lat,
										'label' => array(
											"title" => esc_html__( 'Latitude', 'anemos' ),
										),
									)
								);
								anemos_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_anemos_eutf_map_item_point_lng[]',
										'value' => $map_lng,
										'label' => array(
											"title" => esc_html__( 'Longitude', 'anemos' ),
										),
									)
								);
							?>

						</div>
						<div id="eut-feature-single-map-tab-infobox<?php echo esc_attr( $map_uniqid ); ?>" class="eut-tab-item">
							<?php
								anemos_eutf_print_admin_option(
									array(
										'type' => 'label',
										'label' => array(
											"title" => esc_html__( 'Title / Info Text', 'anemos' ),
										),
									)
								);
								anemos_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_anemos_eutf_map_item_point_title[]',
										'value' => $map_title,
										'label' => array(
											"title" => esc_html__( 'Title', 'anemos' ),
										),
										'id' => $map_id . '_title',
										'extra_class' => 'eut-admin-label-update',
									)
								);
								anemos_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_anemos_eutf_map_item_point_infotext[]',
										'value' => $map_infotext,
										'label' => array(
											"title" => esc_html__( 'Info Text', 'anemos' ),
										),
									)
								);
								anemos_eutf_print_admin_option(
									array(
										'type' => 'select-boolean',
										'name' => '_anemos_eutf_map_item_point_infotext_open[]',
										'value' => $map_infotext_open,
										'label' => esc_html__( 'Open Info Text Onload', 'anemos' ),
									)
								);
							?>
						</div>
						<div id="eut-feature-single-map-tab-button<?php echo esc_attr( $map_uniqid ); ?>" class="eut-tab-item">
							<?php
								anemos_eutf_print_admin_option(
									array(
										'type' => 'label',
										'label' => array(
											"title" => esc_html__( 'Link', 'anemos' ),
										),
									)
								);
								anemos_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_anemos_eutf_map_item_point_button_text[]',
										'value' => $button_text,
										'label' => array(
											"title" => esc_html__( 'Link Text', 'anemos' ),
										),
									)
								);
								anemos_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_anemos_eutf_map_item_point_button_url[]',
										'value' => $button_url,
										'label' => array(
											"title" => esc_html__( 'Link URL', 'anemos' ),
										),
										'width' => 'fullwidth',
									)
								);

								anemos_eutf_print_admin_option(
									array(
										'type' => 'select-button-target',
										'name' => '_anemos_eutf_map_item_point_button_target[]',
										'value' => $button_target,
										'label' => array(
											"title" => esc_html__( 'Link Target', 'anemos' ),
										),
									)
								);

								anemos_eutf_print_admin_option(
									array(
										'type' => 'textfield',
										'name' => '_anemos_eutf_map_item_point_button_class[]',
										'value' => $button_class,
										'label' => array(
											"title" => esc_html__( 'Link Class', 'anemos' ),
										),
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}

/**
 * Prints Section Slider items
 */
function anemos_eutf_print_admin_feature_slider_items( $slider_items ) {

	foreach ( $slider_items as $slider_item ) {
		anemos_eutf_print_admin_feature_slider_item( $slider_item, '' );
	}

}

/**
* Prints Single Feature Slider Item
*/
function anemos_eutf_print_admin_feature_slider_item( $slider_item, $new = "" ) {

	$slide_id = anemos_eutf_array_value( $slider_item, 'id', uniqid() );
	$slider_item['id'] = $slide_id;
	$slide_type = anemos_eutf_array_value( $slider_item, 'type' );
	$slide_post_id = anemos_eutf_array_value( $slider_item, 'post_id' );

	$bg_image_id = anemos_eutf_array_value( $slider_item, 'bg_image_id' );

	$bg_position = anemos_eutf_array_value( $slider_item, 'bg_position', 'center-center' );
	$bg_tablet_sm_position = anemos_eutf_array_value( $slider_item, 'bg_tablet_sm_position' );


	$header_style = anemos_eutf_array_value( $slider_item, 'header_style', 'default' );
	$title = anemos_eutf_array_value( $slider_item, 'title' );

	$slider_item_button = anemos_eutf_array_value( $slider_item, 'button' );
	$slider_item_button2 = anemos_eutf_array_value( $slider_item, 'button2' );

	$anemos_eutf_button_class = "eut-feature-slider-item-delete-button";
	$anemos_eutf_replace_image_class = "eut-upload-replace-image";
	$anemos_eutf_open_modal_class = "eut-open-slider-modal";
	$anemos_eutf_closed_class = 'closed';
	$anemos_eutf_new_class = '';

	if( "new" == $new ) {
		$anemos_eutf_button_class = "eut-feature-slider-item-delete-button eut-item-new";
		$anemos_eutf_replace_image_class = "eut-upload-replace-image eut-item-new";
		$anemos_eutf_open_modal_class = "eut-open-slider-modal eut-item-new";
		$anemos_eutf_closed_class = 'eut-item-new';
		$anemos_eutf_new_class = ' eut-item-new';
	}

	$slide_uniqid = '-' . $slide_id;

	$slide_type_string = esc_html__( 'Slide', 'anemos' );
	if ( 'post' == $slide_type ) {
		$slide_type_string = esc_html__( 'Post Slide', 'anemos' );
		if( !empty( $slide_post_id ) ) {
			$title = get_the_title ( $slide_post_id  );
		}
	}

?>

	<div class="eut-slider-item postbox <?php echo esc_attr( $anemos_eutf_closed_class ); ?>">
		<button class="handlediv button-link" type="button">
			<span class="screen-reader-text"><?php esc_attr_e( 'Toggle panel: Feature Section Slide', 'anemos' ); ?></span>
			<span class="toggle-indicator"></span>
		</button>
		<input class="<?php echo esc_attr( $anemos_eutf_button_class ); ?> button" type="button" value="<?php esc_attr_e( 'Delete', 'anemos' ); ?>">
		<span class="eut-button-spacer">&nbsp;</span>
		<span class="eut-modal-spinner"></span>
		<h3 class="hndle eut-title">
			<span><?php echo esc_html( $slide_type_string ); ?>: </span><span id="_anemos_eutf_slider_item_title<?php echo esc_attr( $slide_id ); ?>_admin_label"><?php if ( !empty ($title) ) { echo esc_html( $title ); } ?></span>
		</h3>
		<div class="inside">
			<!--  METABOXES -->
			<div class="eut-metabox-content">

				<!-- TABS -->
				<div class="eut-tabs<?php echo esc_attr( $anemos_eutf_new_class ); ?>">

					<ul class="eut-tab-links">
						<li class="active"><a href="#eut-feature-single-tab-bg<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'Background / Header', 'anemos' ); ?></a></li>
						<li><a href="#eut-feature-single-tab-content<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'Content', 'anemos' ); ?></a></li>
						<?php if ( 'post' != $slide_type ) { ?>
						<li><a href="#eut-feature-single-tab-button<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'First Button', 'anemos' ); ?></a></li>
						<li><a href="#eut-feature-single-tab-button2<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'Second Button', 'anemos' ); ?></a></li>
						<?php } ?>
						<li><a href="#eut-feature-single-tab-extra<?php echo esc_attr( $slide_uniqid ); ?>"><?php esc_html_e( 'Extra', 'anemos' ); ?></a></li>
					</ul>

					<div class="eut-tab-content">

						<div id="eut-feature-single-tab-bg<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item active">
							<?php

								anemos_eutf_print_admin_option(
									array(
										'type' => 'hidden',
										'name' => '_anemos_eutf_slider_item_id[]',
										'value' => $slide_id,
									)
								);

								anemos_eutf_print_admin_option(
									array(
										'type' => 'hidden',
										'name' => '_anemos_eutf_slider_item_type[]',
										'value' => $slide_type,
									)
								);
								if ( 'post' == $slide_type ) {
									anemos_eutf_print_admin_option(
										array(
											'type' => 'hiddenfield',
											'name' => '_anemos_eutf_slider_item_post_id[]',
											'value' => $slide_post_id,
											'label' => array(
												"title" => esc_html__( 'Post ID', 'anemos' ),
												"desc" => esc_html__( 'Background Image can be still used instead of the Feature Image', 'anemos' ),
											),
										)
									);
								} else {
									anemos_eutf_print_admin_option(
										array(
											'type' => 'hidden',
											'name' => '_anemos_eutf_slider_item_post_id[]',
											'value' => $slide_post_id,
										)
									);
								}

								anemos_eutf_print_admin_option(
									array(
										'type' => 'select-image',
										'name' => '_anemos_eutf_slider_item_bg_image_id[]',
										'value' => $bg_image_id,
										'label' => array(
											"title" => esc_html__( 'Background Image', 'anemos' ),
										),
										'width' => 'fullwidth',
									)
								);

								anemos_eutf_print_admin_option(
									array(
										'type' => 'label',
										'label' => array(
											"title" => esc_html__( 'Header / Background Position', 'anemos' ),
										),
									)
								);

								anemos_eutf_print_admin_option(
									array(
										'type' => 'select-bg-position',
										'name' => '_anemos_eutf_slider_item_bg_position[]',
										'value' => $bg_position,
										'label' => array(
											"title" => esc_html__( 'Background Position', 'anemos' ),
										),
									)
								);
								anemos_eutf_print_admin_option(
									array(
										'type' => 'select-bg-position-inherit',
										'name' => '_anemos_eutf_slider_item_bg_tablet_sm_position[]',
										'value' => $bg_tablet_sm_position,
										'label' => array(
											"title" => esc_html__( 'Background Position ( Tablet Portrait )', 'anemos' ),
											"desc" => esc_html__( 'Tablet devices with portrait orientation and below.', 'anemos' ),
										),
									)
								);
								anemos_eutf_print_admin_option(
									array(
										'type' => 'select-header-style',
										'name' => '_anemos_eutf_slider_item_header_style[]',
										'value' => $header_style,
										'label' => array(
											"title" => esc_html__( 'Header Style', 'anemos' ),
										),
									)
								);

								anemos_eutf_print_admin_feature_item_overlay_options( $slider_item, '_anemos_eutf_slider_item_', 'multi' );
							?>
						</div>
						<div id="eut-feature-single-tab-content<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item">
							<?php anemos_eutf_print_admin_feature_item_content_options( $slider_item, '_anemos_eutf_slider_item_', 'multi' ); ?>
						</div>
						<div id="eut-feature-single-tab-button<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item">
							<?php anemos_eutf_print_admin_feature_item_button_options( $slider_item_button, '_anemos_eutf_slider_item_button_', 'multi' ); ?>
						</div>
						<div id="eut-feature-single-tab-button2<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item">
							<?php anemos_eutf_print_admin_feature_item_button_options( $slider_item_button2, '_anemos_eutf_slider_item_button2_', 'multi' ); ?>
						</div>
						<div id="eut-feature-single-tab-extra<?php echo esc_attr( $slide_uniqid ); ?>" class="eut-tab-item">
							<?php anemos_eutf_print_admin_feature_item_extra_options( $slider_item, '_anemos_eutf_slider_item_', 'multi' ); ?>
						</div>
					</div>

				</div>
				<!-- END TABS -->

			</div>
			<!-- END  METABOXES -->
		</div>

	</div>
<?php

}

/**
* Get Revolution Sliders
*/
function anemos_eutf_get_revolution_selection() {

	$revsliders = array(
		"" => esc_html__( "None", 'anemos' ),
	);

	if ( class_exists( 'RevSlider' ) ) {
		$slider = new RevSlider();
		$arrSliders = $slider->getArrSliders();

		if ( $arrSliders ) {
			foreach ( $arrSliders as $slider ) {
				$revsliders[ $slider->getAlias() ] = $slider->getTitle();
			}
		}
	}

	return $revsliders;
}

/**
* Prints Item Button Options
*/
function anemos_eutf_print_admin_feature_item_button_options( $item, $prefix = '_anemos_eutf_single_item_button_', $mode = '' ) {


	$button_id = anemos_eutf_array_value( $item, 'id', uniqid() );
	$group_id = $prefix . $button_id;

	$button_text = anemos_eutf_array_value( $item, 'text' );
	$button_url = anemos_eutf_array_value( $item, 'url' );
	$button_type = anemos_eutf_array_value( $item, 'type', '' );
	$button_size = anemos_eutf_array_value( $item, 'size', 'medium' );
	$button_color = anemos_eutf_array_value( $item, 'color', 'primary-1' );
	$button_hover_color = anemos_eutf_array_value( $item, 'hover_color', 'black' );
	$button_shape = anemos_eutf_array_value( $item, 'shape', 'square' );
	$button_target = anemos_eutf_array_value( $item, 'target', '_self' );
	$button_class = anemos_eutf_array_value( $item, 'class' );

	$sufix  = '';
	if ( 'multi' == $mode ) {
		$sufix = '[]';
	};
	echo '<div id="' . esc_attr( $group_id ) . '">';


	anemos_eutf_print_admin_option(
		array(
			'type' => 'hidden',
			'name' => $prefix . 'id' . $sufix,
			'value' => $button_id,
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => $prefix . 'text' . $sufix,
			'id' => $prefix . 'text' . '_' . $button_id,
			'value' => $button_text,
			'label' => esc_html__( 'Button Text', 'anemos' ),
		)
	);
	anemos_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => $prefix . 'url' . $sufix,
			'id' => $prefix . 'url' . '_' . $button_id,
			'value' => $button_url,
			'label' => esc_html__( 'Button URL', 'anemos' ),
			'width' => 'fullwidth',
		)
	);
	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-button-target',
			'name' => $prefix . 'target' . $sufix,
			'id' => $prefix . 'target' . '_' . $button_id,
			'value' => $button_target,
			'label' => esc_html__( 'Button Target', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-button-type',
			'name' => $prefix . 'type' . $sufix,
			'id' => $prefix . 'type' . '_' . $button_id,
			'value' => $button_type,
			'group_id' => $group_id,
			'label' => esc_html__( 'Button Type', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-button-color',
			'name' => $prefix . 'color' . $sufix,
			'id' => $prefix . 'color' . '_' . $button_id,
			'value' => $button_color,
			'group_id' => $group_id,
			'label' => esc_html__( 'Button Color', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-button-color',
			'name' => $prefix . 'hover_color' . $sufix,
			'id' => $prefix . 'hover_color' . '_' . $button_id,
			'value' => $button_hover_color,
			'group_id' => $group_id,
			'label' => esc_html__( 'Button Hover Color', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-button-size',
			'name' => $prefix . 'size' . $sufix,
			'value' => $button_size,
			'label' => esc_html__( 'Button Size', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-button-shape',
			'name' => $prefix . 'shape' . $sufix,
			'value' => $button_shape,
			'label' => esc_html__( 'Button Shape', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => $prefix . 'class' . $sufix,
			'id' => $prefix . 'class' . '_' . $button_id,
			'value' => $button_class,
			'label' => esc_html__( 'Button Class', 'anemos' ),
		)
	);

	echo '</div>';

}


/**
* Prints Item Overlay Options
*/
function anemos_eutf_print_admin_feature_item_overlay_options( $item, $prefix = '_anemos_eutf_single_item_', $mode = '' ) {

	$pattern_overlay = anemos_eutf_array_value( $item, 'pattern_overlay' );
	$color_overlay = anemos_eutf_array_value( $item, 'color_overlay', 'dark' );
	$color_overlay_custom  = anemos_eutf_array_value( $item, 'color_overlay_custom', '#000000' );
	$opacity_overlay = anemos_eutf_array_value( $item, 'opacity_overlay', '0' );

	$sufix  = '';
	if ( 'multi' == $mode ) {
		$sufix = '[]';
	};

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-pattern-overlay',
			'name' => $prefix . 'pattern_overlay' . $sufix,
			'value' => $pattern_overlay,
			'label' => esc_html__( 'Pattern Overlay', 'anemos' ),
		)
	);
	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-colorpicker',
			'name' => $prefix . 'color_overlay' . $sufix,
			'value' => $color_overlay,
			'value2' => $color_overlay_custom,
			'label' => esc_html__( 'Color Overlay', 'anemos' ),
			'multiple' => 'multi',
		)
	);
	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-opacity',
			'name' => $prefix . 'opacity_overlay' . $sufix,
			'value' => $opacity_overlay,
			'label' => esc_html__( 'Opacity Overlay', 'anemos' ),
		)
	);

}

/**
* Prints Item Content Options
*/
function anemos_eutf_print_admin_feature_item_content_options( $item, $prefix = '_anemos_eutf_single_item_', $mode = '' ) {

	$item_id = anemos_eutf_array_value( $item, 'id' );
	$title = anemos_eutf_array_value( $item, 'title' );
	$content_bg_color = anemos_eutf_array_value( $item, 'content_bg_color', 'light' );
	$content_bg_color_custom = anemos_eutf_array_value( $item, 'content_bg_color_custom', '#ffffff' );
	$title_color = anemos_eutf_array_value( $item, 'title_color', 'dark' );
	$title_color_custom = anemos_eutf_array_value( $item, 'title_color_custom', '#000000' );
	$title_tag = anemos_eutf_array_value( $item, 'title_tag', 'div' );
	$caption = anemos_eutf_array_value( $item, 'caption' );
	$caption_color = anemos_eutf_array_value( $item, 'caption_color', 'dark' );
	$caption_color_custom = anemos_eutf_array_value( $item, 'caption_color_custom', '#000000' );
	$caption_tag = anemos_eutf_array_value( $item, 'caption_tag', 'div' );
	$subheading = anemos_eutf_array_value( $item, 'subheading' );
	$subheading_color = anemos_eutf_array_value( $item, 'subheading_color', 'dark' );
	$subheading_color_custom = anemos_eutf_array_value( $item, 'subheading_color_custom', '#000000' );
	$subheading_tag = anemos_eutf_array_value( $item, 'subheading_tag', 'div' );

	$content_position = anemos_eutf_array_value( $item, 'content_position', 'center-center' );
	$content_animation = anemos_eutf_array_value( $item, 'content_animation', 'fade-in' );
	$content_image_id = anemos_eutf_array_value( $item, 'content_image_id', '0' );
	$content_image_size = anemos_eutf_array_value( $item, 'content_image_size' );
	$content_image_max_height = anemos_eutf_array_value( $item, 'content_image_max_height', '150' );



	$sufix  = '';
	if ( 'multi' == $mode ) {
		$sufix = '[]';
	};

	$type = anemos_eutf_array_value( $item, 'type' );

	if ( 'post' == $type ) {
?>
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'content_image_id' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'content_image_size' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'content_image_max_height' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'subheading' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'title' . $sufix ); ?>" value="" />
		<input type="hidden" name="<?php echo esc_attr( $prefix . 'caption' . $sufix ); ?>" value="" />
<?php

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'title_content_bg_color' . $sufix,
				'value' => $content_bg_color,
				'value2' => $content_bg_color_custom,
				'label' => array(
					"title" => esc_html__( 'Content Background Color', 'anemos' ),
				),
				'multiple' => 'multi',
				'type_usage' => 'title-content-bg',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'subheading_color' . $sufix,
				'value' => $subheading_color,
				'value2' => $subheading_color_custom,
				'label' => esc_html__( 'Post Meta Color', 'anemos' ),
				'multiple' => 'multi',
			)
		);
		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'title_color' . $sufix,
				'value' => $title_color,
				'value2' => $title_color_custom,
				'label' => esc_html__( 'Post Title Color', 'anemos' ),
				'multiple' => 'multi',
			)
		);
		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'caption_color' . $sufix,
				'value' => $caption_color,
				'value2' => $caption_color_custom,
				'label' => esc_html__( 'Post Description Color', 'anemos' ),
				'multiple' => 'multi',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'subheading_tag' . $sufix,
				'value' => $subheading_tag,
				'label' => esc_html__( 'Post Meta Tag', 'anemos' ),
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'title_tag' . $sufix,
				'value' => $title_tag,
				'label' => esc_html__( 'Post Title Tag', 'anemos' ),
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'caption_tag' . $sufix,
				'value' => $caption_tag,
				'label' => esc_html__( 'Post Description Tag', 'anemos' ),
			)
		);

	} else {
		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-image',
				'name' => $prefix . 'content_image_id' . $sufix,
				'value' => $content_image_id,
				'label' => array(
					"title" => esc_html__( 'Graphic Image', 'anemos' ),
				),
				'width' => 'fullwidth',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => $prefix . 'content_image_size' . $sufix,
				'options' => array(
					'' => esc_html__( 'Resize ( Medium )', 'anemos' ),
					'full' => esc_html__( 'Full size', 'anemos' ),
				),
				'value' => $content_image_size,
				'label' => esc_html__( 'Graphic Image Size', 'anemos' ),
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'textfield',
				'name' => $prefix . 'content_image_max_height' . $sufix,
				'value' => $content_image_max_height,
				'label' => esc_html__( 'Graphic Image Max Height', 'anemos' ),
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'textfield',
				'name' => $prefix . 'subheading' . $sufix,
				'value' => $subheading,
				'label' => esc_html__( 'Sub Heading', 'anemos' ),
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'textfield',
				'name' => $prefix . 'title' . $sufix,
				'value' => $title,
				'label' => esc_html__( 'Title', 'anemos' ),
				'extra_class' =>  'eut-admin-label-update',
				'id' => $prefix . 'title' . $item_id,
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'textfield',
				'name' => $prefix . 'caption' . $sufix,
				'value' => $caption,
				'label' => esc_html__( 'Description', 'anemos' ),
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'title_content_bg_color' . $sufix,
				'value' => $content_bg_color,
				'value2' => $content_bg_color_custom,
				'label' => array(
					"title" => esc_html__( 'Content Background Color', 'anemos' ),
				),
				'multiple' => 'multi',
				'type_usage' => 'title-content-bg',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'subheading_color' . $sufix,
				'value' => $subheading_color,
				'value2' => $subheading_color_custom,
				'label' => esc_html__( 'Sub Heading Color', 'anemos' ),
				'multiple' => 'multi',
			)
		);
		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'title_color' . $sufix,
				'value' => $title_color,
				'value2' => $title_color_custom,
				'label' => esc_html__( 'Title Color', 'anemos' ),
				'multiple' => 'multi',
			)
		);
		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-colorpicker',
				'name' => $prefix . 'caption_color' . $sufix,
				'value' => $caption_color,
				'value2' => $caption_color_custom,
				'label' => esc_html__( 'Description Color', 'anemos' ),
				'multiple' => 'multi',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'subheading_tag' . $sufix,
				'value' => $subheading_tag,
				'label' => esc_html__( 'Sub Heading Tag', 'anemos' ),
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'title_tag' . $sufix,
				'value' => $title_tag,
				'label' => esc_html__( 'Title Tag', 'anemos' ),
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select-tag',
				'name' => $prefix . 'caption_tag' . $sufix,
				'value' => $caption_tag,
				'label' => esc_html__( 'Description Tag', 'anemos' ),
			)
		);

	}

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-bg-position',
			'name' => $prefix . 'content_position' . $sufix,
			'value' => $content_position,
			'label' => esc_html__( 'Content Position', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-text-animation',
			'name' => $prefix . 'content_animation' . $sufix,
			'value' => $content_animation,
			'label' => esc_html__( 'Content Animation', 'anemos' ),
		)
	);

}

/**
* Prints Item Extra Options
*/
function anemos_eutf_print_admin_feature_item_extra_options( $item, $prefix = '_anemos_eutf_single_item_', $mode = '' ) {

	$sufix  = '';
	if ( 'multi' == $mode ) {
		$sufix = '[]';
	};

	$el_class = anemos_eutf_array_value( $item, 'el_class' );
	$arrow_enabled = anemos_eutf_array_value( $item, 'arrow_enabled', 'no' );
	$arrow_align = anemos_eutf_array_value( $item, 'arrow_align', 'left' );
	$arrow_color = anemos_eutf_array_value( $item, 'arrow_color', 'light' );
	$arrow_color_custom = anemos_eutf_array_value( $item, 'arrow_color_custom', '#ffffff' );

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-boolean',
			'name' => $prefix . 'arrow_enabled' . $sufix,
			'value' => $arrow_enabled,
			'label' => esc_html__( 'Enable Bottom Arrow', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-align',
			'name' => $prefix . 'arrow_align' . $sufix,
			'value' => $arrow_align,
			'label' => esc_html__( 'Arrow Alignment', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-colorpicker',
			'name' => $prefix . 'arrow_color' . $sufix,
			'value' => $arrow_color,
			'value2' => $arrow_color_custom,
			'label' => esc_html__( 'Arrow Color', 'anemos' ),
			'multiple' => 'multi',
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'textfield',
			'name' => $prefix . 'el_class' . $sufix,
			'value' => $el_class,
			'label' => esc_html__( 'Extra Class', 'anemos' ),
		)
	);

}

/**
 * Prints Item Background Image Options
 */
function anemos_eutf_print_admin_feature_item_background_options( $item ) {

	$bg_image_id = anemos_eutf_array_value( $item, 'bg_image_id', '0' );
	$bg_position = anemos_eutf_array_value( $item, 'bg_position', 'center-center' );
	$bg_tablet_sm_position = anemos_eutf_array_value( $item, 'bg_tablet_sm_position' );
	$image_effect = anemos_eutf_array_value( $item, 'image_effect' );


	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-image',
			'name' => '_anemos_eutf_single_item_bg_image_id',
			'value' => $bg_image_id,
			'label' => array(
				"title" => esc_html__( 'Background Image', 'anemos' ),
			),
			'width' => 'fullwidth',
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-bg-position',
			'name' => '_anemos_eutf_single_item_bg_position',
			'value' => $bg_position,
			'label' => esc_html__( 'Background Position', 'anemos' ),
		)
	);
	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-bg-position-inherit',
			'name' => '_anemos_eutf_single_item_bg_tablet_sm_position',
			'value' => $bg_tablet_sm_position,
			'label' => array(
				"title" => esc_html__( 'Background Position ( Tablet Portrait )', 'anemos' ),
				"desc" => esc_html__( 'Tablet devices with portrait orientation and below.', 'anemos' ),
			),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select',
			'name' => '_anemos_eutf_single_item_image_effect',
			'options' => array(
				'' => esc_html__( 'None', 'anemos' ),
				'animated' => esc_html__( 'Animated', 'anemos' ),
				'parallax' => esc_html__( 'Parallax', 'anemos' ),
			),
			'value' => $image_effect,
			'label' => esc_html__( 'Background Effect', 'anemos' ),
			'wrap_class' => 'eut-feature-required eut-item-feature-image-settings',
		)
	);

}

/**
 * Prints Item Background Video Options
 */
function anemos_eutf_print_admin_feature_item_video_options( $item ) {

	$video_webm = anemos_eutf_array_value( $item, 'video_webm' );
	$video_mp4 = anemos_eutf_array_value( $item, 'video_mp4' );
	$video_ogv = anemos_eutf_array_value( $item, 'video_ogv' );
	$video_poster = anemos_eutf_array_value( $item, 'video_poster', 'no' );
	$video_loop = anemos_eutf_array_value( $item, 'video_loop', 'yes' );
	$video_muted = anemos_eutf_array_value( $item, 'video_muted', 'yes' );
	$video_effect = anemos_eutf_array_value( $item, 'video_effect' );

	anemos_eutf_print_admin_option(
		array(
			'type' => 'label',
			'label' => esc_html__( 'HTML5 Video', 'anemos' ),
		)
	);
	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-bg-video',
			'name' => '_anemos_eutf_single_item_video_webm',
			'value' => $video_webm,
			'label' => esc_html__( 'WebM File URL', 'anemos' ),
			'width' => 'fullwidth',
		)
	);
	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-bg-video',
			'name' => '_anemos_eutf_single_item_video_mp4',
			'value' => $video_mp4,
			'label' => esc_html__( 'MP4 File URL', 'anemos' ),
			'width' => 'fullwidth',
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-bg-video',
			'name' => '_anemos_eutf_single_item_video_ogv',
			'value' => $video_ogv,
			'label' => esc_html__( 'OGV File URL', 'anemos' ),
			'width' => 'fullwidth',
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-boolean',
			'name' => '_anemos_eutf_single_item_video_poster',
			'value' => $video_poster,
			'label' => esc_html__( 'Use Fallback Image as Poster', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-boolean',
			'name' => '_anemos_eutf_single_item_video_loop',
			'value' => $video_loop,
			'label' => esc_html__( 'Loop', 'anemos' ),
		)
	);

	anemos_eutf_print_admin_option(
		array(
			'type' => 'select-boolean',
			'name' => '_anemos_eutf_single_item_video_muted',
			'value' => $video_muted,
			'label' => esc_html__( 'Muted', 'anemos' ),
		)
	);
	anemos_eutf_print_admin_option(
		array(
			'type' => 'select',
			'name' => '_anemos_eutf_single_item_video_effect',
			'options' => array(
				'' => esc_html__( 'None', 'anemos' ),
				'animated' => esc_html__( 'Animated', 'anemos' ),
			),
			'value' => $video_effect,
			'label' => esc_html__( 'Video Effect', 'anemos' ),
		)
	);


}


function anemos_eutf_admin_get_feature_section( $post_id ) {

	//Feature Section
	$feature_section = get_post_meta( $post_id, '_anemos_eutf_feature_section', true );

	//Feature Settings
	$feature_settings = anemos_eutf_array_value( $feature_section, 'feature_settings' );

	$feature_element = anemos_eutf_array_value( $feature_settings, 'element' );
	$feature_size = anemos_eutf_array_value( $feature_settings, 'size' );
	$feature_height = anemos_eutf_array_value( $feature_settings, 'height', '60' );
	$feature_min_height = anemos_eutf_array_value( $feature_settings, 'min_height', '200' );
	$feature_bg_color  = anemos_eutf_array_value( $feature_settings, 'bg_color', 'dark' );
	$feature_bg_color_custom  = anemos_eutf_array_value( $feature_settings, 'bg_color_custom', '#eef1f6' );
	$feature_header_position = anemos_eutf_array_value( $feature_settings, 'header_position', 'above' );

	//Feature Single Item
	$feature_single_item = anemos_eutf_array_value( $feature_section, 'single_item' );
	$feature_single_item_button = anemos_eutf_array_value( $feature_single_item, 'button' );
	$feature_single_item_button2 = anemos_eutf_array_value( $feature_single_item, 'button2' );


	//Slider Item
	$slider_items = anemos_eutf_array_value( $feature_section, 'slider_items' );
	$slider_settings = anemos_eutf_array_value( $feature_section, 'slider_settings' );

	$slider_speed = anemos_eutf_array_value( $slider_settings, 'slideshow_speed', '3500' );
	$slider_pause = anemos_eutf_array_value( $slider_settings, 'slider_pause', 'no' );
	$slider_dir_nav = anemos_eutf_array_value( $slider_settings, 'direction_nav', '1' );
	$slider_dir_nav_color = anemos_eutf_array_value( $slider_settings, 'direction_nav_color', 'light' );
	$slider_transition = anemos_eutf_array_value( $slider_settings, 'transition', 'slide' );
	$slider_effect = anemos_eutf_array_value( $slider_settings, 'slider_effect' );

	//Map Item
	$map_items = anemos_eutf_array_value( $feature_section, 'map_items' );
	$map_settings = anemos_eutf_array_value( $feature_section, 'map_settings' );

	$map_zoom = anemos_eutf_array_value( $map_settings, 'zoom', 14 );
	$map_marker = anemos_eutf_array_value( $map_settings, 'marker' );
	$map_disable_style = anemos_eutf_array_value( $map_settings, 'disable_style', 'no' );

	//Revolution slider
	$revslider_alias = anemos_eutf_array_value( $feature_section, 'revslider_alias' );

	global $anemos_eutf_area_height;

?>

		<div class="eut-fields-wrapper eut-highlight">
			<div class="eut-label">
				<label for="eut-page-feature-element">
					<span class="eut-title"><?php esc_html_e( 'Feature Element', 'anemos' ); ?></span>
					<span class="eut-description"><?php esc_html_e( 'Select feature section element', 'anemos' ); ?></span>
				</label>
			</div>
			<div class="eut-field-items-wrapper">
				<div class="eut-field-item">
					<select id="eut-page-feature-element" name="_anemos_eutf_page_feature_element">
						<option value="" <?php selected( "", $feature_element ); ?>><?php esc_html_e( 'None', 'anemos' ); ?></option>
						<option value="title" <?php selected( "title", $feature_element ); ?>><?php esc_html_e( 'Title', 'anemos' ); ?></option>
						<option value="image" <?php selected( "image", $feature_element ); ?>><?php esc_html_e( 'Image', 'anemos' ); ?></option>
						<option value="video" <?php selected( "video", $feature_element ); ?>><?php esc_html_e( 'Video', 'anemos' ); ?></option>
						<option value="slider" <?php selected( "slider", $feature_element ); ?>><?php esc_html_e( 'Slider', 'anemos' ); ?></option>
						<option value="map" <?php selected( "map", $feature_element ); ?>><?php esc_html_e( 'Map', 'anemos' ); ?></option>
					</select>
				</div>
			</div>
		</div>

		<div id="eut-feature-section-options" class="eut-feature-section-item postbox" <?php if ( "" != $feature_element ) { ?> style="display:none;" <?php } ?>>

			<div class="eut-fields-wrapper eut-feature-options-wrapper">
				<div class="eut-label">
					<label for="eut-page-feature-element">
						<span class="eut-title"><?php esc_html_e( 'Feature Size', 'anemos' ); ?></span>
						<span class="eut-description"><?php esc_html_e( 'With Custom Size option you can select the feature height in %.', 'anemos' ); ?></span>
					</label>
				</div>
				<div class="eut-field-items-wrapper">
					<div class="eut-field-item">
						<select name="_anemos_eutf_page_feature_size" id="eut-page-feature-size">
							<option value="" <?php selected( "", $feature_size ); ?>><?php esc_html_e( 'Full Screen', 'anemos' ); ?></option>
							<option value="custom" <?php selected( "custom", $feature_size ); ?>><?php esc_html_e( 'Custom Size', 'anemos' ); ?></option>
						</select>
					</div>
					<div class="eut-field-item">
						<span id="eut-feature-section-height" <?php if ( "" == $feature_size ) { ?> style="display:none;" <?php } ?>>
							<select name="_anemos_eutf_page_feature_height">
								<?php anemos_eutf_print_select_options( $anemos_eutf_area_height, $feature_height ); ?>
							</select>
							<span class="eut-sub-title"><?php esc_html_e( 'Height in %', 'anemos' ); ?></span>
							<input type="text" name="_anemos_eutf_page_feature_min_height" value="<?php echo esc_attr( $feature_min_height ); ?>"/>
							<span class="eut-sub-title"><?php esc_html_e( 'Minimum Height in px', 'anemos' ); ?></span>
						</span>
					</div>
				</div>
			</div>
			<?php
				anemos_eutf_print_admin_option(
					array(
						'type' => 'select',
						'options' => array(
							'above' => esc_html__( 'Header above Feature', 'anemos' ),
							'below' => esc_html__( 'Header below Feature', 'anemos' ),
						),
						'name' => '_anemos_eutf_page_feature_header_position',
						'value' => $feature_header_position,
						'label' => array(
							'title' => esc_html__( 'Feature/Header Position', 'anemos' ),
							'desc' => esc_html__( 'With this option header will be shown above or below feature section.', 'anemos' ),
						),
					)
				);
			?>
			<div class="eut-feature-options-wrapper">
			<?php

				anemos_eutf_print_admin_option(
					array(
						'type' => 'select-colorpicker',
						'name' => '_anemos_eutf_page_feature_bg_color',
						'value' => $feature_bg_color,
						'value2' => $feature_bg_color_custom,
						'label' => esc_html__( 'Background Color', 'anemos' ),
						'multiple' => 'multi',
					)
				);
			?>
			</div>

		</div>



		<div id="eut-feature-section-slider" class="eut-feature-section-item" <?php if ( "slider" != $feature_element ) { ?> style="display:none;" <?php } ?>>

			<div class="postbox">
				<h3 class="eut-title">
					<span><?php esc_html_e( 'Slider Settings', 'anemos' ); ?></span>
				</h3>
				<div class="inside">

					<?php
						anemos_eutf_print_admin_option(
							array(
								'type' => 'textfield',
								'name' => '_anemos_eutf_page_slider_settings_speed',
								'value' => $slider_speed,
								'label' => esc_html__( 'Slideshow Speed', 'anemos' ),
							)
						);
						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'name' => '_anemos_eutf_page_slider_settings_pause',
								'options' => array(
									'no' => esc_html__( 'No', 'anemos' ),
									'yes' => esc_html__( 'Yes', 'anemos' ),
								),
								'value' => $slider_pause,
								'label' => esc_html__( 'Pause on Hover', 'anemos' ),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'options' => array(
									'1' => esc_html__( 'Style 1', 'anemos' ),
									'2' => esc_html__( 'Style 2', 'anemos' ),
									'3' => esc_html__( 'Style 3', 'anemos' ),
									'4' => esc_html__( 'Style 4', 'anemos' ),
									'0' => esc_html__( 'No Navigation', 'anemos' ),
								),
								'name' => '_anemos_eutf_page_slider_settings_direction_nav',
								'value' => $slider_dir_nav,
								'label' => array(
									'title' => esc_html__( 'Navigation Buttons', 'anemos' ),
								),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'options' => array(
									'slide' => esc_html__( 'Slide', 'anemos' ),
									'fade' => esc_html__( 'Fade', 'anemos' ),
									'backSlide' => esc_html__( 'Back Slide', 'anemos' ),
									'goDown' => esc_html__( 'Go Down', 'anemos' ),
								),
								'name' => '_anemos_eutf_page_slider_settings_transition',
								'value' => $slider_transition,
								'label' => array(
									'title' => esc_html__( 'Transition', 'anemos' ),
								),
							)
						);

						anemos_eutf_print_admin_option(
							array(
								'type' => 'select',
								'options' => array(
									'' => esc_html__( 'None', 'anemos' ),
									'animated' => esc_html__( 'Animated', 'anemos' ),
									'parallax' => esc_html__( 'Parallax', 'anemos' ),
								),
								'name' => '_anemos_eutf_page_slider_settings_effect',
								'value' => $slider_effect,
								'label' => array(
									'title' => esc_html__( 'Slider Effect', 'anemos' ),
								),
							)
						);
					?>

					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Add Slides', 'anemos' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item">
								<input type="button" class="eut-upload-feature-slider-button button-primary" value="<?php esc_attr_e( 'Insert Images to Slider', 'anemos' ); ?>"/>
								<span id="eut-upload-feature-slider-button-spinner" class="eut-action-spinner"></span>
							</div>
						</div>
					</div>
					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Add Post Slides', 'anemos' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item">
								<input type="button" class="eut-upload-feature-slider-post-button button-primary" value="<?php esc_attr_e( 'Insert Posts to Slider', 'anemos' ); ?>"/>
							</div>
							<div class="eut-field-item">
								<input id="eut-upload-feature-slider-post-selection" type="hidden" class="eut-post-selector-select2"  value="" />
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div id="eut-feature-slider-container" data-mode="slider-full" class="eut-feature-section-item" <?php if ( 'slider' != $feature_element ) { ?> style="display:none;" <?php } ?>>
			<?php
				if( !empty( $slider_items ) ) {
					anemos_eutf_print_admin_feature_slider_items( $slider_items );
				}
			?>
		</div>

		<div id="eut-feature-map-container" class="eut-feature-section-item" <?php if ( 'map' != $feature_element ) { ?> style="display:none;" <?php } ?>>
			<div class="eut-map-item postbox">
				<h3 class="eut-title">
					<span><?php esc_html_e( 'Map', 'anemos' ); ?></span>
				</h3>
				<div class="inside">
					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Single Point Zoom', 'anemos' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item">
								<select id="eut-page-feature-map-zoom" name="_anemos_eutf_page_feature_map_zoom">
									<?php for ( $i=1; $i < 20; $i++ ) { ?>
										<option value="<?php echo esc_attr( $i ); ?>" <?php selected( $i, $map_zoom ); ?>><?php echo esc_html( $i ); ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Global Marker', 'anemos' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item eut-field-item-fullwidth">
								<input type="text" class="eut-upload-simple-media-field" id="eut-page-feature-map-marker" name="_anemos_eutf_page_feature_map_marker" value="<?php echo esc_attr( $map_marker ); ?>"/>
								<label></label>
								<input type="button" data-media-type="image" class="eut-upload-simple-media-button button-primary" value="<?php esc_attr_e( 'Insert Marker', 'anemos' ); ?>"/>
								<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>
							</div>
						</div>
					</div>
					<div class="eut-fields-wrapper">
						<div class="eut-label">
							<label for="eut-page-feature-element">
								<span class="eut-title"><?php esc_html_e( 'Disable Custom Style', 'anemos' ); ?></span>
							</label>
						</div>
						<div class="eut-field-items-wrapper">
							<div class="eut-field-item">
								<select id="eut-page-feature-map-disable-style" name="_anemos_eutf_page_feature_map_disable_style">
									<option value="no" <?php selected( "no", $map_disable_style ); ?>><?php esc_html_e( 'No', 'anemos' ); ?></option>
									<option value="yes" <?php selected( "yes", $map_disable_style ); ?>><?php esc_html_e( 'Yes', 'anemos' ); ?></option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="eut-fields-wrapper">
					<div class="eut-label">
						<label for="eut-page-feature-element">
							<span class="eut-title"><?php esc_html_e( 'Add Map Points', 'anemos' ); ?></span>
						</label>
					</div>
					<div class="eut-field-items-wrapper">
						<div class="eut-field-item">
							<input type="button" id="eut-upload-multi-map-point" class="eut-upload-multi-map-point button-primary" value="<?php esc_attr_e( 'Insert Point to Map', 'anemos' ); ?>"/>
							<span id="eut-upload-multi-map-button-spinner" class="eut-action-spinner"></span>
						</div>
					</div>
				</div>
			</div>
			<?php anemos_eutf_print_admin_feature_map_items( $map_items ); ?>
		</div>

		<div id="eut-feature-single-container" class="eut-feature-section-item" <?php if ( 'title' != $feature_element && 'image' != $feature_element && 'video' != $feature_element ) { ?> style="display:none;" <?php } ?>>
			<div class="eut-video-item postbox">
				<span class="eut-modal-spinner"></span>
				<h3 class="eut-title">
					<span><?php esc_html_e( 'Options', 'anemos' ); ?></span>
				</h3>
				<div class="inside">

					<!--  METABOXES -->
					<div class="eut-metabox-content">

						<!-- TABS -->
						<div class="eut-tabs">

							<ul class="eut-tab-links">
								<li class="eut-feature-required eut-item-feature-video-settings active"><a id="eut-feature-single-tab-video-link" href="#eut-feature-single-tab-video"><?php esc_html_e( 'Video', 'anemos' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-bg-settings"><a id="eut-feature-single-tab-bg-link" href="#eut-feature-single-tab-bg"><?php esc_html_e( 'Background', 'anemos' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-content-settings"><a id="eut-feature-single-tab-content-link" href="#eut-feature-single-tab-content"><?php esc_html_e( 'Content', 'anemos' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-revslider-settings"><a id="eut-feature-single-tab-revslider-link" href="#eut-feature-single-tab-revslider"><?php esc_html_e( 'Revolution Slider', 'anemos' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-button-settings"><a href="#eut-feature-single-tab-button"><?php esc_html_e( 'First Button', 'anemos' ); ?></a></li>
								<li class="eut-feature-required eut-item-feature-button-settings"><a href="#eut-feature-single-tab-button2"><?php esc_html_e( 'Second Button', 'anemos' ); ?></a></li>
								<li><a href="#eut-feature-single-tab-extra"><?php esc_html_e( 'Extra', 'anemos' ); ?></a></li>
							</ul>

							<div class="eut-tab-content">
								<div id="eut-feature-single-tab-video" class="eut-tab-item active">
									<?php anemos_eutf_print_admin_feature_item_video_options( $feature_single_item ); ?>
								</div>
								<div id="eut-feature-single-tab-revslider" class="eut-tab-item">
									<?php
										anemos_eutf_print_admin_option(
												array(
													'type' => 'select',
													'options' => anemos_eutf_get_revolution_selection(),
													'name' => '_anemos_eutf_page_revslider',
													'value' => $revslider_alias,
													'label' => array(
														'title' => esc_html__( 'Revolution Slider', 'anemos' ),
													),
												)
											);
									?>
								</div>
								<div id="eut-feature-single-tab-bg" class="eut-tab-item">
									<?php anemos_eutf_print_admin_feature_item_background_options( $feature_single_item ); ?>
									<?php anemos_eutf_print_admin_feature_item_overlay_options( $feature_single_item ); ?>
								</div>
								<div id="eut-feature-single-tab-content" class="eut-tab-item">
									<?php anemos_eutf_print_admin_feature_item_content_options( $feature_single_item ); ?>
								</div>
								<div id="eut-feature-single-tab-button" class="eut-tab-item">
									<?php anemos_eutf_print_admin_feature_item_button_options( $feature_single_item_button, '_anemos_eutf_single_item_button_' ); ?>
								</div>
								<div id="eut-feature-single-tab-button2" class="eut-tab-item">
									<?php anemos_eutf_print_admin_feature_item_button_options( $feature_single_item_button2, '_anemos_eutf_single_item_button2_' ); ?>
								</div>
								<div id="eut-feature-single-tab-extra" class="eut-tab-item">
									<?php anemos_eutf_print_admin_feature_item_extra_options( $feature_single_item ); ?>
								</div>
							</div>

						</div>
						<!-- END TABS -->

					</div>
					<!-- END  METABOXES -->
				</div>
			</div>
		</div>
<?php
}

function anemos_eutf_admin_save_feature_section( $post_id ) {

	//Feature Section variable
	$feature_section = array();


	if ( isset( $_POST['_anemos_eutf_page_feature_element'] ) ) {

		//Feature Settings

		$feature_section['feature_settings'] = array (
			'element' => $_POST['_anemos_eutf_page_feature_element'],
			'size' => $_POST['_anemos_eutf_page_feature_size'],
			'height' => $_POST['_anemos_eutf_page_feature_height'],
			'min_height' => $_POST['_anemos_eutf_page_feature_min_height'],
			'header_position' => $_POST['_anemos_eutf_page_feature_header_position'],
			'bg_color' => $_POST['_anemos_eutf_page_feature_bg_color'],
			'bg_color_custom' => $_POST['_anemos_eutf_page_feature_bg_color_custom'],
		);


		//Feature Revolution Slider
		if ( isset( $_POST['_anemos_eutf_page_revslider'] ) ) {
			$feature_section['revslider_alias'] = $_POST['_anemos_eutf_page_revslider'];
		}

		//Feature Single Item
		if ( isset( $_POST['_anemos_eutf_single_item_title'] ) ) {


			$feature_section['single_item'] = array (

				'title' => $_POST['_anemos_eutf_single_item_title'],
				'content_bg_color' => $_POST['_anemos_eutf_single_item_title_content_bg_color'],
				'content_bg_color_custom' => $_POST['_anemos_eutf_single_item_title_content_bg_color_custom'],
				'title_color' => $_POST['_anemos_eutf_single_item_title_color'],
				'title_color_custom' => $_POST['_anemos_eutf_single_item_title_color_custom'],
				'title_tag' => $_POST['_anemos_eutf_single_item_title_tag'],
				'caption' => $_POST['_anemos_eutf_single_item_caption'],
				'caption_color' => $_POST['_anemos_eutf_single_item_caption_color'],
				'caption_color_custom' => $_POST['_anemos_eutf_single_item_caption_color_custom'],
				'caption_tag' => $_POST['_anemos_eutf_single_item_caption_tag'],
				'subheading' => $_POST['_anemos_eutf_single_item_subheading'],
				'subheading_color' => $_POST['_anemos_eutf_single_item_subheading_color'],
				'subheading_color_custom' => $_POST['_anemos_eutf_single_item_subheading_color_custom'],
				'subheading_tag' => $_POST['_anemos_eutf_single_item_subheading_tag'],
				'content_position' => $_POST['_anemos_eutf_single_item_content_position'],
				'content_animation' => $_POST['_anemos_eutf_single_item_content_animation'],
				'content_image_id' => $_POST['_anemos_eutf_single_item_content_image_id'],
				'content_image_size' => $_POST['_anemos_eutf_single_item_content_image_size'],
				'content_image_max_height' => $_POST['_anemos_eutf_single_item_content_image_max_height'],
				'pattern_overlay' => $_POST['_anemos_eutf_single_item_pattern_overlay'],
				'color_overlay' => $_POST['_anemos_eutf_single_item_color_overlay'],
				'color_overlay_custom' => $_POST['_anemos_eutf_single_item_color_overlay_custom'],
				'opacity_overlay' => $_POST['_anemos_eutf_single_item_opacity_overlay'],
				'bg_image_id' => $_POST['_anemos_eutf_single_item_bg_image_id'],
				'bg_position' => $_POST['_anemos_eutf_single_item_bg_position'],
				'bg_tablet_sm_position' => $_POST['_anemos_eutf_single_item_bg_tablet_sm_position'],
				'image_effect' => $_POST['_anemos_eutf_single_item_image_effect'],
				'video_webm' => $_POST['_anemos_eutf_single_item_video_webm'],
				'video_mp4' => $_POST['_anemos_eutf_single_item_video_mp4'],
				'video_ogv' => $_POST['_anemos_eutf_single_item_video_ogv'],
				'video_poster' => $_POST['_anemos_eutf_single_item_video_poster'],
				'video_loop' => $_POST['_anemos_eutf_single_item_video_loop'],
				'video_muted' => $_POST['_anemos_eutf_single_item_video_muted'],
				'video_effect' => $_POST['_anemos_eutf_single_item_video_effect'],

				'button' => array(
					'id' => $_POST['_anemos_eutf_single_item_button_id'],
					'text' => $_POST['_anemos_eutf_single_item_button_text'],
					'url' => $_POST['_anemos_eutf_single_item_button_url'],
					'target' => $_POST['_anemos_eutf_single_item_button_target'],
					'color' => $_POST['_anemos_eutf_single_item_button_color'],
					'hover_color' => $_POST['_anemos_eutf_single_item_button_hover_color'],
					'size' => $_POST['_anemos_eutf_single_item_button_size'],
					'shape' => $_POST['_anemos_eutf_single_item_button_shape'],
					'type' => $_POST['_anemos_eutf_single_item_button_type'],
					'class' => $_POST['_anemos_eutf_single_item_button_class'],
				),
				'button2' => array(
					'id' => $_POST['_anemos_eutf_single_item_button2_id'],
					'text' => $_POST['_anemos_eutf_single_item_button2_text'],
					'url' => $_POST['_anemos_eutf_single_item_button2_url'],
					'target' => $_POST['_anemos_eutf_single_item_button2_target'],
					'color' => $_POST['_anemos_eutf_single_item_button2_color'],
					'hover_color' => $_POST['_anemos_eutf_single_item_button2_hover_color'],
					'size' => $_POST['_anemos_eutf_single_item_button2_size'],
					'shape' => $_POST['_anemos_eutf_single_item_button2_shape'],
					'type' => $_POST['_anemos_eutf_single_item_button2_type'],
					'class' => $_POST['_anemos_eutf_single_item_button2_class'],
				),
				'arrow_enabled' => $_POST['_anemos_eutf_single_item_arrow_enabled'],
				'arrow_align' => $_POST['_anemos_eutf_single_item_arrow_align'],
				'arrow_color' => $_POST['_anemos_eutf_single_item_arrow_color'],
				'arrow_color_custom' => $_POST['_anemos_eutf_single_item_arrow_color_custom'],
				'el_class' => $_POST['_anemos_eutf_single_item_el_class'],

			);
		}

		//Feature Slider Items
		$slider_items = array();
		if ( isset( $_POST['_anemos_eutf_slider_item_id'] ) ) {

			$num_of_images = sizeof( $_POST['_anemos_eutf_slider_item_id'] );
			for ( $i=0; $i < $num_of_images; $i++ ) {

				$slide = array (
					'id' => $_POST['_anemos_eutf_slider_item_id'][ $i ],
					'type' => $_POST['_anemos_eutf_slider_item_type'][ $i ],
					'post_id' => $_POST['_anemos_eutf_slider_item_post_id'][ $i ],
					'bg_image_id' => $_POST['_anemos_eutf_slider_item_bg_image_id'][ $i ],
					'bg_position' => $_POST['_anemos_eutf_slider_item_bg_position'][ $i ],
					'bg_tablet_sm_position' => $_POST['_anemos_eutf_slider_item_bg_tablet_sm_position'][ $i ],
					'header_style' => $_POST['_anemos_eutf_slider_item_header_style'][ $i ],
					'title' => $_POST['_anemos_eutf_slider_item_title'][ $i ],
					'content_bg_color' => $_POST['_anemos_eutf_slider_item_title_content_bg_color'][ $i ],
					'content_bg_color_custom' => $_POST['_anemos_eutf_slider_item_title_content_bg_color_custom'][ $i ],
					'title_color' => $_POST['_anemos_eutf_slider_item_title_color'][ $i ],
					'title_color_custom' => $_POST['_anemos_eutf_slider_item_title_color_custom'][ $i ],
					'title_tag' => $_POST['_anemos_eutf_slider_item_title_tag'][ $i ],
					'caption' => $_POST['_anemos_eutf_slider_item_caption'][ $i ],
					'caption_color' => $_POST['_anemos_eutf_slider_item_caption_color'][ $i ],
					'caption_color_custom' => $_POST['_anemos_eutf_slider_item_caption_color_custom'][ $i ],
					'caption_tag' => $_POST['_anemos_eutf_slider_item_caption_tag'][ $i ],
					'subheading' => $_POST['_anemos_eutf_slider_item_subheading'][ $i ],
					'subheading_color' => $_POST['_anemos_eutf_slider_item_subheading_color'][ $i ],
					'subheading_color_custom' => $_POST['_anemos_eutf_slider_item_subheading_color_custom'][ $i ],
					'subheading_tag' => $_POST['_anemos_eutf_slider_item_subheading_tag'][ $i ],
					'content_position' => $_POST['_anemos_eutf_slider_item_content_position'][ $i ],
					'content_animation' => $_POST['_anemos_eutf_slider_item_content_animation'][ $i ],
					'content_image_id' => $_POST['_anemos_eutf_slider_item_content_image_id'][ $i ],
					'content_image_size' => $_POST['_anemos_eutf_slider_item_content_image_size'][ $i ],
					'content_image_max_height' => $_POST['_anemos_eutf_slider_item_content_image_max_height'][ $i ],
					'pattern_overlay' => $_POST['_anemos_eutf_slider_item_pattern_overlay'][ $i ],
					'color_overlay' => $_POST['_anemos_eutf_slider_item_color_overlay'][ $i ],
					'color_overlay_custom' => $_POST['_anemos_eutf_slider_item_color_overlay_custom'][ $i ],
					'opacity_overlay' => $_POST['_anemos_eutf_slider_item_opacity_overlay'][ $i ],
					'button' => array(
						'id' => $_POST['_anemos_eutf_slider_item_button_id'][ $i ],
						'text' => $_POST['_anemos_eutf_slider_item_button_text'][ $i ],
						'url' => $_POST['_anemos_eutf_slider_item_button_url'][ $i ],
						'target' => $_POST['_anemos_eutf_slider_item_button_target'][ $i ],
						'color' => $_POST['_anemos_eutf_slider_item_button_color'][ $i ],
						'hover_color' => $_POST['_anemos_eutf_slider_item_button_hover_color'][ $i ],
						'size' => $_POST['_anemos_eutf_slider_item_button_size'][ $i ],
						'shape' => $_POST['_anemos_eutf_slider_item_button_shape'][ $i ],
						'type' => $_POST['_anemos_eutf_slider_item_button_type'][ $i ],
						'class' => $_POST['_anemos_eutf_slider_item_button_class'][ $i ],
					),
					'button2' => array(
						'id' => $_POST['_anemos_eutf_slider_item_button2_id'][ $i ],
						'text' => $_POST['_anemos_eutf_slider_item_button2_text'][ $i ],
						'url' => $_POST['_anemos_eutf_slider_item_button2_url'][ $i ],
						'target' => $_POST['_anemos_eutf_slider_item_button2_target'][ $i ],
						'color' => $_POST['_anemos_eutf_slider_item_button2_color'][ $i ],
						'hover_color' => $_POST['_anemos_eutf_slider_item_button2_hover_color'][ $i ],
						'size' => $_POST['_anemos_eutf_slider_item_button2_size'][ $i ],
						'shape' => $_POST['_anemos_eutf_slider_item_button2_shape'][ $i ],
						'type' => $_POST['_anemos_eutf_slider_item_button2_type'][ $i ],
						'class' => $_POST['_anemos_eutf_slider_item_button2_class'][ $i ],
					),
					'arrow_enabled' => $_POST['_anemos_eutf_slider_item_arrow_enabled'][ $i ],
					'arrow_align' => $_POST['_anemos_eutf_slider_item_arrow_align'][ $i ],
					'arrow_color' => $_POST['_anemos_eutf_slider_item_arrow_color'][ $i ],
					'arrow_color_custom' => $_POST['_anemos_eutf_slider_item_arrow_color_custom'][ $i ],
					'el_class' => $_POST['_anemos_eutf_slider_item_el_class'][ $i ],
				);

				$slider_items[] = $slide;
			}

		}



		if( !empty( $slider_items ) ) {
			$feature_section['slider_items'] = $slider_items;

			$feature_section['slider_settings'] = array (
				'slideshow_speed' => $_POST['_anemos_eutf_page_slider_settings_speed'],
				'direction_nav' => $_POST['_anemos_eutf_page_slider_settings_direction_nav'],
				'slider_pause' => $_POST['_anemos_eutf_page_slider_settings_pause'],
				'transition' => $_POST['_anemos_eutf_page_slider_settings_transition'],
				'slider_effect' => $_POST['_anemos_eutf_page_slider_settings_effect'],
			);
		}

		//Feature Map Items
		$map_items = array();
		if ( isset( $_POST['_anemos_eutf_map_item_point_id'] ) ) {

			$num_of_map_points = sizeof( $_POST['_anemos_eutf_map_item_point_id'] );
			for ( $i=0; $i < $num_of_map_points; $i++ ) {

				$this_point = array (
					'id' => $_POST['_anemos_eutf_map_item_point_id'][ $i ],
					'lat' => $_POST['_anemos_eutf_map_item_point_lat'][ $i ],
					'lng' => $_POST['_anemos_eutf_map_item_point_lng'][ $i ],
					'marker' => $_POST['_anemos_eutf_map_item_point_marker'][ $i ],
					'title' => $_POST['_anemos_eutf_map_item_point_title'][ $i ],
					'info_text' => $_POST['_anemos_eutf_map_item_point_infotext'][ $i ],
					'info_text_open' => $_POST['_anemos_eutf_map_item_point_infotext_open'][ $i ],
					'button_text' => $_POST['_anemos_eutf_map_item_point_button_text'][ $i ],
					'button_url' => $_POST['_anemos_eutf_map_item_point_button_url'][ $i ],
					'button_target' => $_POST['_anemos_eutf_map_item_point_button_target'][ $i ],
					'button_class' => $_POST['_anemos_eutf_map_item_point_button_class'][ $i ],
				);
				$map_items[] =  $this_point;
			}

		}

		if( !empty( $map_items ) ) {

			$feature_section['map_items'] = $map_items;
			$feature_section['map_settings'] = array (
				'zoom' => $_POST['_anemos_eutf_page_feature_map_zoom'],
				'marker' => $_POST['_anemos_eutf_page_feature_map_marker'],
				'disable_style' => $_POST['_anemos_eutf_page_feature_map_disable_style'],
			);

		}

	}

	//Save Feature Section

	$new_meta_value = $feature_section;
	$meta_key = '_anemos_eutf_feature_section';
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	if ( $new_meta_value && '' == $meta_value ) {
		if ( !add_post_meta( $post_id, $meta_key, $new_meta_value, true ) ) {
			update_post_meta( $post_id, $meta_key, $new_meta_value );
		}
	} elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
		update_post_meta( $post_id, $meta_key, $new_meta_value );
	} elseif ( '' == $new_meta_value && $meta_value ) {
		delete_post_meta( $post_id, $meta_key, $meta_value );
	}

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
