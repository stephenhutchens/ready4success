<?php
/*
*	Euthemians Category Meta
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

	//Categories
	add_action('category_edit_form_fields','anemos_eutf_category_edit_form_fields', 10);
	add_action('post_tag_edit_form_fields','anemos_eutf_category_edit_form_fields', 10);
	add_action('product_cat_edit_form_fields','anemos_eutf_category_edit_form_fields', 10);
	add_action('product_tag_edit_form_fields','anemos_eutf_category_edit_form_fields', 10);
	add_action('edit_term','anemos_eutf_save_category_fields', 10, 3);

	function anemos_eutf_category_edit_form_fields( $term ) {
		$anemos_eutf_term_meta = anemos_eutf_get_term_meta( $term->term_id, '_anemos_eutf_custom_title_options' );
		anemos_eutf_print_category_fields( $anemos_eutf_term_meta );
	}

	function anemos_eutf_print_category_fields( $anemos_eutf_custom_title_options = array() ) {
?>
		<tr class="form-field">
			<td colspan="2">
				<div id="eut-category-title" class="postbox">
<?php

			//Custom Title Option
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => 'anemos_eutf_term_meta[custom]',
					'id' => 'eut-category-title-custom',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'custom' ),
					'options' => array(
						'' => esc_html__( '-- Inherit --', 'anemos' ),
						'custom' => esc_html__( 'Custom', 'anemos' ),

					),
					'label' => array(
						"title" => esc_html__( 'Title Options', 'anemos' ),
					),
					'group_id' => 'eut-category-title',
					'highlight' => 'highlight',
				)
			);

			global $anemos_eutf_area_height;
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select',
					'options' => $anemos_eutf_area_height,
					'name' => 'anemos_eutf_term_meta[height]',
					'id' => 'eut-category-title-height',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'height', '40' ),
					'label' => array(
						"title" => esc_html__( 'Title Area Height in %', 'anemos' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);
			anemos_eutf_print_admin_option(
				array(
					'type' => 'textfield',
					'name' => 'anemos_eutf_term_meta[min_height]',
					'id' => 'eut-category-title-min-height',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'min_height', '200' ),
					'label' => array(
						"title" => esc_html__( 'Title Area Minimum Height in px', 'anemos' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);

			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'anemos_eutf_term_meta[bg_color]',
					'id' => 'eut-category-title-bg-color',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_color', 'dark' ),
					'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_color_custom', '#000000' ),
					'label' => array(
						"title" => esc_html__( 'Background Color', 'anemos' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
					'multiple' => 'multi',
				)
			);
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'anemos_eutf_term_meta[content_bg_color]',
					'id' => 'eut-category-title-content-bg-color',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'content_bg_color', 'light' ),
					'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'content_bg_color_custom', '#ffffff' ),
					'label' => array(
						"title" => esc_html__( 'Content Background Color', 'anemos' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
					'multiple' => 'multi',
					'type_usage' => 'title-content-bg',
				)
			);
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'anemos_eutf_term_meta[title_color]',
					'id' => 'eut-category-title-title-color',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'title_color', 'dark' ),
					'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'title_color_custom', '#000000' ),
					'label' => array(
						"title" => esc_html__( 'Title Color', 'anemos' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
					'multiple' => 'multi',
				)
			);

			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'anemos_eutf_term_meta[caption_color]',
					'id' => 'eut-category-title-caption_color',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'caption_color', 'dark' ),
					'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'caption_color_custom', '#000000' ),
					'label' => array(
						"title" => esc_html__( 'Description Color', 'anemos' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
					'multiple' => 'multi',
				)
			);

			global $anemos_eutf_media_bg_position_selection;
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => 'anemos_eutf_term_meta[content_position]',
					'id' => 'eut-category-title-content_position',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'content_position', 'center-center' ),
					'options' => $anemos_eutf_media_bg_position_selection,
					'label' => array(
						"title" => esc_html__( 'Content Position', 'anemos' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);

			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-text-animation',
					'name' => 'anemos_eutf_term_meta[content_animation]',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'content_animation', 'fade-in' ),
					'label' => esc_html__( 'Content Animation', 'anemos' ),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }
					]',
				)
			);


			anemos_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => 'anemos_eutf_term_meta[bg_mode]',
					'id' => 'eut-category-title-bg-mode',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_mode'),
					'options' => array(
						'' => esc_html__( 'Color Only', 'anemos' ),
						'custom' => esc_html__( 'Custom Image', 'anemos' ),

					),
					'label' => array(
						"title" => esc_html__( 'Background', 'anemos' ),
					),
					'group_id' => 'eut-category-title',
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] }

					]',
				)
			);
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-image',
					'name' => 'anemos_eutf_term_meta[bg_image_id]',
					'id' => 'eut-category-title-bg-image-id',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_image_id'),
					'label' => array(
						"title" => esc_html__( 'Background Image', 'anemos' ),
					),
					'width' => 'fullwidth',
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }

					]',
				)
			);
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-bg-position',
					'name' => 'anemos_eutf_term_meta[bg_position]',
					'id' => 'eut-category-title-bg-position',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'bg_position', 'center-center'),
					'label' => array(
						"title" => esc_html__( 'Background Position', 'anemos' ),
					),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }
					]',
				)
			);

			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-pattern-overlay',
					'name' => 'anemos_eutf_term_meta[pattern_overlay]',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'pattern_overlay'),
					'label' => esc_html__( 'Pattern Overlay', 'anemos' ),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }
					]',
				)
			);
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-colorpicker',
					'name' => 'anemos_eutf_term_meta[color_overlay]',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'color_overlay', 'dark' ),
					'value2' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'color_overlay_custom', '#000000' ),
					'label' => esc_html__( 'Color Overlay', 'anemos' ),
					'multiple' => 'multi',
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }
					]',
				)
			);
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select-opacity',
					'name' => 'anemos_eutf_term_meta[opacity_overlay]',
					'value' => anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'opacity_overlay', '0' ),
					'label' => esc_html__( 'Opacity Overlay', 'anemos' ),
					'dependency' =>
					'[
						{ "id" : "eut-category-title-custom", "values" : ["custom"] },
						{ "id" : "eut-category-title-bg-mode", "values" : ["custom"] }
					]',
				)
			);
?>
			</div>
		</td>
	</tr>
<?php
	}

	//Save Category Meta
	function anemos_eutf_save_category_fields( $term_id, $tt_id = '', $taxonomy = '' ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$custom_meta_tax = array ( 'category', 'post_tag', 'product_cat', 'product_tag' );

		if ( isset( $_POST['anemos_eutf_term_meta'] ) && in_array( $taxonomy, $custom_meta_tax ) ) {
			$anemos_eutf_term_meta = anemos_eutf_get_term_meta( $term_id, '_anemos_eutf_custom_title_options' );
			$cat_keys = array_keys( $_POST['anemos_eutf_term_meta'] );
			foreach ( $cat_keys as $key ) {
				if ( isset( $_POST['anemos_eutf_term_meta'][$key] ) ) {
					$anemos_eutf_term_meta[$key] = $_POST['anemos_eutf_term_meta'][$key];
				}
			}
			anemos_eutf_update_term_meta( $term_id , '_anemos_eutf_custom_title_options', $anemos_eutf_term_meta );
		}
	}

//Omit closing PHP tag to avoid accidental whitespace output errors.
