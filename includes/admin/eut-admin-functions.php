<?php

/*
*	Admin functions and definitions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

/**
 * Enqueue scripts and styles for the back end.
 */
function anemos_eutf_backend_scripts( $hook ) {
	global $post, $pagenow;

	wp_register_style( 'anemos-eutf-page-feature-section', get_template_directory_uri() . '/includes/css/eut-page-feature-section.css', array(), '1.3.0' );
	wp_register_style( 'anemos-eutf-admin-meta', get_template_directory_uri() . '/includes/css/eut-admin-meta.css', array(), '1.0' );
	wp_register_style( 'anemos-eutf-custom-sidebars', get_template_directory_uri() . '/includes/css/eut-custom-sidebars.css', array(), '1.0'  );
	wp_register_style( 'anemos-eutf-custom-nav-menu', get_template_directory_uri() . '/includes/css/eut-custom-nav-menu.css', array(), '1.0'  );
	wp_register_style( 'select2-css', get_template_directory_uri() . '/includes/admin/extensions/vendor_support/vendor/select2/select2.css', array(), time() );

	$anemos_eutf_upload_slider_texts = array(
		'modal_title' => esc_html__( 'Insert Images', 'anemos' ),
		'modal_button_title' => esc_html__( 'Insert Images', 'anemos' ),
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
	);

	$anemos_eutf_upload_image_replace_texts = array(
		'modal_title' => esc_html__( 'Replace Image', 'anemos' ),
		'modal_button_title' => esc_html__( 'Replace Image', 'anemos' ),
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
	);

	$anemos_eutf_upload_media_texts = array(
		'modal_title' => esc_html__( 'Insert Media', 'anemos' ),
		'modal_button_title' => esc_html__( 'Insert Media', 'anemos' ),
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
	);

	$anemos_eutf_upload_image_texts = array(
		'modal_title' => esc_html__( 'Insert Image', 'anemos' ),
		'modal_button_title' => esc_html__( 'Insert Image', 'anemos' ),
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
	);

	$anemos_eutf_feature_section_texts = array(
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
	);

	$anemos_eutf_custom_sidebar_texts = array(
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
	);

	wp_register_script( 'anemos-eutf-custom-sidebars', get_template_directory_uri() . '/includes/js/eut-custom-sidebars.js', array( 'jquery'), false, '1.0.0' );
	wp_localize_script( 'anemos-eutf-custom-sidebars', 'anemos_eutf_custom_sidebar_texts', $anemos_eutf_custom_sidebar_texts );

	wp_register_script( 'anemos-eutf-upload-slider-script', get_template_directory_uri() . '/includes/js/eut-upload-slider.js', array( 'jquery'), false, '1.0.0' );
	wp_localize_script( 'anemos-eutf-upload-slider-script', 'anemos_eutf_upload_slider_texts', $anemos_eutf_upload_slider_texts );

	wp_register_script( 'anemos-eutf-upload-feature-slider-script', get_template_directory_uri() . '/includes/js/eut-upload-feature-slider.js', array( 'jquery'), false, '1.0.0' );
	wp_localize_script( 'anemos-eutf-upload-feature-slider-script', 'anemos_eutf_upload_feature_slider_texts', $anemos_eutf_upload_slider_texts );

	wp_register_script( 'anemos-eutf-upload-image-replace-script', get_template_directory_uri() . '/includes/js/eut-upload-image-replace.js', array( 'jquery'), false, '1.0.0' );
	wp_localize_script( 'anemos-eutf-upload-image-replace-script', 'anemos_eutf_upload_image_replace_texts', $anemos_eutf_upload_image_replace_texts );

	wp_register_script( 'anemos-eutf-upload-simple-media-script', get_template_directory_uri() . '/includes/js/eut-upload-simple.js', array( 'jquery'), false, '1.0.0' );
	wp_localize_script( 'anemos-eutf-upload-simple-media-script', 'anemos_eutf_upload_media_texts', $anemos_eutf_upload_media_texts );

	wp_register_script( 'anemos-eutf-upload-image-script', get_template_directory_uri() . '/includes/js/eut-upload-image.js', array( 'jquery'), false, '1.0.0' );
	wp_localize_script( 'anemos-eutf-upload-image-script', 'anemos_eutf_upload_image_texts', $anemos_eutf_upload_image_texts );

	wp_register_script( 'anemos-eutf-page-feature-section-script', get_template_directory_uri() . '/includes/js/eut-page-feature-section.js', array( 'jquery', 'wp-color-picker' ), false, '1.0.0' );
	wp_localize_script( 'anemos-eutf-page-feature-section-script', 'anemos_eutf_feature_section_texts', $anemos_eutf_feature_section_texts );

	wp_register_script( 'anemos-eutf-post-options-script', get_template_directory_uri() . '/includes/js/eut-post-options.js', array( 'jquery'), false, '1.0.0' );
	wp_register_script( 'anemos-eutf-portfolio-options-script', get_template_directory_uri() . '/includes/js/eut-portfolio-options.js', array( 'jquery'), false, '1.0.0' );

	wp_register_script( 'anemos-eutf-custom-nav-menu-script', get_template_directory_uri().'/includes/js/eut-custom-nav-menu.js', array( 'jquery'), false, '1.0.0' );

	wp_register_script( 'select2-js', get_template_directory_uri().'/includes/admin/extensions/vendor_support/vendor/select2/select2.js', array( 'jquery'), false, time() );

	if ( $hook == 'post-new.php' || $hook == 'post.php' ) {


		$feature_section_post_types = anemos_eutf_option( 'feature_section_post_types' );

		if ( !empty( $feature_section_post_types ) && in_array( $post->post_type, $feature_section_post_types ) && 'attachment' != $post->post_type ) {

			wp_enqueue_style( 'anemos-eutf-admin-meta' );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'anemos-eutf-page-feature-section' );
			wp_enqueue_style( 'select2-css' );

			wp_enqueue_script( 'anemos-eutf-upload-simple-media-script' );
			wp_enqueue_script( 'anemos-eutf-upload-image-script' );
			wp_enqueue_script( 'anemos-eutf-upload-slider-script' );
			wp_enqueue_script( 'anemos-eutf-upload-feature-slider-script' );
			wp_enqueue_script( 'anemos-eutf-upload-image-replace-script' );
			wp_enqueue_script( 'select2-js' );
			wp_enqueue_script( 'anemos-eutf-page-feature-section-script' );
		}


        if ( 'post' === $post->post_type ) {

			wp_enqueue_style( 'anemos-eutf-admin-meta' );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'anemos-eutf-page-feature-section' );
			wp_enqueue_style( 'select2-css' );

			wp_enqueue_script( 'anemos-eutf-upload-simple-media-script' );
			wp_enqueue_script( 'anemos-eutf-upload-image-script' );
			wp_enqueue_script( 'anemos-eutf-upload-slider-script' );
			wp_enqueue_script( 'anemos-eutf-upload-feature-slider-script' );
			wp_enqueue_script( 'anemos-eutf-upload-image-replace-script' );
			wp_enqueue_script( 'anemos-eutf-page-feature-section-script' );
			wp_enqueue_script( 'select2-js' );
			wp_enqueue_script( 'anemos-eutf-post-options-script' );

        } else if ( 'page' === $post->post_type || 'portfolio' === $post->post_type ) {

			wp_enqueue_style( 'anemos-eutf-admin-meta' );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'anemos-eutf-page-feature-section' );
			wp_enqueue_style( 'select2-css' );

			wp_enqueue_script( 'anemos-eutf-upload-simple-media-script' );
			wp_enqueue_script( 'anemos-eutf-upload-image-script' );
			wp_enqueue_script( 'anemos-eutf-upload-slider-script' );
			wp_enqueue_script( 'anemos-eutf-upload-feature-slider-script' );
			wp_enqueue_script( 'anemos-eutf-upload-image-replace-script' );
			wp_enqueue_script( 'select2-js' );
			wp_enqueue_script( 'anemos-eutf-page-feature-section-script' );

			wp_enqueue_script( 'anemos-eutf-portfolio-options-script' );

        } else if ( 'testimonial' === $post->post_type ) {

			wp_enqueue_style( 'anemos-eutf-admin-meta' );

        }
    }

	if ( $hook == 'edit-tags.php' || $hook == 'term.php') {
		wp_enqueue_style( 'anemos-eutf-admin-meta' );
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_style( 'anemos-eutf-page-feature-section' );


		wp_enqueue_media();
		wp_enqueue_script( 'anemos-eutf-page-feature-section-script' );
		wp_enqueue_script( 'anemos-eutf-upload-image-script' );
		wp_enqueue_script( 'anemos-eutf-upload-image-replace-script' );

	}

	if ( $hook == 'nav-menus.php' ) {
		wp_enqueue_style( 'anemos-eutf-custom-nav-menu' );

		wp_enqueue_media();
		wp_enqueue_script( 'anemos-eutf-upload-simple-media-script' );
		wp_enqueue_script( 'anemos-eutf-custom-nav-menu-script' );
	}

	if ( isset( $_GET['page'] ) && ( 'anemos-eutf-custom-sidebar-settings' == $_GET['page'] ) ) {

		wp_enqueue_style( 'anemos-eutf-custom-sidebars' );
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'anemos-eutf-custom-sidebars' );
	}

	wp_register_style(
		'redux-custom-css',
		get_template_directory_uri() . '/includes/css/eut-redux-panel.css',
		array(),
		time(),
		'all'
	);
	wp_enqueue_style( 'redux-custom-css' );



}
add_action( 'admin_enqueue_scripts', 'anemos_eutf_backend_scripts', 10, 1 );

/**
 * Helper function to get custom fields with fallback
 */
function anemos_eutf_post_meta( $id, $fallback = false ) {
	global $post;
	$post_id = $post->ID;
	if ( $fallback == false ) $fallback = '';
	$post_meta = get_post_meta( $post_id, $id, true );
	$output = ( $post_meta !== '' ) ? $post_meta : $fallback;
	return $output;
}

function anemos_eutf_admin_post_meta( $post_id, $id, $fallback = false ) {
	if ( $fallback == false ) $fallback = '';
	$post_meta = get_post_meta( $post_id, $id, true );
	$output = ( $post_meta !== '' ) ? $post_meta : $fallback;
	return $output;
}

function anemos_eutf_get_term_meta( $term_id, $meta_key ) {
	$anemos_eutf_term_meta  = '';

	if ( function_exists( 'get_term_meta' ) ) {
		$anemos_eutf_term_meta = get_term_meta( $term_id, $meta_key, true );
	}
	if( empty ( $anemos_eutf_term_meta ) ) {
		$anemos_eutf_term_meta = array();
	}
	return $anemos_eutf_term_meta;
}

function anemos_eutf_update_term_meta( $term_id , $meta_key, $meta_value ) {

	if ( function_exists( 'update_term_meta' ) ) {
		update_term_meta( $term_id, $meta_key, $meta_value );
	}

}

/**
 * Helper function to get theme options with fallback
 */
function anemos_eutf_option( $id, $fallback = false, $param = false ) {
	global $anemos_eutf_options;
	$eut_theme_options = $anemos_eutf_options;

	if ( $fallback == false ) $fallback = '';
	$output = ( isset($eut_theme_options[$id]) && $eut_theme_options[$id] !== '' ) ? $eut_theme_options[$id] : $fallback;
	if ( !empty($eut_theme_options[$id]) && $param ) {
		$output = ( isset($eut_theme_options[$id][$param]) && $eut_theme_options[$id][$param] !== '' ) ? $eut_theme_options[$id][$param] : $fallback;
		if ( 'font-family' == $param ) {
			$output = urldecode( $output );
			if ( strpos($output, ' ') && !strpos($output, ',') ) {
				$output = '"' . $output . '"';
			}
		}
	}
	return $output;
}

/**
 * Helper function to print css code if not empty
 */
function anemos_eutf_css_option( $id, $fallback = false, $param = false ) {
	$option = anemos_eutf_option( $id, $fallback, $param );
	if ( !empty( $option ) && 0 !== $option && $param ) {
		return $param . ': ' . $option . ';';
	}
}

/**
 * Helper function to get array value with fallback
 */
function anemos_eutf_array_value( $input_array, $id, $fallback = false, $param = false ) {

	if ( $fallback == false ) $fallback = '';
	$output = ( isset($input_array[$id]) && $input_array[$id] !== '' ) ? $input_array[$id] : $fallback;
	if ( !empty($input_array[$id]) && $param ) {
		$output = ( isset($input_array[$id][$param]) && $input_array[$id][$param] !== '' ) ? $input_array[$id][$param] : $fallback;
	}
	return $output;
}

/**
 * Helper function to return trimmed css code
 */
function anemos_eutf_get_css_output( $css ) {
	/* Trim css for speed */
	$css_trim =  preg_replace( '/\s+/', ' ', $css );

	/* Add stylesheet Tag */
	return "<!-- Dynamic css -->\n<style type=\"text/css\">\n" . $css_trim . "\n</style>";
}

/**
 * Helper functions to set/get current template
 */
function anemos_eutf_set_current_view( $id ) {
	global $anemos_eutf_options;
	$anemos_eutf_options['current_view'] = $id;
}
function anemos_eutf_get_current_view( $fallback = '' ) {
	global $anemos_eutf_options;
	$anemos_eutf_theme_options = $anemos_eutf_options;

	if ( $fallback == false ) $fallback = '';
	$output = ( isset($anemos_eutf_theme_options['current_view']) && $anemos_eutf_theme_options['current_view'] !== '' ) ? $anemos_eutf_theme_options['current_view'] : $fallback;
	return $output;
}

/**
 * Helper function convert hex to rgb
 */
function anemos_eutf_hex2rgb( $hex ) {
	$hex = str_replace("#", "", $hex);

	if(strlen($hex) == 3) {
		$r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1) );
		$g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1) );
		$b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1) );
	} else {
		$r = hexdec( substr( $hex, 0, 2) );
		$g = hexdec( substr( $hex, 2, 2) );
		$b = hexdec( substr( $hex, 4, 2) );
	}
	$rgb = array($r, $g, $b);
	return implode(",", $rgb);
}

/**
 * Helper function to get theme visibility options
 */
function anemos_eutf_visibility( $id, $fallback = '' ) {
	$visibility = anemos_eutf_option( $id, $fallback  );
	if ( '1' == $visibility ) {
		return true;
	}
	return false;
}

/**
 * Get Color
 */
function anemos_eutf_get_color( $color = 'dark', $color_custom = '#000000' ) {

	switch( $color ) {

		case 'dark':
			$color_custom = '#000000';
			break;
		case 'light':
			$color_custom = '#ffffff';
			break;
		case 'primary-1':
			$color_custom = anemos_eutf_option( 'body_primary_1_color' );
			break;
		case 'primary-2':
			$color_custom = anemos_eutf_option( 'body_primary_2_color' );
			break;
		case 'primary-3':
			$color_custom = anemos_eutf_option( 'body_primary_3_color' );
			break;
		case 'primary-4':
			$color_custom = anemos_eutf_option( 'body_primary_4_color' );
			break;
		case 'primary-5':
			$color_custom = anemos_eutf_option( 'body_primary_5_color' );
			break;
	}

	return $color_custom;
}

/**
 * Backend Theme Activation Actions
 */
function anemos_eutf_backend_theme_activation() {
	global $pagenow;

	if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) {
		//Redirect to Theme Options
		header( 'Location: ' . esc_url( admin_url() ) . 'admin.php?page=anemos_eutf_options&tab=0' ) ;
	}
}

add_action('admin_init','anemos_eutf_backend_theme_activation');

/**
 * Check if Revolution slider is active
 */

/**
 * Check if to replace Backend Logo
 */
function anemos_eutf_admin_login_logo() {

	$replace_logo = anemos_eutf_option( 'replace_admin_logo' );
	if ( $replace_logo ) {
		$admin_logo = anemos_eutf_option( 'admin_logo','','url' );
		$admin_logo_height = anemos_eutf_option( 'admin_logo_height','84');
		$admin_logo_height = preg_match('/(px|em|\%|pt|cm)$/', $admin_logo_height) ? $admin_logo_height : $admin_logo_height . 'px';
		if( empty( $admin_logo ) ) {
			$admin_logo = anemos_eutf_option( 'logo','','url' );
		}
		if ( !empty( $admin_logo ) ) {
			$admin_logo = str_replace( array( 'http:', 'https:' ), '', $admin_logo );
			echo "
			<style>
			.login h1 a {
				background-image: url('" . esc_url( $admin_logo ) . "');
				width: 100%;
				max-width: 300px;
				background-size: auto " . esc_attr( $admin_logo_height ) . ";
				height: " . esc_attr( $admin_logo_height ) . ";
			}
			</style>
			";
		}
	}
}
add_action( 'login_head', 'anemos_eutf_admin_login_logo' );

function anemos_eutf_login_headerurl( $url ){
	$replace_logo = anemos_eutf_option( 'replace_admin_logo' );
	if ( $replace_logo ) {
		return esc_url( home_url( '/' ) );
	}
	return esc_url( $url );
}
add_filter('login_headerurl', 'anemos_eutf_login_headerurl');

function anemos_eutf_login_headertitle( $title ) {
	$replace_logo = anemos_eutf_option( 'replace_admin_logo' );
	if ( $replace_logo ) {
		return esc_attr( get_bloginfo( 'name' ) );
	}
	return esc_attr( $title );
}
add_filter('login_headertitle', 'anemos_eutf_login_headertitle' );

/**
 * Disable SEO Page Analysis
 */
function anemos_eutf_disable_page_analysis( $bool ) {
	if( '1' == anemos_eutf_option( 'disable_seo_page_analysis', '0' ) ) {
		return false;
	}
	return $bool;
}
add_filter( 'wpseo_use_page_analysis', 'anemos_eutf_disable_page_analysis' );

/**
 * Browser Webkit Check
 */
function anemos_eutf_browser_webkit_check() {

	if ( empty($_SERVER['HTTP_USER_AGENT'] ) ) {
		return false;
	}

	$u_agent = $_SERVER['HTTP_USER_AGENT'];

	if (
		( preg_match( '!linux!i', $u_agent ) || preg_match( '!windows|win32!i', $u_agent ) ) && preg_match( '!webkit!i', $u_agent )
	) {
		return true;
	}

	return false;
}

/**
 * Add Hooks for Page Redirect ( Coming Soon )
 */
add_filter( 'template_include', 'anemos_eutf_redirect_page_template', 99 );

if ( ! function_exists( 'anemos_eutf_redirect_page_template' ) ) {
	function anemos_eutf_redirect_page_template( $template ) {
		if ( anemos_eutf_visibility('coming_soon_enabled' )  && !is_user_logged_in() ) {
			$redirect_page = anemos_eutf_option( 'coming_soon_page' );
			$redirect_template = anemos_eutf_option( 'coming_soon_template' );
			if ( !empty( $redirect_page ) && 'content' == $redirect_template ) {
				$new_template = locate_template( array( 'page-templates/template-content-only.php' ) );
				if ( '' != $new_template ) {
					return $new_template ;
				}
			}
		}
		return $template;
	}
}

add_filter( 'template_redirect', 'anemos_eutf_redirect' );

if ( ! function_exists( 'anemos_eutf_redirect' ) ) {
	function anemos_eutf_redirect() {
		if ( anemos_eutf_visibility('coming_soon_enabled' ) && !is_user_logged_in() ) {
			$redirect_page = anemos_eutf_option( 'coming_soon_page' );
			$protocol = is_ssl() ? 'https://' : 'http://';

			if ( !empty( $redirect_page )
				&& !in_array( $GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php') )
				&& !is_admin()
				&& ( $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] != get_permalink( $redirect_page ) ) ) {

				wp_redirect( get_permalink( $redirect_page ) );
				exit();

			}
		}
		return false;
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
