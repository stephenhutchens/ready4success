<?php
/*
*	Euthemians Post Items
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

	add_action( 'add_meta_boxes', 'anemos_eutf_post_options_add_custom_boxes' );
	add_action( 'save_post', 'anemos_eutf_post_options_save_postdata', 10, 2 );

	$anemos_eutf_post_options = array (

		//Standard Format

		array(
			'name' => 'Standard Style',
			'id' => '_anemos_eutf_post_standard_style',
		),
		array(
			'name' => 'Standard Background Color',
			'id' => '_anemos_eutf_post_standard_bg_color',
		),
		array(
			'name' => 'Standard Background Opacity',
			'id' => '_anemos_eutf_post_standard_bg_opacity',
		),
		//Gallery Format
		array(
			'name' => 'Media Mode',
			'id' => '_anemos_eutf_post_type_gallery_mode',
		),
		array(
			'name' => 'Media Image Mode',
			'id' => '_anemos_eutf_post_type_gallery_image_mode',
		),
		array(
			'name' => 'Image Popup Title Caption',
			'id' => '_anemos_eutf_post_type_gallery_image_popup_title_caption',
		),
		//Link Format
		array(
			'name' => 'Link URL',
			'id' => '_anemos_eutf_post_link_url',
		),
		array(
			'name' => 'Open Link in a new window',
			'id' => '_anemos_eutf_post_link_new_window',
		),
		array(
			'name' => 'Link Background Color',
			'id' => '_anemos_eutf_post_link_bg_color',
		),
		array(
			'name' => 'Link Background Hover Color',
			'id' => '_anemos_eutf_post_link_bg_hover_color',
		),
		array(
			'name' => 'Link Background Opacity',
			'id' => '_anemos_eutf_post_link_bg_opacity',
		),
		//Quote Format
		array(
			'name' => 'Quote Text',
			'id' => '_anemos_eutf_post_quote_text',
		),
		array(
			'name' => 'Quote Name',
			'id' => '_anemos_eutf_post_quote_name',
		),
		array(
			'name' => 'Quote Background Color',
			'id' => '_anemos_eutf_post_quote_bg_color',
		),
		array(
			'name' => 'Quote Background Hover Color',
			'id' => '_anemos_eutf_post_quote_bg_hover_color',
		),
		array(
			'name' => 'Quote Background Opacity',
			'id' => '_anemos_eutf_post_quote_bg_opacity',
		),
		//Audio Format
		array(
			'name' => 'Audio mode',
			'id' => '_anemos_eutf_post_type_audio_mode',
		),
		array(
			'name' => 'Audio mp3 format',
			'id' => '_anemos_eutf_post_audio_mp3',
		),
		array(
			'name' => 'Audio ogg format',
			'id' => '_anemos_eutf_post_audio_ogg',
		),
		array(
			'name' => 'Audio wav format',
			'id' => '_anemos_eutf_post_audio_wav',
		),
		array(
			'name' => 'Audio embed',
			'id' => '_anemos_eutf_post_audio_embed',
		),
		//Video Format
		array(
			'name' => 'Video Style',
			'id' => '_anemos_eutf_post_video_style',
		),
		array(
			'name' => 'Video Background Color',
			'id' => '_anemos_eutf_post_video_bg_color',
		),
		array(
			'name' => 'Video Background Opacity',
			'id' => '_anemos_eutf_post_video_bg_opacity',
		),
		array(
			'name' => 'Video Mode',
			'id' => '_anemos_eutf_post_type_video_mode',
		),
		array(
			'name' => 'Video webm format',
			'id' => '_anemos_eutf_post_video_webm',
		),
		array(
			'name' => 'Video mp4 format',
			'id' => '_anemos_eutf_post_video_mp4',
		),
		array(
			'name' => 'Video ogv format',
			'id' => '_anemos_eutf_post_video_ogv',
		),
		array(
			'name' => 'Video Poster',
			'id' => '_anemos_eutf_post_video_poster',
		),
		array(
			'name' => 'Video embed Vimeo/Youtube',
			'id' => '_anemos_eutf_post_video_embed',
		),

	);

	function anemos_eutf_post_options_add_custom_boxes() {

		if ( function_exists( 'vc_is_inline' ) && vc_is_inline() ) {
			return;
		}

		add_meta_box(
			'eut-meta-box-post-format-standard',
			esc_html__( 'Standard Format Options', 'anemos' ),
			'anemos_eutf_meta_box_post_format_standard',
			'post'
		);
		add_meta_box(
			'eut-meta-box-post-format-gallery',
			esc_html__( 'Gallery Format Options', 'anemos' ),
			'anemos_eutf_meta_box_post_format_gallery',
			'post'
		);
		add_meta_box(
			'eut-meta-box-post-format-link',
			esc_html__( 'Link Format Options', 'anemos' ),
			'anemos_eutf_meta_box_post_format_link',
			'post'
		);
		add_meta_box(
			'eut-meta-box-post-format-quote',
			esc_html__( 'Quote Format Options', 'anemos' ),
			'anemos_eutf_meta_box_post_format_quote',
			'post'
		);
		add_meta_box(
			'eut-meta-box-post-format-video',
			esc_html__( 'Video Format Options', 'anemos' ),
			'anemos_eutf_meta_box_post_format_video',
			'post'
		);
		add_meta_box(
			'eut-meta-box-post-format-audio',
			esc_html__( 'Audio Format Options', 'anemos' ),
			'anemos_eutf_meta_box_post_format_audio',
			'post'
		);

	}

	function anemos_eutf_meta_box_post_format_standard( $post ) {

		global $anemos_eutf_post_color_selection, $anemos_eutf_post_bg_opacity_selection;
		$anemos_eutf_post_standard_style = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_standard_style' );
		$anemos_eutf_post_standard_bg_color = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_standard_bg_color', 'black' );
		$anemos_eutf_post_standard_bg_opacity = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_standard_bg_opacity', '70' );

	?>
		<table class="form-table eut-metabox">
			<tbody>
				<tr>
					<td colspan="2">
						<p class="howto"><?php esc_html_e( 'Select one of the choices below for the post overview.', 'anemos' ); ?></p>
					</td>
				</tr>
			</tbody>
		</table>
		<div id="eut-stadard-format-options">

	<?php
		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_anemos_eutf_post_standard_style',
				'id' => 'eut-standard-format-style',
				'options' => array(
					'' => esc_html__( 'Classic', 'anemos' ),
					'anemos' => esc_html__( 'Anemos', 'anemos' ),
				),
				'value' => $anemos_eutf_post_standard_style,
				'default_value' => '',
				'label' => array(
					'title' => esc_html__( 'Post Style', 'anemos' ),
					'desc' => esc_html__( 'Note: Anemos style affects only Grid/Masonry style.', 'anemos' ),
				),
				'group_id' => 'eut-stadard-format-options',
				'highlight' => 'highlight',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_anemos_eutf_post_standard_bg_color',
				'options' => $anemos_eutf_post_color_selection,
				'value' => $anemos_eutf_post_standard_bg_color,
				'label' => esc_html__( 'Background Color', 'anemos' ),
				'default_value' => 'black',
				'dependency' =>
				'[
					{ "id" : "eut-standard-format-style", "values" : ["anemos"] }
				]',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'options' => $anemos_eutf_post_bg_opacity_selection,
				'name' => '_anemos_eutf_post_standard_bg_opacity',
				'value' => $anemos_eutf_post_standard_bg_opacity,
				'label' => array(
					'title' => esc_html__( 'Background Opacity', 'anemos' ),
					'desc' => esc_html__( 'Note: only if Featured Image is available.', 'anemos' ),
				),
				'default_value' => '70',
				'dependency' =>
				'[
					{ "id" : "eut-standard-format-style", "values" : ["anemos"] }
				]',
			)
		);

	?>
		</div>
	<?php
	}

	function anemos_eutf_meta_box_post_format_gallery( $post ) {

		wp_nonce_field( 'anemos_eutf_nonce_post_save', '_anemos_eutf_nonce_post_save' );

		$gallery_mode = get_post_meta( $post->ID, '_anemos_eutf_post_type_gallery_mode', true );
		$gallery_image_mode = get_post_meta( $post->ID, '_anemos_eutf_post_type_gallery_image_mode', true );
		$gallery_image_popup_title_caption = get_post_meta( $post->ID, '_anemos_eutf_post_type_gallery_image_popup_title_caption', true );

		$media_slider_items = get_post_meta( $post->ID, '_anemos_eutf_post_slider_items', true );

		$media_slider_settings = get_post_meta( $post->ID, '_anemos_eutf_post_slider_settings', true );
		$media_slider_speed = anemos_eutf_array_value( $media_slider_settings, 'slideshow_speed', '3500' );
		$media_slider_dir_nav = anemos_eutf_array_value( $media_slider_settings, 'direction_nav', '1' );
		$media_slider_dir_nav_color = anemos_eutf_array_value( $media_slider_settings, 'direction_nav_color', 'dark' );

	?>
			<table class="form-table eut-metabox">
				<tbody>
					<tr class="eut-border-bottom">
						<th>
							<label for="eut-post-gallery-mode">
								<strong><?php esc_html_e( 'Gallery Mode', 'anemos' ); ?></strong>
								<span>
									<?php esc_html_e( 'Select Gallery mode.', 'anemos' ); ?>
								</span>
							</label>
						</th>
						<td>
							<select id="eut-post-gallery-mode" name="_anemos_eutf_post_type_gallery_mode">
								<option value="" <?php selected( '', $gallery_mode ); ?>><?php esc_html_e( 'Gallery', 'anemos' ); ?></option>
								<option value="slider" <?php selected( 'slider', $gallery_mode ); ?>><?php esc_html_e( 'Slider', 'anemos' ); ?></option>
							</select>
						</td>
					</tr>
					<tr id="eut-post-gallery-image-mode-section" class="eut-post-media-item" <?php if ( "" == $gallery_mode ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-post-gallery-image-mode">
								<strong><?php esc_html_e( 'Image Mode', 'anemos' ); ?></strong>
								<span>
									<?php esc_html_e( 'Select image mode.', 'anemos' ); ?>
								</span>
							</label>
						</th>
						<td>
							<select id="eut-post-gallery-image-mode" name="_anemos_eutf_post_type_gallery_image_mode">
								<option value="" <?php selected( '', $gallery_image_mode ); ?>><?php esc_html_e( 'Auto Crop', 'anemos' ); ?></option>
								<option value="resize" <?php selected( 'resize', $gallery_image_mode ); ?>><?php esc_html_e( 'Resize', 'anemos' ); ?></option>
							</select>
						</td>
					</tr>
					<tr id="eut-post-gallery-image-popup-title-caption" class="eut-post-gallery-item" <?php if ( "" == $gallery_mode ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-post-gallery-image-mode">
								<strong><?php esc_html_e( 'Image Popup Title & Caption Visibility', 'anemos' ); ?></strong>
								<span>
									<?php esc_html_e( 'Define the visibility for your popup image title - caption.', 'anemos' ); ?>
								</span>
							</label>
						</th>
						<td>
							<select id="eut-post-gallery-image-popup-title-caption" name="_anemos_eutf_post_type_gallery_image_popup_title_caption">
								<option value="" <?php selected( '', $gallery_image_popup_title_caption ); ?>><?php esc_html_e( 'None', 'anemos' ); ?></option>
								<option value="title-caption" <?php selected( 'title-caption', $gallery_image_popup_title_caption ); ?>><?php esc_html_e( 'Title and Caption', 'anemos' ); ?></option>
								<option value="title-only" <?php selected( 'title-only', $gallery_image_popup_title_caption ); ?>><?php esc_html_e( 'Title Only', 'anemos' ); ?></option>
								<option value="caption-only" <?php selected( 'caption-only', $gallery_image_popup_title_caption ); ?>><?php esc_html_e( 'Caption Only', 'anemos' ); ?></option>
							</select>
						</td>
					</tr>


					<tr id="eut-post-media-slider-speed" class="eut-post-media-item" <?php if ( "" == $gallery_mode ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-post-slider-speed">
								<strong><?php esc_html_e( 'Slideshow Speed', 'anemos' ); ?></strong>
							</label>
						</th>
						<td>
							<input type="text" id="eut-post-slider-speed" name="_anemos_eutf_post_slider_settings_speed" value="<?php echo esc_attr( $media_slider_speed ); ?>" /> ms
						</td>
					</tr>
					<tr id="eut-post-media-slider-direction-nav" class="eut-post-media-item" <?php if ( "" == $gallery_mode ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-post-slider-direction-nav">
								<strong><?php esc_html_e( 'Navigation Buttons', 'anemos' ); ?></strong>
							</label>
						</th>
						<td>
							<select id="eut-post-slider-direction-nav" name="_anemos_eutf_post_slider_settings_direction_nav">
								<option value="1" <?php selected( "1", $media_slider_dir_nav ); ?>><?php esc_html_e( 'Style 1', 'anemos' ); ?></option>
								<option value="2" <?php selected( "2", $media_slider_dir_nav ); ?>><?php esc_html_e( 'Style 2', 'anemos' ); ?></option>
								<option value="3" <?php selected( "3", $media_slider_dir_nav ); ?>><?php esc_html_e( 'Style 3', 'anemos' ); ?></option>
								<option value="4" <?php selected( "4", $media_slider_dir_nav ); ?>><?php esc_html_e( 'Style 4', 'anemos' ); ?></option>
								<option value="0" <?php selected( "0", $media_slider_dir_nav ); ?>><?php esc_html_e( 'No Navigation', 'anemos' ); ?></option>
							</select>
						</td>
					</tr>
					<tr id="eut-post-media-slider-direction-nav-color" class="eut-post-media-item" <?php if ( "" == $gallery_mode ) { ?> style="display:none;" <?php } ?>>
						<th>
							<label for="eut-post-slider-direction-nav-color">
								<strong><?php esc_html_e( 'Navigation Buttons Color', 'anemos' ); ?></strong>
							</label>
						</th>
						<td>
							<select id="eut-post-slider-direction-nav-color" name="_anemos_eutf_post_slider_settings_direction_nav_color">
								<option value="dark" <?php selected( "dark", $media_slider_dir_nav_color ); ?>><?php esc_html_e( 'Dark', 'anemos' ); ?></option>
								<option value="light" <?php selected( "light", $media_slider_dir_nav_color ); ?>><?php esc_html_e( 'Light', 'anemos' ); ?></option>
							</select>
						</td>
					</tr>
					<tr>
						<th>
							<label><?php esc_html_e( 'Images', 'anemos' ); ?></label>
						</th>
						<td>
							<input type="button" class="eut-upload-slider-button button-primary" value="<?php esc_attr_e( 'Insert Images to Gallery/Slider', 'anemos' ); ?>"/>
							<span id="eut-upload-slider-button-spinner" class="eut-action-spinner"></span>
						</td>
					</tr>
				</tbody>
			</table>
			<div id="eut-slider-container" class="eut-slider-container-minimal" data-mode="minimal">
				<?php
					if( !empty( $media_slider_items ) ) {
						anemos_eutf_print_admin_media_slider_items( $media_slider_items );
					}
				?>
			</div>
	<?php
	}


	function anemos_eutf_meta_box_post_format_link( $post ) {

		global $anemos_eutf_post_color_selection, $anemos_eutf_post_bg_opacity_selection;

		$link_url = get_post_meta( $post->ID, '_anemos_eutf_post_link_url', true );
		$new_window = get_post_meta( $post->ID, '_anemos_eutf_post_link_new_window', true );

		$anemos_eutf_post_link_bg_color = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_link_bg_color', 'primary-1' );
		$anemos_eutf_post_link_bg_hover_color = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_link_bg_hover_color', 'black' );
		$anemos_eutf_post_link_bg_opacity = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_link_bg_opacity', '70' );

	?>
		<table class="form-table eut-metabox">
			<tbody>
				<tr>
					<td colspan="2">
						<p class="howto"><?php esc_html_e( 'Add your text in the content area. The text will be wrapped with a link.', 'anemos' ); ?></p>
					</td>
				</tr>
			</tbody>
		</table>
<?php

			anemos_eutf_print_admin_option(
				array(
					'type' => 'textfield',
					'name' => '_anemos_eutf_post_link_url',
					'value' => $link_url ,
					'label' => array(
						'title' => esc_html__( 'Link URL', 'anemos' ),
						'desc' => esc_html__( 'Enter the full URL of your link.', 'anemos' ),
					),
					'width' => 'fullwidth',
				)
			);

			anemos_eutf_print_admin_option(
				array(
					'type' => 'checkbox',
					'name' => '_anemos_eutf_post_link_new_window',
					'value' => $new_window ,
					'label' => array(
						'title' => esc_html__( 'Open Link in new window', 'anemos' ),
						'desc' => esc_html__( 'If selected, link will open in a new window.', 'anemos' ),
					),
				)
			);

			anemos_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => '_anemos_eutf_post_link_bg_color',
					'options' => $anemos_eutf_post_color_selection,
					'value' => $anemos_eutf_post_link_bg_color,
					'label' => esc_html__( 'Background Color', 'anemos' ),
				)
			);
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select',
					'name' => '_anemos_eutf_post_link_bg_hover_color',
					'options' => $anemos_eutf_post_color_selection,
					'value' => $anemos_eutf_post_link_bg_hover_color,
					'label' => esc_html__( 'Background Hover Color', 'anemos' ),
				)
			);
			anemos_eutf_print_admin_option(
				array(
					'type' => 'select',
					'options' => $anemos_eutf_post_bg_opacity_selection,
					'name' => '_anemos_eutf_post_link_bg_opacity',
					'value' => $anemos_eutf_post_link_bg_opacity,
					'label' => array(
						'title' => esc_html__( 'Background Opacity', 'anemos' ),
						'desc' => esc_html__( 'Note: only if Featured Image is available.', 'anemos' ),
					),
					'default_value' => '70',
				)
			);

	}

	function anemos_eutf_meta_box_post_format_quote( $post ) {

		global $anemos_eutf_post_color_selection, $anemos_eutf_post_bg_opacity_selection;
		$anemos_eutf_post_quote_bg_color = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_quote_bg_color', 'primary-1' );
		$anemos_eutf_post_quote_bg_hover_color = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_quote_bg_hover_color', 'black' );
		$anemos_eutf_post_quote_bg_opacity = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_quote_bg_opacity', '70' );

		$anemos_eutf_post_quote_text = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_quote_text' );
		$anemos_eutf_post_quote_name = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_quote_name' );

	?>
		<table class="form-table eut-metabox">
			<tbody>
				<tr>
					<td colspan="2">
						<p class="howto"><?php esc_html_e( 'Simply add some text in the text area. This text will automatically displayed as quote.', 'anemos' ); ?></p>
					</td>
				</tr>
			</tbody>
		</table>

<?php

		anemos_eutf_print_admin_option(
			array(
				'type' => 'textarea',
				'name' => '_anemos_eutf_post_quote_text',
				'id' => '_anemos_eutf_post_quote_text',
				'value' => $anemos_eutf_post_quote_text,
				'label' => array(
					'title' => esc_html__( 'Quote Text', 'anemos' ),
					'desc' => esc_html__( 'Enter your quote text.', 'anemos' ),
				),
				'width' => 'fullwidth',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'textfield',
				'name' => '_anemos_eutf_post_quote_name',
				'id' => '_anemos_eutf_post_quote_name',
				'value' => $anemos_eutf_post_quote_name,
				'label' => array(
					'title' => esc_html__( 'Quote Name', 'anemos' ),
					'desc' => esc_html__( 'Enter your quote name.', 'anemos' ),
				),
				'width' => 'fullwidth',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_anemos_eutf_post_quote_bg_color',
				'options' => $anemos_eutf_post_color_selection,
				'value' => $anemos_eutf_post_quote_bg_color,
				'label' => esc_html__( 'Background color', 'anemos' ),
			)
		);
		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_anemos_eutf_post_quote_bg_hover_color',
				'options' => $anemos_eutf_post_color_selection,
				'value' => $anemos_eutf_post_quote_bg_hover_color,
				'label' => esc_html__( 'Background Hover color', 'anemos' ),
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'options' => $anemos_eutf_post_bg_opacity_selection,
				'name' => '_anemos_eutf_post_quote_bg_opacity',
				'value' => $anemos_eutf_post_quote_bg_opacity,
				'label' => array(
					'title' => esc_html__( 'Background Opacity', 'anemos' ),
					'desc' => esc_html__( 'Note: only if Featured Image is available.', 'anemos' ),
				),
				'default_value' => '70',
			)
		);

	}

	function anemos_eutf_meta_box_post_format_video( $post ) {

		global $anemos_eutf_post_color_selection, $anemos_eutf_post_bg_opacity_selection;
		$anemos_eutf_post_video_style = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_video_style' );
		$anemos_eutf_post_video_bg_color = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_video_bg_color', 'black' );
		$anemos_eutf_post_video_bg_opacity = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_video_bg_opacity', '70' );

		$video_mode = get_post_meta( $post->ID, '_anemos_eutf_post_type_video_mode', true );
		$anemos_eutf_post_video_webm = get_post_meta( $post->ID, '_anemos_eutf_post_video_webm', true );
		$anemos_eutf_post_video_mp4 = get_post_meta( $post->ID, '_anemos_eutf_post_video_mp4', true );
		$anemos_eutf_post_video_ogv = get_post_meta( $post->ID, '_anemos_eutf_post_video_ogv', true );
		$anemos_eutf_post_video_poster = get_post_meta( $post->ID, '_anemos_eutf_post_video_poster', true );
		$anemos_eutf_post_video_embed = get_post_meta( $post->ID, '_anemos_eutf_post_video_embed', true );

	?>
		<table class="form-table eut-metabox">
			<tbody>
				<tr>
					<td colspan="2">
						<p class="howto"><?php esc_html_e( 'Select one of the choices below for the featured video.', 'anemos' ); ?></p>
					</td>
				</tr>
				<tr class="eut-border-bottom">
					<th>
						<label for="eut-post-type-video-mode">
							<strong><?php esc_html_e( 'Video Mode', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Select your Video Mode', 'anemos' ); ?>
							</span>
						</label>
					</th>
					<td>
						<select id="eut-post-type-video-mode" name="_anemos_eutf_post_type_video_mode">
							<option value="" <?php selected( "", $video_mode ); ?>><?php esc_html_e( 'YouTube/Vimeo Video', 'anemos' ); ?></option>
							<option value="html5" <?php selected( "html5", $video_mode ); ?>><?php esc_html_e( 'HTML5 Video', 'anemos' ); ?></option>
						</select>
					</td>
				</tr>
				<tr class="eut-post-video-html5"<?php if ( "" == $video_mode ) { ?> style="display:none;" <?php } ?>>
					<th>
						<label for="eut-post-video-webm">
							<strong><?php esc_html_e( 'WebM File URL', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Upload the .webm video file.', 'anemos' ); ?>
								<br/>
								<strong><?php esc_html_e( 'This Format must be included for HTML5 Video.', 'anemos' ); ?></strong>
							</span>
						</label>
					</th>
					<td>
						<input type="text" id="eut-post-video-webm" class="eut-upload-simple-media-field eut-meta-text" name="_anemos_eutf_post_video_webm" value="<?php echo esc_attr( $anemos_eutf_post_video_webm ); ?>"/>
						<input type="button" data-media-type="video" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'anemos' ); ?>"/>
						<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>
					</td>
				</tr>
				<tr class="eut-post-video-html5"<?php if ( "" == $video_mode ) { ?> style="display:none;" <?php } ?>>
					<th>
						<label for="eut-post-video-mp4">
							<strong><?php esc_html_e( 'MP4 File URL', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Upload the .mp4 video file.', 'anemos' ); ?>
								<br/>
								<strong><?php esc_html_e( 'This Format must be included for HTML5 Video.', 'anemos' ); ?></strong>
							</span>
						</label>
					</th>
					<td>
						<input type="text" id="eut-post-video-mp4" class="eut-upload-simple-media-field eut-meta-text" name="_anemos_eutf_post_video_mp4" value="<?php echo esc_attr( $anemos_eutf_post_video_mp4 ); ?>"/>
						<input type="button" data-media-type="video" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'anemos' ); ?>"/>
						<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>
					</td>
				</tr>
				<tr class="eut-post-video-html5"<?php if ( "" == $video_mode ) { ?> style="display:none;" <?php } ?>>
					<th>
						<label for="eut-post-video-ogv">
							<strong><?php esc_html_e( 'OGV File URL', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Upload the .ogv video file (optional).', 'anemos' ); ?>
							</span>
						</label>
					</th>
					<td>
						<input type="text" id="eut-post-video-ogv" class="eut-upload-simple-media-field eut-meta-text" name="_anemos_eutf_post_video_ogv" value="<?php echo esc_attr( $anemos_eutf_post_video_ogv ); ?>"/>
						<input type="button" data-media-type="video" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'anemos' ); ?>"/>
						<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>
					</td>
				</tr>
				<tr class="eut-post-video-html5"<?php if ( "" == $video_mode ) { ?> style="display:none;" <?php } ?>>
					<th>
						<label for="eut-post-video-poster">
							<strong><?php esc_html_e( 'Poster Image', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Use same resolution as video.', 'anemos' ); ?>
							</span>
						</label>
					</th>
					<td>
						<input type="text" id="eut-post-video-poster" class="eut-upload-simple-media-field eut-meta-text" name="_anemos_eutf_post_video_poster" value="<?php echo esc_attr( $anemos_eutf_post_video_poster ); ?>"/>
						<input type="button" data-media-type="image" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'anemos' ); ?>"/>
						<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>
					</td>
				</tr>
				<tr class="eut-post-video-embed"<?php if ( "html5" == $video_mode ) { ?> style="display:none;" <?php } ?>>
					<th>
						<label for="eut-post-video-embed">
							<strong><?php esc_html_e( 'Vimeo/YouTube URL', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Enter the full URL of your video from Vimeo or YouTube.', 'anemos' ); ?>
							</span>
						</label>
					</th>
					<td>
						<input type="text" id="eut-post-video-embed" class="eut-meta-text" name="_anemos_eutf_post_video_embed" value="<?php echo esc_attr( $anemos_eutf_post_video_embed ); ?>"/>
					</td>
				</tr>
			</tbody>
		</table>

		<div id="eut-video-format-options">
	<?php
		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_anemos_eutf_post_video_style',
				'id' => 'eut-video-format-style',
				'options' => array(
					'' => esc_html__( 'Classic', 'anemos' ),
					'anemos' => esc_html__( 'Anemos', 'anemos' ),
				),
				'value' => $anemos_eutf_post_video_style,
				'default_value' => '',
				'label' => array(
					'title' => esc_html__( 'Post Style', 'anemos' ),
					'desc' => esc_html__( 'Note: Anemos style affects only Grid/Masonry style.', 'anemos' ),
				),
				'group_id' => 'eut-video-format-options',
				'highlight' => 'highlight',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'name' => '_anemos_eutf_post_video_bg_color',
				'options' => $anemos_eutf_post_color_selection,
				'value' => $anemos_eutf_post_video_bg_color,
				'label' => esc_html__( 'Background color', 'anemos' ),
				'default_value' => 'black',
				'dependency' =>
				'[
					{ "id" : "eut-video-format-style", "values" : ["anemos"] }
				]',
			)
		);

		anemos_eutf_print_admin_option(
			array(
				'type' => 'select',
				'options' => $anemos_eutf_post_bg_opacity_selection,
				'name' => '_anemos_eutf_post_video_bg_opacity',
				'value' => $anemos_eutf_post_video_bg_opacity,
				'label' => array(
					'title' => esc_html__( 'Background Opacity', 'anemos' ),
					'desc' => esc_html__( 'Note: only if Featured Image is available.', 'anemos' ),
				),
				'default_value' => '70',
				'dependency' =>
				'[
					{ "id" : "eut-video-format-style", "values" : ["anemos"] }
				]',
			)
		);
	?>
		</div>
	<?php
	}

	function anemos_eutf_meta_box_post_format_audio( $post ) {

		$audio_mode = get_post_meta( $post->ID, '_anemos_eutf_post_type_audio_mode', true );
		$anemos_eutf_post_audio_mp3 = get_post_meta( $post->ID, '_anemos_eutf_post_audio_mp3', true );
		$anemos_eutf_post_audio_ogg = get_post_meta( $post->ID, '_anemos_eutf_post_audio_ogg', true );
		$anemos_eutf_post_audio_wav = get_post_meta( $post->ID, '_anemos_eutf_post_audio_wav', true );
		$anemos_eutf_post_audio_embed = get_post_meta( $post->ID, '_anemos_eutf_post_audio_embed', true );

	?>
		<table class="form-table eut-metabox">
			<tbody>
				<tr>
					<td colspan="2">
						<p class="howto"><?php esc_html_e( 'Select one of the choices below for the featured audio.', 'anemos' ); ?></p>
					</td>
				</tr>
				<tr class="eut-border-bottom">
					<th>
						<label for="eut-post-type-audio-mode">
							<strong><?php esc_html_e( 'Audio Mode', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Select your Audio Mode', 'anemos' ); ?>
							</span>
						</label>
					</th>
					<td>
						<select id="eut-post-type-audio-mode" name="_anemos_eutf_post_type_audio_mode">
							<option value="" <?php selected( "", $audio_mode ); ?>><?php esc_html_e( 'Embed Audio', 'anemos' ); ?></option>
							<option value="html5" <?php selected( "html5", $audio_mode ); ?>><?php esc_html_e( 'HTML5 Audio', 'anemos' ); ?></option>
						</select>
					</td>
				</tr>
				<tr class="eut-post-audio-html5"<?php if ( "" == $audio_mode ) { ?> style="display:none;" <?php } ?>>
					<th>
						<label for="eut-post-audio-mp3">
							<strong><?php esc_html_e( 'MP3 File URL', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Upload the .mp3 audio file.', 'anemos' ); ?>
							</span>
						</label>
					</th>
					<td>
						<input type="text" id="eut-post-audio-mp3" class="eut-upload-simple-media-field eut-meta-text" name="_anemos_eutf_post_audio_mp3" value="<?php echo esc_attr( $anemos_eutf_post_audio_mp3 ); ?>"/>
						<input type="button" data-media-type="audio" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'anemos' ); ?>"/>
						<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>
					</td>
				</tr>
				<tr class="eut-post-audio-html5"<?php if ( "" == $audio_mode ) { ?> style="display:none;" <?php } ?>>
					<th>
						<label for="eut-post-audio-ogg">
							<strong><?php esc_html_e( 'OGG File URL', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Upload the .ogg audio file.', 'anemos' ); ?>
							</span>
						</label>
					</th>
					<td>
						<input type="text" id="eut-post-audio-ogg" class="eut-upload-simple-media-field eut-meta-text" name="_anemos_eutf_post_audio_ogg" value="<?php echo esc_attr( $anemos_eutf_post_audio_ogg ); ?>"/>
						<input type="button" data-media-type="audio" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'anemos' ); ?>"/>
						<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>
					</td>
				</tr>
				<tr class="eut-post-audio-html5"<?php if ( "" == $audio_mode ) { ?> style="display:none;" <?php } ?>>
					<th>
						<label for="eut-post-audio-wav">
							<strong><?php esc_html_e( 'WAV File URL', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Upload the .wav audio file (optional).', 'anemos' ); ?>
							</span>
						</label>
					</th>
					<td>
						<input type="text" id="eut-post-audio-wav" class="eut-upload-simple-media-field eut-meta-text" name="_anemos_eutf_post_audio_wav" value="<?php echo esc_attr( $anemos_eutf_post_audio_wav ); ?>"/>
						<input type="button" data-media-type="audio" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'anemos' ); ?>"/>
						<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>
					</td>
				</tr>
				<tr class="eut-post-audio-embed"<?php if ( "html5" == $audio_mode ) { ?> style="display:none;" <?php } ?>>
					<th>
						<label for="eut-post-audio-embed">
							<strong><?php esc_html_e( 'Audio embed code', 'anemos' ); ?></strong>
							<span>
								<?php esc_html_e( 'Type your audio embed code.', 'anemos' ); ?>
							</span>
						</label>
					</th>
					<td>
						<textarea id="eut-post-audio-embed" name="_anemos_eutf_post_audio_embed" cols="40" rows="5"><?php echo esc_textarea( $anemos_eutf_post_audio_embed ); ?></textarea>
					</td>
				</tr>
			</tbody>
		</table>

	<?php
	}

	function anemos_eutf_post_options_save_postdata( $post_id , $post ) {
		global $anemos_eutf_post_options;

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! isset( $_POST['_anemos_eutf_nonce_post_save'] ) || !wp_verify_nonce( $_POST['_anemos_eutf_nonce_post_save'], 'anemos_eutf_nonce_post_save' ) ) {
			return;
		}

		// Check permissions
		if ( 'post' == $_POST['post_type'] )
		{
			if ( !current_user_can( 'edit_post', $post_id ) ) {
				return;
			}
		}

		foreach ( $anemos_eutf_post_options as $value ) {
			$new_meta_value = ( isset( $_POST[$value['id']] ) ? $_POST[$value['id']] : '' );
			$meta_key = $value['id'];


			$meta_value = get_post_meta( $post_id, $meta_key, true );

			if ( $new_meta_value && '' == $meta_value ) {
				add_post_meta( $post_id, $meta_key, $new_meta_value, true );
			} elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
				update_post_meta( $post_id, $meta_key, $new_meta_value );
			} elseif ( '' == $new_meta_value && $meta_value ) {
				delete_post_meta( $post_id, $meta_key, $meta_value );
			}
		}



		//Feature Slider Items
		$media_slider_items = array();
		if ( isset( $_POST['_anemos_eutf_media_slider_item_id'] ) ) {

			$num_of_images = sizeof( $_POST['_anemos_eutf_media_slider_item_id'] );
			for ( $i=0; $i < $num_of_images; $i++ ) {

				$this_image = array (
					'id' => $_POST['_anemos_eutf_media_slider_item_id'][ $i ],
				);
				array_push( $media_slider_items, $this_image );
			}

		}

		if( empty( $media_slider_items ) ) {
			delete_post_meta( $post->ID, '_anemos_eutf_post_slider_items' );
			delete_post_meta( $post->ID, '_anemos_eutf_post_slider_settings' );
		} else{
			update_post_meta( $post->ID, '_anemos_eutf_post_slider_items', $media_slider_items );
			$media_slider_speed = 3500;
			$media_slider_direction_nav = '1';
			$media_slider_direction_nav_color = 'dark';

			if ( isset( $_POST['_anemos_eutf_post_slider_settings_speed'] ) ) {
				$media_slider_speed = $_POST['_anemos_eutf_post_slider_settings_speed'];
			}
			if ( isset( $_POST['_anemos_eutf_post_slider_settings_direction_nav'] ) ) {
				$media_slider_direction_nav = $_POST['_anemos_eutf_post_slider_settings_direction_nav'];
			}
			if ( isset( $_POST['_anemos_eutf_post_slider_settings_direction_nav_color'] ) ) {
				$media_slider_direction_nav_color = $_POST['_anemos_eutf_post_slider_settings_direction_nav_color'];
			}

			$media_slider_settings = array (
				'slideshow_speed' => $media_slider_speed,
				'direction_nav' => $media_slider_direction_nav,
				'direction_nav_color' => $media_slider_direction_nav_color,
			);
			update_post_meta( $post->ID, '_anemos_eutf_post_slider_settings', $media_slider_settings );
		}


	}

//Omit closing PHP tag to avoid accidental whitespace output errors.
