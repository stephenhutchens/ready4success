<?php
/*
*	Collection of functions for admin options
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

$anemos_eutf_media_boolean_selection = array(
	'yes' => esc_html__( 'Yes', 'anemos' ),
	'no' => esc_html__( 'No', 'anemos' ),
);

$anemos_eutf_media_align_selection = array(
	'left' => esc_html__( 'Left', 'anemos' ),
	'right' => esc_html__( 'Right', 'anemos' ),
	'center' => esc_html__( 'Center', 'anemos' ),
);

$anemos_eutf_media_color_selection = array(
	'dark' => esc_html__( 'Dark', 'anemos' ),
	'light' => esc_html__( 'Light', 'anemos' ),
	'primary-1' => esc_html__( 'Primary 1', 'anemos' ),
	'primary-2' => esc_html__( 'Primary 2', 'anemos' ),
	'primary-3' => esc_html__( 'Primary 3', 'anemos' ),
	'primary-4' => esc_html__( 'Primary 4', 'anemos' ),
	'primary-5' => esc_html__( 'Primary 5', 'anemos' ),
);

$anemos_eutf_media_color_extra_selection = array(
	'dark' => esc_html__( 'Dark', 'anemos' ),
	'light' => esc_html__( 'Light', 'anemos' ),
	'primary-1' => esc_html__( 'Primary 1', 'anemos' ),
	'primary-2' => esc_html__( 'Primary 2', 'anemos' ),
	'primary-3' => esc_html__( 'Primary 3', 'anemos' ),
	'primary-4' => esc_html__( 'Primary 4', 'anemos' ),
	'primary-5' => esc_html__( 'Primary 5', 'anemos' ),
	'custom' => esc_html__( 'Custom', 'anemos' ),
);

$anemos_eutf_media_opacity_selection = array(
	'0'    => '0%',
	'0.05' => '5%',
	'0.10' => '10%',
	'0.15' => '15%',
	'0.20' => '20%',
	'0.25' => '25%',
	'0.30' => '30%',
	'0.35' => '35%',
	'0.40' => '40%',
	'0.45' => '45%',
	'0.50' => '50%',
	'0.55' => '55%',
	'0.60' => '60%',
	'0.65' => '65%',
	'0.70' => '70%',
	'0.75' => '75%',
	'0.80' => '80%',
	'0.85' => '85%',
	'0.90' => '90%',
	'0.95' => '95%',
	'1'    => '100%',
);

$anemos_eutf_media_header_style_selection = array(
	'default' => esc_html__( 'Default', 'anemos' ),
	'dark' => esc_html__( 'Dark', 'anemos' ),
	'light' => esc_html__( 'Light', 'anemos' ),
);

$anemos_eutf_media_color_overlay_selection = array(
	'' => esc_html__( 'None', 'anemos' ),
	'dark' => esc_html__( 'Dark', 'anemos' ),
	'light' => esc_html__( 'Light', 'anemos' ),
	'primary-1' => esc_html__( 'Primary 1', 'anemos' ),
	'primary-2' => esc_html__( 'Primary 2', 'anemos' ),
	'primary-3' => esc_html__( 'Primary 3', 'anemos' ),
	'primary-4' => esc_html__( 'Primary 4', 'anemos' ),
	'primary-5' => esc_html__( 'Primary 5', 'anemos' ),
);


$anemos_eutf_media_style_selection = array(
	'default' => esc_html__( 'Default', 'anemos' ),
	'1' => esc_html__( 'Style 1', 'anemos' ),
	'2' => esc_html__( 'Style 2', 'anemos' ),
	'3' => esc_html__( 'Style 3', 'anemos' ),
	'4' => esc_html__( 'Style 4', 'anemos' ),
);

$anemos_eutf_media_pattern_overlay_selection = array(
	'' => esc_html__( 'No', 'anemos' ),
	'default' => esc_html__( 'Yes', 'anemos' ),
);

$anemos_eutf_media_text_animation_selection = array(
	'fade-in' => esc_html__( 'Default', 'anemos' ),
	'fade-in-up' => esc_html__( 'Fade In Up', 'anemos' ),
	'fade-in-down' => esc_html__( 'Fade In Down', 'anemos' ),
	'fade-in-left' => esc_html__( 'Fade In Left', 'anemos' ),
	'fade-in-right' => esc_html__( 'Fade In Right', 'anemos' ),
	'zoom-in' => esc_html__( 'Zoom In', 'anemos' ),
	'zoom-out' => esc_html__( 'Zoom Out', 'anemos' ),
);

$anemos_eutf_button_target_selection = array(
	'_self' => esc_html__( 'Same Page', 'anemos' ),
	'_blank' => esc_html__( 'New page', 'anemos' ),
);

$anemos_eutf_media_bg_position_selection = array(
	'left-top' => esc_html__( 'Left Top', 'anemos' ),
	'left-center' => esc_html__( 'Left Center', 'anemos' ),
	'left-bottom' => esc_html__( 'Left Bottom', 'anemos' ),
	'center-top' => esc_html__( 'Center Top', 'anemos' ),
	'center-center' => esc_html__( 'Center Center', 'anemos' ),
	'center-bottom' => esc_html__( 'Center Bottom', 'anemos' ),
	'right-top' => esc_html__( 'Right Top', 'anemos' ),
	'right-center' => esc_html__( 'Right Center', 'anemos' ),
	'right-bottom' => esc_html__( 'Right Bottom', 'anemos' ),
);

$anemos_eutf_media_bg_effect_selection = array(
	'none' => esc_html__( 'None', 'anemos' ),
	'zoom' => esc_html__( 'Zoom', 'anemos' ),
);

$anemos_eutf_media_tag_selection = array(
	'div' => esc_html__( 'div', 'anemos' ),
	'h1' => esc_html__( 'h1', 'anemos' ),
	'h2' => esc_html__( 'h2', 'anemos' ),
	'h3' => esc_html__( 'h3', 'anemos' ),
	'h4' => esc_html__( 'h4', 'anemos' ),
	'h5' => esc_html__( 'h5', 'anemos' ),
	'h6' => esc_html__( 'h6', 'anemos' ),
);




/**
 * Print Media Selector Functions
 */
function anemos_eutf_print_select_options( $selector_array, $current_value = "" ) {

	foreach ( $selector_array as $value=>$display_value ) {
	?>
		<option value="<?php echo esc_attr( $value ); ?>" <?php selected( $current_value, $value ); ?>><?php echo esc_html( $display_value ); ?></option>
	<?php
	}

}

function anemos_eutf_print_media_tag_selection( $current_value = "" ) {
	global $anemos_eutf_media_tag_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_tag_selection, $current_value );
}

function anemos_eutf_print_media_boolean_selection( $current_value = "" ) {
	global $anemos_eutf_media_boolean_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_boolean_selection, $current_value );
}
function anemos_eutf_print_media_button_color_selection( $current_value = "" ) {
	global $anemos_eutf_button_color_selection;
	anemos_eutf_print_select_options( $anemos_eutf_button_color_selection, $current_value );
}
function anemos_eutf_print_media_button_size_selection( $current_value = "" ) {
	global $anemos_eutf_button_size_selection;
	anemos_eutf_print_select_options( $anemos_eutf_button_size_selection, $current_value );
}
function anemos_eutf_print_media_button_shape_selection( $current_value = "" ) {
	global $anemos_eutf_button_shape_selection;
	anemos_eutf_print_select_options( $anemos_eutf_button_shape_selection, $current_value );
}
function anemos_eutf_print_media_button_type_selection( $current_value = "" ) {
	global $anemos_eutf_button_type_selection;
	anemos_eutf_print_select_options( $anemos_eutf_button_type_selection, $current_value );
}
function anemos_eutf_print_media_button_target_selection( $current_value = "" ) {
	global $anemos_eutf_button_target_selection;
	anemos_eutf_print_select_options( $anemos_eutf_button_target_selection, $current_value );
}

function anemos_eutf_print_media_style_selection( $current_value = "" ) {
	global $anemos_eutf_media_style_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_style_selection, $current_value );
}
function anemos_eutf_print_media_color_selection( $current_value = "" ) {
	global $anemos_eutf_media_color_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_color_selection, $current_value );
}

function anemos_eutf_print_media_color_extra_selection( $current_value = "" ) {
	global $anemos_eutf_media_color_extra_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_color_extra_selection, $current_value );
}

function anemos_eutf_print_media_opacity_selection( $current_value = "" ) {
	global $anemos_eutf_media_opacity_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_opacity_selection, $current_value );
}

function anemos_eutf_print_media_align_selection( $current_value = "" ) {
	global $anemos_eutf_media_align_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_align_selection, $current_value );
}
function anemos_eutf_print_media_header_style_selection( $current_value = "" ) {
	global $anemos_eutf_media_header_style_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_header_style_selection, $current_value );
}

function anemos_eutf_print_media_color_overlay_selection( $current_value = "" ) {
	global $anemos_eutf_media_color_overlay_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_color_overlay_selection, $current_value );
}
function anemos_eutf_print_media_pattern_overlay_selection( $current_value = "" ) {
	global $anemos_eutf_media_pattern_overlay_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_pattern_overlay_selection, $current_value );
}

function anemos_eutf_print_media_text_animation_selection( $current_value = "" ) {
	global $anemos_eutf_media_text_animation_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_text_animation_selection, $current_value );
}

function anemos_eutf_print_media_bg_position_selection( $current_value = "center-center" ) {
	global $anemos_eutf_media_bg_position_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_bg_position_selection, $current_value );
}

function anemos_eutf_print_media_bg_effect_selection( $current_value = "" ) {
	global $anemos_eutf_media_bg_effect_selection;
	anemos_eutf_print_select_options( $anemos_eutf_media_bg_effect_selection, $current_value );
}



/**
 * Prints Media Slider items
 */
function anemos_eutf_print_admin_media_slider_items( $slider_items ) {

	foreach ( $slider_items as $slider_item ) {
		anemos_eutf_print_admin_media_slider_item( $slider_item, '' );
	}

}

/**
 * Get Single Slider Media with ajax
 */
function anemos_eutf_get_slider_media() {

	if( isset( $_POST['attachment_ids'] ) ) {

		$attachment_ids = $_POST['attachment_ids'];

		if( !empty( $attachment_ids ) ) {

			$media_ids = explode(",", $attachment_ids);

			foreach ( $media_ids as $media_id ) {
				$slider_item = array (
					'id' => $media_id,
				);
				anemos_eutf_print_admin_media_slider_item( $slider_item, "new" );
			}
		}
	}
	if( isset( $_POST['attachment_ids'] ) ) { die(); }
}
add_action( 'wp_ajax_anemos_eutf_get_slider_media', 'anemos_eutf_get_slider_media' );


/**
 * Prints Single Slider Media  Item
 */
function anemos_eutf_print_admin_media_slider_item( $slider_item, $new = "" ) {
	$media_id = $slider_item['id'];

	$thumb_src = wp_get_attachment_image_src( $media_id, 'thumbnail' );
	$thumbnail_url = $thumb_src[0];
	$alt = get_post_meta( $media_id, '_wp_attachment_image_alt', true );

	$anemos_eutf_button_class = "eut-slider-item-delete-button";

	if( $new = "new" ) {
		$anemos_eutf_button_class = "eut-slider-item-delete-button eut-item-new";
	}

?>
	<div class="eut-slider-item-minimal">
		<input class="<?php echo esc_attr( $anemos_eutf_button_class ); ?> button" type="button" value="<?php esc_attr_e( 'Delete', 'anemos' ); ?>">
		<h3 class="hndle eut-title">
			<span><?php esc_html_e( 'Image', 'anemos' ); ?></span>
		</h3>
		<div class="inside">
			<input type="hidden" value="<?php echo esc_attr( $media_id ); ?>" name="_anemos_eutf_media_slider_item_id[]">
			<?php echo '<img class="eut-thumb" src="' . esc_url( $thumbnail_url ) . '" attid="' . esc_attr( $media_id ) . '" alt="' . esc_attr( $alt ) . '" width="120" height="120"/>'; ?>
		</div>
	</div>
<?php

}

/**
 * Prints Admin Option Selector
 */
function anemos_eutf_print_admin_option_wrapper_start( $item ) {

	$data_dependency = $item_highlight = $item_width = '';

	$item_type = anemos_eutf_array_value( $item, 'type' );
	$item_label = anemos_eutf_array_value( $item, 'label' );
	$item_required = anemos_eutf_array_value( $item, 'required' );
	$item_dependency = anemos_eutf_array_value( $item, 'dependency' );
	$item_multiple = anemos_eutf_array_value( $item, 'multiple' );
	$item_highlight = anemos_eutf_array_value( $item, 'highlight', 'standard' );
	$item_width = anemos_eutf_array_value( $item, 'width', 'normal' );
	$item_wrap_class = anemos_eutf_array_value( $item, 'wrap_class' );

	$wrapper_attributes = array();
	if( !empty( $item_dependency ) ) {
		$wrapper_attributes[] = "data-dependency='" . esc_attr( $item_dependency ) . "'";
	}

	$label_class = 'eut-label';
	if ( 'label' == $item_type ) {
		$label_class = 'eut-label eut-header-label';
	}

	$item_title = $item_desc = $item_info = '';

	if ( is_array ( $item_label ) ) {
		$item_title = anemos_eutf_array_value( $item_label, 'title' );
		$item_desc = anemos_eutf_array_value( $item_label, 'desc' );
		$item_info = anemos_eutf_array_value( $item_label, 'info' );
	} else {
		$item_title = $item_label;
	}

	//Classes
	$option_wrapper_classes = array( 'eut-fields-wrapper' );
	$option_wrapper_classes[] = 'eut-' . $item_highlight;
	if ( !empty ( $item_wrap_class ) ) {
		$option_wrapper_classes[] = $item_wrap_class;
	}
	$option_wrapper_class_string = implode( ' ', $option_wrapper_classes );

	$wrapper_attributes[] = 'class="' . esc_attr( $option_wrapper_class_string ) . '"';

?>
	<div <?php echo implode( ' ', $wrapper_attributes ); ?>>
		<div class="<?php echo esc_attr( $label_class ); ?>">
			<label>
				<span class="eut-title"><?php echo esc_html( $item_title ); ?></span>
				<span class="eut-description"><?php echo esc_html( $item_desc ); ?></span>
				<span class="eut-info"><?php echo esc_html( $item_info ); ?></span>
			</label>
		</div>
		<div class="eut-field-items-wrapper">
			<?php if ( '' == $item_multiple ) { ?>
			<div class="eut-field-item eut-field-item-<?php echo esc_attr( $item_width ); ?>">
			<?php } ?>

<?php
}

function anemos_eutf_print_admin_option_wrapper_end( $multiple = '' ) {
?>
			<?php if ( '' == $multiple ) { ?>
			</div>
			<?php } ?>
		</div>
	</div>

<?php
}

/**
 * Prints Admin Feature Setting
 */
function anemos_eutf_print_admin_option( $item ) {

	$item_type = anemos_eutf_array_value( $item, 'type' );
	$item_options = anemos_eutf_array_value( $item, 'options' );
	$item_label = anemos_eutf_array_value( $item, 'label' );
	$item_id = anemos_eutf_array_value( $item, 'id' );
	$item_group_id = anemos_eutf_array_value( $item, 'group_id' );
	$item_name = anemos_eutf_array_value( $item, 'name' );
	$item_default_value = anemos_eutf_array_value( $item, 'default_value' );
	$item_value = anemos_eutf_array_value( $item, 'value', $item_default_value );
	$item2_default_value = anemos_eutf_array_value( $item, 'default_value2' );
	$item2_value = anemos_eutf_array_value( $item, 'value2', $item2_default_value );
	$item_required = anemos_eutf_array_value( $item, 'required' );
	$item_dependency = anemos_eutf_array_value( $item, 'dependency' );
	$item_multiple = anemos_eutf_array_value( $item, 'multiple' );
	$item_type_usage = anemos_eutf_array_value( $item, 'type_usage' );
	$item_class = anemos_eutf_array_value( $item, 'extra_class' );

	$item_attributes = array();

	$dependency_field = $item_id_attr = '';
	if( !empty( $item_group_id ) ) {
		$item_attributes[] = 'class="eut-dependency-field ' . esc_attr( $item_class ) . '"';
		$item_attributes[] = 'data-group="' . esc_attr( $item_group_id ) . '"';
	} else {
		$item_attributes[] = 'class="' . esc_attr( $item_class ) . '"';
	}

	if( !empty( $item_id ) ) {
		$item_attributes[] = 'id="' . esc_attr( $item_id ) . '"';
	}

	if ( 'hidden' == $item_type ) {
?>
	<input type="hidden" name="<?php echo esc_attr( $item_name ); ?>" value="<?php echo esc_attr( $item_value ); ?>" <?php echo implode( ' ', $item_attributes ); ?>/>
<?php
		return;
	}

	anemos_eutf_print_admin_option_wrapper_start( $item );
?>
	<?php if ( 'hiddenfield' == $item_type ) { ?>

		<span class="eut-info"><?php echo esc_html( $item_value ); ?></span>

		<input type="hidden" name="<?php echo esc_attr( $item_name ); ?>" value="<?php echo esc_attr( $item_value ); ?>" <?php echo implode( ' ', $item_attributes ); ?>/>

	<?php } elseif ( 'textfield' == $item_type ) { ?>

		<input type="text" name="<?php echo esc_attr( $item_name ); ?>" value="<?php echo esc_attr( $item_value ); ?>" <?php echo implode( ' ', $item_attributes ); ?>/>

	<?php } elseif ( 'textarea' == $item_type ) { ?>

		<textarea name="<?php echo esc_attr( $item_name ); ?>" cols="40" rows="5" <?php echo implode( ' ', $item_attributes ); ?>><?php echo wp_kses_post( $item_value ); ?></textarea>

	<?php } elseif ( 'select' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_select_options( $item_options, $item_value ); ?>
		</select>

	<?php } elseif ( 'checkbox' == $item_type ) { ?>

		<input type="checkbox" name="<?php echo esc_attr( $item_name ); ?>" value="yes" <?php checked( $item_value, 'yes' ); ?> <?php echo implode( ' ', $item_attributes ); ?>/>

	<?php } elseif ( 'select-boolean' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_boolean_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-tag' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_tag_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-colorpicker' == $item_type ) { ?>

		<div class="eut-field-item">
			<select class="eut-select-color-extra" name="<?php echo esc_attr( $item_name ); ?>">
			<?php
				if ( 'sidebar-inpage' == $item_type_usage ) {
					anemos_eutf_print_select_options(
						array(
							'' => esc_html__( '-- Inherit --', 'anemos' ),
							'none' => esc_html__( 'None', 'anemos' ),
						),
						$item_value
					);
				} else if ( 'title-content-bg'  == $item_type_usage ) {
					anemos_eutf_print_select_options(
						array(
							'none' => esc_html__( 'None', 'anemos' ),
						),
						$item_value
					);
				}
				anemos_eutf_print_media_color_extra_selection( $item_value );
			?>
			</select>
		</div>
		<div class="eut-field-item">
			<div class="eut-wp-colorpicker">
				<?php
					if ( strpos( $item_name,'color_overlay') !== false) {
						$custom_name = str_replace ( 'color_overlay' , 'color_overlay_custom', $item_name );
					} else {
						$custom_name = str_replace ( 'color' , 'color_custom', $item_name );
					}
				?>
				<input type="text" name="<?php echo esc_attr( $custom_name ); ?>" class="wp-color-picker-field" value="<?php echo esc_attr( $item2_value ); ?>" data-default-color="#000000"/>
			</div>
		</div>

	<?php } elseif ( 'select-image' == $item_type ) { ?>

		<?php

			$thumb_src = wp_get_attachment_image_src( $item_value, 'thumbnail' );
			$thumbnail_url = $thumb_src[0];
			$visibility_class = '';
			if ( empty( $thumbnail_url ) ) {
				$thumbnail_url = get_template_directory_uri() . '/includes/images/no-image.jpg';
				$alt = '';
			} else {
				$alt = get_post_meta( $item_value, '_wp_attachment_image_alt', true );
				$alt = ! empty( $alt ) ? esc_attr( $alt ) : '';
				$visibility_class = 'eut-visible';
			}
		?>

			<div class="eut-thumb-container <?php echo esc_attr( $visibility_class ); ?>" data-mode="custom-image" data-field-name="<?php echo esc_attr( $item_name ); ?>" >
				<input class="eut-upload-media-id" type="hidden" value="<?php echo esc_attr( $item_value ); ?>" name="<?php echo esc_attr( $item_name ); ?>">
				<?php echo '<img class="eut-thumb" src="' . esc_url( $thumbnail_url ) . '" attid="' . $item_value . '" alt="' . $alt . '" width="120" height="120"/>'; ?>
				<a class="eut-upload-remove-image" href="#"></a>
			</div>
			<div class="eut-upload-replace-image"></div>


	<?php } elseif ( 'colorpicker' == $item_type ) { ?>

		<input type="text" name="<?php echo esc_attr( $item_name ); ?>" class="wp-color-picker-field" value="<?php echo esc_attr( $item_value ); ?>" data-default-color="#ffffff" <?php echo implode( ' ', $item_attributes ); ?>/>

		<?php } elseif ( 'select-color' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_color_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-color-extra' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_color_extra_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-opacity' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_opacity_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-style' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_style_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-header-style' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_header_style_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-align' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_align_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-text-animation' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_text_animation_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-button-target' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_button_target_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-button-type' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_button_type_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-button-color' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_button_color_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-button-size' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_button_size_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-button-shape' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_button_shape_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-pattern-overlay' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_pattern_overlay_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-color-overlay' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_color_overlay_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-bg-image' == $item_type ) { ?>

		<input type="text" class="eut-upload-simple-media-field"  name="<?php echo esc_attr( $item_name ); ?>" value="<?php echo esc_attr( $item_value ); ?>"/>
		<label></label>
		<input type="button" data-media-type="image" class="eut-upload-simple-media-button button-primary" value="<?php esc_attr_e( 'Upload Image', 'anemos' ); ?>"/>
		<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>

	<?php } elseif ( 'select-bg-position' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_bg_position_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-bg-position-inherit' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<option value="" <?php selected( "", $item_value ); ?>><?php esc_html_e( 'Inherit from above', 'anemos' ); ?></option>
			<?php anemos_eutf_print_media_bg_position_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-bg-effect' == $item_type ) { ?>

		<select name="<?php echo esc_attr( $item_name ); ?>" <?php echo implode( ' ', $item_attributes ); ?>>
			<?php anemos_eutf_print_media_bg_effect_selection( $item_value ); ?>
		</select>

	<?php } elseif ( 'select-bg-video' == $item_type ) { ?>

		<input type="text" class="eut-upload-simple-media-field eut-meta-text" name="<?php echo esc_attr( $item_name ); ?>" value="<?php echo esc_attr( $item_value ); ?>"/>
		<label></label>
		<input type="button" data-media-type="video" class="eut-upload-simple-media-button button" value="<?php esc_attr_e( 'Upload Media', 'anemos' ); ?>"/>
		<input type="button" class="eut-remove-simple-media-button button" value="<?php esc_attr_e( 'Remove', 'anemos' ); ?>"/>

	<?php } ?>

<?php
	anemos_eutf_print_admin_option_wrapper_end( $item_multiple );
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
