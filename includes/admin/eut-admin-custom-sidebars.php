<?php
/*
*	Admin Custom Sidebars
*
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

	function anemos_eutf_add_sidebar_settings() {
	
		if ( isset( $_POST['_anemos_eutf_nonce_sidebar_save'] ) && wp_verify_nonce( $_POST['_anemos_eutf_nonce_sidebar_save'], 'anemos_eutf_nonce_sidebar_save' ) ) {

			$sidebars_items = array();
			if( isset( $_POST['_anemos_eutf_custom_sidebar_item_id'] ) ) {
				$num_of_sidebars = sizeof( $_POST['_anemos_eutf_custom_sidebar_item_id'] );
				for ( $i=0; $i < $num_of_sidebars; $i++ ) {
					$this_sidebar = array (
						'id' => $_POST['_anemos_eutf_custom_sidebar_item_id'][ $i ],
						'name' => $_POST['_anemos_eutf_custom_sidebar_item_name'][ $i ],
					);
					array_push( $sidebars_items, $this_sidebar );
				}
			}
			if ( empty( $sidebars_items ) ) {
				delete_option( '_anemos_eutf_custom_sidebars' );
			} else {
				update_option( '_anemos_eutf_custom_sidebars', $sidebars_items );
			}
			//Update Sidebar list
			wp_get_sidebars_widgets();
			wp_safe_redirect( 'themes.php?page=anemos-eutf-custom-sidebar-settings&sidebar-settings=saved' );
			
		}
					
		add_theme_page(
			esc_html__( 'Sidebars', 'anemos' ),
			esc_html__( 'Sidebars', 'anemos' ),
			'manage_options',
			'anemos-eutf-custom-sidebar-settings',
			'anemos_eutf_show_sidebar_settings'
		);

	}

	add_action( 'admin_menu', 'anemos_eutf_add_sidebar_settings' );

	function anemos_eutf_show_sidebar_settings() {
		$anemos_eutf_custom_sidebars = get_option( '_anemos_eutf_custom_sidebars' );
?>
	<div id="eut-sidebar-wrap" class="wrap">
		<h2><?php esc_html_e( "Sidebars", 'anemos' ); ?></h2>
		
		<?php if( isset( $_GET['sidebar-settings'] ) ) { ?>
		<div class="eut-sidebar-saved eut-notice-green">
			<strong><?php esc_html_e('Settings Saved!', 'anemos' ); ?></strong>
		</div>
		<?php } ?>		
		<input type="text" id="eut-custom-sidebar-item-name-new" value=""/>
		<input type="button" id="eut-add-custom-sidebar-item" class="button button-primary" value="<?php esc_html_e('Add New', 'anemos' ); ?>"/>
		<span class="eut-sidebar-spinner"></span>
		<div class="eut-sidebar-notice eut-notice-red" style="display:none;">
			<strong><?php esc_html_e('Field must not be empty!', 'anemos' ); ?></strong>
		</div>
		<div class="eut-sidebar-notice-exists eut-notice-red" style="display:none;">
			<strong><?php esc_html_e('Sidebar with this name already exists!', 'anemos' ); ?></strong>
		</div>		
		<form method="post" action="themes.php?page=anemos-eutf-custom-sidebar-settings">
			<?php wp_nonce_field( 'anemos_eutf_nonce_sidebar_save', '_anemos_eutf_nonce_sidebar_save' ); ?>
			<div id="eut-custom-sidebar-container">
				<?php anemos_eutf_print_admin_custom_sidebars( $anemos_eutf_custom_sidebars ); ?>
			</div>
			<?php submit_button(); ?>
		</form>
	</div>
<?php
	}
	
	function  anemos_eutf_print_admin_custom_sidebars( $anemos_eutf_custom_sidebars ) {

		
		if ( ! empty( $anemos_eutf_custom_sidebars ) ) {
			foreach ( $anemos_eutf_custom_sidebars as $anemos_eutf_custom_sidebar ) {
				anemos_eutf_print_admin_single_custom_sidebar( $anemos_eutf_custom_sidebar );
			}
		}
	}

	function  anemos_eutf_print_admin_single_custom_sidebar( $sidebar_item, $mode = '' ) {

		$anemos_eutf_button_class = "eut-custom-sidebar-item-delete-button";
		$sidebar_item_id = uniqid('anemos_eutf_sidebar_');
		
		if( $mode = "new" ) {
			$anemos_eutf_button_class = "eut-custom-sidebar-item-delete-button eut-item-new";			
		}	
?>
	
	
	<div class="eut-custom-sidebar-item">
		<input class="<?php echo esc_attr( $anemos_eutf_button_class ); ?> button" type="button" value="<?php esc_attr_e('Delete', 'anemos' ); ?>">
		<h3 class="eut-custom-sidebar-title">
			<span><?php esc_html_e('Custom Sidebar', 'anemos' ); ?>: <?php echo anemos_eutf_array_value( $sidebar_item, 'name' ); ?></span>
		</h3>
		<div class="eut-custom-sidebar-settings">
			<input type="hidden" name="_anemos_eutf_custom_sidebar_item_id[]" value="<?php echo anemos_eutf_array_value( $sidebar_item, 'id', $sidebar_item_id ); ?>">
			<input type="hidden" class="eut-custom-sidebar-item-name" name="_anemos_eutf_custom_sidebar_item_name[]" value="<?php echo anemos_eutf_array_value( $sidebar_item, 'name' ); ?>"/>
		</div>
	</div>
	
<?php

	}

	add_action( 'wp_ajax_anemos_eutf_get_custom_sidebar', 'anemos_eutf_get_custom_sidebar' );

	function anemos_eutf_get_custom_sidebar() {
	
		if( isset( $_POST['sidebar_name'] ) ) {
		
			$sidebar_item_name = $_POST['sidebar_name'];
			$sidebar_item_id = uniqid('anemos_eutf_sidebar_');
			if( empty( $sidebar_item_name ) ) {
				$sidebar_item_name = $sidebar_item_id;
			}
			
			$this_sidebar = array (
				'id' => $sidebar_item_id,
				'name' => $sidebar_item_name,
			);

			anemos_eutf_print_admin_single_custom_sidebar( $this_sidebar, 'new' );
		}
		die();

	}

//Omit closing PHP tag to avoid accidental whitespace output errors.
