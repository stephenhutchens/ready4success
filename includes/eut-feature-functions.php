<?php

/*
*	Feature Helper functions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


/**
 * Get Validate Header Style
 */

function anemos_eutf_validate_header_style( $anemos_eutf_header_style ) {

	$header_styles = array( 'default', 'dark', 'light' );
	if ( !in_array( $anemos_eutf_header_style, $header_styles ) ) {
		$anemos_eutf_header_style = 'default';
	}
	return $anemos_eutf_header_style;

}

/**
 * Get Header Feature Header Section Data
 */

function anemos_eutf_get_feature_header_data() {
	global $post;

	$anemos_eutf_header_position = 'above';
	if( anemos_eutf_is_woo_tax() ) {
		$anemos_eutf_header_style = anemos_eutf_option( 'product_tax_header_style', 'default' );
		$anemos_eutf_header_overlapping = anemos_eutf_option( 'product_tax_header_overlapping', 'no' );

	} else {
		$anemos_eutf_header_style = anemos_eutf_option( 'blog_header_style', 'default' );
		$anemos_eutf_header_overlapping = anemos_eutf_option( 'blog_header_overlapping', 'no' );
	}

	$feature_size = '';

	$anemos_eutf_woo_shop = anemos_eutf_is_woo_shop();

	if ( is_search() ) {
		$anemos_eutf_header_style =  anemos_eutf_option( 'search_page_header_style' );
		$anemos_eutf_header_overlapping =  anemos_eutf_option( 'search_page_header_overlapping' );
	}

	if ( is_singular() || $anemos_eutf_woo_shop ) {

		if ( $anemos_eutf_woo_shop ) {
			$post_id = wc_get_page_id( 'shop' );
		} else {
			$post_id = $post->ID;
		}
		$post_type = get_post_type( $post_id );

		switch( $post_type ) {
			case 'product':
				$anemos_eutf_header_style =  anemos_eutf_post_meta( '_anemos_eutf_header_style', anemos_eutf_option( 'product_header_style' ) );
				$anemos_eutf_header_overlapping =  anemos_eutf_post_meta( '_anemos_eutf_header_overlapping', anemos_eutf_option( 'product_header_overlapping' ) );
			break;
			case 'portfolio':
				$anemos_eutf_header_style =  anemos_eutf_post_meta( '_anemos_eutf_header_style', anemos_eutf_option( 'portfolio_header_style' ) );
				$anemos_eutf_header_overlapping =  anemos_eutf_post_meta( '_anemos_eutf_header_overlapping', anemos_eutf_option( 'portfolio_header_overlapping' ) );
			break;
			case 'post':
				$anemos_eutf_header_style =  anemos_eutf_post_meta( '_anemos_eutf_header_style', anemos_eutf_option( 'post_header_style' ) );
				$anemos_eutf_header_overlapping =  anemos_eutf_post_meta( '_anemos_eutf_header_overlapping', anemos_eutf_option( 'post_header_overlapping' ) );
			break;
			case 'page':
			default:
				if ( $anemos_eutf_woo_shop ) {
					$anemos_eutf_header_style =  anemos_eutf_post_meta_shop( '_anemos_eutf_header_style', anemos_eutf_option( 'page_header_style' ) );
					$anemos_eutf_header_overlapping =  anemos_eutf_post_meta_shop( '_anemos_eutf_header_overlapping', anemos_eutf_option( 'page_header_overlapping' ) );
				} else {
					$anemos_eutf_header_style =  anemos_eutf_post_meta( '_anemos_eutf_header_style', anemos_eutf_option( 'page_header_style' ) );
					$anemos_eutf_header_overlapping =  anemos_eutf_post_meta( '_anemos_eutf_header_overlapping', anemos_eutf_option( 'page_header_overlapping' ) );
				}
			break;
		}

		//Force Overlapping for Scrolling Full Width Sections Template
		if ( is_page_template( 'page-templates/template-full-page.php' ) ) {
			$anemos_eutf_header_overlapping = 'yes';
		} else {

			$feature_section_post_types = anemos_eutf_option( 'feature_section_post_types');

			if ( !empty( $feature_section_post_types ) && in_array( $post_type, $feature_section_post_types ) ) {

				$feature_section = get_post_meta( $post_id, '_anemos_eutf_feature_section', true );
				$feature_settings = anemos_eutf_array_value( $feature_section, 'feature_settings' );
				$feature_element = anemos_eutf_array_value( $feature_settings, 'element' );

				if ( !empty( $feature_element ) ) {

					$feature_single_item = anemos_eutf_array_value( $feature_section, 'single_item' );
					$anemos_eutf_header_position = anemos_eutf_array_value( $feature_settings, 'header_position' );
					if ( 'slider' ==  $feature_element ) {

						$slider_items = anemos_eutf_array_value( $feature_section, 'slider_items' );
						if ( !empty( $slider_items ) ) {
							$anemos_eutf_header_style = isset( $slider_items[0]['header_style'] ) ? $slider_items[0]['header_style'] : 'default';
						}

					}
				}
			}
		}
	}

	return array(
		'data_overlap' => $anemos_eutf_header_overlapping,
		'data_header_position' => $anemos_eutf_header_position,
		'header_style' => anemos_eutf_validate_header_style( $anemos_eutf_header_style ),
	);

}

/**
 * Prints Header Feature Section Page/Post/Portfolio
 */
function anemos_eutf_print_header_feature() {

	//Skip for  Scrolling Full Width Sections Template
	if ( is_page_template( 'page-templates/template-full-page.php' ) ) {
		return false;
	}

	global $post;

	$anemos_eutf_woo_shop = anemos_eutf_is_woo_shop();

	if ( is_singular() || $anemos_eutf_woo_shop ) {

		if ( $anemos_eutf_woo_shop ) {
			$post_id = wc_get_page_id( 'shop' );
		} else {
			$post_id = $post->ID;
		}
		$post_type = get_post_type( $post_id );
		$feature_section_post_types = anemos_eutf_option( 'feature_section_post_types');
		if ( in_array( $post_type, $feature_section_post_types ) ) {

			$feature_section = get_post_meta( $post_id, '_anemos_eutf_feature_section', true );
			$feature_settings = anemos_eutf_array_value( $feature_section, 'feature_settings' );
			$feature_element = anemos_eutf_array_value( $feature_settings, 'element' );

			if ( !empty( $feature_element ) ) {

				$feature_single_item = anemos_eutf_array_value( $feature_section, 'single_item' );

				switch( $feature_element ) {
					case 'title':
						if ( !empty( $feature_single_item ) ) {
							anemos_eutf_print_header_feature_single( $feature_settings, $feature_single_item, 'title' );
						}
						break;
					case 'image':
						if ( !empty( $feature_single_item ) ) {
							anemos_eutf_print_header_feature_single( $feature_settings, $feature_single_item, 'image' );
						}
						break;
					case 'video':
						if ( !empty( $feature_single_item ) ) {
							anemos_eutf_print_header_feature_single( $feature_settings, $feature_single_item, 'video' );
						}
						break;
					case 'slider':
						$slider_items = anemos_eutf_array_value( $feature_section, 'slider_items' );
						$slider_settings = anemos_eutf_array_value( $feature_section, 'slider_settings' );
						if ( !empty( $slider_items ) ) {
							anemos_eutf_print_header_feature_slider( $feature_settings, $slider_items, $slider_settings );
						}
						break;
					case 'map':
						$map_items = anemos_eutf_array_value( $feature_section, 'map_items' );
						$map_settings = anemos_eutf_array_value( $feature_section, 'map_settings' );
						if ( !empty( $map_items ) ) {
							anemos_eutf_print_header_feature_map( $feature_settings, $map_items, $map_settings );
						}
						break;
					case 'revslider':
						$revslider_alias = anemos_eutf_array_value( $feature_section, 'revslider_alias' );
						if ( !empty( $revslider_alias ) ) {
							anemos_eutf_print_header_feature_revslider( $feature_settings, $revslider_alias, $feature_single_item );
						}
						break;
					default:
						break;

				}
			}
		}
	}
}


/**
 * Prints Overlay Container
 */
function anemos_eutf_print_overlay_container( $item ) {

	$pattern_overlay = anemos_eutf_array_value( $item, 'pattern_overlay' );
	$color_overlay = anemos_eutf_array_value( $item, 'color_overlay', 'dark' );
	$color_overlay_custom = anemos_eutf_array_value( $item, 'color_overlay_custom', '#000000' );
	$color_overlay_custom = anemos_eutf_get_color( $color_overlay, $color_overlay_custom );
	$opacity_overlay = anemos_eutf_array_value( $item, 'opacity_overlay', '0' );

	if ( 'default' == $pattern_overlay ) {
		echo '<div class="eut-pattern"></div>';
	}
	if ( '0' != $opacity_overlay && !empty( $opacity_overlay ) ) {
		$overlay_classes = array('eut-bg-overlay');
		$overlay_string = implode( ' ', $overlay_classes );

		echo '<div class="' . esc_attr( $overlay_string ) . '" style="background-color: rgba(' . esc_attr( anemos_eutf_hex2rgb( $color_overlay_custom ) ) . ',' . esc_attr( $opacity_overlay ) . ');"></div>';
	}
}

/**
 * Prints Background Image Container
 */
function anemos_eutf_print_bg_image_container( $item ) {

	$bg_position = anemos_eutf_array_value( $item, 'bg_position', 'center-center' );
	$bg_tablet_sm_position = anemos_eutf_array_value( $item, 'bg_tablet_sm_position' );

	$bg_image_id = anemos_eutf_array_value( $item, 'bg_image_id' );

	$full_src = wp_get_attachment_image_src( $bg_image_id, 'anemos-eutf-fullscreen' );
	$image_url = esc_url( $full_src[0] );
	if( !empty( $image_url ) ) {

		$bg_image_classes = array( 'eut-bg-image' );
		$bg_image_classes[] = 'eut-bg-' . $bg_position;
		if ( !empty( $bg_tablet_sm_position ) ) {
			$bg_image_classes[] = 'eut-bg-tablet-sm-' . $bg_tablet_sm_position;
		}
		$bg_image_classes_string = implode( ' ', $bg_image_classes );

		echo '<div class="' . esc_attr( $bg_image_classes_string ) . '" style="background-image: url(' . esc_url( $image_url ) . ');"></div>';
	}

}


/**
 * Prints Background Video Container
 */
function anemos_eutf_print_bg_video_container( $item ) {

	if ( wp_is_mobile() ) {
		return;
	}

	$bg_video_webm = anemos_eutf_array_value( $item, 'video_webm' );
	$bg_video_mp4 = anemos_eutf_array_value( $item, 'video_mp4' );
	$bg_video_ogv = anemos_eutf_array_value( $item, 'video_ogv' );
	$bg_video_poster = anemos_eutf_array_value( $item, 'video_poster', 'no' );
	$bg_image_id = anemos_eutf_array_value( $item, 'bg_image_id' );

	$loop = anemos_eutf_array_value( $item, 'video_loop', 'yes' );
	$muted = anemos_eutf_array_value( $item, 'video_muted', 'yes' );

	$full_src = wp_get_attachment_image_src( $bg_image_id, 'anemos-eutf-fullscreen' );
	$image_url = esc_url( $full_src[0] );

	$video_poster = '';

	if ( !empty( $image_url ) && 'yes' == $bg_video_poster ) {
		$video_poster = $image_url;
	}

	$video_settings = array(
		'preload' => 'auto',
		'autoplay' => 'yes',
		'loop' => $loop,
		'muted' => $muted,
		'poster' => $video_poster,
	);
	$video_settings = apply_filters( 'anemos_eutf_feature_video_settings', $video_settings );


	if ( !empty ( $bg_video_webm ) || !empty ( $bg_video_mp4 ) || !empty ( $bg_video_ogv ) ) {
?>
		<div class="eut-bg-video">
			<video <?php echo anemos_eutf_print_media_video_settings( $video_settings );?>>
			<?php if ( !empty ( $bg_video_webm ) ) { ?>
				<source src="<?php echo esc_url( $bg_video_webm ); ?>" type="video/webm">
			<?php } ?>
			<?php if ( !empty ( $bg_video_mp4 ) ) { ?>
				<source src="<?php echo esc_url( $bg_video_mp4 ); ?>" type="video/mp4">
			<?php } ?>
			<?php if ( !empty ( $bg_video_ogv ) ) { ?>
				<source src="<?php echo esc_url( $bg_video_ogv ); ?>" type="video/ogg">
			<?php } ?>
			</video>
		</div>
<?php
	}

}

/**
 * Get Feature Section data
 */
function anemos_eutf_get_feature_data( $feature_settings, $item_type, $item_effect = '', $el_class = '' ) {


	$wrapper_attributes = array();

	//Background Color
	$bg_color = anemos_eutf_array_value( $feature_settings, 'bg_color', 'dark' );
	$bg_color_custom = anemos_eutf_array_value( $feature_settings, 'bg_color_custom', '#000000' );
	$bg_color_custom = anemos_eutf_get_color( $bg_color, $bg_color_custom );

	//Data and Style
	if( 'revslider' != $item_type ) {
		$feature_size = anemos_eutf_array_value( $feature_settings, 'size' );
		$feature_height = anemos_eutf_array_value( $feature_settings, 'height', '60' );
		if ( !empty($feature_size) ) {
			if ( empty( $feature_height ) ) {
				$feature_height = "60";
			}
			$wrapper_attributes[] = 'data-height="' . esc_attr( $feature_height ) . '"';
			$wrapper_attributes[] = 'style="background-color: ' . esc_attr( $bg_color_custom ) . ';"';
		} else {
			$wrapper_attributes[] = 'style="background-color: ' . esc_attr( $bg_color_custom ) . ';"';
		}
	}

	//Classes
	$feature_item_classes = array( 'eut-with-' . $item_type  );

	if( 'revslider' != $item_type ) {
		if ( empty( $feature_size ) ) {
			$feature_item_classes[] = 'eut-fullscreen';
		} else {
			$feature_item_classes[] = 'eut-custom-size';
		}

		if ( !empty( $item_effect ) ) {
			$feature_item_classes[] = 'eut-bg-' . $item_effect;
		}
	}

	if ( !empty ( $el_class ) ) {
		$feature_item_classes[] = $el_class;
	}
	$feature_item_class_string = implode( ' ', $feature_item_classes );

	//Add Classes
	$wrapper_attributes[] = 'class="' . esc_attr( $feature_item_class_string ) . '"';

	return $wrapper_attributes;
}

/**
 * Get Feature Section data
 */
function anemos_eutf_get_feature_height( $feature_settings ) {

	//Data and Style
	$feature_style_height = '';

	$feature_size = anemos_eutf_array_value( $feature_settings, 'size' );
	$feature_height = anemos_eutf_array_value( $feature_settings, 'height', '60' );
	$feature_min_height = anemos_eutf_array_value( $feature_settings, 'min_height', '200' );
	if ( !empty($feature_size) ) {
		$feature_style_height = 'style="height:' . esc_attr( $feature_height ) . 'vh; min-height:' . esc_attr( $feature_min_height ) . 'px;"';
	}
	return $feature_style_height;
}


/**
 * Prints Header Section Feature Single Item
 */
function anemos_eutf_print_header_feature_single( $feature_settings, $item, $item_type  ) {

	if( 'image' == $item_type ) {
		$item_effect = anemos_eutf_array_value( $item, 'image_effect' );
	} elseif( 'video' == $item_type ) {
		$item_effect = anemos_eutf_array_value( $item, 'video_effect' );
	} else {
		$item_effect = '';
	}

	$el_class = anemos_eutf_array_value( $item, 'el_class' );

	$feature_data = anemos_eutf_get_feature_data( $feature_settings, $item_type, $item_effect, $el_class );

?>
	<div id="eut-feature-section" <?php echo implode( ' ', $feature_data ); ?>>
		<div class="eut-wrapper clearfix" <?php echo anemos_eutf_get_feature_height( $feature_settings ); ?>>
			<?php anemos_eutf_print_header_feature_content( $feature_settings, $item, $item_type ); ?>
		</div>
		<div class="eut-background-wrapper">
		<?php
			anemos_eutf_print_overlay_container( $item  );
			if( 'image' == $item_type || 'video' == $item_type ) {
				anemos_eutf_print_bg_image_container( $item );
			}
			if( 'video' == $item_type ) {
				anemos_eutf_print_bg_video_container( $item );
			}
		?>
		</div>
		<?php anemos_eutf_print_feature_go_to_section( $feature_settings, $item ); ?>
	</div>
<?php
}

/**
 * Prints Feature Slider
 */
function anemos_eutf_print_header_feature_slider( $feature_settings, $slider_items, $slider_settings ) {

	$slider_speed = anemos_eutf_array_value( $slider_settings, 'slideshow_speed', '3500' );
	$slider_pause = anemos_eutf_array_value( $slider_settings, 'slider_pause', 'no' );
	$slider_transition = anemos_eutf_array_value( $slider_settings, 'transition', 'slide' );
	$slider_dir_nav = anemos_eutf_array_value( $slider_settings, 'direction_nav', '1' );
	$slider_effect = anemos_eutf_array_value( $slider_settings, 'slider_effect', '' );

	$feature_data = anemos_eutf_get_feature_data( $feature_settings, 'slider', $slider_effect, 'eut-carousel-wrapper'  );

	$anemos_eutf_header_style = isset( $slider_items[0]['header_style'] ) ? $slider_items[0]['header_style'] : 'default';

?>
	<div id="eut-feature-section" <?php echo implode( ' ', $feature_data ); ?>>

		<?php echo anemos_eutf_element_navigation( $slider_dir_nav, $anemos_eutf_header_style ); ?>

		<div id="eut-feature-slider" data-slider-speed="<?php echo esc_attr( $slider_speed ); ?>" data-slider-pause="<?php echo esc_attr( $slider_pause ); ?>" data-slider-transition="<?php echo esc_attr( $slider_transition ); ?>">

<?php

			foreach ( $slider_items as $item ) {

				$header_style = anemos_eutf_array_value( $item, 'header_style', 'default' );
				$anemos_eutf_header_style = anemos_eutf_validate_header_style( $header_style );

				$slide_type = anemos_eutf_array_value( $item, 'type' );
				$slide_post_id = anemos_eutf_array_value( $item, 'post_id' );
				if( 'post' == $slide_type &&  !empty( $slide_post_id ) ) {
					if( has_post_thumbnail( $slide_post_id ) && empty( $item['bg_image_id'] ) ) {
						$item['bg_image_id'] = get_post_thumbnail_id( $slide_post_id );
					}
				}

				$el_class = anemos_eutf_array_value( $item, 'el_class' );
				$el_id = anemos_eutf_array_value( $item, 'id', uniqid() );

?>
				<div class="eut-slider-item eut-slider-item-id-<?php echo esc_attr( $el_id ); ?> <?php echo esc_attr( $el_class ); ?>" data-header-color="<?php echo esc_attr( $anemos_eutf_header_style ); ?>">
					<div class="eut-wrapper clearfix" <?php echo anemos_eutf_get_feature_height( $feature_settings ); ?>>
						<?php anemos_eutf_print_header_feature_content( $feature_settings, $item ); ?>
					</div>
					<div class="eut-background-wrapper">
					<?php
						anemos_eutf_print_overlay_container( $item  );
						anemos_eutf_print_bg_image_container( $item );
					?>
					</div>
					<?php anemos_eutf_print_feature_go_to_section( $feature_settings, $item ); ?>
				</div>
<?php
			}
?>
		</div>

	</div>
<?php

}

/**
 * Prints Header Feature Map
 */
function anemos_eutf_print_header_feature_map( $feature_settings, $map_items, $map_settings ) {

	wp_enqueue_script( 'anemos-eutf-maps-script');

	$feature_data = anemos_eutf_get_feature_data( $feature_settings, 'map' );

	$map_marker = anemos_eutf_array_value( $map_settings, 'marker', get_template_directory_uri() . '/images/markers/markers.png' );
	$map_zoom = anemos_eutf_array_value( $map_settings, 'zoom', 14 );
	$map_disable_style = anemos_eutf_array_value( $map_settings, 'disable_style', 'no' );

	$map_lat = anemos_eutf_array_value( $map_items[0], 'lat', '51.516221' );
	$map_lng = anemos_eutf_array_value( $map_items[0], 'lng', '-0.136986' );

?>
	<div id="eut-feature-section" <?php echo implode( ' ', $feature_data ); ?>>
		<div class="eut-map eut-wrapper clearfix" <?php echo anemos_eutf_get_feature_height( $feature_settings ); ?> data-lat="<?php echo esc_attr( $map_lat ); ?>" data-lng="<?php echo esc_attr( $map_lng ); ?>" data-zoom="<?php echo esc_attr( $map_zoom ); ?>" data-disable-style="<?php echo esc_attr( $map_disable_style ); ?>"></div>
		<?php
			foreach ( $map_items as $map_item ) {
				anemos_eutf_print_feature_map_point( $map_item, $map_marker );
			}
		?>
	</div>
<?php
}

function anemos_eutf_print_feature_map_point( $map_item, $default_marker ) {

	$map_lat = anemos_eutf_array_value( $map_item, 'lat', '51.516221' );
	$map_lng = anemos_eutf_array_value( $map_item, 'lng', '-0.136986' );
	$map_marker = anemos_eutf_array_value( $map_item, 'marker', $default_marker );

	$map_title = anemos_eutf_array_value( $map_item, 'title' );
	$map_infotext = anemos_eutf_array_value( $map_item, 'info_text','' );
	$map_infotext_open = anemos_eutf_array_value( $map_item, 'info_text_open', 'no' );

	$button_text = anemos_eutf_array_value( $map_item, 'button_text' );
	$button_url = anemos_eutf_array_value( $map_item, 'button_url' );
	$button_target = anemos_eutf_array_value( $map_item, 'button_target', '_self' );
	$button_class = anemos_eutf_array_value( $map_item, 'button_class' );

?>
	<div style="display:none" class="eut-map-point" data-point-lat="<?php echo esc_attr( $map_lat ); ?>" data-point-lng="<?php echo esc_attr( $map_lng ); ?>" data-point-marker="<?php echo esc_attr( $map_marker ); ?>" data-point-title="<?php echo esc_attr( $map_title ); ?>" data-point-open="<?php echo esc_attr( $map_infotext_open ); ?>">
		<?php if ( !empty( $map_title ) || !empty( $map_infotext ) || !empty( $button_text ) ) { ?>
		<div class="eut-map-infotext">
			<?php if ( !empty( $map_title ) ) { ?>
			<h6 class="eut-infotext-title"><?php echo esc_html( $map_title ); ?></h6>
			<?php } ?>
			<?php if ( !empty( $map_infotext ) ) { ?>
			<p class="eut-infotext-description"><?php echo wp_kses_post( $map_infotext ); ?></p>
			<?php } ?>
			<?php if ( !empty( $button_text ) ) { ?>
			<a class="eut-infotext-link <?php echo esc_attr( $button_class ); ?>" href="<?php echo esc_url( $button_url ); ?>" target="<?php echo esc_attr( $button_target ); ?>"><?php echo esc_html( $button_text ); ?></a>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
<?php

}

/**
 * Prints Header Feature Revolution Slider
 */
function anemos_eutf_print_header_feature_revslider( $feature_settings, $revslider_alias, $item  ) {

	$el_class = anemos_eutf_array_value( $item, 'el_class' );
	$feature_data = anemos_eutf_get_feature_data( $feature_settings, 'revslider', '', $el_class );

?>
	<div id="eut-feature-section" <?php echo implode( ' ', $feature_data ); ?>>
		<?php echo do_shortcode( '[rev_slider ' . $revslider_alias . ']' ); ?>
		<?php anemos_eutf_print_feature_go_to_section( $feature_settings, $item ); ?>
	</div>

<?php
}

/**
 * Prints Header Feature Go to Section ( Bottom Arrow )
 */
function anemos_eutf_print_feature_go_to_section( $feature_settings, $item ) {

	$arrow_enabled = anemos_eutf_array_value( $item, 'arrow_enabled', 'no' );
	$arrow_color = anemos_eutf_array_value( $item, 'arrow_color', 'light' );
	$arrow_color_custom = anemos_eutf_array_value( $item, 'arrow_color_custom', '#ffffff' );
	$arrow_color_custom = anemos_eutf_get_color( $arrow_color, $arrow_color_custom );
	$arrow_align = anemos_eutf_array_value( $item, 'arrow_align', 'left' );

	if( 'yes' == $arrow_enabled ) {
?>
		<div id="eut-goto-section-wrapper" class="eut-align-<?php echo esc_attr( $arrow_align ); ?>">
			<div class="eut-container">
				<i id="eut-goto-section" class="eut-icon-nav-down" style=" color: <?php echo esc_attr( $arrow_color_custom ); ?>;"></i>
			</div>
		</div>
<?php
	}

}

/**
 * Prints Header Feature Content Image
 */
function anemos_eutf_print_feature_content_image( $item ) {

	$media_id = anemos_eutf_array_value( $item, 'content_image_id', '0' );
	$media_size = anemos_eutf_array_value( $item, 'content_image_size', 'medium' );

	if( !empty( $media_id ) ) {
?>
		<div class="eut-graphic">
			<?php echo wp_get_attachment_image( $media_id, $media_size ); ?>
		</div>
<?php
	}

}


/**
 * Prints Header Section Feature Content
 */
function anemos_eutf_print_header_feature_content( $feature_settings, $item, $mode = ''  ) {

	$feature_size = anemos_eutf_array_value( $feature_settings, 'size' );

	$title = anemos_eutf_array_value( $item, 'title' );
	$caption = anemos_eutf_array_value( $item, 'caption' );
	$subheading = anemos_eutf_array_value( $item, 'subheading' );

	$subheading_tag = anemos_eutf_array_value( $item, 'subheading_tag', 'div' );
	$title_tag = anemos_eutf_array_value( $item, 'title_tag', 'div' );
	$caption_tag = anemos_eutf_array_value( $item, 'caption_tag', 'div' );

	$anemos_eutf_subheading_classes = array( 'eut-subheading', 'eut-title-categories', 'clearfix' );
	$anemos_eutf_title_classes = array( 'eut-title' );
	$anemos_eutf_caption_classes = array( 'eut-description', 'clearfix' );
	$anemos_eutf_title_meta_classes = array( 'eut-title-meta-content' );
	$anemos_eutf_content_classes = array( 'eut-title-content-wrapper' );

	$content_bg_color = anemos_eutf_array_value( $item, 'content_bg_color', 'light' );
	if ( 'custom' != $content_bg_color ) {
		$anemos_eutf_content_classes[] = 'eut-bg-' . $content_bg_color;
	}

	$subheading_color = anemos_eutf_array_value( $item, 'subheading_color', 'dark' );
	$title_color = anemos_eutf_array_value( $item, 'title_color', 'dark' );
	$caption_color = anemos_eutf_array_value( $item, 'caption_color', 'dark' );

	if ( 'custom' != $subheading_color ) {
		$anemos_eutf_subheading_classes[] = 'eut-text-' . $subheading_color;
		$anemos_eutf_title_meta_classes[] = 'eut-text-' . $subheading_color;
	}
	if ( 'custom' != $title_color ) {
		$anemos_eutf_title_classes[] = 'eut-text-' . $title_color;
	}
	if ( 'custom' != $caption_color ) {
		$anemos_eutf_caption_classes[] = 'eut-text-' . $caption_color;
	}

	$anemos_eutf_subheading_classes = implode( ' ', $anemos_eutf_subheading_classes );
	$anemos_eutf_title_classes = implode( ' ', $anemos_eutf_title_classes );
	$anemos_eutf_caption_classes = implode( ' ', $anemos_eutf_caption_classes );
	$anemos_eutf_title_meta_classes = implode( ' ', $anemos_eutf_title_meta_classes );
	$anemos_eutf_content_classes = implode( ' ', $anemos_eutf_content_classes );

	$content_position = anemos_eutf_array_value( $item, 'content_position', 'center-center' );
	$content_animation = anemos_eutf_array_value( $item, 'content_animation', 'fade-in' );

	$button = anemos_eutf_array_value( $item, 'button' );
	$button2 = anemos_eutf_array_value( $item, 'button2' );

	$button_text = anemos_eutf_array_value( $button, 'text' );
	$button_text2 = anemos_eutf_array_value( $button2, 'text' );

	$slide_type = anemos_eutf_array_value( $item, 'type' );
	$slide_post_id = anemos_eutf_array_value( $item, 'post_id' );
	if( 'post' == $slide_type &&  !empty( $slide_post_id ) ) {
		$title = get_the_title ( $slide_post_id  );
		$caption = get_post_meta( $slide_post_id, '_anemos_eutf_description', true );
		$link_url = get_permalink( $slide_post_id ) ;
	}

?>
	<div class="eut-content eut-align-<?php echo esc_attr( $content_position ); ?>" data-animation="<?php echo esc_attr( $content_animation ); ?>">
		<div class="eut-container">
			<div class="<?php echo esc_attr( $anemos_eutf_content_classes ); ?>">
			<?php if( 'post' == $slide_type &&  !empty( $slide_post_id ) ) { ?>
				<div class="<?php echo esc_attr( $anemos_eutf_subheading_classes ); ?>">
					<?php anemos_eutf_print_post_title_categories( $slide_post_id ); ?>
				</div>
				<a href="<?php echo esc_url( $link_url ); ?>">
					<<?php echo tag_escape( $title_tag ); ?> class="<?php echo esc_attr( $anemos_eutf_title_classes ); ?>"><span><?php echo wp_kses_post( $title ); ?></span></<?php echo tag_escape( $title_tag ); ?>>
				</a>
				<div class="eut-line-divider"><span></span></div>
				<?php if ( !empty( $caption ) ) { ?>
				<<?php echo tag_escape( $caption_tag ); ?> class="<?php echo esc_attr( $anemos_eutf_caption_classes ); ?>"><span><?php echo wp_kses_post( $caption ); ?></span></<?php echo tag_escape( $caption_tag ); ?>>
				<?php } ?>
				<div class="<?php echo esc_attr( $anemos_eutf_title_meta_classes ); ?>">
				<?php anemos_eutf_print_feature_post_title_meta( $slide_post_id ); ?>
				</div>
			<?php } else { ?>
				<?php anemos_eutf_print_feature_content_image( $item ); ?>
				<?php if ( !empty( $subheading ) ) { ?>
				<<?php echo tag_escape( $subheading_tag ); ?> class="<?php echo esc_attr( $anemos_eutf_subheading_classes ); ?>"><span><?php echo wp_kses_post( $subheading ); ?></span></<?php echo tag_escape( $subheading_tag ); ?>>
				<?php } ?>
				<?php if ( !empty( $title ) ) { ?>
				<<?php echo tag_escape( $title_tag ); ?> class="<?php echo esc_attr( $anemos_eutf_title_classes ); ?>"><span><?php echo wp_kses_post( $title ); ?></span></<?php echo tag_escape( $title_tag ); ?>>
				<div class="eut-line-divider"><span></span></div>
				<?php } ?>
				<?php if ( !empty( $caption ) ) { ?>
				<<?php echo tag_escape( $caption_tag ); ?> class="<?php echo esc_attr( $anemos_eutf_caption_classes ); ?>"><span><?php echo wp_kses_post( $caption ); ?></span></<?php echo tag_escape( $caption_tag ); ?>>
				<?php } ?>

				<?php
					if( 'title' != $mode && ( !empty( $button_text ) || !empty( $button_text2 ) ) ) {
					$btn1_class = $btn2_class = 'eut-btn-1';
					if ( !empty( $button_text ) && !empty( $button_text2 ) ) {
						$btn2_class = 'eut-btn-2';
					}
				?>
				<div class="eut-button-wrapper">
					<?php anemos_eutf_print_feature_button( $button, $btn1_class ); ?>
					<?php anemos_eutf_print_feature_button( $button2, $btn2_class ); ?>
				</div>
				<?php
					}
				?>

			<?php } ?>
			</div>
		</div>
	</div>
<?php
}

/**
 * Prints Header Feature Button
 */
function anemos_eutf_print_feature_button( $item, $extra_class = 'eut-btn-1' ) {

	$button_id = anemos_eutf_array_value( $item, 'id' );
	$button_text = anemos_eutf_array_value( $item, 'text' );
	$button_url = anemos_eutf_array_value( $item, 'url' );
	$button_type = anemos_eutf_array_value( $item, 'type' );
	$button_size = anemos_eutf_array_value( $item, 'size', 'medium' );
	$button_color = anemos_eutf_array_value( $item, 'color', 'primary-1' );
	$button_hover_color = anemos_eutf_array_value( $item, 'hover_color', 'black' );
	$button_shape = anemos_eutf_array_value( $item, 'shape', 'square' );
	$button_target = anemos_eutf_array_value( $item, 'target', '_self' );
	$button_class = anemos_eutf_array_value( $item, 'class' );

	if ( !empty( $button_text ) ) {

		//Button Classes
		$button_classes = array( 'eut-btn' );

		$button_classes[] = $extra_class;
		$button_classes[] = 'eut-btn-' . $button_size;
		$button_classes[] = 'eut-' . $button_shape;
		if ( 'outline' == $button_type ) {
			$button_classes[] = 'eut-btn-line';
		}
		if ( !empty( $button_class ) ) {
			$button_classes[] = $button_class;
		}
		$button_classes[] = 'eut-bg-' . $button_color;
		$button_classes[] = 'eut-bg-hover-' . $button_hover_color;

		$button_class_string = implode( ' ', $button_classes );

		if ( !empty( $button_url ) ) {
			$url = $button_url;
			$target = $button_target;
		} else {
			$url = "#";
			$target= "_self";
		}

?>
		<a class="<?php echo esc_attr( $button_class_string ); ?>" href="<?php echo esc_url( $url ); ?>"  target="<?php echo esc_attr( $target ); ?>">
			<span><?php echo esc_html( $button_text ); ?></span>
		</a>
<?php

	}

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
