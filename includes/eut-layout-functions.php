<?php

/*
*	Layout Helper functions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

/**
 * Function to fetch sidebar class
 */
function anemos_eutf_sidebar_class( $sidebar_view = '' ) {

	if( is_search() ) {
		return '';
	}
	$anemos_eutf_sidebar_class = "";
	$anemos_eutf_sidebar_extra_content = false;

	if ( 'shop' == $sidebar_view ) {
		if ( is_shop() ) {
			$anemos_eutf_sidebar_id = anemos_eutf_post_meta_shop( '_anemos_eutf_sidebar', anemos_eutf_option( 'page_sidebar' ) );
			$anemos_eutf_sidebar_layout = anemos_eutf_post_meta_shop( '_anemos_eutf_layout', anemos_eutf_option( 'page_layout', 'none' ) );
		} else if( is_product() ) {
			$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidebar', anemos_eutf_option( 'product_sidebar' ) );
			$anemos_eutf_sidebar_layout = anemos_eutf_post_meta( '_anemos_eutf_layout', anemos_eutf_option( 'product_layout', 'none' ) );
		} else {
			$anemos_eutf_sidebar_id = anemos_eutf_option( 'product_tax_sidebar' );
			$anemos_eutf_sidebar_layout = anemos_eutf_option( 'product_tax_layout', 'none' );
		}
	} else if ( is_singular() ) {
		if ( is_singular( 'post' ) ) {
			$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidebar', anemos_eutf_option( 'post_sidebar' ) );
			$anemos_eutf_sidebar_layout = anemos_eutf_post_meta( '_anemos_eutf_layout', anemos_eutf_option( 'post_layout', 'none' ) );
		} else if ( is_singular( 'portfolio' ) ) {
			$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidebar', anemos_eutf_option( 'portfolio_sidebar' ) );
			$anemos_eutf_sidebar_layout = anemos_eutf_post_meta( '_anemos_eutf_layout', anemos_eutf_option( 'portfolio_layout', 'none' ) );
			$anemos_eutf_sidebar_extra_content = anemos_eutf_check_portfolio_details();
			if( $anemos_eutf_sidebar_extra_content && 'none' == $anemos_eutf_sidebar_layout ) {
				$anemos_eutf_sidebar_layout = 'right';
			}
		} else {
			$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidebar', anemos_eutf_option( 'page_sidebar' ) );
			$anemos_eutf_sidebar_layout = anemos_eutf_post_meta( '_anemos_eutf_layout', anemos_eutf_option( 'page_layout', 'none' ) );
		}
	} else {
		$anemos_eutf_sidebar_id = anemos_eutf_option( 'blog_sidebar' );
		$anemos_eutf_sidebar_layout = anemos_eutf_option( 'blog_layout', 'none' );
	}

	if ( 'none' != $anemos_eutf_sidebar_layout && ( is_active_sidebar( $anemos_eutf_sidebar_id ) || $anemos_eutf_sidebar_extra_content ) ) {

		if ( 'right' == $anemos_eutf_sidebar_layout ) {
			$anemos_eutf_sidebar_class = 'eut-right-sidebar';
		} else if ( 'left' == $anemos_eutf_sidebar_layout ) {
			$anemos_eutf_sidebar_class = 'eut-left-sidebar';
		}

	}

	return $anemos_eutf_sidebar_class;

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
