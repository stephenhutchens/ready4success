<?php
/**
 * Plugin Name: Euthemians Contact Info
 * Description: A widget that displays Contact Info e.g: Address, Phone number.etc.
 * @author		Euthemians Team
 * @URI			http://euthemians.com
 */

add_action( 'widgets_init', 'anemos_eutf_widget_contact_info' );

function anemos_eutf_widget_contact_info() {
	register_widget( 'Anemos_EUTF_Widget_Contact_Info' );
}

class Anemos_EUTF_Widget_Contact_Info extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'eut-contact-info',
			'description' => esc_html__( 'A widget that displays contact info', 'anemos' ),
		);
		$control_ops = array(
			'width' => 300,
			'height' => 400,
			'id_base' => 'eut-widget-contact-info',
		);
		parent::__construct( 'eut-widget-contact-info', '(Euthemians) ' . esc_html__( 'Contact Info', 'anemos' ), $widget_ops, $control_ops );
	}

	function Anemos_EUTF_Widget_Contact_Info() {
		$this->__construct();
	}

	function widget( $args, $instance ) {

		$anemos_eutf_microdata_allowed_html = anemos_eutf_get_microdata_allowed_html();

		//Our variables from the widget settings.
		extract( $args );

		//Our variables from the widget settings.
		//$contact_info_name = apply_filters('wpml_translate_single_string', $instance['name'], 'Widgets', '(Euthemians) Contact Info Widget - Name' );
		//$contact_info_address = apply_filters('wpml_translate_single_string', $instance['address'], 'Widgets', '(Euthemians) Contact Info Widget - Address' );
		//$contact_info_phone = apply_filters('wpml_translate_single_string', $instance['phone'], 'Widgets', '(Euthemians) Contact Info Widget - Phone' );
		//$contact_info_mobile = apply_filters('wpml_translate_single_string', $instance['mobile'], 'Widgets', '(Euthemians) Contact Info Widget - Mobile Phone' );
		//$contact_info_fax = apply_filters('wpml_translate_single_string', $instance['fax'], 'Widgets', '(Euthemians) Contact Info Widget - Fax' );
		//$contact_info_mail = apply_filters('wpml_translate_single_string', $instance['mail'], 'Widgets', '(Euthemians) Contact Info Widget - Mail' );
		//$contact_info_web = apply_filters('wpml_translate_single_string', $instance['web'], 'Widgets', '(Euthemians) Contact Info Widget - Website' );
		$contact_info_name = $instance['name'];
		$contact_info_address = $instance['address'];
		$contact_info_phone = $instance['phone'];
		$contact_info_mobile = $instance['mobile'];
		$contact_info_fax = $instance['fax'];
		$contact_info_mail = $instance['mail'];
		$contact_info_web = $instance['web'];
		$microdata = anemos_eutf_array_value( $instance, 'microdata' );


		echo $before_widget; // XSS OK

		// Display the widget title
		$title = apply_filters( 'widget_title', $instance['title'] );
		if ( $title ) {
			echo $before_title . esc_html( $title ) . $after_title; // XSS OK
		}

		if ( !empty( $microdata ) ) {
			echo '<div itemscope itemtype="http://schema.org/' . esc_attr( $microdata ) . '">';
		}
		?>

		<ul>

			<?php if ( ! empty( $contact_info_name ) ) { ?>
			<li>
				<i class="fa fa-user"></i>
				<?php if ( !empty( $microdata ) ) { ?>
				<div class="eut-info-content" itemprop="name">
				<?php } else { ?>
				<div class="eut-info-content">
				<?php } ?>
					<?php echo esc_html( $contact_info_name ); ?>
				</div>
			</li>
			<?php } ?>

			<?php if ( ! empty( $contact_info_address ) ) { ?>
			<li>
				<i class="fa fa-home"></i>
				<?php if ( !empty( $microdata ) ) { ?>
				<div class="eut-info-content" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<?php } else { ?>
				<div class="eut-info-content">
				<?php } ?>
					<?php echo wp_kses( $contact_info_address, $anemos_eutf_microdata_allowed_html ); ?>
				</div>
			</li>
			<?php } ?>

			<?php if ( ! empty( $contact_info_phone ) ) { ?>
			<li>
				<i class="fa fa-phone"></i>
				<?php if ( !empty( $microdata ) ) { ?>
				<div class="eut-info-content" itemprop="telephone">
				<?php } else { ?>
				<div class="eut-info-content">
				<?php } ?>
					<?php echo esc_html( $contact_info_phone ); ?>
				</div>
			</li>
			<?php } ?>

			<?php if ( ! empty( $contact_info_mobile ) ) { ?>
			<li>
				<i class="fa fa-mobile"></i>
				<?php if ( !empty( $microdata ) ) { ?>
				<div class="eut-info-content" itemprop="telephone">
				<?php } else { ?>
				<div class="eut-info-content">
				<?php } ?>
					<?php echo esc_html( $contact_info_mobile ); ?>
				</div>
			</li>
			<?php } ?>

			<?php if ( ! empty( $contact_info_fax ) ) { ?>
			<li>
				<i class="fa fa-fax"></i>
				<?php if ( !empty( $microdata ) ) { ?>
				<div class="eut-info-content" itemprop="faxNumber">
				<?php } else { ?>
				<div class="eut-info-content">
				<?php } ?>
					<?php echo esc_html( $contact_info_fax ); ?>
				</div>
			</li>
			<?php } ?>

			<?php if ( ! empty( $contact_info_mail ) ) { ?>
			<li>
				<i class="fa fa-envelope"></i>
				<?php if ( !empty( $microdata ) ) { ?>
				<div class="eut-info-content" itemprop="email">
				<?php } else { ?>
				<div class="eut-info-content">
				<?php } ?>
					<a href="mailto:<?php echo antispambot( $contact_info_mail ); ?>"><?php echo antispambot( $contact_info_mail ); ?></a>
				</div>
			</li>
			<?php } ?>

			<?php if ( ! empty( $contact_info_web ) ) { ?>
			<li>
				<i class="fa fa-link"></i>
				<div class="eut-info-content">
					<a href="<?php echo esc_url( $contact_info_web ); ?>" target="_blank"><?php echo esc_html( $contact_info_web ); ?></a>
				</div>
			</li>
			<?php } ?>
		</ul>


		<?php

		if ( !empty( $microdata ) ) {
			echo '</div>';
		}
		echo $after_widget; // XSS OK
	}

	//Update the widget

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		//Strip tags from title and name to remove HTML
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['name'] = strip_tags( $new_instance['name'] );
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = strip_tags( $new_instance['phone'] );
		$instance['mobile'] = strip_tags( $new_instance['mobile'] );
		$instance['fax'] = strip_tags( $new_instance['fax'] );
		$instance['mail'] = strip_tags( $new_instance['mail'] );
		$instance['web'] = strip_tags( $new_instance['web'] );
		$instance['microdata'] = strip_tags( $new_instance['microdata'] );

		//WMPL
		/**
		 * register strings for translation
		 */
		do_action( 'wpml_register_single_string', 'Widgets', '(Euthemians) Contact Info Widget - Name', $instance['name'] );
		do_action( 'wpml_register_single_string', 'Widgets', '(Euthemians) Contact Info Widget - Address', $instance['address'] );
		do_action( 'wpml_register_single_string', 'Widgets', '(Euthemians) Contact Info Widget - Phone', $instance['phone'] );
		do_action( 'wpml_register_single_string', 'Widgets', '(Euthemians) Contact Info Widget - Mobile Phone', $instance['mobile'] );
		do_action( 'wpml_register_single_string', 'Widgets', '(Euthemians) Contact Info Widget - Fax', $instance['fax'] );
		do_action( 'wpml_register_single_string', 'Widgets', '(Euthemians) Contact Info Widget - Mail', $instance['mail'] );
		do_action( 'wpml_register_single_string', 'Widgets', '(Euthemians) Contact Info Widget - Website', $instance['web'] );

		return $instance;
	}


	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array(
			'title' => '',
			'name' => '',
			'address' => '',
			'phone' => '',
			'mobile' => '',
			'fax' => '',
			'mail' => '',
			'web' => '',
			'microdata' => '',
		);

		$anemos_eutf_microdata_allowed_html = anemos_eutf_get_microdata_allowed_html();

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'name' ) ); ?>"><?php esc_html_e( 'Name:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'name' ) ); ?>" value="<?php echo esc_attr( $instance['name'] ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>"><?php esc_html_e( 'Address:', 'anemos' ); ?> <?php esc_html_e( '( Allowed tags: span, br )', 'anemos' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" style="width:100%;"><?php echo wp_kses( $instance['address'] , $anemos_eutf_microdata_allowed_html ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>"><?php esc_html_e( 'Phone:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" value="<?php echo esc_attr( $instance['phone'] ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'mobile' ) ); ?>"><?php esc_html_e( 'Mobile Phone:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'mobile' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'mobile' ) ); ?>" value="<?php echo esc_attr( $instance['mobile'] ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>"><?php esc_html_e( 'Fax:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'fax' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'fax' ) ); ?>" value="<?php echo esc_attr( $instance['fax'] ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'mail' ) ); ?>"><?php esc_html_e( 'Mail:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'mail' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'mail' ) ); ?>" value="<?php echo esc_attr( $instance['mail'] ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'web' ) ); ?>"><?php esc_html_e( 'Website:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'web' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'web' ) ); ?>" value="<?php echo esc_attr( $instance['web'] ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'microdata' ) ); ?>"><?php echo esc_html__( 'Microdata ( Schema.org ):', 'anemos' ); ?></label>
			<select id="<?php echo esc_attr( $this->get_field_id('microdata') ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'microdata' ) ); ?>" style="width:100%;">
				<?php $microdata = $instance['microdata']; ?>
				<option value="" <?php selected( '', $microdata ); ?>><?php echo esc_html__( 'None', 'anemos' ); ?></option>
				<option value="Person" <?php selected( 'Person', $microdata ); ?>><?php esc_html_e( 'Person', 'anemos' ); ?></option>
				<option value="Organization" <?php selected( 'Organization', $microdata ); ?>><?php esc_html_e( 'Organization', 'anemos' ); ?></option>
				<option value="Corporation" <?php selected( 'Corporation', $microdata ); ?>><?php esc_html_e( 'Corporation', 'anemos' ); ?></option>
				<option value="EducationalOrganization" <?php selected( 'EducationalOrganization', $microdata ); ?>><?php esc_html_e( 'School', 'anemos' ); ?></option>
				<option value="GovernmentOrganization" <?php selected( 'GovernmentOrganization', $microdata ); ?>><?php esc_html_e( 'Government', 'anemos' ); ?></option>
				<option value="LocalBusiness" <?php selected( 'LocalBusiness', $microdata ); ?>><?php esc_html_e( 'Local Business', 'anemos' ); ?></option>
				<option value="NGO" <?php selected( 'NGO', $microdata ); ?>><?php esc_html_e( 'NGO', 'anemos' ); ?></option>
				<option value="PerformingGroup" <?php selected( 'PerformingGroup', $microdata ); ?>><?php esc_html_e( 'Performing Group', 'anemos' ); ?></option>
				<option value="SportsTeam" <?php selected( 'SportsTeam', $microdata ); ?>><?php esc_html_e( 'Sports Team', 'anemos' ); ?></option>
			</select>
		</p>

	<?php
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
