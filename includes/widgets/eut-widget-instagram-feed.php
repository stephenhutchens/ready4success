<?php
/**
 * Plugin Name: Euthemians Instagram Feed
 * Description: A widget that displays latest posts.
 * @author		Euthemians Team
 * @URI			http://euthemians.com
 */

add_action( 'widgets_init', 'anemos_eutf_widget_instagram_feed' );

function anemos_eutf_widget_instagram_feed() {
	register_widget( 'Anemos_EUTF_Widget_Instagram_Feed' );
}

class Anemos_EUTF_Widget_Instagram_Feed extends WP_Widget {

	function __construct() {
		$widget_ops = array(
			'classname' => 'eut-instagram-feed',
			'description' => esc_html__( 'A widget that displays instagram feed', 'anemos'),
		);
		$control_ops = array(
			'width' => 300,
			'height' => 400,
			'id_base' => 'eut-widget-instagram-feed',
		);
		parent::__construct( 'eut-widget-instagram-feed', '(Euthemians) ' . esc_html__( 'Instagram Feed', 'anemos' ), $widget_ops, $control_ops );
	}

	function Anemos_EUTF_Widget_Instagram_Feed() {
		$this->__construct();
	}

	function widget( $args, $instance ) {

		//Our variables from the widget settings.
		extract( $args );

		$username = $instance['username'];
		$limit = $instance['limit'];
		$order_by = $instance['order_by'];
		$order = $instance['order'];
		$target = $instance['target'];
		$cache = $instance['cache'];

		$access_token = $instance['access_token'];
		$user_id = $instance['user_id'];

		if( !isset( $cache ) ) {
			$cache = '';
		}

		if( empty( $limit ) ) {
			$limit = 9;
		}

		echo $before_widget; // XSS OK

		// Display the widget title
		$title = apply_filters( 'widget_title', $instance['title'] );
		if ( $title ) {
			echo $before_title . esc_html( $title ) . $after_title; // XSS OK
		}

		if ( !empty( $username ) ) {

			$media_array = $this->anemos_eutf_get_instagram_array( $username, $limit, $order_by, $order, $cache, $access_token, $user_id );
			$output = '';

			if ( is_wp_error( $media_array ) ) {

			   echo wp_kses_post( $media_array->get_error_message() );

			} else {

			?>
				<ul class="eut-instagram-images">
			<?php
				foreach ($media_array as $item) {
			?>
					<li>
						<div class="eut-item-wrapper">
							<div class="eut-bg-wrapper eut-small-square">
								<div class="eut-bg-image" style="background-image: url(<?php echo esc_url( $item['thumbnail']['url'] ); ?>);"></div>
							</div>
							<a href="<?php echo esc_url( $item['link'] ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
							<img width="150" height="150" src="<?php echo esc_url( $item['thumbnail']['url'] ); ?>"  alt="<?php echo esc_attr( $item['description'] ); ?>" title="<?php echo esc_attr( $item['description'] ); ?>"/>
						</div>
					</li>
			<?php
				}
			?>
				</ul>
			<?php
			}
		}

		echo $after_widget; // XSS OK
	}

	//Get instagram array
	function anemos_eutf_get_instagram_array( $username, $limit, $order_by, $order, $cache = "", $access_token = "", $user_id = ""  ) {

		$username = strtolower( $username );
		$transient_string = $access_token .'-'. $username .'-'. $order_by .'-'. $order;

		if ( false === ( $instagram = get_transient('eut-instagram-feed-v2-'.sanitize_title_with_dashes( $transient_string ) ) ) || empty( $cache ) ) {

			if( !empty( $user_id ) && !empty( $access_token ) ) {
				$url = 'https://api.instagram.com/v1/users/' . $user_id . '/media/recent/?access_token=' . $access_token;
			} else {
				$url = 'https://www.instagram.com/' . $username . '/media/';
			}
			$remote = wp_remote_get( $url );

			if ( is_wp_error( $remote ) ) {
				if( current_user_can( 'administrator' ) ) {
					return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram!', 'anemos' ) );
				} else {
					return new WP_Error('site_down', '' );
				}
			}
  			if ( 200 != wp_remote_retrieve_response_code( $remote ) ) {
				if( current_user_can( 'administrator' ) ) {
					return new WP_Error('invalid_response', esc_html__( 'Instagram invalid response!', 'anemos' ) );
				} else {
					return new WP_Error('invalid_response', '' );
				}
			}

			$insta_array = json_decode( $remote['body'], TRUE );

			if( !empty( $user_id ) && !empty( $access_token ) ) {
				$images = isset( $insta_array['data'] ) ? $insta_array['data'] : array();
			} else {
				$images = isset( $insta_array['items'] ) ? $insta_array['items'] : array();
			}

			$instagram = array();

			foreach ( $images as $image ) {

				if ($image['user']['username'] == $username) {

					$image['link']                          = preg_replace( "/^http:/i", "", $image['link'] );
					$image['images']['thumbnail']           = preg_replace( "/^http:/i", "", $image['images']['thumbnail'] );
					$image['images']['standard_resolution'] = preg_replace( "/^http:/i", "", $image['images']['standard_resolution'] );

					$instagram[] = array(
						'description'   => $image['caption']['text'],
						'link'          => $image['link'],
						'time'          => $image['created_time'],
						'comments'      => $image['comments']['count'],
						'likes'         => $image['likes']['count'],
						'thumbnail'     => $image['images']['thumbnail'],
						'large'         => $image['images']['standard_resolution'],
						'type'          => $image['type']
					);
				}
			}

			//Instagram Order
			if ( 'none' != $order_by ) {
				foreach ($instagram as $key => $row) {
					$time[$key] = $row['time'];
					$comments[$key]  = $row['comments'];
					$likes[$key] = $row['likes'];
				}
				if ( 'ASC' == $order ) {
					$order = SORT_ASC;
				} else {
					$order = SORT_DESC;
				}
				if ( 'datetime' == $order_by ) {
					$order_by = $time;
				} elseif ( 'comments' == $order_by ) {
					$order_by = $comments;
				} elseif ( 'likes' == $order_by ) {
					$order_by = $likes;
				}
				array_multisort( $order_by, $order, $instagram );
			}

			if( !empty( $cache ) ) {
				set_transient('eut-instagram-feed-v2-'.sanitize_title_with_dashes( $transient_string ), $instagram, apply_filters( 'anemos_eutf_instagram_cache_time', HOUR_IN_SECONDS ) );
			}
		}

		return array_slice( $instagram, 0, $limit );
	}


	//Update the widget

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		//Strip tags from title and name to remove HTML
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['username'] = strip_tags( $new_instance['username'] );
		$instance['limit'] = strip_tags( $new_instance['limit'] );
		$instance['order_by'] = strip_tags( $new_instance['order_by'] );
		$instance['order'] = strip_tags( $new_instance['order'] );
		$instance['target'] = strip_tags( $new_instance['target'] );
		$instance['cache'] = strip_tags( $new_instance['cache'] );
		$instance['access_token'] = strip_tags( $new_instance['access_token'] );
		$instance['user_id'] = strip_tags( $new_instance['user_id'] );

		return $instance;
	}


	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array(
			'title' => '',
			'username' => '',
			'limit' => '9',
			'order_by' => 'none',
			'order' => 'ASC',
			'target' => '_blank',
			'cache' => '1',
			'access_token' => '',
			'user_id' => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$title = esc_attr($instance['title']);
		$limit = absint($instance['limit']);
		$order_by = esc_attr($instance['order_by']);
		$order = esc_attr($instance['order']);
		$target = esc_attr($instance['target']);
		$cache = esc_attr($instance['cache']);

		$access_token = $instance['access_token'];
		$user_id = $instance['user_id'];
		$username = $instance['username'];
		$access_token_url = "http://euthemians.com/instagram-feed/";

		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'access_token' ) ); ?>"><?php esc_html_e( 'Access Token:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'access_token' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'access_token' ) ); ?>" value="<?php echo esc_attr( $access_token ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'user_id' ) ); ?>"><?php esc_html_e( 'User ID:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'user_id' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'user_id' ) ); ?>" value="<?php echo esc_attr( $user_id ); ?>" style="width:100%;" />
		</p>
		<p>
			<a href="<?php echo esc_url( $access_token_url ); ?>" target="_blank"><?php esc_html_e( 'Get Access Token and User ID', 'anemos' ); ?></a>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>"><?php esc_html_e( 'Username:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>" value="<?php echo esc_attr( $username ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $title ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>"><?php echo esc_html__( 'Number of Images:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" value="<?php echo esc_attr( $limit ); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'order_by' ) ); ?>"><?php echo esc_html__( 'Order By:', 'anemos' ); ?></label>
			<select id="<?php echo esc_attr( $this->get_field_id('order_by') ); ?>" name="<?php echo esc_attr( $this->get_field_name('order_by') ); ?>" style="width:100%;">
				<option value="none" <?php selected('none', $order_by) ?>><?php esc_html_e( 'None', 'anemos' ); ?></option>
				<option value="datetime" <?php selected('datetime', $order_by) ?>><?php esc_html_e( 'Recent', 'anemos' ); ?></option>
				<option value="likes" <?php selected('likes', $order_by) ?>><?php esc_html_e( 'Likes', 'anemos' ); ?></option>
				<option value="comments" <?php selected('comments', $order_by) ?>><?php esc_html_e( 'Comments', 'anemos' ); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>"><?php echo esc_html__( 'Order:', 'anemos' ); ?></label>
			<select id="<?php echo esc_attr( $this->get_field_id('order') ); ?>" name="<?php echo esc_attr( $this->get_field_name('order') ); ?>" style="width:100%;">
				<option value="ASC" <?php selected('ASC', $order) ?>><?php esc_html_e( 'Ascending', 'anemos' ); ?></option>
				<option value="DESC" <?php selected('DESC', $order) ?>><?php esc_html_e( 'Descending', 'anemos' ); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>"><?php echo esc_html__( 'Link Target:', 'anemos' ); ?></label>
			<select id="<?php echo esc_attr( $this->get_field_id('target') ); ?>" name="<?php echo esc_attr( $this->get_field_name('target') ); ?>" style="width:100%;">
				<option value="_self" <?php selected('_self', $target) ?>><?php esc_html_e( 'Same Page', 'anemos' ); ?></option>
				<option value="_blank" <?php selected('_blank', $target) ?>><?php esc_html_e( 'New Page', 'anemos' ); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'cache' ) ); ?>"><?php echo esc_html__( 'Caching:', 'anemos' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id('cache') ); ?>" name="<?php echo esc_attr( $this->get_field_name('cache') ); ?>" type="checkbox" value="1" <?php checked( $cache, 1 ); ?> />
		</p>
		<p>
			<em><?php echo esc_html__( 'Note: Uncheck caching if you want to test your configuration. It is recommended to leave caching enabled to increase performance. Caching timeout is 60 minutes.', 'anemos' ); ?></em>
		</p>

	<?php
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
