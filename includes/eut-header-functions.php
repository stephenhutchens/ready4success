<?php

/*
*	Header Helper functions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

 /**
 * Print Logo
 */
function anemos_eutf_print_logo( $mode = 'default', $align = '' ) {

	if ( !empty( $align ) ) {
		$align = 'eut-position-' . $align;
	}
	$anemos_eutf_disable_logo = '';
	if ( is_singular() ) {
		$anemos_eutf_disable_logo = anemos_eutf_post_meta( '_anemos_eutf_disable_logo', $anemos_eutf_disable_logo );
	} else if( anemos_eutf_is_woo_shop() ) {
		$anemos_eutf_disable_logo = anemos_eutf_post_meta_shop( '_anemos_eutf_disable_logo', $anemos_eutf_disable_logo );
	}

	if ( 'yes' != $anemos_eutf_disable_logo ) {

		if ( anemos_eutf_visibility( 'logo_as_text_enabled' ) ) {
?>
		<!-- Logo As Text-->
		<div class="eut-logo eut-logo-text <?php echo esc_attr( $align ); ?>">
			<div class="eut-wrapper">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></a>
			</div>
		</div>
<?php
		} else {
?>
		<!-- Logo -->
		<div class="eut-logo <?php echo esc_attr( $align ); ?>">
			<div class="eut-wrapper">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
<?php
				switch( $mode ) {
					case 'side':
						anemos_eutf_print_logo_data( 'logo_side', 'eut-logo-side' );
					break;
					case 'responsive':
						anemos_eutf_print_logo_data( 'logo_responsive', 'eut-logo-responsive'  );
					break;
					case 'anemos-sticky':
						anemos_eutf_print_logo_data( 'logo_sticky', 'eut-sticky');
					break;
					default:
						anemos_eutf_print_logo_data( 'logo', 'eut-default');
						anemos_eutf_print_logo_data( 'logo_light', 'eut-light');
						anemos_eutf_print_logo_data( 'logo_dark', 'eut-dark');
						anemos_eutf_print_logo_data( 'logo_sticky', 'eut-sticky');
					break;
				}
?>
				</a>
			</div>
		</div>
		<!-- End Logo -->
<?php
		}
	}
}

 /**
 * Get Logo Data
 */
function anemos_eutf_print_logo_data( $logo_id, $logo_class ) {

	$logo_url = anemos_eutf_option( $logo_id, '', 'url' );

	$logo_attributes = array();
	$logo_attributes[] = 'data-no-retina=""';
	$logo_width = anemos_eutf_option( $logo_id, '', 'width' );
	$logo_height = anemos_eutf_option( $logo_id, '', 'height' );

	if ( !empty( $logo_width ) && !empty( $logo_height ) ) {
		$logo_attributes[] = 'width="' . esc_attr( $logo_width ) . '"';
		$logo_attributes[] = 'height="' . esc_attr( $logo_height ) . '"';
	}

	if ( !empty( $logo_url ) ) {
		$logo_url = str_replace( array( 'http:', 'https:' ), '', $logo_url );
?>
		<img class="<?php echo esc_attr( $logo_class ); ?>" src="<?php echo esc_url( $logo_url ); ?>" alt="<?php echo esc_attr( get_bloginfo('name') ); ?>" <?php echo implode( ' ', $logo_attributes ); ?>>
<?php
	}

}


 /**
 * Prints correct title/subtitle for all cases
 */
function anemos_eutf_header_title() {
	global $post;
	$page_title = $page_description = $page_reversed = '';

	//Shop
	if( anemos_eutf_woocommerce_enabled() ) {

		if ( is_shop() && !is_search() ) {
			$post_id = wc_get_page_id( 'shop' );
			$page_title   = get_the_title( $post_id );
			$page_description = get_post_meta( $post_id, '_anemos_eutf_description', true );
			return array(
				'title' => $page_title,
				'description' => $page_description,
			);
		} else if( is_product_taxonomy() ) {
			$page_title  = single_term_title("", false);
			$page_description = category_description();
			return array(
				'title' => $page_title,
				'description' => $page_description,
			);
		}
	}

	//Main Pages
	if ( is_front_page() && is_home() ) {
		// Default homepage
		$page_title = get_bloginfo( 'name' );
		$page_description = get_bloginfo( 'description' );
	} else if ( is_front_page() ) {
		// static homepage
		$page_title = get_bloginfo( 'name' );
		$page_description = get_bloginfo( 'description' );
	} else if ( is_home() ) {
		// blog page
		$page_title = get_bloginfo( 'name' );
		$page_description = get_bloginfo( 'description' );
	} else if( is_search() ) {
		$page_description = esc_html__( 'Search Results for :', 'anemos' );
		$page_title = esc_attr( get_search_query() );
		$page_reversed = 'reversed';
	} else if ( is_singular() ) {
		$post_id = $post->ID;
		$page_title = get_the_title();
		$page_description = get_post_meta( $post_id, '_anemos_eutf_description', true );
	} else if ( is_archive() ) {
		//Post Categories
		if ( is_category() ) {
			$page_title = single_cat_title("", false);
			$page_description = category_description();
		} else if ( is_tag() ) {
			$page_title = single_tag_title("", false);
			$page_description = tag_description();
		} else if ( is_tax() ) {
			$page_title = single_term_title("", false);
			$page_description = term_description();
		} else if ( is_author() ) {
			global $author;
			$userdata = get_userdata( $author );
			$page_description = esc_html__( "Posts By :", 'anemos' );
			$page_title = $userdata->display_name;
			$page_reversed = 'reversed';
		} else if ( is_day() ) {
			$page_description = esc_html__( "Daily Archives :", 'anemos' );
			$page_title = get_the_time( 'l, F j, Y' );
			$page_reversed = 'reversed';
		} else if ( is_month() ) {
			$page_description = esc_html__( "Monthly Archives :", 'anemos' );
			$page_title = get_the_time( 'F Y' );
			$page_reversed = 'reversed';
		} else if ( is_year() ) {
			$page_description = esc_html__( "Yearly Archives :", 'anemos' );
			$page_title = get_the_time( 'Y' );
			$page_reversed = 'reversed';
		} else {
			$page_title = esc_html__( "Archives", 'anemos' );
		}
	} else {
		$page_title = get_bloginfo( 'name' );
		$page_description = get_bloginfo( 'description' );
	}

	return array(
		'title' => $page_title,
		'description' => $page_description,
		'reversed' => $page_reversed,
	);


}

 /**
 * Check title visibility
 */
function anemos_eutf_check_title_visibility() {

	$blog_title = anemos_eutf_option( 'blog_title', 'sitetitle' );

	if ( is_front_page() && is_home() ) {
		// Default homepage
		if ( 'none' == $blog_title ) {
			return false;
		}
	} elseif ( is_front_page() ) {
		// static homepage
		if ( 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_title' ) || ( anemos_eutf_is_woo_shop() && 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_disable_title' ) ) ) {
			return false;
		}
	} elseif ( is_home() ) {
		// blog page
		if ( 'none' == $blog_title ) {
			return false;
		}
	} else {
		if ( ( is_singular() && 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_title' ) ) || ( anemos_eutf_is_woo_shop() && 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_disable_title' ) ) ) {
			return false;
		}
	}

	return true;

}

/**
 * Prints side Header Background Image
 */
 if ( !function_exists('anemos_eutf_print_side_header_bg_image') ) {

	function anemos_eutf_print_side_header_bg_image() {

		if ( 'custom' == anemos_eutf_option( 'header_side_bg_mode' ) ) {
			$anemos_eutf_header_custom_bg = array(
				'bg_mode' => 'custom',
				'bg_image_id' => anemos_eutf_option( 'header_side_bg_image', '', 'id' ),
				'bg_position' => anemos_eutf_option( 'header_side_bg_position', 'center-center' ),
				'pattern_overlay' => anemos_eutf_option( 'header_side_pattern_overlay' ),
				'color_overlay' => anemos_eutf_option( 'header_side_color_overlay' ),
				'opacity_overlay' => anemos_eutf_option( 'header_side_opacity_overlay' ),
			);
			anemos_eutf_print_title_bg_image( $anemos_eutf_header_custom_bg );
		}

	}
}

function anemos_eutf_print_title_bg_image( $anemos_eutf_page_title = array() ) {

	$image_url = '';
	$bg_mode = anemos_eutf_array_value( $anemos_eutf_page_title, 'bg_mode', 'color' );

	if ( 'color' != $bg_mode ) {

		$bg_position = anemos_eutf_array_value( $anemos_eutf_page_title, 'bg_position', 'center-center' );

		$media_id = '0';



		if ( 'featured' == $bg_mode ) {
			$anemos_eutf_woo_shop = anemos_eutf_is_woo_shop();
			if ( is_singular() || $anemos_eutf_woo_shop ) {
				if ( $anemos_eutf_woo_shop ) {
					$id = wc_get_page_id( 'shop' );
				} else {
					$id = get_the_ID();
				}
				if( has_post_thumbnail( $id ) ) {
					$media_id = get_post_thumbnail_id( $id );
				}
			}
		} else if ( 'custom' ) {
			$media_id = anemos_eutf_array_value( $anemos_eutf_page_title, 'bg_image_id' );
		}
		$full_src = wp_get_attachment_image_src( $media_id, 'anemos-eutf-fullscreen' );
		$image_url = $full_src[0];

		if( !empty( $image_url ) ) {
			echo '<div class="eut-background-wrapper">';
			anemos_eutf_print_overlay_container( $anemos_eutf_page_title );
			echo '<div class="eut-bg-image eut-bg-' . esc_attr( $bg_position ) . '" style="background-image: url(' . esc_url( $image_url ) . ');"></div>';
			echo '</div>';
		}
	}

}

 /**
 * Prints title/subtitle ( Page )
 */
function anemos_eutf_print_header_title( $mode = 'page') {
	global $post;

	if ( anemos_eutf_check_title_visibility() ) {

        $item_type = str_replace ( '_' , '-', $mode );
		$anemos_eutf_page_title_id = 'eut-' . $item_type  . '-title';
		$anemos_eutf_page_title = array(
			'height' => anemos_eutf_option( $mode . '_title_height' ),
			'min_height' => anemos_eutf_option( $mode . '_title_min_height' ),
			'subheading_color' => anemos_eutf_option( $mode . '_subheading_color' ),
			'subheading_color_custom' => anemos_eutf_option( $mode . '_subheading_color_custom' ),
			'title_color' => anemos_eutf_option( $mode . '_title_color' ),
			'title_color_custom' => anemos_eutf_option( $mode . '_title_color_custom' ),
			'caption_color' => anemos_eutf_option( $mode . '_description_color' ),
			'caption_color_custom' => anemos_eutf_option( $mode . '_description_color_custom' ),
			'content_bg_color' => anemos_eutf_option( $mode . '_title_content_bg_color' ),
			'content_bg_color_custom' => anemos_eutf_option( $mode . '_title_content_bg_color_custom' ),
			'content_position' => anemos_eutf_option( $mode . '_title_content_position' ),
			'content_animation' => anemos_eutf_option( $mode . '_title_content_animation' ),
			'bg_mode' => anemos_eutf_option( $mode . '_title_bg_mode' ),
			'bg_image_id' => anemos_eutf_option( $mode . '_title_bg_image', '', 'id' ),
			'bg_position' => anemos_eutf_option( $mode . '_title_bg_position' ),
			'bg_color' => anemos_eutf_option( $mode . '_title_bg_color', 'dark' ),
			'bg_color_custom' => anemos_eutf_option( $mode . '_title_bg_color_custom' ),
			'pattern_overlay' => anemos_eutf_option( $mode . '_title_pattern_overlay' ),
			'color_overlay' => anemos_eutf_option( $mode . '_title_color_overlay' ),
			'color_overlay_custom' => anemos_eutf_option( $mode . '_title_color_overlay_custom' ),
			'opacity_overlay' => anemos_eutf_option( $mode . '_title_opacity_overlay' ),
		);

		$header_data = anemos_eutf_header_title();
		$header_title = isset( $header_data['title'] ) ? $header_data['title'] : '';
		$header_description = isset( $header_data['description'] ) ? $header_data['description'] : '';
		$header_reversed = isset( $header_data['reversed'] ) ? $header_data['reversed'] : '';

		$anemos_eutf_woo_shop = anemos_eutf_is_woo_shop();

		if ( is_singular() || $anemos_eutf_woo_shop  ) {
			if ( $anemos_eutf_woo_shop ) {
				$post_id = wc_get_page_id( 'shop' );
			} else {
				$post_id = $post->ID;
			}

			$anemos_eutf_custom_title_options = get_post_meta( $post_id, '_anemos_eutf_custom_title_options', true );
			$anemos_eutf_title_style = anemos_eutf_option( $mode . '_title_style' );
			$anemos_eutf_page_title_custom = anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'custom', $anemos_eutf_title_style );
			if ( 'custom' == $anemos_eutf_page_title_custom ) {
				$anemos_eutf_page_title = $anemos_eutf_custom_title_options;
			} else if ( 'simple' == $anemos_eutf_page_title_custom ) {
				return;
			}

		} else if ( is_tag() || is_category() || anemos_eutf_is_woo_category() || anemos_eutf_is_woo_tag() ) {
			$category_id = get_queried_object_id();
			$anemos_eutf_custom_title_options = anemos_eutf_get_term_meta( $category_id, '_anemos_eutf_custom_title_options' );
			$anemos_eutf_page_title_custom = anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'custom' );
			if ( 'custom' == $anemos_eutf_page_title_custom ) {
				$anemos_eutf_page_title = $anemos_eutf_custom_title_options;
			}
		}

		$anemos_eutf_wrapper_title_classes = array( 'eut-page-title' );

		$bg_mode = anemos_eutf_array_value( $anemos_eutf_page_title, 'bg_mode', 'color' );
		if ( 'color' == $bg_mode ) {
			$anemos_eutf_wrapper_title_classes[] = 'eut-with-title';
		} else {
			$anemos_eutf_wrapper_title_classes[] = 'eut-with-image';
		}

		$anemos_eutf_subheading_classes = array( 'eut-subheading', 'eut-title-categories', 'clearfix' );
		$anemos_eutf_title_classes = array( 'eut-title' );
		$anemos_eutf_caption_classes = array( 'eut-description', 'clearfix' );
		$anemos_eutf_title_meta_classes = array( 'eut-title-meta-content' );
		$anemos_eutf_content_classes = array( 'eut-title-content-wrapper' );

		$content_position = anemos_eutf_array_value( $anemos_eutf_page_title, 'content_position', 'center-center' );
		$content_animation = anemos_eutf_array_value( $anemos_eutf_page_title, 'content_animation', 'fade-in' );
		$page_title_height = anemos_eutf_array_value( $anemos_eutf_page_title, 'height', '40' );
		$page_title_min_height = anemos_eutf_array_value( $anemos_eutf_page_title, 'min_height', '200' );


		$page_title_bg_color = anemos_eutf_array_value( $anemos_eutf_page_title, 'bg_color', 'dark' );
		if ( 'custom' != $page_title_bg_color ) {
			$anemos_eutf_wrapper_title_classes[] = 'eut-bg-' . $page_title_bg_color;
		}

		$page_title_content_bg_color = anemos_eutf_array_value( $anemos_eutf_page_title, 'content_bg_color', 'light' );
		if ( 'custom' != $page_title_content_bg_color ) {
			$anemos_eutf_content_classes[] = 'eut-bg-' . $page_title_content_bg_color;
		}

		$page_title_subheading_color = anemos_eutf_array_value( $anemos_eutf_page_title, 'subheading_color', 'dark' );
		if ( 'custom' != $page_title_subheading_color ) {
			$anemos_eutf_subheading_classes[] = 'eut-text-' . $page_title_subheading_color;
			$anemos_eutf_title_meta_classes[] = 'eut-text-' . $page_title_subheading_color;
		}

		$page_title_color = anemos_eutf_array_value( $anemos_eutf_page_title, 'title_color', 'dark' );
		if ( 'custom' != $page_title_color ) {
			$anemos_eutf_title_classes[] = 'eut-text-' . $page_title_color;
		}

		$page_title_caption_color = anemos_eutf_array_value( $anemos_eutf_page_title, 'caption_color', 'dark' );
		if ( 'custom' != $page_title_caption_color ) {
			$anemos_eutf_caption_classes[] = 'eut-text-' . $page_title_caption_color;
		}

		$anemos_eutf_wrapper_title_classes = implode( ' ', $anemos_eutf_wrapper_title_classes );
		$anemos_eutf_title_classes = implode( ' ', $anemos_eutf_title_classes );
		$anemos_eutf_caption_classes = implode( ' ', $anemos_eutf_caption_classes );
		$anemos_eutf_subheading_classes = implode( ' ', $anemos_eutf_subheading_classes );
		$anemos_eutf_title_meta_classes = implode( ' ', $anemos_eutf_title_meta_classes );
		$anemos_eutf_content_classes = implode( ' ', $anemos_eutf_content_classes );
?>
	<!-- Page Title -->
	<div id="<?php echo esc_attr( $anemos_eutf_page_title_id ); ?>" class="<?php echo esc_attr( $anemos_eutf_wrapper_title_classes ); ?>" data-height="<?php echo esc_attr( $page_title_height ); ?>">
		<div class="eut-wrapper clearfix" style="height:<?php echo esc_attr( $page_title_height ); ?>vh;min-height:<?php echo esc_attr( $page_title_min_height ); ?>px;">
			<?php do_action( 'anemos_eutf_page_title_top' ); ?>
			<div class="eut-content eut-align-<?php echo esc_attr( $content_position ); ?>" data-animation="<?php echo esc_attr( $content_animation ); ?>">
				<div class="eut-container">
					<div class="<?php echo esc_attr( $anemos_eutf_content_classes ); ?>">
					<?php if ( empty( $header_reversed ) ) { ?>

						<?php if( 'post' == $mode && anemos_eutf_visibility( 'post_category_visibility', '1' ) ) { ?>
						<div class="<?php echo esc_attr( $anemos_eutf_subheading_classes ); ?>">
							<?php anemos_eutf_print_post_title_categories(); ?>
						</div>
						<?php } ?>

						<h1 class="<?php echo esc_attr( $anemos_eutf_title_classes ); ?>"><span><?php echo esc_html( $header_title ); ?></span></h1>
						<div class="eut-line-divider"><span></span></div>
						<?php if ( !empty( $header_description ) ) { ?>
						<div class="<?php echo esc_attr( $anemos_eutf_caption_classes ); ?>"><?php echo wp_kses_post( $header_description ); ?></div>
						<?php } ?>

						<?php if( 'post' == $mode ) { ?>
							<div class="<?php echo esc_attr( $anemos_eutf_title_meta_classes ); ?>">
								<?php anemos_eutf_print_post_title_meta(); ?>
							</div>
						<?php } ?>

					<?php } else { ?>
						<?php if ( !empty( $header_description ) ) { ?>
						<div class="<?php echo esc_attr( $anemos_eutf_caption_classes ); ?>"><?php echo wp_kses_post( $header_description ); ?></div>
						<?php } ?>
						<h1 class="<?php echo esc_attr( $anemos_eutf_title_classes ); ?>"><span><?php echo esc_html( $header_title ); ?></span></h1>
					<?php } ?>
					</div>
				</div>
			</div>
			<?php do_action( 'anemos_eutf_page_title_bottom' ); ?>
		</div>
		<?php anemos_eutf_print_title_bg_image( $anemos_eutf_page_title ); ?>
	</div>
	<!-- End Page Title -->
<?php
	}
}

 /**
 * Prints Anchor Menu
 */
function anemos_eutf_print_anchor_menu( $mode = 'page') {

	$item_type = str_replace ( '_' , '-', $mode );
	$anemos_eutf_anchor_id = 'eut-' . $item_type  . '-anchor';
	if ( anemos_eutf_is_woo_shop() ) {
		$anchor_nav_menu = anemos_eutf_post_meta_shop( '_anemos_eutf_anchor_navigation_menu' );
	} else {
		$anchor_nav_menu = anemos_eutf_post_meta( '_anemos_eutf_anchor_navigation_menu' );
	}

	if ( !empty( $anchor_nav_menu ) ) {

		$anemos_eutf_anchor_fullwidth = anemos_eutf_option( $mode . '_anchor_menu_fullwidth' );
		$anemos_eutf_anchor_alignment = anemos_eutf_option( $mode . '_anchor_menu_alignment', 'left' );
		$anemos_eutf_anchor_current_link = anemos_eutf_option( $mode . '_anchor_menu_highlight_current' );

		$anemos_eutf_anchor_classes = array( 'eut-anchor-menu' );
		if ( '1' == $anemos_eutf_anchor_current_link ) {
			$anemos_eutf_anchor_classes[] = 'eut-current-link';
		}
		if ( '1' == $anemos_eutf_anchor_fullwidth ) {
			$anemos_eutf_anchor_classes[] = ' eut-fullwidth';
		}
		$anemos_eutf_anchor_classes[] = 'eut-align-' . $anemos_eutf_anchor_alignment ;
		$anemos_eutf_anchor_classes = implode( ' ', $anemos_eutf_anchor_classes );
?>
		<!-- ANCHOR MENU -->
		<div id="<?php echo esc_attr( $anemos_eutf_anchor_id ); ?>" class="<?php echo esc_attr( $anemos_eutf_anchor_classes ); ?>">
			<div class="eut-wrapper eut-anchor-wrapper">
				<div class="eut-container">
					<a href="#" class="eut-anchor-btn"><i class="eut-icon-menu"></i></a>
					<?php
					wp_nav_menu(
						array(
							'menu' => $anchor_nav_menu, /* menu id */
							'container' => false, /* no container */
							'walker' => new Anemos_Eutf_Simple_Navigation_Walker(),
						)
					);
					?>
				</div>
			</div>
		</div>
		<!-- END ANCHOR MENU -->
<?php
	}
}

 /**
 * Prints header breadcrumbs
 */
function anemos_eutf_print_header_breadcrumbs( $mode = 'page') {

	$anemos_eutf_disable_breadcrumbs = 'yes';

	if( anemos_eutf_visibility( $mode . '_breadcrumbs_enabled' ) ) {
		$anemos_eutf_disable_breadcrumbs = 'no';
		if ( is_singular() ) {
			$anemos_eutf_disable_breadcrumbs = anemos_eutf_post_meta( '_anemos_eutf_disable_breadcrumbs', $anemos_eutf_disable_breadcrumbs );
		} else if( anemos_eutf_is_woo_shop() ) {
			$anemos_eutf_disable_breadcrumbs = anemos_eutf_post_meta_shop( '_anemos_eutf_disable_breadcrumbs', $anemos_eutf_disable_breadcrumbs );
		}
	}

	if ( 'yes' != $anemos_eutf_disable_breadcrumbs  ) {

		$item_type = str_replace ( '_' , '-', $mode );
		$anemos_eutf_breadcrumbs_id = 'eut-' . $item_type  . '-breadcrumbs';
		$anemos_eutf_breadcrumbs_fullwidth = anemos_eutf_option( $mode . '_breadcrumbs_fullwidth' );
		$anemos_eutf_breadcrumbs_alignment = anemos_eutf_option( $mode . '_breadcrumbs_alignment', 'left' );

		$anemos_eutf_breadcrumbs_classes = array( 'eut-breadcrumbs', 'clearfix' );
		if ( '1' == $anemos_eutf_breadcrumbs_fullwidth ) {
			$anemos_eutf_breadcrumbs_classes[] = ' eut-fullwidth';
		}
		$anemos_eutf_breadcrumbs_classes[] = 'eut-align-' . $anemos_eutf_breadcrumbs_alignment ;
		$anemos_eutf_breadcrumbs_classes = implode( ' ', $anemos_eutf_breadcrumbs_classes );
?>
	<div id="<?php echo esc_attr( $anemos_eutf_breadcrumbs_id ); ?>" class="<?php echo esc_attr( $anemos_eutf_breadcrumbs_classes ); ?>">
		<div class="eut-breadcrumbs-wrapper">
			<div class="eut-container">
				<?php anemos_eutf_print_breadcrumbs(); ?>
			</div>
		</div>
	</div>
<?php
	}
}

/**
 * Prints header top bar text
 */
function anemos_eutf_print_header_top_bar_text( $text ) {
	if ( !empty( $text ) ) {
?>
		<li class="eut-topbar-item"><p><?php echo do_shortcode( $text ); ?></p></li>
<?php
	}
}

/**
 * Prints header top bar navigation
 */
function anemos_eutf_print_header_top_bar_nav( $position = 'left' ) {
?>
	<li class="eut-topbar-item">
		<nav class="eut-top-bar-menu eut-small-text eut-list-divider">
			<?php
				if( 'left' == $position ) {
					anemos_eutf_top_left_nav();
				} else {
					anemos_eutf_top_right_nav();
				}
			?>
		</nav>
	</li>
<?php
}

/**
 * Prints header top bar search icon
 */
function anemos_eutf_print_header_top_bar_search( $position = 'left' ) {
?>
	<li class="eut-topbar-item"><a href="#eut-search-modal" class="eut-icon-search eut-toggle-modal"></a></li>
<?php
}

/**
 * Prints header top bar form icon
 */
function anemos_eutf_print_header_top_bar_form( $position = 'left' ) {

	if( 'left' == $position ) {
		$modal_id = '#eut-top-left-form-modal';
	} else {
		$modal_id = '#eut-top-right-form-modal';
	}
?>
	<li class="eut-topbar-item"><a href="<?php echo esc_attr( $modal_id ); ?>" class="eut-icon-envelope eut-toggle-modal"></a></li>
<?php

}

/**
 * Prints header top bar socials
 */
function anemos_eutf_print_header_top_bar_socials( $options ) {

	$social_options = anemos_eutf_option('social_options');
	if ( !empty( $options ) && !empty( $social_options ) ) {
		?>
			<li class="eut-topbar-item">
				<ul class="eut-social">
		<?php
		foreach ( $social_options as $key => $value ) {
			if ( isset( $options[$key] ) && 1 == $options[$key] && $value ) {
				if ( 'skype' == $key ) {
					echo '<li><a href="' . esc_url( $value, array( 'skype', 'http', 'https' ) ) . '" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
				} else {
					echo '<li><a href="' . esc_url( $value ) . '" target="_blank" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
				}
			}
		}
		?>
				</ul>
			</li>
		<?php
	}

}

/**
 * Prints header top bar language selector
 */
function anemos_eutf_print_header_top_bar_language_selector() {

	//start language selector output buffer
    ob_start();

	$languages = '';

	//Polylang
	if( function_exists( 'pll_the_languages' ) ) {
		$languages = pll_the_languages( array( 'raw'=>1 ) );

		$lang_option_current = $lang_options = '';

		foreach ( $languages as $l ) {

			if ( !$l['current_lang'] ) {
				$lang_options .= '<li>';
				$lang_options .= '<a href="' . esc_url( $l['url'] ) . '" class="eut-language-item">';
				$lang_options .= '<img src="' . esc_url( $l['flag'] ) . '" alt="' . esc_attr( $l['name'] ) . '"/>';
				$lang_options .= esc_html( $l['name'] );
				$lang_options .= '</a>';
				$lang_options .= '</li>';
			} else {
				$lang_option_current .= '<a href="#" class="eut-language-item">';
				$lang_option_current .= '<img src="' . esc_url( $l['flag'] ) . '" alt="' . esc_attr( $l['name'] ) . '"/>';
				$lang_option_current .= esc_html( $l['name'] );
				$lang_option_current .= '</a>';
			}
		}

	}

	//WPML
	if ( defined( 'ICL_SITEPRESS_VERSION' ) && defined( 'ICL_LANGUAGE_CODE' ) ) {

		$languages = icl_get_languages( 'skip_missing=0' );
		if ( ! empty( $languages ) ) {

			$lang_option_current = $lang_options = '';

			foreach ( $languages as $l ) {

				if ( !$l['active'] ) {
					$lang_options .= '<li>';
					$lang_options .= '<a href="' . esc_url( $l['url'] ) . '" class="eut-language-item">';
					$lang_options .= '<img src="' . esc_url( $l['country_flag_url'] ) . '" alt="' . esc_attr( $l['language_code'] ) . '"/>';
					$lang_options .= esc_html( $l['native_name'] );
					$lang_options .= '</a>';
					$lang_options .= '</li>';
				} else {
					$lang_option_current .= '<a href="#" class="eut-language-item">';
					$lang_option_current .= '<img src="' . esc_url( $l['country_flag_url'] ) . '" alt="' . esc_attr( $l['language_code'] ) . '"/>';
					$lang_option_current .= esc_html( $l['native_name'] );
					$lang_option_current .= '</a>';
				}
			}
		}
	}
	if ( ! empty( $languages ) ) {

?>
	<li class="eut-topbar-item">
		<ul class="eut-language eut-small-text">
			<li>
				<?php echo wp_kses_post( $lang_option_current ); ?>
				<ul>
					<?php echo wp_kses_post( $lang_options ); ?>
				</ul>
			</li>
		</ul>
	</li>
<?php
	}
	//store the language selector buffer and clean
	$anemos_eutf_lang_selector_out = ob_get_clean();
	echo apply_filters( 'anemos_eutf_header_top_bar_language_selector', $anemos_eutf_lang_selector_out );
}

/**
 * Prints header top bar login
 */
function anemos_eutf_print_header_top_bar_login() {

?>
	<li class="eut-topbar-item"><a href="#eut-login-modal" class="eut-icon-user eut-toggle-modal"></a></li>
<?php

}


/**
 * Prints header top bar
 */
function anemos_eutf_print_header_top_bar() {

	if ( anemos_eutf_visibility( 'top_bar_enabled' ) ) {
		if ( ( is_singular() && 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_top_bar' ) ) || ( anemos_eutf_is_woo_shop() && 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_disable_top_bar' ) ) ) {
			return;
		}

		$section_type = anemos_eutf_option( 'top_bar_section_type', 'fullwidth-background' );
		$top_bar_class = '';
		if( 'fullwidth-element' == $section_type ) {
			$top_bar_class = 'eut-fullwidth';
		}
?>

		<!-- Top Bar -->
		<div id="eut-top-bar" class="<?php echo esc_attr( $top_bar_class ); ?>">
			<div class="eut-wrapper clearfix">
				<div class="eut-container">

					<?php
					if ( anemos_eutf_visibility( 'top_bar_left_enabled' ) ) {
					?>
					<ul class="eut-bar-content eut-left-side">
						<?php

							//Top Left First Item Hook
							do_action( 'anemos_eutf_header_top_bar_left_first_item' );

							//Top Left Options
							$top_bar_left_options = anemos_eutf_option('top_bar_left_options');

							if ( !empty( $top_bar_left_options ) ) {
								foreach ( $top_bar_left_options as $key => $value ) {
									if( !empty( $value ) && '0' != $value ) {

										switch( $key ) {
											case 'menu':
												anemos_eutf_print_header_top_bar_nav( 'left' );
											break;
											case 'search':
												anemos_eutf_print_header_top_bar_search( 'left' );
											break;
											case 'form':
												anemos_eutf_print_header_top_bar_form( 'left' );
											break;
											case 'text':
												$anemos_eutf_left_text = anemos_eutf_option('top_bar_left_text');
												anemos_eutf_print_header_top_bar_text( $anemos_eutf_left_text );
											break;
											case 'language':
												anemos_eutf_print_header_top_bar_language_selector();
											break;
											case 'login':
												anemos_eutf_print_header_top_bar_login();
											break;
											case 'social':
												$top_bar_left_social_options = anemos_eutf_option('top_bar_left_social_options');
												anemos_eutf_print_header_top_bar_socials( $top_bar_left_social_options);
											break;
											default:
											break;
										}
									}
								}
							}

							//Top Left Last Item Hook
							do_action( 'anemos_eutf_header_top_bar_left_last_item' );

						?>
					</ul>
					<?php
						}
					?>

					<?php
					if ( anemos_eutf_visibility( 'top_bar_right_enabled' ) ) {
					?>
					<ul class="eut-bar-content eut-right-side">
						<?php

							//Top Right First Item Hook
							do_action( 'anemos_eutf_header_top_bar_right_first_item' );

							//Top Right Options
							$top_bar_right_options = anemos_eutf_option('top_bar_right_options');
							if ( !empty( $top_bar_right_options ) ) {
								foreach ( $top_bar_right_options as $key => $value ) {
									if( !empty( $value ) && '0' != $value ) {

										switch( $key ) {
											case 'menu':
												anemos_eutf_print_header_top_bar_nav( 'right' );
											break;
											case 'search':
												anemos_eutf_print_header_top_bar_search( 'right' );
											break;
											case 'form':
												anemos_eutf_print_header_top_bar_form( 'right' );
											break;
											case 'text':
												$anemos_eutf_right_text = anemos_eutf_option('top_bar_right_text');
												anemos_eutf_print_header_top_bar_text( $anemos_eutf_right_text );
											break;
											case 'language':
												anemos_eutf_print_header_top_bar_language_selector();
											break;
											case 'login':
												anemos_eutf_print_header_top_bar_login();
											break;
											case 'social':
												$top_bar_right_social_options = anemos_eutf_option('top_bar_right_social_options');
												anemos_eutf_print_header_top_bar_socials( $top_bar_right_social_options );
											break;
											default:
											break;
										}
									}
								}
							}

							//Top Right Last Item Hook
							do_action( 'anemos_eutf_header_top_bar_right_last_item' );

						?>


					</ul>
					<?php
						}
					?>
				</div>
			</div>
		</div>
		<!-- End Top Bar -->
<?php

	}
}

/**
 * Prints check header elements visibility
 */
function anemos_eutf_check_header_elements_visibility_any() {

	if ( !anemos_eutf_visibility( 'header_menu_options_enabled' ) ) {
		return false;
	}

	$header_menu_options = anemos_eutf_option('header_menu_options');
	if ( !empty( $header_menu_options ) ) {
		foreach ( $header_menu_options as $key => $value ) {
			if( !empty( $value ) && '0' != $value && anemos_eutf_check_header_elements_visibility( $key ) ) {
				return true;
			}
		}
	}
	return false;
}

function anemos_eutf_check_header_elements_visibility( $item = 'none' ) {

	$visibility = false;

	if ( anemos_eutf_visibility( 'header_menu_options_enabled' ) ) {

		if ( is_singular() ) {
			$anemos_eutf_disable_menu_items = anemos_eutf_post_meta( '_anemos_eutf_disable_menu_items' );
			if ( 'yes' == anemos_eutf_array_value( $anemos_eutf_disable_menu_items, $item  ) ) {
				return false;
			}
		}
		if ( anemos_eutf_is_woo_shop() ) {
			$anemos_eutf_disable_menu_items = anemos_eutf_post_meta_shop( '_anemos_eutf_disable_menu_items' );
			if ( 'yes' == anemos_eutf_array_value( $anemos_eutf_disable_menu_items, $item  ) ) {
				return false;
			}
		}

		$header_menu_options = anemos_eutf_option('header_menu_options');
		if ( !empty( $header_menu_options ) ) {
			if ( isset( $header_menu_options[ $item ] ) && !empty( $header_menu_options[ $item ] ) && '0' != $header_menu_options[ $item ] ) {
				$visibility = true;
			}
		}

	}

	return $visibility;
}

/**
 * Prints Header Back To Top Link
 */
if ( !function_exists('anemos_eutf_print_header_back_top') ) {
	function anemos_eutf_print_header_back_top() {

		if ( ( is_singular() && 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_back_to_top' ) ) || ( anemos_eutf_is_woo_shop() && 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_disable_back_to_top' ) ) ) {
			return;
		}

		?>
			<div class="eut-header-back-top eut-position-right">
				<div class="eut-header-element">
					<a href="#">
						<span class="eut-item eut-bg-primary-1">
							<i class="eut-icon-nav-up"></i>
						</span>
					</a>
				</div>
			</div>
		<?php
	}
}

/**
 * Prints header elements e.g: social, language selector, search
 */
function anemos_eutf_print_header_elements( $anemos_eutf_sidearea_data = '') {

	if ( anemos_eutf_check_header_elements_visibility_any() ) {

		$header_menu_options = anemos_eutf_option('header_menu_options');
		$anemos_eutf_header_mode = anemos_eutf_option( 'header_mode', 'default' );

		$align = '';
		if ( 'side' != $anemos_eutf_header_mode ) {
			$align = 'eut-position-left';
		}

?>
		<!-- Header Elements -->
		<div class="eut-header-elements <?php echo esc_attr( $align ); ?>">
			<div class="eut-wrapper">
				<ul>
<?php

			if ( !empty( $anemos_eutf_sidearea_data ) ) {
				anemos_eutf_print_header_sidearea_button( $anemos_eutf_sidearea_data, 'list' );
			}
			$header_menu_social_mode = anemos_eutf_option('header_menu_social_mode', 'modal');
			do_action( 'anemos_eutf_header_elements_first_item' );

			if ( !empty( $header_menu_options ) ) {
				foreach ( $header_menu_options as $key => $value ) {
					if( !empty( $value ) && '0' != $value && anemos_eutf_check_header_elements_visibility( $key ) ) {
						if ( 'search' == $key ) {
						?>
							<li class="eut-header-element"><a href="#eut-search-modal" class="eut-toggle-modal"><span class="eut-item"><i class="eut-icon-search"></i></span></a></li>
						<?php
						} else if ( 'language' == $key ) {
						?>
							<li class="eut-header-element"><a href="#eut-language-modal" class="eut-toggle-modal"><span class="eut-item"><i class="eut-icon-globe"></i></span></a></li>
						<?php
						} else if ( 'login' == $key ) {
						?>
							<li class="eut-header-element"><a href="#eut-login-modal" class="eut-toggle-modal"><span class="eut-item"><i class="eut-icon-user"></i></span></a></li>
						<?php
						}else if ( 'form' == $key ) {
						?>
							<li class="eut-header-element"><a href="#eut-menu-form-modal" class="eut-toggle-modal"><span class="eut-item"><i class="eut-icon-envelope"></i></span></a></li>
						<?php
						} else if ( 'social' == $key ) {
							$header_social_options = anemos_eutf_option('header_menu_social_options');
							$social_options = anemos_eutf_option('social_options');
							if( 'modal' == $header_menu_social_mode ) {
						?>
							<li class="eut-header-element"><a href="#eut-socials-modal" class="eut-toggle-modal"><span class="eut-item"><i class="eut-icon-socials"></i></span></a></li>
						<?php
							} else {

								if ( !empty( $header_social_options ) && !empty( $social_options ) ) {

									foreach ( $social_options as $key => $value ) {
										if ( isset( $header_social_options[$key] ) && 1 == $header_social_options[$key] && $value ) {
											if ( 'skype' == $key ) {
												echo '<li class="eut-header-element"><a href="' . esc_url( $value, array( 'skype', 'http', 'https' ) ) . '"><span class="eut-item"><i class="fa fa-' . esc_attr( $key ) . '"></i></span></a></li>';
											} else {
												echo '<li class="eut-header-element"><a href="' . esc_url( $value ) . '" target="_blank"><span class="eut-item"><i class="fa fa-' . esc_attr( $key ) . '"></i></span></a></li>';
											}
										}
									}

								}

							}
						}
					}
				}
			}

			do_action( 'anemos_eutf_header_elements_last_item' );
?>
				</ul>
			</div>
		</div>
		<!-- End Header Elements -->
<?php
	}

}

/**
 * Prints header elements e.g: social, language selector, search
 */
function anemos_eutf_print_header_elements_responsive() {

	if ( anemos_eutf_check_header_elements_visibility_any() ) {
		$header_menu_options = anemos_eutf_option('header_menu_options');

		foreach ( $header_menu_options as $key => $value ) {
			if( !empty( $value ) && '0' != $value && anemos_eutf_check_header_elements_visibility( $key ) ) {
				if ( 'search' == $key ) {
				?>
					<div class="eut-header-responsive-elements">
						<div class="eut-wrapper">
							<div class="eut-widget">
								<?php get_search_form(); ?>
							</div>
						</div>
					</div>
				<?php
				} else if ( 'language' == $key ) {
				?>
					<div class="eut-header-responsive-elements">
						<div class="eut-wrapper">
							<?php anemos_eutf_print_language_modal_selector(); ?>
						</div>
					</div>
				<?php
				} else if ( 'form' == $key ) {
				?>
					<div class="eut-header-responsive-elements">
						<div class="eut-wrapper">
							<div class="eut-newsletter">
								<?php anemos_eutf_print_contact_form( 'header_menu_form' ); ?>
							</div>
						</div>
					</div>
				<?php
				} else if ( 'social' == $key ) {
					$header_social_options = anemos_eutf_option('header_menu_social_options');
					$social_options = anemos_eutf_option('social_options');
					if ( !empty( $header_social_options ) && !empty( $social_options ) ) {
?>
						<!-- Responsive social Header Elements -->
						<div class="eut-header-responsive-elements">
							<div class="eut-wrapper">
								<ul>
<?php
									foreach ( $social_options as $key => $value ) {
										if ( isset( $header_social_options[$key] ) && 1 == $header_social_options[$key] && $value ) {
											if ( 'skype' == $key ) {
												echo '<li class="eut-header-responsive-element"><a href="' . esc_url( $value, array( 'skype', 'http', 'https' ) ) . '"><span class="eut-item"><i class="fa fa-' . esc_attr( $key ) . '"></i></span></a></li>';
											} else {
												echo '<li class="eut-header-responsive-element"><a href="' . esc_url( $value ) . '" target="_blank"><span class="eut-item"><i class="fa fa-' . esc_attr( $key ) . '"></i></span></a></li>';
											}
										}
									}
?>
								</ul>
							</div>
						</div>
						<!-- End Social Header Elements -->
<?php
					}
				}
			}
		}
	}

}



/**
 * Prints Form modals
 */
function anemos_eutf_print_contact_form( $option = 'header_menu_form' ) {

	if ( anemos_eutf_is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
		$contact_form_id = anemos_eutf_option( $option );
		if ( !empty( $contact_form_id ) ) {
			echo do_shortcode('[contact-form-7 id="' . esc_attr( $contact_form_id ) . '"]');
		}
	}

}

function anemos_eutf_print_form_modals() {
?>
		<div id="eut-top-left-form-modal" class="eut-modal">
			<div class="eut-modal-wrapper">
				<div class="eut-modal-content">
					<div class="eut-modal-form">
						<div class="eut-modal-item">
							<?php anemos_eutf_print_contact_form( 'top_bar_left_form' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="eut-top-right-form-modal" class="eut-modal">
			<div class="eut-modal-wrapper">
				<div class="eut-modal-content">
					<div class="eut-modal-form">
						<div class="eut-modal-item">
							<?php anemos_eutf_print_contact_form( 'top_bar_right_form' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="eut-menu-form-modal" class="eut-modal">
			<div class="eut-modal-wrapper">
				<div class="eut-modal-content">
					<div class="eut-modal-form">
						<div class="eut-modal-item">
							<?php anemos_eutf_print_contact_form( 'header_menu_form' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
}

/**
 * Prints Search modal
 */
function anemos_eutf_print_search_modal() {
		$form = '';
?>
		<div id="eut-search-modal" class="eut-modal">
			<div class="eut-modal-wrapper">
				<div class="eut-modal-content">
					<div class="eut-modal-item">
						<?php echo anemos_eutf_modal_wpsearch( $form ); ?>
					</div>
				</div>
			</div>
		</div>
<?php

}

/**
 * Prints header language selector
 * WPML/Polylang is required
 * Can be used to add custom php code for other translation flags.
 */
if( !function_exists( 'anemos_eutf_print_language_modal_selector' ) ) {
	function anemos_eutf_print_language_modal_selector() {

		//start language selector output buffer
		ob_start();
?>
		<ul class="eut-language">
<?php
		//Polylang
		if( function_exists( 'pll_the_languages' ) ) {
			$languages = pll_the_languages( array( 'raw'=>1 ) );
			if ( ! empty( $languages ) ) {
				foreach ( $languages as $l ) {
					echo '<li>';
					if ( !$l['current_lang'] ) {
						echo '<a href="' . esc_url( $l['url'] ) . '">';
					} else {
						echo '<a href="#" class="active">';
					}
					echo esc_html( $l['name'] );

					echo '</a></li>';
				}
			}
		}

		//WPML
		if ( defined( 'ICL_SITEPRESS_VERSION' ) && defined( 'ICL_LANGUAGE_CODE' ) ) {
			$languages = icl_get_languages( 'skip_missing=0' );
			if ( ! empty( $languages ) ) {
				foreach ( $languages as $l ) {
					echo '<li>';
					if ( !$l['active'] ) {
						echo '<a href="' . esc_url( $l['url'] ) . '">';
					} else {
						echo '<a href="#" class="active">';
					}
					echo esc_html( $l['native_name'] );

					echo '</a></li>';
				}
			}
		}
?>
		</ul>
<?php
		//store the language selector buffer and clean
		$anemos_eutf_lang_selector_out = ob_get_clean();
		echo apply_filters( 'anemos_eutf_language_modal_selector', $anemos_eutf_lang_selector_out );
	}
}

function anemos_eutf_print_language_modal() {
?>
	<div id="eut-language-modal" class="eut-modal">
		<div class="eut-modal-wrapper">
			<div class="eut-modal-content">
				<div class="eut-modal-item">
					<?php anemos_eutf_print_language_modal_selector(); ?>
				</div>
			</div>
		</div>
	</div>
<?php

}

function anemos_eutf_print_login_modal() {
?>
	<div id="eut-login-modal" class="eut-modal">
		<div class="eut-modal-wrapper">
			<div class="eut-modal-content">
				<div class="eut-modal-item">
					<?php get_template_part( 'templates/modal', 'login' ); ?>
				</div>
			</div>
		</div>
	</div>
<?php

}

/**
 * Prints header login responsive button
 */
function anemos_eutf_print_login_responsive_button() {

	if( anemos_eutf_check_header_elements_visibility( 'login' ) ) {
?>
		<div class="eut-header-elements eut-position-left">
			<div class="eut-wrapper">
				<ul>
					<li class="eut-header-element">
						<a href="#eut-login-modal" class="eut-toggle-modal">
							<span class="eut-item"><i class="eut-icon-user"></i></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
<?php

	}
}

function anemos_eutf_print_social_modal() {

	$header_menu_options = anemos_eutf_option('header_menu_options');
	$header_menu_social_mode = anemos_eutf_option('header_menu_social_mode', 'modal');
	$show_social_modal = false;

	if ( !empty( $header_menu_options ) ) {
		if ( isset( $header_menu_options['social'] ) && !empty( $header_menu_options['social'] ) && '0' != $header_menu_options['social'] ) {
			if( 'modal' == $header_menu_social_mode ) {
				$show_social_modal = true;
			}
		}
	}


	if( $show_social_modal ) {

?>
	<div id="eut-socials-modal" class="eut-modal">
		<div class="eut-modal-wrapper">
			<div class="eut-modal-content eut-align-center">
				<div class="eut-modal-item">
		<?php
				$header_social_options = anemos_eutf_option('header_menu_social_options');
				$social_options = anemos_eutf_option('social_options');

					if ( !empty( $header_social_options ) && !empty( $social_options ) ) {
		?>
					<ul class="eut-social">
		<?php

						foreach ( $social_options as $key => $value ) {
							if ( isset( $header_social_options[$key] ) && 1 == $header_social_options[$key] && $value ) {
								if ( 'skype' == $key ) {
									echo '<li><a href="' . esc_url( $value, array( 'skype', 'http', 'https' ) ) . '" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
								} else {
									echo '<li><a href="' . esc_url( $value ) . '" target="_blank" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
								}
							}
						}
		?>
					</ul>
		<?php
					}

		?>
				</div>
			</div>
		</div>
	</div>
<?php
	}
}

/**
 * Gets side area data
 */
function anemos_eutf_get_sidearea_data() {

	$anemos_eutf_sidebar_visibility = 'no';
	$anemos_eutf_sidebar_id = '';

	if ( ! is_singular() ) {
		//Overview Pages
		if( anemos_eutf_woocommerce_enabled() && is_woocommerce() ) {
			if ( is_shop() && !is_search() ) {
				$anemos_eutf_sidebar_visibility =  anemos_eutf_post_meta_shop( '_anemos_eutf_sidearea_visibility', anemos_eutf_option( 'page_sidearea_visibility' ) );
				$anemos_eutf_sidebar_id = anemos_eutf_post_meta_shop( '_anemos_eutf_sidearea_sidebar', anemos_eutf_option( 'page_sidearea_sidebar' ) );
			} else {
				$anemos_eutf_sidebar_visibility = anemos_eutf_option( 'product_tax_sidearea_visibility' );
				$anemos_eutf_sidebar_id = anemos_eutf_option( 'product_tax_sidearea_sidebar' );
			}
		} else {
			$anemos_eutf_sidebar_visibility = anemos_eutf_option( 'blog_sidearea_visibility' );
			$anemos_eutf_sidebar_id = anemos_eutf_option( 'blog_sidearea_sidebar' );
		}
	} else {

		global $post;
		$post_id = $post->ID;
		$post_type = get_post_type( $post_id );

		switch( $post_type ) {
			case 'product':
				$anemos_eutf_sidebar_visibility =  anemos_eutf_post_meta( '_anemos_eutf_sidearea_visibility', anemos_eutf_option( 'product_sidearea_visibility' ) );
				$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidearea_sidebar', anemos_eutf_option( 'product_sidearea_sidebar' ) );
			break;
			case 'portfolio':
				$anemos_eutf_sidebar_visibility =  anemos_eutf_post_meta( '_anemos_eutf_sidearea_visibility', anemos_eutf_option( 'portfolio_sidearea_visibility' ) );
				$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidearea_sidebar', anemos_eutf_option( 'portfolio_sidearea_sidebar' ) );
			break;
			case 'post':
				$anemos_eutf_sidebar_visibility =  anemos_eutf_post_meta( '_anemos_eutf_sidearea_visibility', anemos_eutf_option( 'post_sidearea_visibility' ) );
				$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidearea_sidebar', anemos_eutf_option( 'post_sidearea_sidebar' ) );
			break;
			case 'page':
			default:
				$anemos_eutf_sidebar_visibility =  anemos_eutf_post_meta( '_anemos_eutf_sidearea_visibility', anemos_eutf_option( 'page_sidearea_visibility' ) );
				$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidearea_sidebar', anemos_eutf_option( 'page_sidearea_sidebar' ) );
			break;
		}
	}

	return array(
		'visibility' => $anemos_eutf_sidebar_visibility,
		'sidebar' => $anemos_eutf_sidebar_id,
	);
}

/**
 * Prints header side area toggle button
 */
function anemos_eutf_print_header_sidearea_button( $sidearea_data, $mode = '' ) {

	$anemos_eutf_sidebar_visibility = $sidearea_data['visibility'];
	$anemos_eutf_sidebar_id = $sidearea_data['sidebar'];

	if ( 'yes' == $anemos_eutf_sidebar_visibility ) {
		if ( 'list' == $mode ) {
?>
		<li class="eut-header-element">
			<a href="#eut-sidearea" class="eut-sidearea-btn eut-toggle-hiddenarea">
				<span class="eut-item"><i class="eut-icon-safebutton"></i></span>
			</a>
		</li>
<?php
		} else {
?>
		<div class="eut-header-elements eut-position-left">
			<div class="eut-wrapper">
				<ul>
					<li class="eut-header-element">
						<a href="#eut-sidearea" class="eut-sidearea-btn eut-toggle-hiddenarea">
							<span class="eut-item"><i class="eut-icon-safebutton"></i></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
<?php
		}
	}
}

/**
 * Prints header hidden area toggle button
 */
function anemos_eutf_print_header_hiddenarea_button() {
	$anemos_eutf_responsive_menu_selection = anemos_eutf_option( 'menu_responsive_toggle_selection', 'icon' );
	$anemos_eutf_responsive_menu_text = anemos_eutf_option( 'menu_responsive_toggle_text');
?>
	<div class="eut-hidden-menu-btn eut-position-right">
		<div class="eut-header-element">
			<a href="#eut-hidden-menu" class="eut-toggle-hiddenarea">
				<?php if ( 'icon' == $anemos_eutf_responsive_menu_selection ) { ?>
				<span class="eut-item">
					<i class="eut-icon-menu"></i>
				</span>
				<?php } else { ?>
				<span class="eut-item eut-with-text">
					<span class="eut-label">
						<?php echo esc_html( $anemos_eutf_responsive_menu_text ); ?>
					</span>
				</span>
				<?php } ?>
			</a>
		</div>
	</div>
<?php

}

/**
 * Prints Side Area
 */
function anemos_eutf_print_side_area( $sidearea_data ) {

	$anemos_eutf_sidebar_visibility = $sidearea_data['visibility'];
	$anemos_eutf_sidebar_id = $sidearea_data['sidebar'];

	if ( 'yes' == $anemos_eutf_sidebar_visibility ) {
?>
	<aside id="eut-sidearea" class="eut-hidden-area">
		<div class="eut-hiddenarea-wrapper">
			<!-- Close Button -->
			<div class="eut-close-btn-wrapper">
				<div class="eut-close-btn"><i class="eut-icon-close"></i></div>
			</div>
			<!-- End Close Button -->
			<div class="eut-hiddenarea-content">
				<?php
					if( is_active_sidebar( $anemos_eutf_sidebar_id ) ) {
						dynamic_sidebar( $anemos_eutf_sidebar_id );
					} else {
						if( current_user_can( 'administrator' ) ) {
							echo esc_html__( 'No widgets found in Side Area!', 'anemos'  ) . "<br/>" .
							"<a href='" . esc_url( admin_url() ) . "widgets.php'>" .
							esc_html__( "Activate Widgets", 'anemos' ) .
							"</a>";
						}
					}
				?>
			</div>

		</div>
	</aside>
<?php
	}
}

/**
 * Prints Hidden Menu
 */
function anemos_eutf_print_hidden_menu() {

	$anemos_eutf_menu_open_type = anemos_eutf_option( 'menu_responsive_open_type', 'toggle' );

?>
	<nav id="eut-hidden-menu" class="eut-hidden-area eut-<?php echo esc_attr( $anemos_eutf_menu_open_type ); ?>-menu">
		<div class="eut-hiddenarea-wrapper">
			<!-- Close Button -->
			<div class="eut-close-btn-wrapper">
				<div class="eut-close-btn"><i class="eut-icon-close"></i></div>
			</div>
			<!-- End Close Button -->
			<div class="eut-hiddenarea-content">
				<div class="eut-menu-wrapper">
					<?php
						$anemos_eutf_main_menu = anemos_eutf_get_header_nav();
						if ( $anemos_eutf_main_menu != 'disabled' ) {
							anemos_eutf_header_nav( $anemos_eutf_main_menu );
						}
					?>
				</div>
				<?php anemos_eutf_print_header_elements_responsive(); ?>
			</div>

		</div>
	</nav>
<?php

}

function anemos_eutf_print_item_nav_link( $post_id,  $direction, $title = '' ) {

	$icon_class = 'arrow-right';
	if ( 'prev' == $direction ) {
		$icon_class = 'arrow-left';
	}
?>
	<li><a href="<?php echo esc_url( get_permalink( $post_id ) ); ?>" class="eut-icon-<?php echo esc_attr( $icon_class ); ?>" title="<?php echo esc_attr($title); ?>"></a></li>
<?php
}


/**
 * Check Theme Loader Visibility
 */
function anemos_eutf_check_theme_loader_visibility() {

	$anemos_eutf_theme_loader = '';

	if ( is_singular() ) {
		$anemos_eutf_theme_loader = anemos_eutf_post_meta( '_anemos_eutf_theme_loader' );
	}
	if ( anemos_eutf_is_woo_shop() ) {
		$anemos_eutf_theme_loader = anemos_eutf_post_meta_shop( '_anemos_eutf_theme_loader' );
	}

	if( empty( $anemos_eutf_theme_loader ) ) {
		return anemos_eutf_visibility( 'theme_loader' );
	} else {
		if ( 'yes' == $anemos_eutf_theme_loader ) {
			return true;
		} else {
			return false;
		}
	}

}

/**
 * Prints Tracking code
 */
add_action('wp_head', 'anemos_eutf_print_tracking_code');
if ( !function_exists('anemos_eutf_print_tracking_code') ) {

	function anemos_eutf_print_tracking_code() {
		$tracking_code = anemos_eutf_option( 'tracking_code' );
		if ( !empty( $tracking_code ) ) {
			echo "" . $tracking_code;
		}
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
