<?php
/**
 *  Add Dynamic css to header
 *  @version	1.0
 *  @author		Euthemians Team
 *  @URI		http://euthemians.com
 */


add_action('wp_head', 'anemos_eutf_load_dynamic_css');

if ( !function_exists( 'anemos_eutf_load_dynamic_css' ) ) {

	function anemos_eutf_load_dynamic_css() {
		include_once get_template_directory() . '/includes/eut-dynamic-typography-css.php';
		include_once get_template_directory() . '/includes/eut-dynamic-css.php';

		$custom_css_code = anemos_eutf_option( 'css_code' );
		if ( !empty( $custom_css_code ) ) {
			echo anemos_eutf_get_css_output( $custom_css_code );
		}

		anemos_eutf_add_custom_page_css();
		anemos_eutf_get_global_button_style();
		anemos_eutf_get_global_shape_style();
	}
}

function anemos_eutf_add_custom_page_css( $id = null ) {

	$anemos_eutf_custom_css = '';
	$anemos_eutf_woo_shop = anemos_eutf_is_woo_shop();

	if ( is_front_page() && is_home() ) {
		// Default homepage
		$mode = 'blog';
	} else if ( is_front_page() ) {
		// static homepage
		$mode = 'page';
	} else if ( is_home() ) {
		// blog page
		$mode = 'blog';
	} else if( is_search() ) {
		$mode = 'search_page';
	} else if ( is_singular() || $anemos_eutf_woo_shop ) {
		if ( is_singular( 'post' ) ) {
			$mode = 'post';
		} else if ( is_singular( 'portfolio' ) ) {
			$mode = 'portfolio';
		} else if ( is_singular( 'product' ) ) {
			$mode = 'product';
		} else {
			$mode = 'page';
		}
	} else if ( is_archive() ) {
		if( anemos_eutf_is_woo_tax() ) {
			$mode = 'product_tax';
		} else {
			$mode = 'blog';
		}

	} else {
		$mode = 'page';
	}

	$anemos_eutf_page_title = array(
		'bg_color' => anemos_eutf_option( $mode . '_title_bg_color', 'dark' ),
		'bg_color_custom' => anemos_eutf_option( $mode . '_title_bg_color_custom', '#000000' ),
		'content_bg_color' => anemos_eutf_option( $mode . '_title_content_bg_color', 'light' ),
		'content_bg_color_custom' => anemos_eutf_option( $mode . '_title_content_bg_color_custom', '#ffffff' ),
		'title_color' => anemos_eutf_option( $mode . '_title_color', 'dark' ),
		'title_color_custom' => anemos_eutf_option( $mode . '_title_color_custom', '#000000' ),
		'caption_color' => anemos_eutf_option( $mode . '_description_color', 'dark' ),
		'caption_color_custom' => anemos_eutf_option( $mode . '_description_color_custom', '#000000' ),
	);

	if ( is_tag() || is_category() || anemos_eutf_is_woo_category() || anemos_eutf_is_woo_tag() ) {
		$category_id = get_queried_object_id();
		$anemos_eutf_custom_title_options = anemos_eutf_get_term_meta( $category_id, '_anemos_eutf_custom_title_options' );
		$anemos_eutf_page_title_custom = anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'custom' );
		if ( 'custom' == $anemos_eutf_page_title_custom ) {
			$anemos_eutf_page_title = $anemos_eutf_custom_title_options;
		}
	}


	if ( is_singular() || $anemos_eutf_woo_shop ) {

		if ( ! $id ) {
			if ( $anemos_eutf_woo_shop ) {
				$id = wc_get_page_id( 'shop' );
			} else {
				$id = get_the_ID();
			}
		}
		if ( $id ) {

			//Custom Title
			$anemos_eutf_custom_title_options = get_post_meta( $id, '_anemos_eutf_custom_title_options', true );
			$anemos_eutf_page_title_custom = anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'custom' );
			if ( !empty( $anemos_eutf_page_title_custom ) ) {
				$anemos_eutf_page_title = $anemos_eutf_custom_title_options;
			}

			//Feature Section
			$feature_section = get_post_meta( $id, '_anemos_eutf_feature_section', true );
			$feature_settings = anemos_eutf_array_value( $feature_section, 'feature_settings' );
			$feature_element = anemos_eutf_array_value( $feature_settings, 'element' );

			if ( !empty( $feature_element ) ) {

				switch( $feature_element ) {

					case 'title':
					case 'image':
					case 'video':
						$single_item = anemos_eutf_array_value( $feature_section, 'single_item' );
						if ( !empty( $single_item ) ) {
							$anemos_eutf_custom_css .= anemos_eutf_get_feature_title_css( $single_item );
						}
						break;
					case 'slider':
						$slider_items = anemos_eutf_array_value( $feature_section, 'slider_items' );
						if ( !empty( $slider_items ) ) {
							foreach ( $slider_items as $item ) {
								$anemos_eutf_custom_css .= anemos_eutf_get_feature_title_css( $item, 'slider' );
							}
						}
						break;
					default:
						break;

				}
			}
		}
	}

	$anemos_eutf_custom_css .= anemos_eutf_get_title_css( $anemos_eutf_page_title );

	if ( ! empty( $anemos_eutf_custom_css ) ) {
		echo anemos_eutf_get_css_output( $anemos_eutf_custom_css );
	}
}

function anemos_eutf_get_feature_title_css( $item, $type = 'single' ) {

	$anemos_eutf_custom_css = '';
	$custom_class = '';

	if( 'slider' == $type ) {
		$id = anemos_eutf_array_value( $item, 'id' );
		if ( !empty( $id ) ) {
			$custom_class = ' .eut-slider-item-id-' . $id ;
		}
	}

	$content_bg_color = anemos_eutf_array_value( $item, 'content_bg_color', 'light' );
	if ( 'custom' == $content_bg_color ) {
		$content_bg_color_custom = anemos_eutf_array_value( $item, 'content_bg_color_custom', '#ffffff' );
		$anemos_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-title-content-wrapper {';
		$anemos_eutf_custom_css .= anemos_eutf_get_css_color( 'background-color', $content_bg_color_custom );
		$anemos_eutf_custom_css .= '}';
	}

	$subheading_color = anemos_eutf_array_value( $item, 'subheading_color', 'dark' );
	if ( 'custom' == $subheading_color ) {
		$subheading_color_custom = anemos_eutf_array_value( $item, 'subheading_color_custom', '#000000' );
		$anemos_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-subheading, #eut-feature-section' . esc_attr( $custom_class ) . ' .eut-title-meta {';
		$anemos_eutf_custom_css .= anemos_eutf_get_css_color( 'color', $subheading_color_custom );
		$anemos_eutf_custom_css .= '}';
	}

	$title_color = anemos_eutf_array_value( $item, 'title_color', 'dark' );
	if ( 'custom' == $title_color ) {
		$title_color_custom = anemos_eutf_array_value( $item, 'title_color_custom', '#000000' );
		$anemos_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-title {';
		$anemos_eutf_custom_css .= anemos_eutf_get_css_color( 'color', $title_color_custom );
		$anemos_eutf_custom_css .= '}';
	}

	$caption_color = anemos_eutf_array_value( $item, 'caption_color', 'dark' );
	if ( 'custom' == $caption_color ) {
		$caption_color_custom = anemos_eutf_array_value( $item, 'caption_color_custom', '#000000' );
		$anemos_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-description {';
		$anemos_eutf_custom_css .= anemos_eutf_get_css_color( 'color', $caption_color_custom );
		$anemos_eutf_custom_css .= '}';
	}

	$media_id = anemos_eutf_array_value( $item, 'content_image_id', '0' );
	$media_max_height = anemos_eutf_array_value( $item, 'content_image_max_height', '150' );

	if( '0' != $media_id ) {
		$anemos_eutf_custom_css .= '#eut-feature-section' . esc_attr( $custom_class ) . ' .eut-content .eut-graphic img  {';
		$anemos_eutf_custom_css .= 'max-height:' . esc_attr( $media_max_height ) .'px';
		$anemos_eutf_custom_css .= '}';
	}

	return $anemos_eutf_custom_css;

}

function anemos_eutf_get_title_css( $title ) {
	$anemos_eutf_custom_css = '';

	$bg_color = anemos_eutf_array_value( $title, 'bg_color', 'dark' );
	if ( 'custom' == $bg_color ) {
		$bg_color_custom = anemos_eutf_array_value( $title, 'bg_color_custom', '#000000' );
		$anemos_eutf_custom_css .= '.eut-page-title {';
		$anemos_eutf_custom_css .= anemos_eutf_get_css_color( 'background-color', $bg_color_custom );
		$anemos_eutf_custom_css .= '}';
	}

	$content_bg_color = anemos_eutf_array_value( $title, 'content_bg_color', 'light' );
	if ( 'custom' == $content_bg_color ) {
		$content_bg_color_custom = anemos_eutf_array_value( $title, 'content_bg_color_custom', '#ffffff' );
		$anemos_eutf_custom_css .= '.eut-page-title .eut-title-content-wrapper {';
		$anemos_eutf_custom_css .= anemos_eutf_get_css_color( 'background-color', $content_bg_color_custom );
		$anemos_eutf_custom_css .= '}';
	}

	$subheading_color = anemos_eutf_array_value( $title, 'subheading_color', 'dark' );
	if ( 'custom' == $subheading_color ) {
		$subheading_color_custom = anemos_eutf_array_value( $title, 'subheading_color_custom', '#000000' );
		$anemos_eutf_custom_css .= '.eut-page-title .eut-title-categories, .eut-page-title .eut-title-meta {';
		$anemos_eutf_custom_css .= anemos_eutf_get_css_color( 'color', $subheading_color_custom );
		$anemos_eutf_custom_css .= '}';
	}

	$title_color = anemos_eutf_array_value( $title, 'title_color', 'dark' );
	if ( 'custom' == $title_color ) {
		$title_color_custom = anemos_eutf_array_value( $title, 'title_color_custom', '#000000' );
		$anemos_eutf_custom_css .= '.eut-page-title .eut-title, .eut-page-title .eut-title-meta {';
		$anemos_eutf_custom_css .= anemos_eutf_get_css_color( 'color', $title_color_custom );
		$anemos_eutf_custom_css .= '}';
	}

	$caption_color = anemos_eutf_array_value( $title, 'caption_color', 'dark' );
	if ( 'custom' == $caption_color ) {
		$caption_color_custom = anemos_eutf_array_value( $title, 'caption_color_custom', '#000000' );
		$anemos_eutf_custom_css .= '.eut-page-title .eut-description {';
		$anemos_eutf_custom_css .= anemos_eutf_get_css_color( 'color', $caption_color_custom );
		$anemos_eutf_custom_css .= '}';
	}

	return $anemos_eutf_custom_css;
}

function anemos_eutf_get_global_button_style() {
	$anemos_eutf_custom_css = "";

	$button_type = anemos_eutf_option( 'button_type', 'simple' );
	$button_shape = anemos_eutf_option( 'button_shape', 'square' );
	$button_color = anemos_eutf_option( 'button_color', 'primary-1' );
	$button_hover_color = anemos_eutf_option( 'button_hover_color', 'black' );

	$anemos_eutf_colors = array(
		'primary-1' => anemos_eutf_option( 'body_primary_1_color' ),
		'primary-2' => anemos_eutf_option( 'body_primary_2_color' ),
		'primary-3' => anemos_eutf_option( 'body_primary_3_color' ),
		'primary-4' => anemos_eutf_option( 'body_primary_4_color' ),
		'primary-5' => anemos_eutf_option( 'body_primary_5_color' ),
		'primary-5' => anemos_eutf_option( 'body_primary_5_color' ),
		'green' => '#66bb6a',
		'red' => '#ff5252',
		'orange' => '#ffb74d',
		'aqua' => '#1de9b6',
		'blue' => '#00b0ff',
		'purple' => '#b388ff',
		'black' => '#000000',
		'grey' => '#bababa',
		'white' => '#ffffff',
	);

		$anemos_eutf_custom_css .= ".eut-modal input[type='submit']:not(.eut-custom-btn), #eut-theme-wrapper input[type='submit']:not(.eut-custom-btn), #eut-theme-wrapper input[type='reset']:not(.eut-custom-btn), #eut-theme-wrapper input[type='button']:not(.eut-custom-btn), #eut-theme-wrapper button:not(.eut-custom-btn):not(.vc_general), #eut-theme-wrapper .eut-search button[type='submit'] {";
			switch( $button_shape ) {
				case "round":
					$anemos_eutf_custom_css .= "-webkit-border-radius: 3px;";
					$anemos_eutf_custom_css .= "border-radius: 3px;";
				break;
				case "extra-round":
					$anemos_eutf_custom_css .= "-webkit-border-radius: 50px;";
					$anemos_eutf_custom_css .= "border-radius: 50px;";
				break;
				case "square":
				default:
				break;
			}

			$default_color = anemos_eutf_option( 'body_primary_1_color' );
			$color = anemos_eutf_array_value( $anemos_eutf_colors, $button_color, $default_color );

			if ( "outline" == $button_type ) {

				$anemos_eutf_custom_css .= "border: 2px solid;";
				$anemos_eutf_custom_css .= "background-color: transparent;";
				//$anemos_eutf_custom_css .= "background-image: none;";
				$anemos_eutf_custom_css .= "border-color: " . esc_attr( $color ) . ";";
				$anemos_eutf_custom_css .= "color: " . esc_attr( $color ) . ";";

			} else {
				$anemos_eutf_custom_css .= "background-color: " . esc_attr( $color ) . ";";
				if ( 'white' == $button_color ) {
					$anemos_eutf_custom_css .= "color: #bababa;";
				} else {
					$anemos_eutf_custom_css .= "color: #ffffff;";
				}
			}


		$anemos_eutf_custom_css .= "}";

		$anemos_eutf_custom_css .= ".eut-modal input[type='submit']:not(.eut-custom-btn):hover, #eut-theme-wrapper input[type='submit']:not(.eut-custom-btn):hover, #eut-theme-wrapper input[type='reset']:not(.eut-custom-btn):hover, #eut-theme-wrapper input[type='button']:not(.eut-custom-btn):hover, #eut-theme-wrapper button:not(.eut-custom-btn):not(.vc_general):hover,.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover, .woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover, #eut-theme-wrapper .eut-search button[type='submit']:hover {";

		$hover_color = anemos_eutf_array_value( $anemos_eutf_colors, $button_hover_color, "#bababa" );

		if ( "outline" == $button_type ) {

			$anemos_eutf_custom_css .= "background-color: " . esc_attr( $hover_color ) . ";";
			$anemos_eutf_custom_css .= "border-color: " . esc_attr( $hover_color ) . ";";
			if ( 'white' == $button_hover_color ) {
				$anemos_eutf_custom_css .= "color: #bababa;";
			} else {
				$anemos_eutf_custom_css .= "color: #ffffff;";
			}

		} else {
			$anemos_eutf_custom_css .= "background-color: " . esc_attr( $hover_color ) . ";";
			if ( 'white' == $button_hover_color ) {
				$anemos_eutf_custom_css .= "color: #bababa;";
			} else {
				$anemos_eutf_custom_css .= "color: #ffffff;";
			}
		}

		$anemos_eutf_custom_css .= "}";

	echo anemos_eutf_get_css_output( $anemos_eutf_custom_css );
}


function anemos_eutf_get_global_shape_style() {
	$anemos_eutf_custom_css = "";

	$global_shape = anemos_eutf_option( 'button_shape', 'square' );

		$anemos_eutf_custom_css .= "#eut-related-post .eut-related-title, .eut-nav-btn a, .eut-bar-socials li a, #eut-single-post-tags .eut-tags li a, #eut-single-post-categories .eut-categories li a, .widget.widget_tag_cloud a, .eut-header-back-top .eut-item, #eut-body #eut-theme-wrapper .eut-newsletter input[type='email'], #eut-bottom-bar .eut-social li a, #eut-theme-wrapper .eut-search:not(.eut-search-modal) input[type='text'], #eut-socials-modal .eut-social li a, .eut-pagination ul li, .eut-dropcap span.eut-style-2 {";
			switch( $global_shape ) {
				case "round":
					$anemos_eutf_custom_css .= "-webkit-border-radius: 3px !important;";
					$anemos_eutf_custom_css .= "border-radius: 3px !important;";
				break;
				case "extra-round":
					$anemos_eutf_custom_css .= "-webkit-border-radius: 50px !important;";
					$anemos_eutf_custom_css .= "border-radius: 50px !important;";
				break;
				case "square":
				default:
				break;
			}

		$anemos_eutf_custom_css .= "}";

	echo anemos_eutf_get_css_output( $anemos_eutf_custom_css );
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
