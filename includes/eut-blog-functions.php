<?php

/*
 *	Blog Helper functions
 *
 * 	@version	1.0
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */


 /**
 * Prints excerpt
 */
if ( !function_exists('anemos_eutf_print_post_excerpt') ) {
	function anemos_eutf_print_post_excerpt( $post_format = 'standard' ) {

		$excerpt_length = anemos_eutf_option( 'blog_excerpt_length' );
		$excerpt_more = anemos_eutf_option( 'blog_excerpt_more' );


		if ( 'large' != anemos_eutf_option( 'blog_style', 'large' ) ) {
			$excerpt_length = anemos_eutf_option( 'blog_excerpt_length_small' );
			$excerpt_auto = '1';
		} else {
			$excerpt_length = anemos_eutf_option( 'blog_excerpt_length' );
			$excerpt_auto = anemos_eutf_option( 'blog_auto_excerpt' );
		}

		if ( 'link' ==  $post_format || 'quote' ==  $post_format ) {
			$excerpt_more = 0;
			$excerpt_auto = '1';
		}

		if ( '1' == $excerpt_auto ) {
			if ( 'quote' ==  $post_format ) {
				echo anemos_eutf_quote_excerpt( $excerpt_length );
			} else {
				echo anemos_eutf_excerpt( $excerpt_length, $excerpt_more  );
			}
		} else {
			if ( '1' == $excerpt_more ) {
				the_content( esc_html__( 'read more', 'anemos' ) );
			} else {
				the_content( '' );
			}
		}

	}
}

function anemos_eutf_isotope_inner_before() {
	echo '<div class="eut-blog-item-inner eut-isotope-item-inner">';
}
function anemos_eutf_isotope_inner_after() {
	echo '</div>';
}
add_action( 'anemos_eutf_inner_post_loop_item_before', 'anemos_eutf_isotope_inner_before' );
add_action( 'anemos_eutf_inner_post_loop_item_after', 'anemos_eutf_isotope_inner_after' );

function anemos_eutf_get_loop_title_heading_tag() {

	$heading = anemos_eutf_option( 'blog_heading_tag', 'auto' );
	$blog_style = anemos_eutf_option( 'blog_style', 'large' );

	if( 'auto' != $heading ) {
		$title_tag = $heading;
	} else {
		$title_tag = 'h3';
		if( 'large' == $blog_style || 'small' == $blog_style  ) {
			$title_tag = 'h2';
		}
	}
	return $title_tag;
}

function anemos_eutf_get_loop_title_heading() {

	$heading = anemos_eutf_option( 'blog_heading', 'auto' );
	$blog_style = anemos_eutf_option( 'blog_style', 'large' );

	if( 'auto' != $heading ) {
		$heading_class = $heading;
	} else {
		$heading_class = 'h3';
		if( 'large' == $blog_style || 'small' == $blog_style  ) {
			$heading_class = 'h2';
		}
	}
	return $heading_class;
}

function anemos_eutf_loop_post_title( $class = "eut-post-title" ) {
	$title_tag = anemos_eutf_get_loop_title_heading_tag();
	$title_class = anemos_eutf_get_loop_title_heading();
	the_title( '<' . tag_escape( $title_tag ) . ' class="' . esc_attr( $class ). ' eut-' . esc_attr( $title_class ) . '" itemprop="name headline">', '</' . tag_escape( $title_tag ) . '>' );
}
function anemos_eutf_loop_post_title_link() {
	$title_tag = anemos_eutf_get_loop_title_heading_tag();
	$title_class = anemos_eutf_get_loop_title_heading();
	the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><' . tag_escape( $title_tag ) . ' class="eut-post-title eut-text-hover-primary-1 eut-' . esc_attr( $title_class ) . '" itemprop="name headline">', '</' . tag_escape( $title_tag ) . '></a>' );
}
function anemos_eutf_loop_post_title_hidden() {
	$title_tag = anemos_eutf_get_loop_title_heading_tag();
	the_title( '<' . tag_escape( $title_tag ) . ' class="eut-hidden" itemprop="name headline">', '</' . tag_escape( $title_tag ) . '>' );
}


add_action( 'anemos_eutf_inner_post_loop_item_title', 'anemos_eutf_loop_post_title' );
add_action( 'anemos_eutf_inner_post_loop_item_title_link', 'anemos_eutf_loop_post_title_link' );
add_action( 'anemos_eutf_inner_post_loop_item_title_hidden', 'anemos_eutf_loop_post_title_hidden' );

 /**
 * Prints Single Post Title
 */
if ( !function_exists('anemos_eutf_print_post_simple_title') ) {
	function anemos_eutf_print_post_simple_title() {
		global $post;
		if ( anemos_eutf_check_title_visibility() ) {

			$post_id = $post->ID;
			$anemos_eutf_custom_title_options = get_post_meta( $post_id, '_anemos_eutf_custom_title_options', true );

			$anemos_eutf_title_style = anemos_eutf_option( 'post_title_style' );
			$anemos_eutf_page_title_custom = anemos_eutf_array_value( $anemos_eutf_custom_title_options, 'custom', $anemos_eutf_title_style );
			if ( 'simple' == $anemos_eutf_page_title_custom ) {
				echo '<div class="eut-container">';
				the_title( '<h1 class="eut-single-simple-title" itemprop="name headline">', '</h1>' );
				echo '</div>';
			} else {
				the_title( '<h2 class="eut-hidden" itemprop="name headline">', '</h2>' );
			}
		} else {
			the_title( '<h2 class="eut-hidden" itemprop="name headline">', '</h2>' );
		}
	}
}


/**
 * Gets Blog Class
 */
function anemos_eutf_get_blog_class() {

	$blog_style = anemos_eutf_option( 'blog_style', 'large' );
	$blog_mode = anemos_eutf_option( 'blog_mode', 'no-shadow-mode' );
	switch( $blog_style ) {

		case 'small':
			$anemos_eutf_blog_style_class = 'eut-blog eut-blog-small eut-non-isotope';
			break;
		case 'masonry':
			$anemos_eutf_blog_style_class = 'eut-blog eut-blog-columns eut-blog-masonry eut-isotope eut-with-gap';
			break;
		case 'grid':
			$anemos_eutf_blog_style_class = 'eut-blog eut-blog-columns eut-blog-grid eut-isotope eut-with-gap';
			break;
		case 'large':
		default:
			$anemos_eutf_blog_style_class = 'eut-blog eut-blog-large eut-non-isotope';
			break;

	}

	if ( 'shadow-mode' == $blog_mode && ( 'masonry' == $blog_style || 'grid' == $blog_style ) ) {
		$anemos_eutf_blog_style_class .= ' eut-with-shadow';
	}

	return $anemos_eutf_blog_style_class;

}
/**
 * Gets post class
 */
function anemos_eutf_get_post_class( $extra_class = '' ) {

	$blog_style = anemos_eutf_option( 'blog_style', 'large' );
	$post_classes = array( 'eut-blog-item' );
	if ( !empty( $extra_class ) ){
		$post_classes[] = $extra_class;
	}

	switch( $blog_style ) {

		case 'small':
			$post_classes[] = 'eut-small-post';
			$post_classes[] = 'eut-non-isotope-item';
			break;

		case 'masonry':
		case 'grid':
			$post_classes[] = 'eut-isotope-item';
			break;
		default:
			$post_classes[] = 'eut-big-post';
			$post_classes[] = 'eut-non-isotope-item';
			break;
	}

	return implode( ' ', $post_classes );

}

/**
 * Prints post item data
 */
function anemos_eutf_print_blog_data() {

	$blog_style = anemos_eutf_option( 'blog_style', 'large' );
	$columns_large_screen = anemos_eutf_option( 'blog_columns_large_screen', '5' );
	$columns = anemos_eutf_option( 'blog_columns', '4' );
	$columns_tablet_landscape  = anemos_eutf_option( 'blog_columns_tablet_landscape', '4' );
	$columns_tablet_portrait  = anemos_eutf_option( 'blog_columns_tablet_portrait', '2' );
	$columns_mobile  = anemos_eutf_option( 'blog_columns_mobile', '1' );
	$item_spinner  = anemos_eutf_option( 'blog_item_spinner', 'no' );
	$gutter_size = 40;


	switch( $blog_style ) {

		case 'masonry':
			echo 'data-columns="' . esc_attr( $columns ) . '" data-columns-large-screen="' . esc_attr( $columns_large_screen ) . '" data-columns-tablet-landscape="' . esc_attr( $columns_tablet_landscape ) . '" data-columns-tablet-portrait="' . esc_attr( $columns_tablet_portrait ) . '" data-columns-mobile="' . esc_attr( $columns_mobile ) . '" data-layout="masonry" data-spinner="' . esc_attr( $item_spinner ) . '" data-gutter-size="' . esc_attr( $gutter_size ) . '"';
			break;
		case 'grid':
			echo 'data-columns="' . esc_attr( $columns ) . '" data-columns-large-screen="' . esc_attr( $columns_large_screen ) . '" data-columns-tablet-landscape="' . esc_attr( $columns_tablet_landscape ) . '" data-columns-tablet-portrait="' . esc_attr( $columns_tablet_portrait ) . '" data-columns-mobile="' . esc_attr( $columns_mobile ) . '" data-layout="fitRows" data-spinner="' . esc_attr( $item_spinner ) . '" data-gutter-size="' . esc_attr( $gutter_size ) . '"';
			break;
		default:
			break;
	}

}

 /**
 * Prints post feature media
 */
function anemos_eutf_print_post_feature_media( $post_type ) {

	if ( !anemos_eutf_visibility( 'blog_media_area', '1' ) ){
		return;
	}
	$blog_image_prio = anemos_eutf_option( 'blog_image_prio', 'no' );
	$blog_style = anemos_eutf_option( 'blog_style', 'large' );

	if ( 'yes' == $blog_image_prio && has_post_thumbnail() ) {
		anemos_eutf_print_post_feature_image();
	} else {

		switch( $post_type ) {
			case 'audio':
				anemos_eutf_print_post_audio();
				break;
			case 'video':
				anemos_eutf_print_post_video();
				break;
			case 'gallery':
				$slider_items = anemos_eutf_post_meta( '_anemos_eutf_post_slider_items' );
				switch( $blog_style ) {
					case 'small':
					case 'grid':
						$image_size = 'anemos-eutf-small-rect-horizontal';
						break;
					case 'masonry' :
						$image_size  = 'anemos-eutf-small-rect-horizontal';
						break;
					default:
						$image_size = 'anemos-eutf-large-rect-horizontal';
						break;
				}
				if ( !empty( $slider_items ) ) {
					anemos_eutf_print_gallery_slider( 'slider', $slider_items, $image_size  );
				}
				break;
			default:
				anemos_eutf_print_post_feature_image();
				break;
		}
	}

}


 /**
 * Prints post feature image
 */
function anemos_eutf_print_post_feature_image() {

	$blog_style = anemos_eutf_option( 'blog_style', 'large' );
	$blog_image_mode = anemos_eutf_option( 'blog_image_mode', 'auto' );
	$blog_masonry_image_mode = anemos_eutf_option( 'blog_masonry_image_mode', 'large' );

	if ( 'masonry' == $blog_style) {
		$blog_image_mode = $blog_masonry_image_mode;
	}

	if ( 'resize' == $blog_image_mode ) {
		switch( $blog_style ) {
			case 'large':
				$image_size = 'anemos-eutf-fullscreen';
			break;
			default:
				$image_size = 'large';
			break;
		}
	} elseif ( 'large' == $blog_image_mode  ) {
		$image_size = 'large';
	} elseif( 'medium_large' == $blog_image_mode ) {
		$image_size = 'medium_large';
	} elseif( 'medium' == $blog_image_mode ) {
		$image_size = 'medium';
	} else {
		switch( $blog_style ) {
			case 'large':
				$image_size = 'anemos-eutf-large-rect-horizontal';
			break;
			case 'small':
			case 'grid':
			default:
				$image_size = 'anemos-eutf-small-rect-horizontal-wide';
				break;
		}
	}

	$image_href = get_permalink();

	if ( has_post_thumbnail() ) {
?>
	<div class="eut-media eut-image-hover clearfix">
		<a class="eut-post-link" href="<?php echo esc_url( $image_href ); ?>"></a>
		<?php the_post_thumbnail( $image_size ); ?>
	</div>
<?php
	}

}

 /**
 * Prints post meta area
 */
if ( !function_exists('anemos_eutf_print_post_meta_top') ) {
	function anemos_eutf_print_post_meta_top() {

		$blog_style = anemos_eutf_option( 'blog_style', 'large' );
?>
			<div class="eut-post-header">
				<div class="eut-small-text eut-post-date"><?php anemos_eutf_print_post_date(); ?></div>
				<?php do_action( 'anemos_eutf_inner_post_loop_item_title_link' ); ?>
			</div>
<?php

	}
}

if ( !function_exists('anemos_eutf_print_post_meta_bottom') ) {
	function anemos_eutf_print_post_meta_bottom() {

		$blog_style = anemos_eutf_option( 'blog_style', 'large' );
?>
		<div class="eut-post-meta-wrapper">
			<ul class="eut-post-meta eut-small-text">
				<?php anemos_eutf_print_post_author_by( 'list'); ?>
				<?php anemos_eutf_print_post_loop_comments(); ?>
				<?php anemos_eutf_print_like_counter_overview(); ?>
			</ul>
		</div>
<?php
	}
}

 /**
 * Prints post meta Sticky
 */
function anemos_eutf_print_post_meta_sticky() {

?>
		<!-- SINGLE POST META -->
		<div id="eut-single-post-meta-sticky">
			<div class="eut-meta-item">

			<?php
				if ( anemos_eutf_visibility( 'post_author_visibility', '1' ) ) {
			?>
				<div class="eut-post-author">
					<?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>
					<a class="eut-link-text eut-author-name eut-text-content eut-text-hover-primary-1 eut-toggle-modal" href="#eut-post-author-modal"><?php the_author(); ?></a>
				</div>
			<?php
				}
			?>
				<?php anemos_eutf_print_post_date_simple( 'default' ); ?>
			</div>

			<?php anemos_eutf_print_post_socials(); ?>

		</div>
		<!-- END SINGLE POST META -->
<?php

}

 /**
 * Prints post meta
 */
function anemos_eutf_print_post_meta() {

?>
		<!-- SINGLE POST META -->
		<div id="eut-single-post-meta" class="eut-margin-top-2x">
			<div class="eut-tags-categories">
				<?php
					anemos_eutf_print_post_tags();
					anemos_eutf_print_post_categories();
				?>
			</div>
			<div class="eut-single-post-responsive-meta">
				<?php
					anemos_eutf_print_post_socials();
					anemos_eutf_print_post_navigation( 'default' );
					if ( anemos_eutf_visibility( 'post_author_visibility' ) ) {
						anemos_eutf_print_post_about_author();
					}
				?>
			</div>
		</div>
		<!-- END SINGLE POST META -->
<?php

}


 /**
 * Prints post Categories
 */
function anemos_eutf_print_post_categories() {
	global $post;
	$post_id = $post->ID;

	if ( anemos_eutf_visibility( 'post_category_visibility', '1' ) ) {
	?>
		<div id="eut-single-post-categories">
			<?php
			$post_terms = wp_get_object_terms( $post_id, 'category', array( 'fields' => 'ids' ) );
			if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {
				$term_ids = implode( ',' , $post_terms );
				echo '<ul class="eut-categories">';
				echo wp_list_categories( 'title_li=&style=list&echo=0&hierarchical=0&taxonomy=category&include=' . $term_ids );
				echo '</ul>';
			}
			?>
		</div>

	<?php
	}
}

 /**
 * Prints Post Title Categories
 */
function anemos_eutf_print_post_title_categories( $post_id = null) {
	if ( ! $post_id ) {
		$post_id = get_the_ID();
	}

	$post_terms = wp_get_object_terms( $post_id, 'category', array( 'fields' => 'ids' ) );
	if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {
		$term_ids = implode( ',' , $post_terms );
		echo '<ul class="eut-categories">';
		echo wp_list_categories( 'title_li=&style=list&echo=0&hierarchical=0&taxonomy=category&include=' . $term_ids );
		echo '</ul>';
	}
}


 /**
 * Prints Post Title Meta
 */

function anemos_eutf_print_post_title_meta() {

?>
	<div class="eut-title-meta">
		<ul class="eut-post-meta eut-link-text">
			<?php anemos_eutf_print_post_author_by( 'list_modal'); ?>
			<?php anemos_eutf_print_post_date_simple( 'list'); ?>
			<?php anemos_eutf_print_post_loop_comments(); ?>
			<?php anemos_eutf_print_like_counter_overview( 'single' ); ?>
		</ul>
	</div>
<?php
}

 /**
 * Prints Post Title Meta
 */

if ( !function_exists('anemos_eutf_print_feature_post_title_meta') ) {
	function anemos_eutf_print_feature_post_title_meta( $post_id = null ) {

		if( $post_id ) {
			$post_author_id = get_post_field( 'post_author', $post_id );
			$userdata = get_userdata( $post_author_id );
			$post_comments_number = get_comments_number( $post_id );
			$post_likes = anemos_eutf_option( 'post_social', '', 'eut-likes' );

?>
	<div class="eut-title-meta">
		<ul class="eut-post-meta eut-link-text">
			<li class="eut-post-author"><i class="eut-icon-user"></i>
				<span><?php echo esc_html( $userdata->display_name ); ?></span>
			</li>
			<li class="eut-post-date"><i class="eut-icon-date"></i>
				<time datetime="<?php echo esc_attr( get_the_date( 'c', $post_id  ) ); ?>"><?php echo esc_html( get_the_date( '', $post_id  ) ); ?></time>
			</li>
			<li class="eut-post-comments"><i class="eut-icon-comment"></i>
				<span><?php echo esc_html( $post_comments_number ); ?></span>
			</li>
			<?php if ( !empty( $post_likes  ) ) { ?>
			<li class="eut-like-counter <?php echo anemos_eutf_likes( $post_id, 'status' ); ?>"><i class="eut-icon-heart-o"></i>
				<span><?php echo anemos_eutf_likes( $post_id ); ?></span>
			</li>
			<?php } ?>
		</ul>
	</div>
<?php
		}
	}
}


 /**
 * Prints post author by
 */
function anemos_eutf_print_post_author_by( $mode = '') {

	if ( anemos_eutf_visibility( 'blog_author_visibility', '1' ) ) {

		if( 'list' == $mode ) {
			echo '<li class="eut-post-author"><i class="eut-icon-user"></i>';
			echo '<span>' . get_the_author_link() . '</span>';
			echo '</li>';
		} else if( 'list_modal' == $mode ) {
			echo '<li class="eut-post-author"><i class="eut-icon-user"></i>';
			echo '<span><a class="eut-toggle-modal" href="#eut-post-author-modal">' . get_the_author() . '</a></span>';
			echo '</li>';
		} else {
			echo '<div class="eut-post-author">';
			echo '<span>' . get_the_author_link() . '</span>';
			echo '</div>';
		}
	}
}



 /**
 * Prints like counter for overview pages
 */
function anemos_eutf_print_like_counter_overview( $mode = '' ) {

	if( anemos_eutf_visibility( 'blog_like_visibility', '1' ) ) {
		anemos_eutf_print_like_counter( $mode );
	}

}

 /**
 * Prints like counter
 */
function anemos_eutf_print_like_counter( $mode = '' ) {

	$post_likes = anemos_eutf_option( 'post_social', '', 'eut-likes' );
	if ( !empty( $post_likes  ) ) {
		global $post;
		$post_id = $post->ID;
		if ( 'single' == $mode ) {
?>
		<li class="eut-like-counter <?php echo anemos_eutf_likes( $post_id, 'status' ); ?>"><i class="eut-icon-heart-o"></i><span><?php echo anemos_eutf_likes( $post_id ); ?></span></li>
<?php
		} else {
?>
		<li class="eut-like-counter eut-small-text <?php echo anemos_eutf_likes( $post_id, 'status' ); ?>"><i class="eut-icon-heart-o"></i><span><?php echo anemos_eutf_likes( $post_id ); ?></span></li>
<?php
		}
	}

}

/**
 * Prints post date
 */
function anemos_eutf_print_post_date( $mode = '' ) {
	if ( anemos_eutf_visibility( 'blog_date_visibility' ) ) {
		$class = "";
		if( 'list' == $mode ) {
			echo '<li class="eut-post-date"><i class="eut-icon-date"></i>';
		} else if ( 'quote' == $mode ) {
			$class = "eut-post-date eut-small-text eut-circle-arrow";
		} else if ( 'default' == $mode ) {
			$class = "eut-post-date eut-link-text eut-text-primary-1";
		}
		global $post;
?>
	<time class="<?php echo esc_attr( $class ); ?>" datetime="<?php echo mysql2date( 'c', $post->post_date ); ?>">
		<?php echo esc_html( get_the_date() ); ?>
	</time>
<?php
		if( 'list' == $mode ) {
			echo '</li>';
		}
	}
}
function anemos_eutf_print_post_date_simple( $mode = '' ) {
	if ( anemos_eutf_visibility( 'blog_date_visibility' ) ) {
		$class = "";
		if( 'list' == $mode ) {
			echo '<li class="eut-post-date"><i class="eut-icon-date"></i>';
		} else if ( 'quote' == $mode ) {
			$class = "eut-post-date eut-small-text eut-circle-arrow";
		} else if ( 'default' == $mode ) {
			$class = "eut-post-date eut-link-text eut-text-primary-1";
		}
		global $post;
?>
	<time class="<?php echo esc_attr( $class ); ?>" datetime="<?php echo mysql2date( 'c', $post->post_date ); ?>">
		<?php echo esc_html( get_the_date() ); ?>
	</time>
<?php
		if( 'list' == $mode ) {
			echo '</li>';
		}
	}
}

function anemos_eutf_print_post_loop_comments() {
	if ( anemos_eutf_visibility( 'blog_comments_visibility' ) ) {
?>
	<li class="eut-post-comments"><i class="eut-icon-comment"></i><span><?php comments_number( '0' , '1', '%' ); ?></span></li>
	<?php
	}
}

function anemos_eutf_print_post_loop_categories() {
	if ( anemos_eutf_visibility( 'blog_categories_visibility' ) ) {
		global $post;
		$post_id = $post->ID;
		$post_terms = wp_get_object_terms( $post_id, 'category', array( 'fields' => 'ids' ) );
		if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {
			$term_ids = implode( ',' , $post_terms );
			echo '<ul class="eut-categories">';
			echo wp_list_categories( 'title_li=&style=list&echo=0&hierarchical=0&taxonomy=category&include=' . $term_ids );
			echo '</ul>';
		}
	}
}

/**
 * Prints post feature bg image container
 */
function anemos_eutf_print_post_bg_image_container( $options ) {

	$bg_color = anemos_eutf_array_value( $options, 'bg_color' );
	$bg_hover_color = anemos_eutf_array_value( $options, 'bg_hover_color' );
	$bg_opacity = anemos_eutf_array_value( $options, 'bg_opacity', '80' );
	$mode = anemos_eutf_array_value( $options, 'mode' );
	$image_size = anemos_eutf_array_value( $options, 'image_size', 'large' );
	$overlay = true;

	if( 'anemos-eutf-small-square' == $image_size ) {
		$bg_image_size_class = 'eut-small-square';
	} elseif( 'anemos-eutf-small-rect-vertical' == $image_size ) {
		$bg_image_size_class = 'eut-small-rect-vertical';
	} elseif( 'anemos-eutf-small-rect-horizontal' == $image_size ) {
		$bg_image_size_class = 'eut-small-rect-horizontal';
	} else {
		$bg_image_size_class = 'eut-full-size';
	}

	$link_classes = array();
	$link_classes[] = 'eut-bg-' . $bg_color;
	if( !empty( $bg_hover_color ) ){
		$link_classes[] = 'eut-bg-hover-' . $bg_hover_color;
	}
	$link_classes[] = 'eut-bg-overlay';
	if ( has_post_thumbnail() ) {
		$link_classes[] = 'eut-opacity-' . $bg_opacity;
		if ( 'none' == $bg_opacity || '0' == $bg_opacity ) {
			$overlay = false;
		}
	} else {
		$link_classes[] = 'eut-opacity-100';
	}
	$link_class_string = implode( ' ', $link_classes );

?>
	<div class="eut-media">
		<div class="eut-bg-wrapper <?php echo esc_attr( $bg_image_size_class ); ?>">
			<?php if( $overlay ) { ?>
			<div class="<?php echo esc_attr( $link_class_string ); ?>"></div>
			<?php } ?>
			<?php anemos_eutf_print_post_bg_image( $image_size ); ?>
		</div>
	</div>
<?php
}

function anemos_eutf_print_post_image( $options = array() ) {

	$image_size = anemos_eutf_array_value( $options, 'image_size', 'large' );

	if ( has_post_thumbnail() ) {
		the_post_thumbnail( $image_size );
	} else {
		$image_src = get_template_directory_uri() . '/images/transparent/' . $image_size . '.png';
?>
		<img class="attachment-<?php echo esc_attr( $image_size ); ?>" src="<?php echo esc_url( $image_src ); ?>" alt="<?php the_title_attribute(); ?>"/>
<?php
	}
}


function anemos_eutf_print_post_bg_image( $image_size = 'large' ) {

	if ( has_post_thumbnail() ) {
		$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
		$attachment_src = wp_get_attachment_image_src( $post_thumbnail_id, $image_size );
		$image_src = $attachment_src[0];
?>
		<div class="eut-bg-image" style="background-image: url(<?php echo esc_url( $image_src ); ?>);"></div>
<?php
	}
}

/**
 * Prints author avatar
 */
function anemos_eutf_print_post_author() {
	global $post;
	$post_id = $post->ID;
	$post_type = get_post_type( $post_id );

	if ( 'page' == $post_type ||  'portfolio' == $post_type  ) {
		return;
	}
?>
	<div class="eut-post-author">
		<?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>
	</div>
<?php

}

/**
 * Prints audio shortcode of post format audio
 */
function anemos_eutf_print_post_audio() {
	global $wp_embed;

	$audio_mode = anemos_eutf_post_meta( '_anemos_eutf_post_type_audio_mode' );
	$audio_mp3 = anemos_eutf_post_meta( '_anemos_eutf_post_audio_mp3' );
	$audio_ogg = anemos_eutf_post_meta( '_anemos_eutf_post_audio_ogg' );
	$audio_wav = anemos_eutf_post_meta( '_anemos_eutf_post_audio_wav' );
	$audio_embed = anemos_eutf_post_meta( '_anemos_eutf_post_audio_embed' );

	$audio_output = '';

	if( empty( $audio_mode ) && !empty( $audio_embed ) ) {
		echo '<div class="eut-media">' . $audio_embed . '</div>';
	} else {
		if ( !empty( $audio_mp3 ) || !empty( $audio_ogg ) || !empty( $audio_wav ) ) {

			$audio_output .= '[audio ';

			if ( !empty( $audio_mp3 ) ) {
				$audio_output .= 'mp3="'. esc_url( $audio_mp3 ) .'" ';
			}
			if ( !empty( $audio_ogg ) ) {
				$audio_output .= 'ogg="'. esc_url( $audio_ogg ) .'" ';
			}
			if ( !empty( $audio_wav ) ) {
				$audio_output .= 'wav="'. esc_url( $audio_wav ) .'" ';
			}

			$audio_output .= ']';

			echo '<div class="eut-media">';
			echo  do_shortcode( $audio_output );
			echo '</div>';
		}
	}

}

/**
 * Prints video of the video post format
 */
function anemos_eutf_print_post_video() {

	$video_mode = anemos_eutf_post_meta( '_anemos_eutf_post_type_video_mode' );
	$video_webm = anemos_eutf_post_meta( '_anemos_eutf_post_video_webm' );
	$video_mp4 = anemos_eutf_post_meta( '_anemos_eutf_post_video_mp4' );
	$video_ogv = anemos_eutf_post_meta( '_anemos_eutf_post_video_ogv' );
	$video_poster = anemos_eutf_post_meta( '_anemos_eutf_post_video_poster' );
	$video_embed = anemos_eutf_post_meta( '_anemos_eutf_post_video_embed' );

	anemos_eutf_print_media_video( $video_mode, $video_webm, $video_mp4, $video_ogv, $video_embed, $video_poster );
}

/**
 * Prints video popup of the video post format
 */
function anemos_eutf_print_post_video_popup() {

	$video_mode = anemos_eutf_post_meta( '_anemos_eutf_post_type_video_mode' );

	if( empty( $video_mode ) ) {
		$video_embed = anemos_eutf_post_meta( '_anemos_eutf_post_video_embed' );
		if ( !empty( $video_embed ) ) {
?>
	<a class="eut-video-popup eut-post-icon eut-video-icon eut-icon-video eut-bg-primary-1" href="<?php echo esc_url( $video_embed ); ?>"></a>
<?php
		}
	} else {
		$video_webm = anemos_eutf_post_meta( '_anemos_eutf_post_video_webm' );
		$video_mp4 = anemos_eutf_post_meta( '_anemos_eutf_post_video_mp4' );
		$video_ogv = anemos_eutf_post_meta( '_anemos_eutf_post_video_ogv' );
		$video_id = uniqid('eut-video-id-');
		if ( !empty( $video_webm ) || !empty( $video_mp4 ) || !empty( $video_ogv ) ) {
?>
	<a class="eut-html5-video-popup eut-post-icon eut-video-icon eut-icon-video eut-bg-primary-1" href="#<?php echo esc_attr( $video_id ); ?>">
		<div id="<?php echo esc_attr( $video_id ); ?>" class="eut-html5-video-popup-container mfp-hide">
			<?php anemos_eutf_print_post_video(); ?>
		</div>
	</a>
<?php
		}
	}
}

/**
 * Prints Post Tags
 */
function anemos_eutf_print_post_tags() {
	global $post;
	$post_id = $post->ID;
?>
	<?php if ( anemos_eutf_visibility( 'post_tag_visibility', '1' ) ) { ?>

		<!-- META -->
		<div id="eut-single-post-tags" class="clearfix">
			<?php if ( anemos_eutf_visibility( 'post_tag_visibility', '1' ) ) { ?>
			<?php the_tags('<ul class="eut-tags"><li>','</li><li>','</li></ul>'); ?>
			<?php } ?>
		</div>
		<!-- END META -->


	<?php } ?>

<?php
}

/**
 * Prints a bar with tags and categories ( Single Post Only )
 */
function anemos_eutf_print_blog_meta_bar() {
	global $post;
	$post_id = $post->ID;
?>
	<?php if ( anemos_eutf_visibility( 'post_tag_visibility', '1' ) || anemos_eutf_visibility( 'post_category_visibility', '1' ) ) { ?>

		<!-- META -->
		<div id="eut-single-post-meta-sticky-bar" class="eut-singular-section clearfix eut-align-center eut-border eut-border-top">
			<div class="eut-container eut-padding-top-md eut-padding-bottom-md">
				<div class="eut-wrapper">

					<?php if ( anemos_eutf_visibility( 'post_category_visibility', '1' ) ) { ?>

					<div class="eut-single-post-meta-sticky eut-categories">
					 <?php
						$post_terms = wp_get_object_terms( $post_id, 'category', array( 'fields' => 'ids' ) );
						if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {
							$term_ids = implode( ',' , $post_terms );
							echo '<ul class="eut-small-text">';
							echo wp_list_categories( 'title_li=&style=list&echo=0&hierarchical=0&taxonomy=category&include=' . $term_ids );
							echo '</ul>';
						}
					?>
					</div>

					<?php } ?>

					<?php if ( anemos_eutf_visibility( 'post_tag_visibility', '1' ) ) { ?>

					<div class="eut-single-post-meta-sticky eut-tags">
						<?php the_tags('<ul class="eut-small-text"><li>','</li><li>','</li></ul>'); ?>
					</div>

					<?php } ?>
				</div>
			</div>
		</div>
		<!-- END META -->


	<?php } ?>

<?php
}

/**
 * Prints related posts ( Single Post )
 */
function anemos_eutf_print_related_posts() {

	$anemos_eutf_tag_ids = array();
	$anemos_eutf_max_related = 3;

	$anemos_eutf_tags_list = get_the_tags();
	if ( ! empty( $anemos_eutf_tags_list ) ) {

		foreach ( $anemos_eutf_tags_list as $tag ) {
			array_push( $anemos_eutf_tag_ids, $tag->term_id );
		}

	}

	$exclude_ids = array( get_the_ID() );
	$tag_found = false;

	$query = array();
	if ( ! empty( $anemos_eutf_tag_ids ) ) {
		$args = array(
			'tag__in' => $anemos_eutf_tag_ids,
			'post__not_in' => $exclude_ids,
			'posts_per_page' => $anemos_eutf_max_related,
			'paged' => 1,
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) {
			$tag_found = true;
		}
	}

	if ( $tag_found ) {
?>

	<div id="eut-related-post" class="eut-singular-section eut-fullwidth clearfix">
		<div class="eut-container">
			<div class="eut-related-title">
				<div class="eut-description eut-small-text"><?php esc_html_e( 'You might also like', 'anemos' ); ?></div>
				<h2 class="eut-title eut-h5"><?php esc_html_e( 'One of the following', 'anemos' ); ?></h2>
			</div>
			<div class="eut-related-post-wrapper">
				<?php anemos_eutf_print_loop_related( $query ); ?>
			</div>
		</div>
	</div>
<?php
	}
}


/**
 * Prints single related item ( used in related posts )
 */
function anemos_eutf_print_loop_related( $query, $filter = ''  ) {

	if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

		$anemos_eutf_link = get_permalink();
		$anemos_eutf_target = '_self';

		if ( 'link' == get_post_format() ) {
			$anemos_eutf_link = get_post_meta( get_the_ID(), '_anemos_eutf_post_link_url', true );
			$new_window = get_post_meta( get_the_ID(), '_anemos_eutf_post_link_new_window', true );
			if( empty( $anemos_eutf_link ) ) {
				$anemos_eutf_link = get_permalink();
			}

			if( !empty( $new_window ) ) {
				$anemos_eutf_target = '_blank';
			}
		}


?>
		<article id="eut-related-post-<?php the_ID(); ?>" <?php post_class( 'eut-related-item' ); ?> itemscope itemType="http://schema.org/BlogPosting">

			<a href="<?php echo esc_url( $anemos_eutf_link ); ?>" target="<?php echo esc_attr( $anemos_eutf_target ); ?>">
				<div class="eut-content">
					<div class="eut-description eut-small-text"><?php anemos_eutf_print_post_date(); ?></div>
					<h5 class="eut-title" itemprop="name headline"><?php the_title(); ?></h5>
					<?php anemos_eutf_print_post_structured_data(); ?>
				</div>
			</a>
			<?php
				if ( has_post_thumbnail() ) {
			?>
				<div class="eut-background-wrapper">
					<?php
						$image_size = 'anemos-eutf-small-rect-horizontal';
						$post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
						$attachment_src = wp_get_attachment_image_src( $post_thumbnail_id, $image_size );
						$image_src = $attachment_src[0];
					?>
					<div class="eut-bg-image" style="background-image: url(<?php echo esc_url( $image_src ); ?>);"></div>
				</div>
			<?php
				}
			?>
		</article>
<?php

	endwhile;
	else :
	endif;

	wp_reset_postdata();

}

/**
 * Likes ajax callback ( used in Single Post )
 */
function anemos_eutf_likes_callback( $post_id ) {

	$likes = 0;
	$status = "";

	if ( isset( $_POST['eut_likes_id'] ) ) {
		$post_id = $_POST['eut_likes_id'];
		$response = anemos_eutf_likes( $post_id, 'update' );
	} else {
		$response = array(
			'status' => $status,
			'likes' => $likes,
		);
	}
	wp_send_json( $response );

	die();
}

add_action( 'wp_ajax_anemos_eutf_likes_callback', 'anemos_eutf_likes_callback' );
add_action( 'wp_ajax_nopriv_anemos_eutf_likes_callback', 'anemos_eutf_likes_callback' );

function anemos_eutf_likes( $post_id, $action = 'get' ) {

	$status = '';

	if( !is_numeric( $post_id ) ) {
		$likes = 0;
	} else {
		$likes = get_post_meta( $post_id, '_anemos_eutf_likes', true );
	}

	if( !$likes || !is_numeric( $likes ) ) {
		$likes = 0;
	}

	if ( 'update' == $action ) {

		if( is_numeric( $post_id ) ) {
			if ( isset( $_COOKIE['_anemos_eutf_likes_' . $post_id] ) ) {

				unset( $_COOKIE['_anemos_eutf_likes_' . $post_id] );
				setcookie( '_anemos_eutf_likes_' . $post_id, "", 1, '/' );
				if( 0 != $likes ) {
					$likes--;
					update_post_meta( $post_id, '_anemos_eutf_likes', $likes );
				}

			} else {
				$likes++;
				update_post_meta( $post_id, '_anemos_eutf_likes', $likes );
				setcookie('_anemos_eutf_likes_' . $post_id, $post_id, time()*20, '/');
				$status = 'active';
			}
		}

		return $response = array(
			'status' => $status,
			'likes' => $likes,
		);

	} elseif ( 'status' == $action ) {
		if( is_numeric( $post_id ) ) {
			if ( isset( $_COOKIE['_anemos_eutf_likes_' . $post_id] ) && 0 != $likes) {
				$status = 'active';
			}
		}
		return $status;
	}

	return $likes;
}

 /**
 * Prints Post Navigation
 */
if ( !function_exists('anemos_eutf_print_post_navigation') ) {
	function anemos_eutf_print_post_navigation( $mode = '' ) {

		$anemos_eutf_in_same_term = anemos_eutf_visibility( 'post_nav_same_term', '0' );
		$prev_post = get_adjacent_post( $anemos_eutf_in_same_term, '', true);
		$next_post = get_adjacent_post( $anemos_eutf_in_same_term, '', false);

		if ( ( anemos_eutf_visibility( 'post_nav_visibility', '1' )  && ( is_a( $prev_post, 'WP_Post' ) || is_a( $next_post, 'WP_Post' ) ) ) ) {

		$nav_class = "";
		if ( 'sticky' == $mode ) {
			$nav_class = "eut-post-nav eut-sticky-post-nav";
		} else if ( 'default' == $mode ) {
			$nav_class = "eut-post-nav eut-border eut-default-post-nav eut-padding-top-2x eut-margin-top-2x";
		}

		$btn_class = "";
		if ( 'sticky' == $mode ) {
			$btn_class = "eut-border eut-bg-hover-black";
		} else if ( 'default' == $mode ) {
			$btn_class = "eut-text-hover-primary-1";
		}
?>
			<!-- POST NAVIGATION -->
			<div class="<?php echo esc_attr( $nav_class ); ?>">

				<?php if ( anemos_eutf_visibility( 'post_nav_visibility', '1' ) && is_a( $prev_post, 'WP_Post' ) ) { ?>
				<div class="eut-nav-btn eut-prev">
					<a class="<?php echo esc_attr( $btn_class ); ?>" href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">
						<div class="eut-nav-arrow eut-icon-nav-left"></div>
						<div class="eut-nav-content">
							<div class="eut-nav-subtitle eut-small-text"><?php esc_html_e( 'Previous', 'anemos' ); ?></div>
							<div class="eut-nav-title eut-link-text"><?php echo get_the_title( $prev_post->ID ); ?></div>
						</div>
					</a>
				</div>
				<?php } ?>

				<?php if ( anemos_eutf_visibility( 'post_nav_visibility', '1' ) && is_a( $next_post, 'WP_Post' ) ) { ?>
				<div class="eut-nav-btn eut-next">
					<a class="<?php echo esc_attr( $btn_class ); ?>" href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
						<div class="eut-nav-content">
							<div class="eut-nav-subtitle eut-small-text"><?php esc_html_e( 'Next', 'anemos' ); ?></div>
							<div class="eut-nav-title eut-link-text"><?php echo get_the_title( $next_post->ID ); ?></div>
						</div>
						<div class="eut-nav-arrow eut-icon-nav-right"></div>
					</a>
				</div>
				<?php } ?>

			</div>
			<!-- END POST NAVIGATION -->

<?php
		}
	}
}


 /**
 * Prints Post Socials
 */
if ( !function_exists('anemos_eutf_print_post_socials') ) {
	function anemos_eutf_print_post_socials() {

		$post_socials = anemos_eutf_option( 'post_social');
		if ( is_array( $post_socials ) ) {
			$post_socials = array_filter( $post_socials );
		} else {
			$post_socials = '';
		}

		if ( !empty( $post_socials ) ) {

?>
			<!-- POST SOCIALS -->
			<div class="eut-post-socials">
				<?php anemos_eutf_print_post_social(); ?>
			</div>
			<!-- END POST SOCIALS -->

<?php
		}
	}
}


 /**
 * Prints Navigation Bar ( Post )
 */
if ( !function_exists('anemos_eutf_print_post_bar') ) {
	function anemos_eutf_print_post_bar() {

		$post_socials = anemos_eutf_option( 'post_social');
		if ( is_array( $post_socials ) ) {
			$post_socials = array_filter( $post_socials );
		} else {
			$post_socials = '';
		}

		$anemos_eutf_in_same_term = anemos_eutf_visibility( 'post_nav_same_term', '0' );
		$prev_post = get_adjacent_post( $anemos_eutf_in_same_term, '', true);
		$next_post = get_adjacent_post( $anemos_eutf_in_same_term, '', false);

		if ( ( anemos_eutf_visibility( 'post_nav_visibility', '1' )  && ( is_a( $prev_post, 'WP_Post' ) || is_a( $next_post, 'WP_Post' ) ) ) || !empty( $post_socials ) ) {

?>
			<!-- POST BAR -->
			<div id="eut-post-bar" class="eut-navigation-bar eut-singular-section eut-fullwidth clearfix">
				<div class="eut-container">
					<div class="eut-wrapper">

						<div class="eut-post-bar-item">
							<?php if ( anemos_eutf_visibility( 'post_nav_visibility', '1' ) && is_a( $prev_post, 'WP_Post' ) ) { ?>

							<a class="eut-nav-item eut-prev" href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">
								<div class="eut-arrow eut-icon-nav-left"></div>
								<div class="eut-nav-content">
									<div class="eut-nav-title eut-small-text"><?php esc_html_e( 'Read Previous', 'anemos' ); ?></div>
									<h6 class="eut-title"><?php echo get_the_title( $prev_post->ID ); ?></h6>
								</div>
							</a>

							<?php } ?>
						</div>

						<div class="eut-post-bar-item eut-post-socials eut-list-divider eut-link-text eut-align-center">
							<?php anemos_eutf_print_post_social(); ?>
						</div>

						<div class="eut-post-bar-item">
							<?php if ( anemos_eutf_visibility( 'post_nav_visibility', '1' ) && is_a( $next_post, 'WP_Post' ) ) { ?>

							<a class="eut-nav-item eut-next" href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
								<div class="eut-nav-content">
									<div class="eut-nav-title eut-small-text"><?php esc_html_e( 'Read Next', 'anemos' ); ?></div>
									<h6 class="eut-title"><?php echo get_the_title( $next_post->ID ); ?></h6>
								</div>
								<div class="eut-arrow eut-icon-nav-right"></div>
							</a>

							<?php } ?>
						</div>

					</div>
				</div>
			</div>
			<!-- END POST BAR -->
<?php
		}
	}
}

 /**
 * Prints social icons ( Post )
 */
if ( !function_exists('anemos_eutf_print_post_social') ) {
	function anemos_eutf_print_post_social() {

		$post_socials = anemos_eutf_option( 'post_social');
		if ( is_array( $post_socials ) ) {
			$post_socials = array_filter( $post_socials );
		} else {
			$post_socials = '';
		}

		if ( !empty( $post_socials ) ) {
			global $post;
			$post_id = $post->ID;

			$post_email = anemos_eutf_option( 'post_social', '', 'email' );
			$post_facebook = anemos_eutf_option( 'post_social', '', 'facebook' );
			$post_twitter = anemos_eutf_option( 'post_social', '', 'twitter' );
			$post_linkedin = anemos_eutf_option( 'post_social', '', 'linkedin' );
			$post_googleplus = anemos_eutf_option( 'post_social', '', 'google-plus' );
			$post_reddit = anemos_eutf_option( 'post_social', '', 'reddit' );
			$post_likes = anemos_eutf_option( 'post_social', '', 'eut-likes' );
			$anemos_eutf_permalink = get_permalink( $post_id );
			$anemos_eutf_title = get_the_title( $post_id );

			$post_email_string = 'mailto:?subject=' . $anemos_eutf_title . '&body=' . $anemos_eutf_title . ': ' . $anemos_eutf_permalink;
?>
			<!-- SOCIALS -->
			<ul class="eut-bar-socials">
				<?php if ( !empty( $post_facebook  ) ) { ?>
				<li class="eut-facebook"><a href="<?php echo esc_url( $anemos_eutf_permalink ); ?>" title="<?php echo esc_attr( $anemos_eutf_title ); ?>" class="eut-social-share-facebook"><i class="fa fa-facebook"></i><span class="eut-link-text">Facebook</span></a></li>
				<?php } ?>
				<?php if ( !empty( $post_twitter  ) ) { ?>
				<li class="eut-twitter"><a href="<?php echo esc_url( $anemos_eutf_permalink ); ?>" title="<?php echo esc_attr( $anemos_eutf_title ); ?>" class="eut-social-share-twitter"><i class="fa fa-twitter"></i><span class="eut-link-text">Twitter</span></a></li>
				<?php } ?>
				<?php if ( !empty( $post_linkedin  ) ) { ?>
				<li class="eut-linkedin"><a href="<?php echo esc_url( $anemos_eutf_permalink ); ?>" title="<?php echo esc_attr( $anemos_eutf_title ); ?>" class="eut-social-share-linkedin"><i class="fa fa-linkedin"></i><span class="eut-link-text">Linkedin</span></a></li>
				<?php } ?>
				<?php if ( !empty( $post_googleplus  ) ) { ?>
				<li class="eut-google"><a href="<?php echo esc_url( $anemos_eutf_permalink ); ?>" title="<?php echo esc_attr( $anemos_eutf_title ); ?>" class="eut-social-share-googleplus"><i class="fa fa-google-plus"></i><span class="eut-link-text">Google +</span></a></li>
				<?php } ?>
				<?php if ( !empty( $post_reddit ) ) { ?>
				<li class="eut-reddit"><a href="<?php echo esc_url( $anemos_eutf_permalink ); ?>" title="<?php echo esc_attr( $anemos_eutf_title ); ?>" class="eut-social-share-reddit"><i class="fa fa-reddit"></i><span class="eut-link-text">reddit</span></a></li>
				<?php } ?>
				<?php if ( !empty( $post_email  ) ) { ?>
				<li class="eut-email"><a href="<?php echo esc_attr( $post_email_string ); ?>" title="<?php echo esc_attr( $anemos_eutf_title ); ?>" class="eut-social-share-email"><i class="fa fa-envelope"></i><span class="eut-link-text"><?php echo esc_html__( 'E-mail', 'anemos' ); ?></span></a></li>
				<?php } ?>
				<?php if ( !empty( $post_likes  ) ) { ?>
				<li class="eut-likes"><a href="#" class="eut-like-counter-link <?php echo anemos_eutf_likes( $post_id, 'status' ); ?>" data-post-id="<?php echo esc_attr( $post_id ); ?>"><i class="fa fa-heart"></i><span class="eut-like-counter eut-link-text"><?php echo anemos_eutf_likes( $post_id ); ?></span></a></li>
				<?php } ?>
			</ul>
			<!-- END SOCIALS -->
<?php
		}
	}
}

 /**
 * Prints About Author ( Post )
 */
 if ( !function_exists('anemos_eutf_print_post_about_author') ) {
	function anemos_eutf_print_post_about_author( $mode = '' ) {

		$author_thumb_size = 50;

		$author_class = 'eut-about-author eut-align-center eut-padding-top-2x eut-margin-top-2x eut-border eut-border-top clearfix';
		if ( 'modal' == $mode ) {
			$author_class = 'eut-about-author eut-align-center';
			$author_thumb_size = 120;
		}
		if ( 'overview' == $mode ) {
			$author_class = 'eut-about-author eut-align-center eut-margin-bottom-md eut-author-overview';
			$author_thumb_size = 120;
		}
?>
		<!-- ABOUT AUTHOR -->
		<div class="<?php echo esc_attr( $author_class ); ?>">
			<div class="eut-author-image">
				<?php echo get_avatar( get_the_author_meta('ID'), $author_thumb_size ); ?>
			</div>
			<div class="eut-author-info">
				<h3 class="eut-title"><?php the_author_link(); ?></h3>
				<p><?php echo get_the_author_meta( 'user_description' ); ?></p>
				<?php if ( 'overview' != $mode ) { ?>
				<a class="eut-author-read-more eut-link-text eut-text-primary-1" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php esc_html_e( 'All stories by:', 'anemos' ) . '  '; ?><?php the_author(); ?> </a>
				<?php } ?>
			</div>
		</div>
		<!-- ABOUT AUTHOR -->
<?php
	}
}

/**
 * Prints post structured data
 */
if ( !function_exists( 'anemos_eutf_print_post_structured_data' ) ) {
	function anemos_eutf_print_post_structured_data( $args = array() ) {
		global $post;

		if ( has_post_thumbnail() ) {
			$url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full') ;
			$image_url = $url[0];
			$image_width = $url[1];
			$image_height = $url[2];

		} else {
			$image_url = get_template_directory_uri() . '/images/empty/thumbnail.jpg';
			$image_width = 150;
			$image_height = 150;
		}
	?>
		<span class="eut-hidden">
			<span class="eut-structured-data" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
			   <span itemprop='url' ><?php echo esc_url( $image_url ); ?></span>
			   <span itemprop='height' ><?php echo esc_html( $image_width ); ?></span>
			   <span itemprop='width' ><?php echo esc_html( $image_height ); ?></span>
			</span>
			<?php if ( anemos_eutf_visibility( 'blog_author_visibility', '1' ) ) { ?>
			<span class="eut-structured-data" itemprop="author" itemscope itemtype="http://schema.org/Person">
				<span itemprop="name"><?php the_author(); ?></span>
			</span>
			<span class="eut-structured-data" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
				<span itemprop='name'><?php the_author(); ?></span>
				<span itemprop='logo' itemscope itemtype='http://schema.org/ImageObject'>
					<span itemprop='url'><?php echo esc_url( get_avatar_url( get_the_author_meta( 'ID' ) ) ); ?></span>
				</span>
			</span>
			<?php } else { ?>
			<span class="eut-structured-data" itemprop="author" itemscope itemtype="http://schema.org/Person">
				<span itemprop="name"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></span>
			</span>
			<span class="eut-structured-data" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
				<span itemprop='name'><?php echo esc_html( get_bloginfo( 'name' ) ); ?></span>
				<span itemprop='logo' itemscope itemtype='http://schema.org/ImageObject'>
					<span itemprop='url'><?php echo esc_url( $image_url ); ?></span>
				</span>
			</span>
			<?php } ?>
			<time class="eut-structured-data" itemprop="datePublished" datetime="<?php echo get_the_time('c'); ?>"><?php echo get_the_date(); ?></time>
			<time class="eut-structured-data" itemprop="dateModified"  datetime="<?php echo get_the_modified_time('c'); ?>"><?php echo get_the_modified_date(); ?></time>
			<span class="eut-structured-data" itemprop="mainEntityOfPage" itemscope itemtype="http://schema.org/WebPage" itemid="<?php echo esc_url( get_permalink() ); ?>"></span>
		</span>
	<?php
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
