<?php
/**
 *  Dynamic typography css style
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */

$typo_css = "";

/**
 * Typography
 * ----------------------------------------------------------------------------
 */

$typo_css .= "

body,
p {
	font-size: " . anemos_eutf_option( 'body_font', '14px', 'font-size'  ) . ";
	font-family: " . anemos_eutf_option( 'body_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'body_font', 'normal', 'font-weight'  ) . ";
	line-height: " . anemos_eutf_option( 'body_font', '36px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'body_font', '0px', 'letter-spacing'  ) . "
}

";

/* Logo as text */
$typo_css .= "

#eut-header .eut-logo.eut-logo-text a {
	font-family: " . anemos_eutf_option( 'logo_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'logo_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'logo_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'logo_font', '11px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'logo_font', 'uppercase', 'text-transform'  ) . ";
	" . anemos_eutf_css_option( 'logo_font', '0px', 'letter-spacing'  ) . "
}

";


/* Main Menu  */
$typo_css .= "

.eut-main-menu .eut-wrapper > ul > li > a,
.eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li > a,
#eut-theme-wrapper #eut-hidden-menu ul.eut-menu > li > a,
.eut-toggle-hiddenarea .eut-label,
#eut-hidden-menu ul.eut-menu > li.megamenu > ul > li > a,
.eut-main-menu .eut-wrapper > ul > li ul li.eut-goback a,
#eut-hidden-menu ul.eut-menu > li ul li.eut-goback a {
	font-family: " . anemos_eutf_option( 'main_menu_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'main_menu_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'main_menu_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'main_menu_font', '11px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'main_menu_font', 'uppercase', 'text-transform'  ) . ";
	" . anemos_eutf_css_option( 'main_menu_font', '0px', 'letter-spacing'  ) . "
}

.eut-slide-menu .eut-main-menu .eut-wrapper ul li.megamenu ul li:not(.eut-goback) > a,
#eut-hidden-menu.eut-slide-menu ul li.megamenu ul li:not(.eut-goback) > a,
.eut-main-menu .eut-wrapper > ul > li ul li a,
#eut-header .eut-shoppin-cart-content {
	font-family: " . anemos_eutf_option( 'sub_menu_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'sub_menu_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'sub_menu_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'sub_menu_font', '11px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'sub_menu_font', 'uppercase', 'text-transform'  ) . ";
	" . anemos_eutf_css_option( 'sub_menu_font', '0px', 'letter-spacing'  ) . "
}

.eut-main-menu .eut-menu-description,
#eut-hidden-menu .eut-menu-description {
	font-family: " . anemos_eutf_option( 'description_menu_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'description_menu_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'description_menu_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'description_menu_font', '11px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'description_menu_font', 'uppercase', 'text-transform'  ) . ";
	" . anemos_eutf_css_option( 'description_menu_font', '0px', 'letter-spacing'  ) . "
}

";


/* Headings */
$typo_css .= "

h1,
.eut-h1,
#eut-theme-wrapper .eut-modal .eut-search input[type='text'],
.eut-dropcap span,
p.eut-dropcap:first-letter,
h2,
.eut-h2,
h3,
.eut-h3,
h4,
.eut-h4,
h5,
.eut-h5,
h6,
.eut-h6,
#eut-anemos-sticky-header .eut-page-title-wrapper {
	font-family: " . anemos_eutf_option( 'headings_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'headings_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'headings_font', 'normal', 'font-style'  ) . ";
	text-transform: " . anemos_eutf_option( 'headings_font', 'uppercase', 'text-transform'  ) . ";
}

h1,
.eut-h1,
#eut-theme-wrapper .eut-modal .eut-search input[type='text'],
.eut-dropcap span,
p.eut-dropcap:first-letter {
	font-size: " . anemos_eutf_option( 'h1_font', '56px', 'font-size'  ) . ";
	line-height: " . anemos_eutf_option( 'h1_font', '60px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'h1_font', '0px', 'letter-spacing'  ) . "
}

h2,
.eut-h2 {
	font-size: " . anemos_eutf_option( 'h2_font', '36px', 'font-size'  ) . ";
	line-height: " . anemos_eutf_option( 'h2_font', '40px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'h2_font', '0px', 'letter-spacing'  ) . "
}

h3,
.eut-h3 {
	font-size: " . anemos_eutf_option( 'h3_font', '30px', 'font-size'  ) . ";
	line-height: " . anemos_eutf_option( 'h3_font', '33px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'h3_font', '0px', 'letter-spacing'  ) . "
}

h4,
.eut-h4 {
	font-size: " . anemos_eutf_option( 'h4_font', '23px', 'font-size'  ) . ";
	line-height: " . anemos_eutf_option( 'h4_font', '26px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'h4_font', '0px', 'letter-spacing'  ) . "
}

h5,
.eut-h5 {
	font-size: " . anemos_eutf_option( 'h5_font', '18px', 'font-size'  ) . ";
	line-height: " . anemos_eutf_option( 'h5_font', '20px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'h5_font', '0px', 'letter-spacing'  ) . "
}

h6,
.eut-h6,
.vc_tta.vc_general .vc_tta-panel-title,
#eut-main-content .vc_tta.vc_general .vc_tta-tab > a {
	font-size: " . anemos_eutf_option( 'h6_font', '16px', 'font-size'  ) . ";
	line-height: " . anemos_eutf_option( 'h6_font', '18px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'h6_font', '0px', 'letter-spacing'  ) . "
}

";

/* Page Title */
$typo_css .= "

#eut-page-title .eut-title,
#eut-blog-title .eut-title {
	font-family: " . anemos_eutf_option( 'page_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'page_title', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'page_title', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'page_title', '60px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'page_title', 'uppercase', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'page_title', '60px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'page_title', '0px', 'letter-spacing'  ) . "
}

#eut-page-title .eut-line-divider,
#eut-blog-title .eut-line-divider {
	font-size: " . anemos_eutf_option( 'page_title', '60px', 'font-size'  ) . ";
}

#eut-page-title .eut-description,
#eut-blog-title .eut-description,
#eut-blog-title .eut-description p {
	font-family: " . anemos_eutf_option( 'page_description', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'page_description', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'page_description', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'page_description', '24px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'page_description', 'none', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'page_description', '60px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'page_description', '0px', 'letter-spacing'  ) . "
}

";


/* Post Title */
$typo_css .= "

#eut-post-title .eut-title-categories {
	font-family: " . anemos_eutf_option( 'post_title_categories', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'post_title_categories', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'post_title_categories', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'post_title_categories', '16px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'post_title_categories', 'none', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'post_title_categories', '24px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'post_title_categories', '0px', 'letter-spacing'  ) . "
}

#eut-post-title .eut-title,
.eut-single-simple-title {
	font-family: " . anemos_eutf_option( 'post_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'post_title', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'post_title', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'post_title', '60px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'post_title', 'uppercase', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'post_title', '112px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'post_title', '0px', 'letter-spacing'  ) . "
}

#eut-post-title .eut-line-divider {
	font-size: " . anemos_eutf_option( 'post_title', '60px', 'font-size'  ) . ";
}

#eut-post-title .eut-description {
	font-family: " . anemos_eutf_option( 'post_title_desc', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'post_title_desc', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'post_title_desc', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'post_title_desc', '26px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'post_title_desc', 'uppercase', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'post_title_desc', '32px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'post_title_desc', '0px', 'letter-spacing'  ) . "
}


";

/* Feature Section Custom */
$typo_css .= "

#eut-feature-section .eut-subheading {
	font-family: " . anemos_eutf_option( 'feature_subheading_custom_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'feature_subheading_custom_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'feature_subheading_custom_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'feature_subheading_custom_font', '60px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'feature_subheading_custom_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'feature_subheading_custom_font', '112px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'feature_subheading_custom_font', '0px', 'letter-spacing'  ) . "
}

#eut-feature-section .eut-title {
	font-family: " . anemos_eutf_option( 'feature_title_custom_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'feature_title_custom_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'feature_title_custom_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'feature_title_custom_font', '60px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'feature_title_custom_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'feature_title_custom_font', '112px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'feature_title_custom_font', '0px', 'letter-spacing'  ) . "
}

#eut-feature-section .eut-line-divider {
	font-size: " . anemos_eutf_option( 'feature_title_custom_font', '60px', 'font-size'  ) . ";
}

#eut-feature-section .eut-description {
	font-family: " . anemos_eutf_option( 'feature_desc_custom_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'feature_desc_custom_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'feature_desc_custom_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'feature_desc_custom_font', '60px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'feature_desc_custom_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'feature_desc_custom_font', '112px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'feature_desc_custom_font', '0px', 'letter-spacing'  ) . "
}


";

/* Feature Section Fullscreen */
$typo_css .= "

#eut-feature-section.eut-fullscreen .eut-subheading {
	font-family: " . anemos_eutf_option( 'feature_subheading_full_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'feature_subheading_full_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'feature_subheading_full_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'feature_subheading_full_font', '60px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'feature_subheading_full_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'feature_subheading_full_font', '112px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'feature_subheading_full_font', '0px', 'letter-spacing'  ) . "
}

#eut-feature-section.eut-fullscreen .eut-title {
	font-family: " . anemos_eutf_option( 'feature_title_full_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'feature_title_full_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'feature_title_full_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'feature_title_full_font', '60px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'feature_title_full_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'feature_title_full_font', '112px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'feature_title_full_font', '0px', 'letter-spacing'  ) . "
}

#eut-feature-section.eut-fullscreen .eut-line-divider {
	font-size: " . anemos_eutf_option( 'feature_title_full_font', '60px', 'font-size'  ) . ";
}

";

$typo_css .= "

#eut-feature-section.eut-fullscreen .eut-description {
	font-family: " . anemos_eutf_option( 'feature_desc_full_font', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'feature_desc_full_font', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'feature_desc_full_font', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'feature_desc_full_font', '60px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'feature_desc_full_font', 'uppercase', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'feature_desc_full_font', '112px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'feature_desc_full_font', '0px', 'letter-spacing'  ) . "
}

";


/* Special Text */
$typo_css .= "

.eut-leader-text,
.eut-leader-text p,
p.eut-leader-text {
	font-family: " . anemos_eutf_option( 'leader_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'leader_text', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'leader_text', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'leader_text', '34px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'leader_text', 'none', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'leader_text', '36px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'leader_text', '0px', 'letter-spacing'  ) . "
}

.eut-subtitle,
.eut-subtitle p,
.eut-subtitle-text {
	font-family: " . anemos_eutf_option( 'subtitle_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'subtitle_text', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'subtitle_text', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'subtitle_text', '34px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'subtitle_text', 'none', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'subtitle_text', '36px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'subtitle_text', '0px', 'letter-spacing'  ) . "
}

.eut-small-text,
span.wpcf7-not-valid-tip,
div.wpcf7-mail-sent-ok,
div.wpcf7-validation-errors,
.eut-post-meta-wrapper .eut-categories li {
	font-family: " . anemos_eutf_option( 'small_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'small_text', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'small_text', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'small_text', '34px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'small_text', 'none', 'text-transform'  ) . ";
	" . anemos_eutf_css_option( 'small_text', '0px', 'letter-spacing'  ) . "
}

";

/* Link Text */
$anemos_eutf_btn_size = anemos_eutf_option( 'link_text', '13px', 'font-size'  );
$anemos_eutf_btn_size = filter_var( $anemos_eutf_btn_size, FILTER_SANITIZE_NUMBER_INT );

$anemos_eutf_btn_size_xsm = $anemos_eutf_btn_size * 0.7;
$anemos_eutf_btn_size_sm = $anemos_eutf_btn_size * 0.85;
$anemos_eutf_btn_size_lg = $anemos_eutf_btn_size * 1.2;
$anemos_eutf_btn_size_xlg = $anemos_eutf_btn_size * 1.35;

$typo_css .= "

.eut-link-text,
.eut-btn,
input[type='submit'],
input[type='reset'],
input[type='button'],
button:not(.mfp-arrow):not(.eut-search-btn),
#eut-header .eut-shoppin-cart-content .total,
#eut-header .eut-shoppin-cart-content .button,
#cancel-comment-reply-link,
.eut-anchor-menu .eut-anchor-wrapper .eut-container > ul > li > a,
.eut-anchor-menu .eut-anchor-wrapper .eut-container ul.sub-menu li a {
	font-family: " . anemos_eutf_option( 'link_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . " !important;
	font-weight: " . anemos_eutf_option( 'link_text', 'normal', 'font-weight'  ) . " !important;
	font-style: " . anemos_eutf_option( 'link_text', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'link_text', '13px', 'font-size'  ) . " !important;
	text-transform: " . anemos_eutf_option( 'link_text', 'uppercase', 'text-transform'  ) . ";
	" . anemos_eutf_css_option( 'link_text', '0px', 'letter-spacing'  ) . "
}

.eut-btn.eut-btn-extrasmall,
.widget.woocommerce button[type='submit'] {
	font-size: " . round( $anemos_eutf_btn_size_xsm, 0 ) . "px !important;
}

.eut-btn.eut-btn-small {
	font-size: " . round( $anemos_eutf_btn_size_sm, 0 ) . "px !important;
}

.eut-btn.eut-btn-large {
	font-size: " . round( $anemos_eutf_btn_size_lg, 0 ) . "px !important;
}

.eut-btn.eut-btn-extralarge {
	font-size: " . round( $anemos_eutf_btn_size_xlg, 0 ) . "px !important;
}


";

/* Widget Text */
$typo_css .= "

.eut-widget-title,
h3#reply-title {
	font-family: " . anemos_eutf_option( 'widget_title', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'widget_title', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'widget_title', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'widget_title', '34px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'widget_title', 'none', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'widget_title', '36px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'widget_title', '0px', 'letter-spacing'  ) . "
}

.widget,
.widgets,
.widget p,
#eut-hidden-menu ul.eut-menu li a {
	font-family: " . anemos_eutf_option( 'widget_text', 'Arial, Helvetica, sans-serif', 'font-family'  ) . ";
	font-weight: " . anemos_eutf_option( 'widget_text', 'normal', 'font-weight'  ) . ";
	font-style: " . anemos_eutf_option( 'widget_text', 'normal', 'font-style'  ) . ";
	font-size: " . anemos_eutf_option( 'widget_text', '34px', 'font-size'  ) . ";
	text-transform: " . anemos_eutf_option( 'widget_text', 'none', 'text-transform'  ) . ";
	line-height: " . anemos_eutf_option( 'widget_text', '36px', 'line-height'  ) . ";
	" . anemos_eutf_css_option( 'widget_text', '0px', 'letter-spacing'  ) . "
}


";


//Responsive Typography

$anemos_eutf_responsive_fonts_group_headings =  array (
	array(
		'id'   => 'h1_font',
		'selector'  => 'h1,.eut-h1,#eut-theme-wrapper .eut-modal .eut-search input[type="text"],.eut-dropcap span,p.eut-dropcap:first-letter',
	),
	array(
		'id'   => 'h2_font',
		'selector'  => 'h2,.eut-h2',
	),
	array(
		'id'   => 'h3_font',
		'selector'  => 'h3,.eut-h3',
	),
	array(
		'id'   => 'h4_font',
		'selector'  => 'h4,.eut-h4',
	),
	array(
		'id'   => 'h5_font',
		'selector'  => 'h5,.eut-h5',
	),
	array(
		'id'   => 'h6_font',
		'selector'  => 'h6,.eut-h6',
	),
);


$anemos_eutf_responsive_fonts_group_1 =  array (
	array(
		'id'   => 'page_title',
		'selector'  => '#eut-page-title .eut-title,#eut-blog-title .eut-title,#eut-page-title .eut-line-divider,#eut-blog-title .eut-line-divider',
	),
	array(
		'id'   => 'post_title',
		'selector'  => '#eut-post-title .eut-title,#eut-post-title .eut-line-divider',
	),
	array(
		'id'   => 'feature_title_custom_font',
		'selector'  => '#eut-feature-section .eut-title,#eut-feature-section .eut-line-divider',
	),
	array(
		'id'   => 'feature_title_full_font',
		'selector'  => '#eut-feature-section.eut-fullscreen .eut-title,#eut-feature-section.eut-fullscreen .eut-line-divider',
	),
	array(
		'id'   => 'feature_desc_full_font',
		'selector'  => '#eut-feature-section.eut-fullscreen .eut-description',
	),
);

$anemos_eutf_responsive_fonts_group_2 =  array (
	array(
		'id'   => 'page_description',
		'selector'  => '#eut-page-title .eut-description,#eut-blog-title .eut-description,#eut-blog-title .eut-description p',
	),
	array(
		'id'   => 'post_title_categories',
		'selector'  => '#eut-post-title .eut-title-categories',
	),
	array(
		'id'   => 'post_title_desc',
		'selector'  => '#eut-post-title .eut-description',
	),
	array(
		'id'   => 'feature_subheading_custom_font',
		'selector'  => '#eut-feature-section .eut-subheading',
	),
	array(
		'id'   => 'feature_subheading_full_font',
		'selector'  => '#eut-feature-section.eut-fullscreen .eut-subheading',
	),
	array(
		'id'   => 'feature_desc_custom_font',
		'selector'  => '#eut-feature-section .eut-description',
	),
	array(
		'id'   => 'leader_text',
		'selector'  => '.eut-leader-text,.eut-leader-text p,p.eut-leader-text',
	),
	array(
		'id'   => 'subtitle_text',
		'selector'  => '.eut-subtitle,.eut-subtitle-text',
	),
	array(
		'id'   => 'link_text',
		'selector'  => '#eut-theme-wrapper .eut-link-text,#eut-theme-wrapper a.eut-btn,#eut-theme-wrapper input[type="submit"],#eut-theme-wrapper input[type="reset"],#eut-theme-wrapper input[type="button"],#eut-theme-wrapper button:not(.mfp-arrow):not(.eut-search-btn),#cancel-comment-reply-link',
	),
);

function anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts = array() , $threshold = 35, $ratio = 0.7) {

	$css = '';

	if ( !empty( $anemos_eutf_responsive_fonts ) && $ratio < 1 ) {

		foreach ( $anemos_eutf_responsive_fonts as $font ) {
			$anemos_eutf_size = anemos_eutf_option( $font['id'], '32px', 'font-size'  );
			$anemos_eutf_size = filter_var( $anemos_eutf_size, FILTER_SANITIZE_NUMBER_INT );
			if ( $anemos_eutf_size >= $threshold ) {
				$line_height = anemos_eutf_option( $font['id'], '32px', 'line-height'  );
				$line_height = filter_var( $line_height, FILTER_SANITIZE_NUMBER_INT );

				$line_height = $line_height / $anemos_eutf_size;
				$anemos_eutf_size = $anemos_eutf_size * $ratio;

				if ( 'link_text' == $font['id'] ) {
					$css .= $font['selector'] . " {
						font-size: " . $anemos_eutf_size . "px !important;
						line-height: " . round( $line_height, 2 ) . "em;
					}
					";
				} else {
					$css .= $font['selector'] . " {
						font-size: " . $anemos_eutf_size . "px;
						line-height: " . round( $line_height, 2 ) . "em;
					}
					";
				}
			}
		}

	}

	return $css;
}

$tablet_landscape_threshold_headings = anemos_eutf_option( 'typography_tablet_landscape_threshold_headings', 20 );
$tablet_landscape_ratio_headings = anemos_eutf_option( 'typography_tablet_landscape_ratio_headings', 1 );
$tablet_portrait_threshold_headings = anemos_eutf_option( 'typography_tablet_portrait_threshold_headings', 20 );
$tablet_portrait_ratio_headings = anemos_eutf_option( 'typography_tablet_portrait_ratio_headings', 1 );
$mobile_threshold_headings = anemos_eutf_option( 'typography_mobile_threshold_headings', 20 );
$mobile_ratio_headings = anemos_eutf_option( 'typography_mobile_ratio_headings', 1 );

$tablet_landscape_threshold = anemos_eutf_option( 'typography_tablet_landscape_threshold', 20 );
$tablet_landscape_ratio = anemos_eutf_option( 'typography_tablet_landscape_ratio', 0.9 );
$tablet_portrait_threshold = anemos_eutf_option( 'typography_tablet_portrait_threshold', 20 );
$tablet_portrait_ratio = anemos_eutf_option( 'typography_tablet_portrait_ratio', 0.85 );
$mobile_threshold = anemos_eutf_option( 'typography_mobile_threshold', 28 );
$mobile_ratio = anemos_eutf_option( 'typography_mobile_ratio', 0.6 );

$tablet_landscape_threshold2 = anemos_eutf_option( 'typography_tablet_landscape_threshold2', 14 );
$tablet_landscape_ratio2 = anemos_eutf_option( 'typography_tablet_landscape_ratio2', 0.9 );
$tablet_portrait_threshold2 = anemos_eutf_option( 'typography_tablet_portrait_threshold2', 14 );
$tablet_portrait_ratio2 = anemos_eutf_option( 'typography_tablet_portrait_ratio2', 0.8 );
$mobile_threshold2 = anemos_eutf_option( 'typography_mobile_threshold2', 13 );
$mobile_ratio2 = anemos_eutf_option( 'typography_mobile_ratio2', 0.7 );

$typo_css .= "
	@media only screen and (min-width: 960px) and (max-width: 1200px) {
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_headings, $tablet_landscape_threshold_headings, $tablet_landscape_ratio_headings ). "
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_1, $tablet_landscape_threshold, $tablet_landscape_ratio ). "
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_2, $tablet_landscape_threshold2, $tablet_landscape_ratio2 ). "
	}
	@media only screen and (min-width: 768px) and (max-width: 959px) {
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_headings, $tablet_portrait_threshold_headings, $tablet_portrait_ratio_headings ). "
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_1, $tablet_portrait_threshold, $tablet_portrait_ratio ). "
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_2, $tablet_portrait_threshold2, $tablet_portrait_ratio2 ). "
	}
	@media only screen and (max-width: 767px) {
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_headings, $mobile_threshold_headings, $mobile_ratio_headings ). "
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_1, $mobile_threshold, $mobile_ratio ). "
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_2, $mobile_threshold2, $mobile_ratio2 ). "
	}
	@media print {
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_headings, $mobile_threshold_headings, $mobile_ratio_headings ). "
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_1, $mobile_threshold, $mobile_ratio ). "
		" . anemos_eutf_print_typography_responsive( $anemos_eutf_responsive_fonts_group_2, $mobile_threshold2, $mobile_ratio2 ). "
	}
";

echo anemos_eutf_get_css_output( $typo_css );

//Omit closing PHP tag to avoid accidental whitespace output errors.
