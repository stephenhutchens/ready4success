<?php

/*
*	Footer Helper functions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


/**
 * Prints Footer Background Image
 */
if ( !function_exists('anemos_eutf_print_footer_bg_image') ) {

	function anemos_eutf_print_footer_bg_image() {

		if ( 'custom' == anemos_eutf_option( 'footer_bg_mode' ) ) {
			$anemos_eutf_footer_custom_bg = array(
				'bg_mode' => 'custom',
				'bg_image_id' => anemos_eutf_option( 'footer_bg_image', '', 'id' ),
				'bg_position' => anemos_eutf_option( 'footer_bg_position', 'center-center' ),
				'pattern_overlay' => anemos_eutf_option( 'footer_pattern_overlay' ),
				'color_overlay' => anemos_eutf_option( 'footer_color_overlay' ),
				'opacity_overlay' => anemos_eutf_option( 'footer_opacity_overlay' ),
			);
			anemos_eutf_print_title_bg_image( $anemos_eutf_footer_custom_bg );
		}

	}
}

/**
 * Prints Footer Widgets
 */
if ( !function_exists('anemos_eutf_print_footer_widgets') ) {

	function anemos_eutf_print_footer_widgets() {

		if ( anemos_eutf_visibility( 'footer_widgets_visibility' ) ) {

			if ( ( is_singular() && 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_footer' ) ) || ( anemos_eutf_is_woo_shop() && 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_disable_footer' ) ) ) {
				return;
			}

			$anemos_eutf_footer_columns = anemos_eutf_option('footer_widgets_layout');

			switch( $anemos_eutf_footer_columns ) {
				case 'footer-1':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-4-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-2':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-2',
							'tablet-column' => '1',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-3':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-2',
							'tablet-column' => '1',
						),
					);
				break;
				case 'footer-4':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-2',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-2',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-5':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-3',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-3',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-3',
						),
					);
				break;
				case 'footer-6':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '2-3',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-7':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-3',
							'tablet-column' => '1-2',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '2-3',
							'tablet-column' => '1-2',
						),
					);
				break;
				case 'footer-8':
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-3',
						),
						array(
							'sidebar-id' => 'eut-footer-2-sidebar',
							'column' => '1-2',
							'tablet-column' => '1-3',
						),
						array(
							'sidebar-id' => 'eut-footer-3-sidebar',
							'column' => '1-4',
							'tablet-column' => '1-3',
						),
					);
				break;
				case 'footer-9':
				default:
					$footer_sidebars = array(
						array(
							'sidebar-id' => 'eut-footer-1-sidebar',
							'column' => '1',
							'tablet-column' => '1',
						),
					);
				break;
			}

			$section_type = anemos_eutf_option( 'footer_section_type', 'fullwidth-background' );

			$anemos_eutf_footer_class = array( 'eut-widget-area', 'eut-padding-top-3x', 'eut-padding-bottom-3x' );

			if( 'fullwidth-element' == $section_type ) {
				$anemos_eutf_footer_class[] = 'eut-fullwidth';
			}
			$anemos_eutf_footer_class_string = implode( ' ', $anemos_eutf_footer_class );

	?>
			<!-- Footer -->
			<div class="<?php echo esc_attr( $anemos_eutf_footer_class_string ); ?>">
				<div class="eut-container">
					<div class="eut-row eut-columns-gap-30">
		<?php

					foreach ( $footer_sidebars as $footer_sidebar ) {
						echo '<div class="wpb_column eut-column eut-column-' . esc_attr( $footer_sidebar['column'] ) . ' eut-tablet-column-' . esc_attr( $footer_sidebar['tablet-column'] ) . '">';
						echo '<div class="eut-column-wrapper">';
						dynamic_sidebar( $footer_sidebar['sidebar-id'] );
						echo '</div>';
						echo '</div>';
					}
		?>
					</div>
				</div>
			</div>
	<?php

		}
	}
}

/**
 * Prints Footer Bar Area
 */

if ( !function_exists('anemos_eutf_print_footer_bar') ) {
	function anemos_eutf_print_footer_bar() {

		if ( anemos_eutf_visibility( 'footer_bar_visibility' ) ) {
			if ( anemos_eutf_visibility( 'footer_copyright_visibility' ) ) {
				if ( ( is_singular() && 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_copyright' ) ) || ( anemos_eutf_is_woo_shop() && 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_disable_copyright' ) ) ) {
					return;
				}

				$section_type = anemos_eutf_option( 'footer_bar_section_type', 'fullwidth-background' );

				$anemos_eutf_footer_bar_class = array( 'eut-footer-bar', 'eut-padding-top-1x', 'eut-padding-bottom-1x' );

				if( 'fullwidth-element' == $section_type ) {
					$anemos_eutf_footer_bar_class[] = 'eut-fullwidth';
				}
				$anemos_eutf_footer_bar_class_string = implode( ' ', $anemos_eutf_footer_bar_class );


				$align_center = anemos_eutf_option( 'footer_bar_align_center', 'no' );
				$second_area = anemos_eutf_option( 'second_area_visibility', '1' );
				$footer_copyright_text = anemos_eutf_option( 'footer_copyright_text' );
	?>

				<div class="<?php echo esc_attr( $anemos_eutf_footer_bar_class_string ); ?>" data-align-center="<?php echo esc_attr( $align_center ); ?>">
					<div class="eut-container">
						<?php if ( !empty( $footer_copyright_text ) ) { ?>
						<div class="eut-bar-content eut-left-side">
							<div class="eut-copyright eut-link-text">
								<?php echo do_shortcode( $footer_copyright_text ); ?>
							</div>
						</div>
						<?php } ?>
						<?php if ( '2' == $second_area ) { ?>
						<div class="eut-bar-content eut-right-side">
							<nav class="eut-footer-menu eut-link-text eut-list-divider">
								<?php anemos_eutf_footer_nav(); ?>
							</nav>
						</div>
						<?php
						} else if ( '3' == $second_area ) { ?>
						<div class="eut-bar-content eut-right-side">
							<?php
							global $anemos_eutf_social_list;
							$options = anemos_eutf_option('footer_social_options');
							$social_display = anemos_eutf_option('footer_social_display', 'text');
							$social_options = anemos_eutf_option('social_options');

							if ( !empty( $options ) && !empty( $social_options ) ) {
								if ( 'text' == $social_display ) {
									echo '<ul class="eut-social eut-small-text eut-list-divider">';
									foreach ( $social_options as $key => $value ) {
										if ( isset( $options[$key] ) && 1 == $options[$key] && $value ) {
											if ( 'skype' == $key ) {
												echo '<li><a href="' . esc_url( $value, array( 'skype', 'http', 'https' ) ) . '">' . $anemos_eutf_social_list[$key] . '</a></li>';
											} else {
												echo '<li><a href="' . esc_url( $value ) . '" target="_blank">' . $anemos_eutf_social_list[$key] . '</a></li>';
											}
										}
									}
									echo '</ul>';
								} else {
									echo '<ul class="eut-social eut-social-icons">';
									foreach ( $social_options as $key => $value ) {
										if ( isset( $options[$key] ) && 1 == $options[$key] && $value ) {
											if ( 'skype' == $key ) {
												echo '<li><a href="' . esc_url( $value, array( 'skype', 'http', 'https' ) ) . '" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
											} else {
												echo '<li><a href="' . esc_url( $value ) . '" target="_blank" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
											}
										}
									}
									echo '</ul>';
								}
							}
							?>
						</div>
						<?php
						}
						?>
					</div>
				</div>

	<?php
			}
		}
	}
}

/**
 * Prints Back To Top Link
 */
if ( !function_exists('anemos_eutf_print_back_top') ) {
	function anemos_eutf_print_back_top() {

		if ( ( is_singular() && 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_back_to_top' ) ) || ( anemos_eutf_is_woo_shop() && 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_disable_back_to_top' ) ) ) {
			return;
		}

		if ( anemos_eutf_visibility( 'back_to_top_enabled' )  ) {

			$anemos_eutf_back_to_top_color = anemos_eutf_option( 'back_to_top_color', 'grey' );
			$anemos_eutf_back_to_top_shape = anemos_eutf_option( 'back_to_top_shape', 'none' );
			$anemos_eutf_back_to_top_bg_color = anemos_eutf_option( 'back_to_top_bg_color', 'primary-1' );

			$anemos_eutf_back_to_top_classes = array('eut-back-top');

			if( 'none' != $anemos_eutf_back_to_top_shape ){
				$anemos_eutf_back_to_top_classes[] = 'eut-' . $anemos_eutf_back_to_top_shape;
				$anemos_eutf_back_to_top_classes[] = 'eut-bg-' . $anemos_eutf_back_to_top_bg_color;
			}
			if( 'anemos' == anemos_eutf_option( 'header_sticky_type' ) ){
				$anemos_eutf_back_to_top_classes[] = 'eut-anemos-back-top';
			}
			$anemos_eutf_back_to_top_class_string = implode( ' ', $anemos_eutf_back_to_top_classes );

			$anemos_eutf_back_to_top_icon_classes = array('eut-icon-nav-up');
			$anemos_eutf_back_to_top_icon_classes[] = 'eut-text-' . $anemos_eutf_back_to_top_color;
			$anemos_eutf_back_to_top_icon_class_string = implode( ' ', $anemos_eutf_back_to_top_icon_classes );

		?>
			<div class="<?php echo esc_attr( $anemos_eutf_back_to_top_class_string ); ?>">
				<i class="<?php echo esc_attr( $anemos_eutf_back_to_top_icon_class_string ); ?>"></i>
			</div>
		<?php
		}
	}
}


/**
 * Prints Custom javascript code
 */
add_action( 'wp_footer', 'anemos_eutf_print_custom_js_code', 100 );
if ( !function_exists('anemos_eutf_print_custom_js_code') ) {

	function anemos_eutf_print_custom_js_code() {
		$custom_js_code = anemos_eutf_option( 'custom_js' );
		if ( !empty( $custom_js_code ) ) {
			echo "<script type='text/javascript'>" . $custom_js_code . "</script>";
		}
	}
}


 /**
 * Prints Bottom Bar
 */
function anemos_eutf_print_bottom_bar() {
	global $anemos_eutf_social_list;
	$options = anemos_eutf_option('bottom_bar_social_options');
	$social_options = anemos_eutf_option('social_options');
	$social_visibility = anemos_eutf_visibility( 'bottom_bar_socials_visibility' );
	$form_visibility = anemos_eutf_visibility( 'bottom_bar_form_visibility' );

	$anemos_eutf_bottom_bar_columns_classes = array('');

	if( $social_visibility && $form_visibility ){
		$anemos_eutf_bottom_bar_columns_classes[] = 'eut-bottom-bar-content eut-bottom-bar-column-2';
	} else {
		$anemos_eutf_bottom_bar_columns_classes[] = 'eut-bottom-bar-content eut-bottom-bar-column-1';
	}
	$anemos_eutf_bottom_bar_columns_class_string = implode( ' ', $anemos_eutf_bottom_bar_columns_classes );

	if ( anemos_eutf_visibility( 'bottom_bar_visibility' ) ) {

		if ( ( is_singular() && 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_bottom_bar' ) ) || ( anemos_eutf_is_woo_shop() && 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_disable_bottom_bar' ) ) ) {
			return;
		}

		if ( is_single() && !anemos_eutf_visibility( 'post_bottom_bar_visibility', '1' ) ) {
			return;
		}
?>
		<!-- BOTTOM BAR -->
		<div id="eut-bottom-bar" class="eut-bottom-bar eut-bookmark eut-padding-top-2x eut-padding-bottom-2x">
			<div class="eut-container">
				<div class="eut-bottom-bar-wrapper">

				<?php
					$bottom_bar_title = anemos_eutf_option( 'bottom_bar_title');

					if ( !empty( $bottom_bar_title ) ) {
						echo '<span class="eut-bottom-bar-title eut-h3">' . esc_html( $bottom_bar_title ) . '</span>';
					}
				?>
					<div class="<?php echo esc_attr( $anemos_eutf_bottom_bar_columns_class_string ); ?>">
					<?php
						if ( $social_visibility ) {

							if ( !empty( $options ) && !empty( $social_options ) ) {
								echo '<div class="eut-content-item eut-bottom-bar-social-wrapper">';

								echo '<ul class="eut-social eut-social-icons">';
								foreach ( $social_options as $key => $value ) {
									if ( isset( $options[$key] ) && 1 == $options[$key] && $value ) {
										if ( 'skype' == $key ) {
											echo '<li><a href="' . esc_url( $value, array( 'skype', 'http', 'https' ) ) . '" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
										} else {
											echo '<li><a href="' . esc_url( $value ) . '" target="_blank" class="fa fa-' . esc_attr( $key ) . '"></a></li>';
										}
									}
								}
								echo '</ul>';

								echo '</div>';
							}

						}
						if ( $form_visibility ) {
					?>
						<div class="eut-content-item eut-bottom-bar-form-wrapper">
							<?php anemos_eutf_print_contact_form( 'bottom_bar_form' ); ?>
						</div>
					<?php
						}
					?>
					</div>

				</div>
			</div>
		</div>
		<!-- END BOTTOM BAR -->
<?php
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
