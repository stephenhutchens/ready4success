<?php
/**
 *  Dynamic css style
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */

$css = "";


/* =========================================================================== */

/* Body
/* Container Size
/* Boxed Size
/* Single Post Content Width
/* Top Bar

/* Default Header
	/* - Default Header Colors
	/* - Default Header Menu Colors
	/* - Default Header Sub Menu Colors
	/* - Default Header Layout
	/* - Default Header Overlaping

/* Logo On Top Header
	/* - Logo On Top Header Colors
	/* - Logo On Top Header Menu Colors
	/* - Logo On Top Header Sub Menu Colors
	/* - Logo On Top Header Layout
	/* - Logo On Top Header Overlaping

/* Light Header
/* Dark Header

/* Sticky Header
	/* - Sticky Default Header
	/* - Sticky Logo On Top Header
	/* - Sticky Header Colors
	/* - Anemos Sticky Header

/* Side Area Colors
/* Modals Colors

/* Responsive Header
	/* - Header Layout
	/* - Responsive Menu
	/* - Responsive Header Elements

/* Spinner
/* Primary Text Color
/* Primary Bg Color
/* Anchor Menu
/* Breadcrumbs
/* Main Content
	/* - Main Content Borders
	/* - Widget Colors

/* Bottom Bar Colors

/* Footer
	/* - Widget Area
	/* - Footer Widget Colors
	/* - Footer Bar Colors




/* =========================================================================== */


/* Body
============================================================================= */
$css .= "
a {
	color: " . anemos_eutf_option( 'body_text_link_color' ) . ";

}

a:hover {
	color: " . anemos_eutf_option( 'body_text_link_hover_color' ) . ";
}
";

/* Container Size
============================================================================= */
$css .= "

.eut-container,
#disqus_thread,
#eut-content.eut-left-sidebar .eut-content-wrapper,
#eut-content.eut-right-sidebar .eut-content-wrapper {
	max-width: " . anemos_eutf_option( 'container_size', 1170 ) . "px;
}

@media only screen and (max-width: 1200px) {
	.eut-container,
	#disqus_thread,
	#eut-content.eut-left-sidebar .eut-content-wrapper,
	#eut-content.eut-right-sidebar .eut-content-wrapper {
		width: 90%;
		max-width: " . anemos_eutf_option( 'container_size', 1170 ) . "px;
	}
}

@media only screen and (min-width: 960px) {

	#eut-theme-wrapper.eut-header-side .eut-container,
	#eut-theme-wrapper.eut-header-side #eut-content.eut-left-sidebar .eut-content-wrapper,
	#eut-theme-wrapper.eut-header-side #eut-content.eut-right-sidebar .eut-content-wrapper {
		width: 90%;
		max-width: " . anemos_eutf_option( 'container_size', 1170 ) . "px;
	}

}

";

/* Boxed Size
============================================================================= */
$css .= "

body.eut-boxed #eut-theme-wrapper {
	width: " . anemos_eutf_option( 'boxed_size', 1220 ) . "px;
}

#eut-body.eut-boxed #eut-header.eut-fixed #eut-main-header,
#eut-body.eut-boxed #eut-anemos-sticky-header,
#eut-body.eut-boxed .eut-anchor-menu .eut-anchor-wrapper.eut-sticky,
#eut-body.eut-boxed #eut-footer.eut-fixed-footer {
	max-width: " . anemos_eutf_option( 'boxed_size', 1220 ) . "px;
}

@media only screen and (max-width: 1200px) {
	#eut-body.eut-boxed #eut-header.eut-sticky-header #eut-main-header.eut-header-default,
	#eut-body.eut-boxed #eut-header.eut-sticky-header #eut-anemos-sticky-header,
	#eut-body.eut-boxed #eut-header.eut-fixed #eut-main-header {
		max-width: 90%;
	}
}

";


/* Single Post Content Width
============================================================================= */
if ( is_singular( 'post' ) ) {
	$anemos_eutf_post_content_width = anemos_eutf_post_meta( '_anemos_eutf_post_content_width', anemos_eutf_option( 'post_content_width', 990 ) );

$css .= "

.single-post #eut-content:not(.eut-right-sidebar):not(.eut-left-sidebar) .eut-container {
	max-width: " . esc_attr( $anemos_eutf_post_content_width ) . "px;
}

";

}


/* Top Bar
============================================================================= */
$css .= "

#eut-top-bar,
#eut-top-bar .eut-language > li > ul,
#eut-top-bar .eut-top-bar-menu ul.sub-menu {
	background-color: " . anemos_eutf_option( 'top_bar_bg_color' ) . ";
	color: " . anemos_eutf_option( 'top_bar_font_color' ) . ";
}

#eut-top-bar a {
	color: " . anemos_eutf_option( 'top_bar_link_color' ) . ";
}

#eut-top-bar a:hover {
	color: " . anemos_eutf_option( 'top_bar_hover_color' ) . ";
}

";


/* Default Header
============================================================================= */
$anemos_eutf_header_mode = anemos_eutf_option( 'header_mode', 'default' );
if ( 'default' == $anemos_eutf_header_mode ) {

	/* - Default Header Colors
	============================================================================= */

	$anemos_eutf_default_header_background_color = anemos_eutf_option( 'default_header_background_color', '#ffffff' );
	$anemos_eutf_default_header_border_color = anemos_eutf_option( 'default_header_border_color', '#000000' );
	$css .= "

	#eut-main-header {
		background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_default_header_background_color ) . "," . anemos_eutf_option( 'default_header_background_color_opacity', '1') . ");
	}

	#eut-main-header.eut-transparent,
	#eut-main-header.eut-light,
	#eut-main-header.eut-dark {
		background-color: transparent;
	}

	#eut-main-header.eut-header-default,
	.eut-header-elements {
		border-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_default_header_border_color ) . "," . anemos_eutf_option( 'default_header_border_color_opacity', '1') . ");
	}

	";

	/* - Default Header Menu Colors
	========================================================================= */
	$css .= "
	#eut-header .eut-main-menu .eut-wrapper > ul > li > a,
	.eut-header-element > a,
	.eut-header-element .eut-purchased-items {
		color: " . anemos_eutf_option( 'default_header_menu_text_color' ) . ";

	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.eut-current > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li:hover > a,
	.eut-header-element > a:hover {
		color: " . anemos_eutf_option( 'default_header_menu_text_hover_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span {
		border-color: " . anemos_eutf_option( 'default_header_menu_type_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span,
	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.active > a span {
		border-color: " . anemos_eutf_option( 'default_header_menu_type_color_hover' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a span:after {
		background-color: " . anemos_eutf_option( 'default_header_menu_type_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a span:after,
	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li.active > a span:after {
		background-color: " . anemos_eutf_option( 'default_header_menu_type_color_hover' ) . ";
	}

	";


	/* - Default Header Sub Menu Colors
	========================================================================= */
	$css .= "
	#eut-header .eut-main-menu .eut-wrapper > ul > li ul  {
		background-color: " . anemos_eutf_option( 'default_header_submenu_bg_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li a {
		color: " . anemos_eutf_option( 'default_header_submenu_text_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li a:hover,
	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li.current-menu-item > a,
	#eut-header .eut-main-menu .eut-wrapper > ul li li.current-menu-ancestor > a {
		color: " . anemos_eutf_option( 'default_header_submenu_text_hover_color' ) . ";
		background-color: " . anemos_eutf_option( 'default_header_submenu_text_bg_hover_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li > a {
		color: " . anemos_eutf_option( 'default_header_submenu_column_text_color' ) . ";
		background-color: transparent;
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li:hover > a {
		color: " . anemos_eutf_option( 'default_header_submenu_column_text_hover_color' ) . ";
	}

	#eut-header .eut-horizontal-menu .eut-wrapper > ul > li.megamenu > ul > li {
		border-color: " . anemos_eutf_option( 'default_header_submenu_border_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li.eut-menu-type-button a {
		background-color: transparent;
	}

	";

	/* - Default Header Layout
	========================================================================= */
	$css .= "
	#eut-main-header,
	.eut-logo {
		height: " . anemos_eutf_option( 'header_height', 120 ) . "px;
	}

	.eut-logo a {
		height: " . anemos_eutf_option( 'logo_height', 20 ) . "px;
	}

	#eut-main-menu .eut-wrapper > ul > li > a,
	.eut-header-element > a,
	.eut-no-assigned-menu {
		line-height: " . anemos_eutf_option( 'header_height', 120 ) . "px;
	}

	.eut-logo .eut-wrapper img {
		padding-top: 0;
		padding-bottom: 0;
	}

	";

	/* Go to section Position */
	$css .= "
	#eut-theme-wrapper.eut-feature-below #eut-goto-section-wrapper {
		margin-bottom: " . anemos_eutf_option( 'header_height', 120 ) . "px;
	}
	";

	/* - Default Header Overlaping
	========================================================================= */
	$css .= "
	@media only screen and (min-width: 1024px) {
		#eut-header.eut-overlapping + * {
			top: -" . anemos_eutf_option( 'header_height', 120 ) . "px;
			margin-bottom: -" . anemos_eutf_option( 'header_height', 120 ) . "px;
		}

		#eut-header.eut-overlapping + div .eut-wrapper {
			padding-top: " . anemos_eutf_option( 'header_height', 120 ) . "px;
		}

		#eut-feature-section + #eut-header.eut-overlapping {
			top: -" . anemos_eutf_option( 'header_height', 120 ) . "px;
		}

		.eut-feature-below #eut-feature-section:not(.eut-with-map) .eut-wrapper {
			margin-bottom: " . anemos_eutf_option( 'header_height', 120 ) . "px;
		}

		#eut-header {
			height: " . anemos_eutf_option( 'header_height', 120 ) . "px;
		}
	}

	";
	/* Sticky Sidebar with header overlaping */
	$css .= "
	@media only screen and (min-width: 1024px) {
		#eut-header.eut-overlapping + #eut-content .eut-sidebar.eut-fixed-sidebar,
		#eut-header.eut-overlapping + .eut-single-wrapper .eut-sidebar.eut-fixed-sidebar {
			top: " . anemos_eutf_option( 'header_height', 120 ) . "px;
		}
	}
	";

/* Logo On Top Header
============================================================================= */
} else if ( 'logo-top' == $anemos_eutf_header_mode ) {


	/* - Logo On Top Header Colors
	============================================================================= */
	$anemos_eutf_logo_top_logo_area_background_color = anemos_eutf_option( 'logo_top_header_logo_area_background_color', '#ffffff' );
	$anemos_eutf_logo_top_menu_area_background_color = anemos_eutf_option( 'logo_top_header_menu_area_background_color', '#ffffff' );
	$anemos_eutf_logo_top_border_color = anemos_eutf_option( 'logo_top_header_border_color', '#000000' );
	$css .= "

	#eut-main-header #eut-top-header {
		background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_logo_top_logo_area_background_color ) . "," . anemos_eutf_option( 'logo_top_header_logo_area_background_color_opacity', '1') . ");
	}

	#eut-main-header #eut-bottom-header {
		background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_logo_top_menu_area_background_color ) . "," . anemos_eutf_option( 'logo_top_header_menu_area_background_color_opacity', '1') . ");
		border-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_logo_top_border_color ) . "," . anemos_eutf_option( 'logo_top_header_border_color_opacity', '1') . ");
	}
	#eut-main-header {
		border-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_logo_top_border_color ) . "," . anemos_eutf_option( 'logo_top_header_border_color_opacity', '1') . ");
	}

	.eut-header-elements {
		border-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_logo_top_border_color ) . "," . anemos_eutf_option( 'logo_top_header_border_color_opacity', '1') . ");
	}

	#eut-main-header.eut-transparent #eut-top-header,
	#eut-main-header.eut-light #eut-top-header,
	#eut-main-header.eut-dark #eut-top-header,
	#eut-main-header.eut-transparent #eut-bottom-header,
	#eut-main-header.eut-light #eut-bottom-header,
	#eut-main-header.eut-dark #eut-bottom-header {
		background-color: transparent;
	}

	";

	/* - Logo On Top Header Menu Colors
	========================================================================= */
	$css .= "
	#eut-header .eut-main-menu .eut-wrapper > ul > li > a,
	.eut-header-element > a,
	.eut-header-element .eut-purchased-items {
		color: " . anemos_eutf_option( 'logo_top_header_menu_text_color' ) . ";

	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.eut-current > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-header .eut-main-menu .eut-wrapper > ul > li:hover > a,
	.eut-header-element > a:hover {
		color: " . anemos_eutf_option( 'logo_top_header_menu_text_hover_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span {
		border-color: " . anemos_eutf_option( 'logo_top_header_menu_type_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span,
	#eut-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.active > a span {
		border-color: " . anemos_eutf_option( 'logo_top_header_menu_type_color_hover' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a span:after {
		background-color: " . anemos_eutf_option( 'logo_top_header_menu_type_color' ) . ";
	}

	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a span:after,
	#eut-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li.active > a span:after {
		background-color: " . anemos_eutf_option( 'logo_top_header_menu_type_color_hover' ) . ";
	}


	";


	/* - Logo On Top Header Sub Menu Colors
	========================================================================= */
	$css .= "
	#eut-header .eut-main-menu .eut-wrapper > ul > li ul  {
		background-color: " . anemos_eutf_option( 'logo_top_header_submenu_bg_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li a {
		color: " . anemos_eutf_option( 'logo_top_header_submenu_text_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li a:hover,
	#eut-header .eut-main-menu .eut-wrapper > ul > li ul li.current-menu-item > a,
	#eut-header .eut-main-menu .eut-wrapper > ul li li.current-menu-ancestor > a {
		color: " . anemos_eutf_option( 'logo_top_header_submenu_text_hover_color' ) . ";
		background-color: " . anemos_eutf_option( 'logo_top_header_submenu_text_bg_hover_color' ) . ";
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li > a {
		color: " . anemos_eutf_option( 'logo_top_header_submenu_column_text_color' ) . ";
		background-color: transparent;
	}

	#eut-header .eut-main-menu .eut-wrapper > ul > li.megamenu > ul > li:hover > a {
		color: " . anemos_eutf_option( 'logo_top_header_submenu_column_text_hover_color' ) . ";
	}

	#eut-header .eut-horizontal-menu .eut-wrapper > ul > li.megamenu > ul > li {
		border-color: " . anemos_eutf_option( 'logo_top_header_submenu_border_color' ) . ";
	}

	";

	/* - Logo On Top Header Layout
	========================================================================= */
	$anemos_eutf_header_height = intval( anemos_eutf_option( 'header_top_height', 120 ) ) + intval( anemos_eutf_option( 'header_bottom_height', 50 ) + 1 );
	$css .= "

	#eut-top-header,
	.eut-logo {
		height: " . anemos_eutf_option( 'header_top_height', 120 ) . "px;
	}

	@media only screen and (min-width: 1024px) {
		#eut-header {
			height: " . esc_attr( $anemos_eutf_header_height ) . "px;
		}
	}

	.eut-logo a {
		height: " . anemos_eutf_option( 'header_top_logo_height', 30 ) . "px;
	}

	#eut-bottom-header,
	#eut-main-menu {
		height: " . ( anemos_eutf_option( 'header_bottom_height', 50 ) + 1 ) . "px;
	}

	#eut-main-menu .eut-wrapper > ul > li > a,
	.eut-header-element > a,
	.eut-no-assigned-menu {
		line-height: " . anemos_eutf_option( 'header_bottom_height', 50 ) . "px;
	}

	";

	/* Go to section Position */
	$css .= "
	#eut-theme-wrapper.eut-feature-below #eut-goto-section-wrapper {
		margin-bottom: " . esc_attr( $anemos_eutf_header_height ) . "px;
	}
	";

	/* - Logo On Top Header Overlaping
	========================================================================= */
	$css .= "

	@media only screen and (min-width: 1024px) {
		#eut-header.eut-overlapping + * {
			top: -" . esc_attr( $anemos_eutf_header_height ) . "px;
			margin-bottom: -" . esc_attr( $anemos_eutf_header_height ) . "px;
		}

		#eut-header.eut-overlapping + div .eut-wrapper {
			padding-top: " . esc_attr( $anemos_eutf_header_height ) . "px;
		}

		#eut-feature-section + #eut-header.eut-overlapping {
			top: -" . esc_attr( $anemos_eutf_header_height ) . "px;
		}

		.eut-feature-below #eut-feature-section:not(.eut-with-map) .eut-wrapper {
			margin-bottom: " . anemos_eutf_option( 'header_top_height', 120 ) . "px;
		}
	}

	";

	/* Sticky Sidebar with header overlaping */
	$css .= "
	@media only screen and (min-width: 1024px) {
		#eut-header.eut-overlapping + #eut-content .eut-sidebar.eut-fixed-sidebar,
		#eut-header.eut-overlapping + .eut-single-wrapper .eut-sidebar.eut-fixed-sidebar {
			top: " . esc_attr( $anemos_eutf_header_height ) . "px;
		}
	}
	";


} else {

	/* - Side Header Colors
	============================================================================= */
	$anemos_eutf_side_header_background_color = anemos_eutf_option( 'side_header_background_color', '#ffffff' );
	$css .= "

	#eut-main-header {
		background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_side_header_background_color ) . "," . anemos_eutf_option( 'side_header_background_color_opacity', '1') . ");
	}

	#eut-main-header.eut-transparent,
	#eut-main-header.eut-light,
	#eut-main-header.eut-dark {
		background-color: transparent;
	}

	";

	/* - Side Header Menu Colors
	========================================================================= */
	$css .= "
	#eut-main-menu .eut-wrapper > ul > li > a,
	#eut-main-menu .eut-wrapper > ul > li > .eut-arrow,
	.eut-header-element > a,
	.eut-header-element .eut-purchased-items {
		color: " . anemos_eutf_option( 'side_header_menu_text_color' ) . ";

	}

	#eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-main-menu .eut-wrapper > ul > li > a:hover,
	.eut-header-element > a:hover,
	#eut-main-menu .eut-wrapper > ul > li > .eut-arrow:hover,
	#eut-main-menu .eut-wrapper > ul > li ul li.eut-goback a {
		color: " . anemos_eutf_option( 'side_header_menu_text_hover_color' ) . ";
	}

	";


	/* - Side Header Sub Menu Colors
	========================================================================= */
	$anemos_eutf_side_header_border_color = anemos_eutf_option( 'side_header_border_color', '#ffffff' );
	$css .= "

	#eut-main-menu .eut-wrapper > ul > li ul li a,
	#eut-main-menu .eut-wrapper > ul > li ul li:not(.eut-goback) .eut-arrow {
		color: " . anemos_eutf_option( 'side_header_submenu_text_color' ) . ";
	}

	#eut-main-menu .eut-wrapper > ul > li ul li a:hover,
	#eut-main-menu .eut-wrapper > ul > li ul li.current-menu-item > a,
	#eut-main-menu .eut-wrapper > ul > li ul li:not(.eut-goback) .eut-arrow:hover {
		color: " . anemos_eutf_option( 'side_header_submenu_text_hover_color' ) . ";
	}

	#eut-main-menu.eut-vertical-menu  ul li a,
	#eut-main-header.eut-header-side .eut-header-elements {
		border-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_side_header_border_color ) . "," . anemos_eutf_option( 'side_header_border_opacity', '1') . ");
	}

	";

	/* - Side Header Layout
	========================================================================= */
	$css .= "

	.eut-logo a {
		height: " . anemos_eutf_option( 'header_side_logo_height', 30 ) . "px;
	}

	@media only screen and (min-width: 1024px) {
		#eut-theme-wrapper.eut-header-side,
		#eut-footer.eut-fixed-footer {
			padding-left: " . anemos_eutf_option( 'header_side_width', 120 ) . "px;
		}

		#eut-main-header.eut-header-side {
			width: " . anemos_eutf_option( 'header_side_width', 120 ) . "px;
		}

		body.eut-boxed #eut-theme-wrapper.eut-header-side #eut-main-header.eut-header-side,
		#eut-footer.eut-fixed-footer {
			margin-left: -" . anemos_eutf_option( 'header_side_width', 120 ) . "px;
		}
		#eut-main-header.eut-header-side .eut-main-header-wrapper {
			width: " . intval( anemos_eutf_option( 'header_side_width', 120 ) + 30 ) . "px;
		}
	}

	";
}

/* Light Header
============================================================================= */
$anemos_eutf_light_header_border_color = anemos_eutf_option( 'light_header_border_color', '#ffffff' );
$css .= "
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li > a,
#eut-main-header.eut-light .eut-header-element > a,
#eut-main-header.eut-light .eut-header-element .eut-purchased-items {
	color: #ffffff;
}

#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li.eut-current > a,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li:hover > a,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
#eut-main-header.eut-light #eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
#eut-main-header.eut-light .eut-header-element > a:hover {
	color: " . anemos_eutf_option( 'light_menu_text_hover_color' ) . ";
}

#eut-main-header.eut-light #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
#eut-main-header.eut-light #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span,
#eut-main-header.eut-light #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span {
	border-color: " . anemos_eutf_option( 'light_menu_type_color_hover' ) . ";
}

#eut-main-header.eut-light #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a span:after,
#eut-main-header.eut-light #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a span:after {
	background-color: " . anemos_eutf_option( 'light_menu_type_color_hover' ) . ";
}

#eut-main-header.eut-light,
#eut-main-header.eut-light .eut-header-elements,
#eut-main-header.eut-header-default.eut-light,
#eut-main-header.eut-light #eut-bottom-header {
	border-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_light_header_border_color ) . "," . anemos_eutf_option( 'light_header_border_color_opacity', '1') . ");
}

";

/* Dark Header
============================================================================= */
$anemos_eutf_dark_header_border_color = anemos_eutf_option( 'dark_header_border_color', '#ffffff' );
$css .= "
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li > a,
#eut-main-header.eut-dark .eut-header-element > a,
#eut-main-header.eut-dark .eut-header-element .eut-purchased-items {
	color: #000000;
}

#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li.eut-current > a,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li:hover > a,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
#eut-main-header.eut-dark #eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
#eut-main-header.eut-dark .eut-header-element > a:hover {
	color: " . anemos_eutf_option( 'dark_menu_text_hover_color' ) . ";
}

#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span,
#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span {
	border-color: " . anemos_eutf_option( 'dark_menu_type_color_hover' ) . ";
}

#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a span:after,
#eut-main-header.eut-dark #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a span:after {
	background-color: " . anemos_eutf_option( 'dark_menu_type_color_hover' ) . ";
}

#eut-main-header.eut-dark,
#eut-main-header.eut-dark .eut-header-elements,
#eut-main-header.eut-header-default.eut-dark,
#eut-main-header.eut-dark #eut-bottom-header {
	border-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_dark_header_border_color ) . "," . anemos_eutf_option( 'dark_header_border_color_opacity', '1') . ");
}

";


/* Sticky Header
============================================================================= */

	/* - Sticky Default Header
	========================================================================= */
	if ( 'default' == $anemos_eutf_header_mode ) {
		$css .= "
			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky {
				height: " . anemos_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}

			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky .eut-logo,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-logo {
				height: " . anemos_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}

			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky .eut-logo a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-logo a {
				height: " . anemos_eutf_option( 'header_sticky_shrink_logo_height', 20 ) . "px;
			}

			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky #eut-main-menu .eut-wrapper > ul > li > a,
			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky .eut-header-element > a,
			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky .eut-no-assigned-menu,

			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky #eut-main-menu .eut-wrapper > ul > li > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-header-element > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-no-assigned-menu {
				line-height: " . anemos_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}

			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky {
				top: -" . anemos_eutf_option( 'header_height', 120 ) . "px;
			}

			#eut-header.eut-sticky-header.eut-show #eut-main-header.eut-advanced-sticky {
				-webkit-transform: translateY(" . anemos_eutf_option( 'header_height', 120 ) . "px);
				-moz-transform:    translateY(" . anemos_eutf_option( 'header_height', 120 ) . "px);
				-ms-transform:     translateY(" . anemos_eutf_option( 'header_height', 120 ) . "px);
				-o-transform:      translateY(" . anemos_eutf_option( 'header_height', 120 ) . "px);
				transform:         translateY(" . anemos_eutf_option( 'header_height', 120 ) . "px);
			}

		";

	/* - Sticky Logo On Top Header
	========================================================================= */
	} else if ( 'logo-top' == $anemos_eutf_header_mode ) {
		$anemos_eutf_header_height = intval( anemos_eutf_option( 'header_sticky_shrink_height', 120 ) ) + intval( anemos_eutf_option( 'header_bottom_height', 50 ) );
		$css .= "
			#eut-header.eut-sticky-header #eut-main-header {
				-webkit-transform: translateY(-" . anemos_eutf_option( 'header_top_height', 120 ) . "px);
				-moz-transform:    translateY(-" . anemos_eutf_option( 'header_top_height', 120 ) . "px);
				-ms-transform:     translateY(-" . anemos_eutf_option( 'header_top_height', 120 ) . "px);
				-o-transform:      translateY(-" . anemos_eutf_option( 'header_top_height', 120 ) . "px);
				transform:         translateY(-" . anemos_eutf_option( 'header_top_height', 120 ) . "px);
			}

			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky #eut-bottom-header,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky #eut-bottom-header {
				height: " . anemos_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}

			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky #eut-main-menu .eut-wrapper > ul > li > a,
			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky .eut-header-element > a,
			#eut-header.eut-sticky-header.eut-shrink #eut-main-header.eut-shrink-sticky .eut-no-assigned-menu,

			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky #eut-main-menu .eut-wrapper > ul > li > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-header-element > a,
			#eut-header.eut-sticky-header #eut-main-header.eut-advanced-sticky .eut-no-assigned-menu {
				line-height: " . anemos_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
			}
		";
	}


	/* - Sticky Header Colors
	========================================================================= */
	$anemos_eutf_header_sticky_border_color = anemos_eutf_option( 'header_sticky_border_color', '#ffffff' );
	$anemos_eutf_header_sticky_background_color = anemos_eutf_option( 'header_sticky_background_color', '#ffffff' );
	$css .= "

	#eut-header.eut-sticky-header #eut-main-header:not(.eut-header-logo-top),
	#eut-header.eut-sticky-header #eut-main-header #eut-bottom-header {
		background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_header_sticky_background_color ) . "," . anemos_eutf_option( 'header_sticky_background_color_opacity', '1') . ");
	}

	#eut-header.eut-header-logo-top.eut-sticky-header #eut-main-header {
		background-color: transparent;
	}

	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li > a,
	#eut-header.eut-sticky-header #eut-main-header .eut-header-element > a,
	#eut-header.eut-sticky-header .eut-header-element .eut-purchased-items {
		color: " . anemos_eutf_option( 'sticky_menu_text_color' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.eut-current > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li:hover > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-header.eut-sticky-header #eut-main-header #eut-main-menu .eut-wrapper > ul > li.active > a,
	#eut-header.eut-sticky-header #eut-main-header .eut-header-element > a:hover {
		color: " . anemos_eutf_option( 'sticky_menu_text_hover_color' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span {
		border-color: " . anemos_eutf_option( 'header_sticky_menu_type_color' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span {
		border-color: " . anemos_eutf_option( 'header_sticky_menu_type_color_hover' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a span:after {
		background-color: " . anemos_eutf_option( 'header_sticky_menu_type_color' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a span:after {
		background-color: " . anemos_eutf_option( 'header_sticky_menu_type_color_hover' ) . ";
	}

	#eut-header.eut-sticky-header #eut-main-header.eut-header-default,
	#eut-header.eut-sticky-header .eut-header-elements {
		border-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_header_sticky_border_color ) . "," . anemos_eutf_option( 'header_sticky_border_color_opacity', '1') . ");
	}

	";

	/* - Anemos Sticky Header
	========================================================================= */
	$css .= "

	#eut-anemos-sticky-header {
		height: " . anemos_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
		top: -" . anemos_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
	}

	#eut-anemos-sticky-header .eut-logo,
	#eut-anemos-sticky-header .eut-page-title-wrapper {
		height: " . anemos_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
	}

	#eut-anemos-sticky-header .eut-logo a {
		height: " . anemos_eutf_option( 'header_sticky_shrink_logo_height', 20 ) . "px;
	}

	#eut-anemos-sticky-header .eut-main-menu .eut-wrapper > ul > li > a,
	#eut-anemos-sticky-header .eut-header-element > a,
	#eut-anemos-sticky-header .eut-no-assigned-menu,
	#eut-anemos-sticky-header .eut-page-title-wrapper {
		line-height: " . anemos_eutf_option( 'header_sticky_shrink_height', 60 ) . "px;
	}

	#eut-anemos-sticky-header {
		background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_header_sticky_background_color ) . "," . anemos_eutf_option( 'header_sticky_background_color_opacity', '1') . ");
	}

	#eut-anemos-sticky-header .eut-main-menu .eut-wrapper > ul > li > a,
	#eut-anemos-sticky-header .eut-header-element > a,
	#eut-anemos-sticky-header .eut-page-title-wrapper {
		color: " . anemos_eutf_option( 'sticky_menu_text_color' ) . ";
	}

	#eut-anemos-sticky-header .eut-main-menu .eut-wrapper > ul > li.eut-current > a,
	#eut-anemos-sticky-header .eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-anemos-sticky-header .eut-main-menu .eut-wrapper > ul > li:hover > a,
	#eut-anemos-sticky-header .eut-main-menu .eut-wrapper > ul > li.current-menu-item > a,
	#eut-anemos-sticky-header .eut-main-menu .eut-wrapper > ul > li.current-menu-ancestor > a,
	#eut-anemos-sticky-header .eut-main-menu .eut-wrapper > ul > li.active > a,
	#eut-anemos-sticky-header .eut-header-element > a:hover {
		color: " . anemos_eutf_option( 'sticky_menu_text_hover_color' ) . ";
	}

	#eut-anemos-sticky-header .eut-logo + .eut-page-title-wrapper {
		border-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_header_sticky_border_color ) . "," . anemos_eutf_option( 'header_sticky_border_color_opacity', '1') . ");
	}

	#eut-anemos-sticky-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-item > a span,
	#eut-anemos-sticky-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li.current-menu-ancestor > a span {
		border-color: " . anemos_eutf_option( 'header_sticky_menu_type_color' ) . ";
	}

	#eut-anemos-sticky-header .eut-main-menu.eut-menu-type-button .eut-wrapper > ul > li:hover > a span {
		border-color: " . anemos_eutf_option( 'header_sticky_menu_type_color_hover' ) . ";
	}

	#eut-anemos-sticky-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li > a span:after {
		background-color: " . anemos_eutf_option( 'header_sticky_menu_type_color' ) . ";
	}

	#eut-anemos-sticky-header .eut-main-menu.eut-menu-type-underline .eut-wrapper > ul > li:hover > a span:after {
		background-color: " . anemos_eutf_option( 'header_sticky_menu_type_color_hover' ) . ";
	}

	";

/* Side Area Colors
============================================================================= */
$anemos_eutf_sliding_area_overflow_background_color = anemos_eutf_option( 'sliding_area_overflow_background_color', '#000000' );
$css .= "
#eut-sidearea {
	background-color: " . anemos_eutf_option( 'sliding_area_background_color' ) . ";
	color: " . anemos_eutf_option( 'sliding_area_text_color' ) . ";
}

#eut-sidearea .widget,
#eut-sidearea form,
#eut-sidearea form p,
#eut-sidearea form div,
#eut-sidearea form span {
	color: " . anemos_eutf_option( 'sliding_area_text_color' ) . ";
}

#eut-sidearea h1,
#eut-sidearea h2,
#eut-sidearea h3,
#eut-sidearea h4,
#eut-sidearea h5,
#eut-sidearea h6,
#eut-sidearea .widget .eut-widget-title {
	color: " . anemos_eutf_option( 'sliding_area_title_color' ) . ";
}

#eut-sidearea a {
	color: " . anemos_eutf_option( 'sliding_area_link_color' ) . ";
}

#eut-sidearea .widget li a .eut-arrow:after,
#eut-sidearea .widget li a .eut-arrow:before {
	color: " . anemos_eutf_option( 'sliding_area_link_color' ) . ";
}

#eut-sidearea a:hover {
	color: " . anemos_eutf_option( 'sliding_area_link_hover_color' ) . ";
}

#eut-sidearea .eut-close-btn:after,
#eut-sidearea .eut-close-btn:before,
#eut-sidearea .eut-close-btn span {
	background-color: " . anemos_eutf_option( 'sliding_area_close_btn_color' ) . ";
}

#eut-sidearea .eut-border,
#eut-sidearea form,
#eut-sidearea form p,
#eut-sidearea form div,
#eut-sidearea form span,
#eut-sidearea .widget a,
#eut-sidearea .widget ul,
#eut-sidearea .widget li,
#eut-sidearea .widget table,
#eut-sidearea .widget table td,
#eut-sidearea .widget table th,
#eut-sidearea .widget table tr,
#eut-sidearea table,
#eut-sidearea tr,
#eut-sidearea td,
#eut-sidearea th,
#eut-sidearea .widget,
#eut-sidearea .widget ul,
#eut-sidearea .widget li,
#eut-sidearea .widget div,
#eut-theme-wrapper #eut-sidearea form,
#eut-theme-wrapper #eut-sidearea .wpcf7-form-control-wrap,
#eut-theme-wrapper #eut-sidearea .widget.widget_tag_cloud a {
	border-color: " . anemos_eutf_option( 'sliding_area_border_color' ) . ";
}

#eut-sidearea-overlay {
	background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_sliding_area_overflow_background_color ) . "," . anemos_eutf_option( 'sliding_area_overflow_background_color_opacity', '0.9') . ");
}
";


/* Modals Colors
============================================================================= */
$anemos_eutf_modal_overflow_background_color = anemos_eutf_option( 'modal_overflow_background_color', '#000000' );
$css .= "

#eut-modal-overlay,
.mfp-bg,
#eut-loader-overflow {
	background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_modal_overflow_background_color ) . "," . anemos_eutf_option( 'modal_overflow_background_color_opacity', '0.9') . ");
}

#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h1,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h2,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h3,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h4,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h5,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) h6,
#eut-theme-wrapper .eut-modal-content .eut-form-style-1:not(.eut-white-bg) .eut-modal-title,
.mfp-title,
.mfp-counter {
	color: " . anemos_eutf_option( 'modal_title_color' ) . ";
}

.mfp-wrap .eut-loader {
	background-color: " . anemos_eutf_option( 'modal_close_btn_color' ) . ";
}

.eut-close-modal,
button.mfp-arrow {
	color: " . anemos_eutf_option( 'modal_close_btn_color' ) . ";
}

#eut-theme-wrapper .eut-modal form,
#eut-theme-wrapper .eut-modal form p,
#eut-theme-wrapper .eut-modal form div,
#eut-theme-wrapper .eut-modal form span,
#eut-theme-wrapper ,eut-login-modal-footer,
#eut-socials-modal .eut-social li a,
#eut-language-modal ul li a {
	color: " . anemos_eutf_option( 'modal_text_color' ) . ";
	border-color: " . anemos_eutf_option( 'modal_border_color' ) . ";
}


";

/* Responsive Header
============================================================================= */
$anemos_eutf_responsive_header_background_color = anemos_eutf_option( 'responsive_header_background_color', '#000000' );
$css .= "
#eut-responsive-header > .eut-wrapper {
	background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_responsive_header_background_color ) . "," . anemos_eutf_option( 'responsive_header_background_opacity', '1') . ");
}
";
	/* - Header Layout
	========================================================================= */
	$css .= "
	#eut-responsive-header {
		height: " . anemos_eutf_option( 'responsive_header_height' ) . "px;
	}

	#eut-responsive-header .eut-logo {
		height: " . anemos_eutf_option( 'responsive_header_height' ) . "px;
	}

	#eut-responsive-header .eut-header-element > a {
		line-height: " . anemos_eutf_option( 'responsive_header_height' ) . "px;
	}

	#eut-responsive-header .eut-logo a {
		height: " . anemos_eutf_option( 'responsive_logo_height' ) . "px;
	}

	#eut-responsive-header .eut-logo .eut-wrapper img {
		padding-top: 0;
		padding-bottom: 0;
	}
	";

	/* - Responsive Header Overlaping
	========================================================================= */
	$css .= "

	@media only screen and (max-width: 1023px) {
		#eut-header.eut-responsive-overlapping + * {
			top: -" . anemos_eutf_option( 'responsive_header_height' ) . "px;
			margin-bottom: -" . anemos_eutf_option( 'responsive_header_height' ) . "px;
		}

		#eut-feature-section + #eut-header.eut-responsive-overlapping {
			top: -" . anemos_eutf_option( 'responsive_header_height' ) . "px;
		}

		#eut-header.eut-responsive-overlapping + * .eut-wrapper {
			padding-top: " . anemos_eutf_option( 'responsive_header_height' ) . "px;
		}

	}
	";

	/* - Responsive Menu
	========================================================================= */
	$anemos_eutf_responsive_menu_overflow_background_color = anemos_eutf_option( 'responsive_menu_overflow_background_color', '#000000' );
	$css .= "

	#eut-hidden-menu {
		background-color: " . anemos_eutf_option( 'responsive_menu_background_color' ) . ";
	}

	#eut-hidden-menu a,
	#eut-hidden-menu.eut-slide-menu ul.eut-menu li:not(.eut-goback) > .eut-arrow {
		color: " . anemos_eutf_option( 'responsive_menu_link_color' ) . ";
	}

	#eut-hidden-menu:not(.eut-slide-menu) ul.eut-menu li a .eut-arrow:after,
	#eut-hidden-menu:not(.eut-slide-menu) ul.eut-menu li a .eut-arrow:before {
		background-color: " . anemos_eutf_option( 'responsive_menu_link_color' ) . ";
	}

	#eut-hidden-menu ul.eut-menu li.open > a .eut-arrow:after,
	#eut-hidden-menu ul.eut-menu li.open > a .eut-arrow:before {
		background-color: " . anemos_eutf_option( 'responsive_menu_link_hover_color' ) . ";
	}

	#eut-theme-wrapper .eut-header-responsive-elements form,
	#eut-theme-wrapper .eut-header-responsive-elements form p,
	#eut-theme-wrapper .eut-header-responsive-elements form div,
	#eut-theme-wrapper .eut-header-responsive-elements form span {
		color: " . anemos_eutf_option( 'responsive_menu_link_color' ) . ";
	}

	#eut-hidden-menu a:hover,
	#eut-hidden-menu ul.eut-menu > li.current-menu-item > a,
	#eut-hidden-menu ul.eut-menu > li.current-menu-ancestor > a,
	#eut-hidden-menu ul.eut-menu li.current-menu-item > a,
	#eut-hidden-menu ul.eut-menu li.open > a,
	#eut-hidden-menu.eut-slide-menu ul.eut-menu li:not(.eut-goback) > .eut-arrow:hover {
		color: " . anemos_eutf_option( 'responsive_menu_link_hover_color' ) . ";
	}

	#eut-hidden-menu .eut-close-btn:after,
	#eut-hidden-menu .eut-close-btn:before,
	#eut-hidden-menu .eut-close-btn span {
		background-color: " . anemos_eutf_option( 'responsive_menu_close_btn_color' ) . ";
	}

	#eut-hidden-menu ul.eut-menu li a,
	#eut-theme-wrapper .eut-header-responsive-elements form,
	#eut-theme-wrapper .eut-header-responsive-elements form p,
	#eut-theme-wrapper .eut-header-responsive-elements form div,
	#eut-theme-wrapper .eut-header-responsive-elements form span {
		border-color: " . anemos_eutf_option( 'responsive_menu_border_color' ) . ";
	}

	#eut-responsive-header > .eut-wrapper,
	#eut-responsive-header .eut-header-elements {
		border-color: " . anemos_eutf_option( 'responsive_menu_border_color' ) . ";
	}

	#eut-hidden-menu-overlay {
		background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_responsive_menu_overflow_background_color ) . "," . anemos_eutf_option( 'responsive_menu_overflow_background_color_opacity', '0.9') . ");
	}

	";

	/* - Responsive Header Elements
	========================================================================= */
	$css .= "
	#eut-responsive-header .eut-header-element > a,
	#eut-responsive-header .eut-header-element .eut-purchased-items {
		color: " . anemos_eutf_option( 'responsive_header_elements_color' ) . ";
	}

	#eut-responsive-header .eut-header-element > a:hover {
		color: " . anemos_eutf_option( 'responsive_header_elements_hover_color' ) . ";
	}

	";


/* Spinner
============================================================================= */


$spinner_image_id = anemos_eutf_option( 'spinner_image', '', 'id' );
if ( empty( $spinner_image_id ) ) {
	$css .= "
	.eut-spinner {
		display: inline-block;
		position: absolute !important;
		top: 50%;
		left: 50%;
		margin-top: -1.500em;
		margin-left: -1.500em;
		text-indent: -9999em;
		-webkit-transform: translateZ(0);
		-ms-transform: translateZ(0);
		transform: translateZ(0);
	}
	.eut-spinner:not(.custom) {
		font-size: 14px;
		border-top: 0.200em solid rgba(127, 127, 127, 0.3);
		border-right: 0.200em solid rgba(127, 127, 127, 0.3);
		border-bottom: 0.200em solid rgba(127, 127, 127, 0.3);
		border-left: 0.200em solid;
		-webkit-animation: spinnerAnim 1.1s infinite linear;
		animation: spinnerAnim 1.1s infinite linear;
	}

	.eut-spinner:not(.custom) {
		border-left-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
	}

	.eut-spinner:not(.custom),
	.eut-spinner:not(.custom):after {
		border-radius: 50%;
		width: 3.000em;
		height: 3.000em;
	}

	@-webkit-keyframes spinnerAnim {
		0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
	}

	@keyframes spinnerAnim {
		0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
	}
	";
} else {

	$spinner_src = wp_get_attachment_image_src( $spinner_image_id, 'full' );
	$spinner_image_url = $spinner_src[0];
	$spinner_width = $spinner_src[1];
	$spinner_height = $spinner_src[2];

	$css .= "

	.eut-spinner:not(.custom) {
		width: " . intval( $spinner_width ) . "px;
		height: " . intval( $spinner_height ) . "px;
		background-image: url(" . esc_url( $spinner_image_url ) . ");
		background-position: center center;
		display: inline-block;
		position: absolute;
		top: 50%;
		left: 50%;
		margin-top: -" . intval( $spinner_height / 2 ) . "px;
		margin-left: -" . intval( $spinner_width / 2 ) . "px;
	}

	";
}

/* Primary Text Color
============================================================================= */
$css .= "
::-moz-selection {
    color: #ffffff;
    background: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
}

::selection {
    color: #ffffff;
    background: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
}
";

/* Headings Colors */
$css .= "

h1,h2,h3,h4,h5,h6,
.eut-h1,
.eut-h2,
.eut-h3,
.eut-h4,
.eut-h5,
.eut-h6,
.eut-heading-color,
.eut-blog.eut-with-shadow .eut-post-content .eut-post-title,
p.eut-dropcap:first-letter {
	color: " . anemos_eutf_option( 'body_heading_color' ) . ";
}

.eut-headings-primary-1 h1,
.eut-headings-primary-1 h2,
.eut-headings-primary-1 h3,
.eut-headings-primary-1 h4,
.eut-headings-primary-1 h5,
.eut-headings-primary-1 h6,
.eut-headings-primary-1 .eut-heading-color,
.wpb_column.eut-headings-primary-1 h1,
.wpb_column.eut-headings-primary-1 h2,
.wpb_column.eut-headings-primary-1 h3,
.wpb_column.eut-headings-primary-1 h4,
.wpb_column.eut-headings-primary-1 h5,
.wpb_column.eut-headings-primary-1 h6,
.wpb_column.eut-headings-primary-1 .eut-heading-color ,
.eut-blog ul.eut-post-meta a:hover,
.eut-blog a.eut-read-more,
#eut-main-content .vc_tta.vc_general .vc_tta-tab.vc_active > a,
#eut-main-content .vc_tta-panel.vc_active .vc_tta-panel-title a,
a .eut-box-title:hover {
	color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
}

.eut-headings-primary-2 h1,
.eut-headings-primary-2 h2,
.eut-headings-primary-2 h3,
.eut-headings-primary-2 h4,
.eut-headings-primary-2 h5,
.eut-headings-primary-2 h6,
.eut-headings-primary-2 .eut-heading-color,
.wpb_column.eut-headings-primary-2 h1,
.wpb_column.eut-headings-primary-2 h2,
.wpb_column.eut-headings-primary-2 h3,
.wpb_column.eut-headings-primary-2 h4,
.wpb_column.eut-headings-primary-2 h5,
.wpb_column.eut-headings-primary-2 h6,
.wpb_column.eut-headings-primary-2 .eut-heading-color {
	color: " . anemos_eutf_option( 'body_primary_2_color' ) . ";
}

.eut-headings-primary-3 h1,
.eut-headings-primary-3 h2,
.eut-headings-primary-3 h3,
.eut-headings-primary-3 h4,
.eut-headings-primary-3 h5,
.eut-headings-primary-3 h6,
.eut-headings-primary-3 .eut-heading-color,
.wpb_column.eut-headings-primary-3 h1,
.wpb_column.eut-headings-primary-3 h2,
.wpb_column.eut-headings-primary-3 h3,
.wpb_column.eut-headings-primary-3 h4,
.wpb_column.eut-headings-primary-3 h5,
.wpb_column.eut-headings-primary-3 h6,
.wpb_column.eut-headings-primary-3 .eut-heading-color {
	color: " . anemos_eutf_option( 'body_primary_3_color' ) . ";
}

.eut-headings-primary-4 h1,
.eut-headings-primary-4 h2,
.eut-headings-primary-4 h3,
.eut-headings-primary-4 h4,
.eut-headings-primary-4 h5,
.eut-headings-primary-4 h6,
.eut-headings-primary-4 .eut-heading-color,
.wpb_column.eut-headings-primary-4 h1,
.wpb_column.eut-headings-primary-4 h2,
.wpb_column.eut-headings-primary-4 h3,
.wpb_column.eut-headings-primary-4 h4,
.wpb_column.eut-headings-primary-4 h5,
.wpb_column.eut-headings-primary-4 h6,
.wpb_column.eut-headings-primary-4 .eut-heading-color {
	color: " . anemos_eutf_option( 'body_primary_4_color' ) . ";
}

.eut-headings-primary-5 h1,
.eut-headings-primary-5 h2,
.eut-headings-primary-5 h3,
.eut-headings-primary-5 h4,
.eut-headings-primary-5 h5,
.eut-headings-primary-5 h6,
.eut-headings-primary-5 .eut-heading-color,
.wpb_column.eut-headings-primary-5 h1,
.wpb_column.eut-headings-primary-5 h2,
.wpb_column.eut-headings-primary-5 h3,
.wpb_column.eut-headings-primary-5 h4,
.wpb_column.eut-headings-primary-5 h5,
.wpb_column.eut-headings-primary-5 h6,
.wpb_column.eut-headings-primary-5 .eut-heading-color {
	color: " . anemos_eutf_option( 'body_primary_5_color' ) . ";
}

.eut-headings-dark h1,
.eut-headings-dark h2,
.eut-headings-dark h3,
.eut-headings-dark h4,
.eut-headings-dark h5,
.eut-headings-dark h6,
.eut-headings-dark .eut-heading-color,
.wpb_column.eut-headings-dark h1,
.wpb_column.eut-headings-dark h2,
.wpb_column.eut-headings-dark h3,
.wpb_column.eut-headings-dark h4,
.wpb_column.eut-headings-dark h5,
.wpb_column.eut-headings-dark h6,
.wpb_column.eut-headings-dark .eut-heading-color {
	color: #000000;
}

.eut-headings-light h1,
.eut-headings-light h2,
.eut-headings-light h3,
.eut-headings-light h4,
.eut-headings-light h5,
.eut-headings-light h6,
.eut-headings-light .eut-heading-color,
.wpb_column.eut-headings-light h1,
.wpb_column.eut-headings-light h2,
.wpb_column.eut-headings-light h3,
.wpb_column.eut-headings-light h4,
.wpb_column.eut-headings-light h5,
.wpb_column.eut-headings-light h6,
.wpb_column.eut-headings-light .eut-heading-color {
	color: #ffffff;
}


";

/* Primary Text */
$css .= "
.eut-text-primary-1,
#eut-theme-wrapper .eut-text-hover-primary-1:hover,
#eut-theme-wrapper a.eut-text-hover-primary-1:hover,
#eut-theme-wrapper a .eut-text-hover-primary-1:hover,
.eut-blog .eut-post-meta-wrapper li a:hover,
#eut-content .eut-read-more,
#eut-content .more-link,
.eut-search button[type='submit']:hover,
.widget.widget_calendar table tbody a,
blockquote > p:before,
.eut-pagination ul li,
.eut-pagination ul li .current,
.eut-pagination ul li a:hover {
	color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
}

.eut-text-primary-2,
#eut-theme-wrapper .eut-text-hover-primary-2:hover,
#eut-theme-wrapper a.eut-text-hover-primary-2:hover,
#eut-theme-wrapper a .eut-text-hover-primary-2:hover {
	color: " . anemos_eutf_option( 'body_primary_2_color' ) . ";
}

.eut-text-primary-3,
#eut-theme-wrapper .eut-text-hover-primary-3:hover,
#eut-theme-wrapper a.eut-text-hover-primary-3:hover,
#eut-theme-wrapper a .eut-text-hover-primary-3:hover  {
	color: " . anemos_eutf_option( 'body_primary_3_color' ) . ";
}

.eut-text-primary-4,
#eut-theme-wrapper .eut-text-hover-primary-4:hover,
#eut-theme-wrapper a.eut-text-hover-primary-4:hover,
#eut-theme-wrapper a .eut-text-hover-primary-4:hover  {
	color: " . anemos_eutf_option( 'body_primary_4_color' ) . ";
}

.eut-text-primary-5,
#eut-theme-wrapper .eut-text-hover-primary-5:hover,
#eut-theme-wrapper a.eut-text-hover-primary-5:hover,
#eut-theme-wrapper a .eut-text-hover-primary-5:hover  {
	color: " . anemos_eutf_option( 'body_primary_5_color' ) . ";
}

";

/* Dark */
$css .= "
.eut-text-dark,
#eut-content .eut-text-dark,
a.eut-text-dark,
.eut-text-dark-hover:hover,
a:hover .eut-text-dark-hover {
	color: #000000;
}

";

/* Light */
$css .= "
.eut-text-light,
#eut-content .eut-text-light,
a.eut-text-light,
.eut-text-light-hover:hover,
a:hover .eut-text-light-hover {
	color: #ffffff;
}

";

/* Green Text */
$css .= "

.eut-text-green,
.eut-text-hover-green:hover,
a.eut-text-hover-green:hover,
a:hover .eut-text-hover-green {
	color: #66bb6a;
}

";

/* Red Text */
$css .= "

.eut-text-red,
.eut-text-hover-red:hover,
a.eut-text-hover-red:hover,
a:hover .eut-text-hover-red {
	color: #ff5252;
}

";

/* Orange Text */
$css .= "

.eut-text-orange,
.eut-text-hover-orange:hover,
a.eut-text-hover-orange:hover,
a:hover .eut-text-hover-orange {
	color: #fd7f24;
}

";

/* Aqua Text */
$css .= "

.eut-text-aqua,
.eut-text-hover-aqua:hover,
a.eut-text-hover-aqua:hover,
a:hover .eut-text-hover-aqua {
	color: #1de9b6;
}

";

/* Blue Text */
$css .= "

.eut-text-blue,
.eut-text-hover-blue:hover,
a.eut-text-hover-blue:hover,
a:hover .eut-text-hover-blue {
	color: #00b0ff;
}

";

/* Purple Text */
$css .= "

.eut-text-purple,
.eut-text-hover-purple:hover,
a.eut-text-hover-purple:hover,
a:hover .eut-text-hover-purple {
	color: #b388ff;
}

";

/* Black Text */
$css .= "

.eut-text-black,
.eut-text-hover-black:hover,
a.eut-text-hover-black:hover,
a:hover .eut-text-hover-black {
	color: #000000;
}

";

/* Grey Text */
$css .= "

.eut-text-grey,
.eut-text-hover-grey:hover,
a.eut-text-hover-grey:hover,
a:hover .eut-text-hover-grey {
	color: #bababa;
}

";

/* White Text */
$css .= "

.eut-text-white,
.eut-text-hover-white:hover,
a.eut-text-hover-white:hover,
a:hover .eut-text-hover-white {
	color: #ffffff;
}

";


/* Primary Bg Color
============================================================================= */
/* Primary Background */
$css .= "
#eut-theme-wrapper .eut-bg-primary-1,
#eut-theme-wrapper .eut-bg-hover-primary-1:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-1:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-1:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-1,

#eut-theme-wrapper .widget.widget_tag_cloud a:hover,
#eut-theme-wrapper #eut-sidearea .widget.widget_tag_cloud a:hover,
#eut-theme-wrapper .eut-with-line:after,
.eut-page-title .eut-line-divider span,
#eut-feature-section .eut-line-divider span,
#eut-single-post-tags .eut-tags li a:hover,
#eut-single-post-categories .eut-categories li a:hover,
#eut-socials-modal .eut-social li a:hover {
	background-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-1 {
	background-color: transparent;
	border-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
	color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-1:hover {
	background-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-1 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-1 > a:hover .eut-item {
	background-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-bg-primary-2,
#eut-theme-wrapper .eut-bg-hover-primary-2:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-2:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-2:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-2 {
	background-color: " . anemos_eutf_option( 'body_primary_2_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_2_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-2 {
	background-color: transparent;
	border-color: " . anemos_eutf_option( 'body_primary_2_color' ) . ";
	color: " . anemos_eutf_option( 'body_primary_2_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-2:hover {
	background-color: " . anemos_eutf_option( 'body_primary_2_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_2_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-2 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-2 > a:hover .eut-item {
	background-color: " . anemos_eutf_option( 'body_primary_2_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-bg-primary-3,
#eut-theme-wrapper .eut-bg-hover-primary-3:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-3:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-3:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-3 {
	background-color: " . anemos_eutf_option( 'body_primary_3_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_3_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-3 {
	background-color: transparent;
	border-color: " . anemos_eutf_option( 'body_primary_3_color' ) . ";
	color: " . anemos_eutf_option( 'body_primary_3_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-3:hover {
	background-color: " . anemos_eutf_option( 'body_primary_3_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_3_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-3 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-3 > a:hover .eut-item {
	background-color: " . anemos_eutf_option( 'body_primary_3_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-bg-primary-4,
#eut-theme-wrapper .eut-bg-hover-primary-4:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-4:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-4:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-4 {
	background-color: " . anemos_eutf_option( 'body_primary_4_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_4_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-4 {
	background-color: transparent;
	border-color: " . anemos_eutf_option( 'body_primary_4_color' ) . ";
	color: " . anemos_eutf_option( 'body_primary_4_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-4:hover {
	background-color: " . anemos_eutf_option( 'body_primary_4_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_4_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-4 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-4 > a:hover .eut-item {
	background-color: " . anemos_eutf_option( 'body_primary_4_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-bg-primary-5,
#eut-theme-wrapper .eut-bg-hover-primary-5:hover,
#eut-theme-wrapper a.eut-bg-hover-primary-5:hover,
#eut-theme-wrapper a .eut-bg-hover-primary-5:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-primary-5 {
	background-color: " . anemos_eutf_option( 'body_primary_5_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_5_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-primary-5 {
	background-color: transparent;
	border-color: " . anemos_eutf_option( 'body_primary_5_color' ) . ";
	color: " . anemos_eutf_option( 'body_primary_5_color' ) . ";
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-primary-5:hover {
	background-color: " . anemos_eutf_option( 'body_primary_5_color' ) . ";
	border-color: " . anemos_eutf_option( 'body_primary_5_color' ) . ";
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-primary-5 > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-primary-5 > a:hover .eut-item {
	background-color: " . anemos_eutf_option( 'body_primary_5_color' ) . ";
	color: #ffffff;
}

";
/* Dark Background */
$css .= "
.eut-bg-dark,
a.eut-bg-dark:hover,
.eut-outline-btn a.eut-bg-dark:hover {
	background-color: #000000;
	color: #ffffff;
}

.eut-outline-btn a.eut-bg-dark {
	background-color: transparent;
	border-color: #000000;
	color: #000000;
}

";
/* Light Background */
$css .= "
.eut-bg-light,
a.eut-bg-light:hover {
	background-color: #ffffff;
	color: #000000;
}

.eut-outline-btn a.eut-bg-light:hover {
	background-color: #ffffff;
	color: #000000;
}

.eut-outline-btn a.eut-bg-light {
	background-color: transparent;
	border-color: #ffffff;
	color: #ffffff;
}
";


/* Green Background */
$css .= "
#eut-theme-wrapper .eut-bg-green,
#eut-theme-wrapper .eut-bg-hover-green:hover,
#eut-theme-wrapper a.eut-bg-hover-green:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-green {
	background-color: #66bb6a;
	border-color: #66bb6a;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-green {
	background-color: transparent;
	border-color: #66bb6a;
	color: #66bb6a;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-green:hover {
	background-color: #66bb6a;
	border-color: #66bb6a;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-green > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-green > a:hover .eut-item {
	background-color: #66bb6a;
	color: #ffffff;
}

";


/* Red Background */
$css .= "
#eut-theme-wrapper .eut-bg-red,
#eut-theme-wrapper .eut-bg-hover-red:hover,
#eut-theme-wrapper a.eut-bg-hover-red:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-red {
	background-color: #ff5252;
	border-color: #ff5252;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-red {
	background-color: transparent;
	border-color: #ff5252;
	color: #ff5252;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-red:hover {
	background-color: #ff5252;
	border-color: #ff5252;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-red > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-red > a:hover .eut-item {
	background-color: #ff5252;
	color: #ffffff;
}

";

/* Orange Background */
$css .= "
#eut-theme-wrapper .eut-bg-orange,
#eut-theme-wrapper .eut-bg-hover-orange:hover,
#eut-theme-wrapper a.eut-bg-hover-orange:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-orange {
	background-color: #fd7f24;
	border-color: #fd7f24;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-orange {
	background-color: transparent;
	border-color: #fd7f24;
	color: #fd7f24;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-orange:hover {
	background-color: #fd7f24;
	border-color: #fd7f24;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-orange > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-orange > a:hover .eut-item {
	background-color: #fd7f24;
	color: #ffffff;
}

";

/* Aqua Background */
$css .= "
#eut-theme-wrapper .eut-bg-aqua,
#eut-theme-wrapper .eut-bg-hover-aqua:hover,
#eut-theme-wrapper a.eut-bg-hover-aqua:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-aqua {
	background-color: #1de9b6;
	border-color: #1de9b6;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-aqua {
	background-color: transparent;
	border-color: #1de9b6;
	color: #1de9b6;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-aqua:hover {
	background-color: #1de9b6;
	border-color: #1de9b6;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-aqua > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-aqua > a:hover .eut-item {
	background-color: #1de9b6;
	color: #ffffff;
}

";


/* Blue Background */
$css .= "
#eut-theme-wrapper .eut-bg-blue,
#eut-theme-wrapper .eut-bg-hover-blue:hover,
#eut-theme-wrapper a.eut-bg-hover-blue:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-blue {
	background-color: #00b0ff;
	border-color: #00b0ff;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-blue {
	background-color: transparent;
	border-color: #00b0ff;
	color: #00b0ff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-blue:hover {
	background-color: #00b0ff;
	border-color: #00b0ff;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-blue > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-blue > a:hover .eut-item {
	background-color: #00b0ff;
	color: #ffffff;
}

";

/* Purple Background */
$css .= "
#eut-theme-wrapper .eut-bg-purple,
#eut-theme-wrapper .eut-bg-hover-purple:hover,
#eut-theme-wrapper a.eut-bg-hover-purple:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-purple {
	background-color: #b388ff;
	border-color: #b388ff;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-purple {
	background-color: transparent;
	border-color: #b388ff;
	color: #b388ff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-purple:hover {
	background-color: #b388ff;
	border-color: #b388ff;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-purple > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-purple > a:hover .eut-item {
	background-color: #b388ff;
	color: #ffffff;
}

";

/* Black Background */
$css .= "
#eut-theme-wrapper .eut-bg-black,
#eut-theme-wrapper .eut-bg-hover-black:hover,
#eut-theme-wrapper a.eut-bg-hover-black:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-black {
	background-color: #000000;
	border-color: #000000;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-black {
	background-color: transparent;
	border-color: #000000;
	color: #000000;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-black:hover {
	background-color: #000000;
	border-color: #000000;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-black > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-black > a:hover .eut-item {
	background-color: #000000;
	color: #ffffff;
}

";

/* Grey Background */
$css .= "
#eut-theme-wrapper .eut-bg-grey,
#eut-theme-wrapper .eut-bg-hover-grey:hover,
#eut-theme-wrapper a.eut-bg-hover-grey:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-grey {
	background-color: #bababa;
	border-color: #bababa;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-grey {
	background-color: transparent;
	border-color: #bababa;
	color: #bababa;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-grey:hover {
	background-color: #bababa;
	border-color: #bababa;
	color: #ffffff;
}

#eut-theme-wrapper .eut-menu-type-button.eut-grey > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-grey > a:hover .eut-item {
	background-color: #bababa;
	color: #ffffff;
}

";

/* White Background */
$css .= "
#eut-theme-wrapper .eut-bg-white,
#eut-theme-wrapper .eut-bg-hover-white:hover,
#eut-theme-wrapper a.eut-bg-hover-white:hover,
#eut-theme-wrapper a:hover .eut-bg-hover-white {
	background-color: #ffffff;
	border-color: #ffffff;
	color: #bababa;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-white {
	background-color: transparent;
	border-color: #ffffff;
	color: #ffffff;
}

#eut-theme-wrapper a.eut-btn-line.eut-bg-hover-white:hover {
	background-color: #ffffff;
	border-color: #ffffff;
	color: #bababa;
}

#eut-theme-wrapper .eut-menu-type-button.eut-white > a .eut-item,
#eut-theme-wrapper .eut-menu-type-button.eut-hover-white > a:hover .eut-item {
	background-color: #ffffff;
	color: #bababa;
}

";

/* Anchor Menu
============================================================================= */

// Anchor Colors
$css .= "

.eut-anchor-menu .eut-anchor-wrapper,
.eut-anchor-menu .eut-container ul {
	background-color: " . anemos_eutf_option( 'page_anchor_menu_background_color' ) . ";
}

.eut-anchor-menu .eut-anchor-wrapper,
.eut-anchor-menu .eut-container > ul > li > a,
.eut-anchor-menu .eut-container ul li a,
.eut-anchor-menu .eut-container > ul > li:last-child > a {
	border-color: " . anemos_eutf_option( 'page_anchor_menu_border_color' ) . ";
}

.eut-anchor-menu a {
	color: " . anemos_eutf_option( 'page_anchor_menu_text_color' ) . ";
	background-color: transparent;
}

.eut-anchor-menu a:hover,
.eut-anchor-menu .eut-container > ul > li.active > a {
	color: " . anemos_eutf_option( 'page_anchor_menu_text_hover_color' ) . ";
	background-color: " . anemos_eutf_option( 'page_anchor_menu_background_hover_color' ) . ";
}

.eut-anchor-menu a .eut-arrow:after,
.eut-anchor-menu a .eut-arrow:before {
	background-color: " . anemos_eutf_option( 'page_anchor_menu_text_hover_color' ) . ";
}

";

// Page Anchor Size
$css .= "

#eut-page-anchor {
	height: " . intval( anemos_eutf_option( 'page_anchor_menu_height', 120 ) ) . "px;
}

#eut-page-anchor .eut-anchor-wrapper {
	line-height: " . anemos_eutf_option( 'page_anchor_menu_height' ) . "px;
}

#eut-page-anchor.eut-anchor-menu .eut-anchor-btn {
	width: " . anemos_eutf_option( 'page_anchor_menu_height' ) . "px;
}

";

// Post Anchor Size
$css .= "

#eut-post-anchor {
	height: " . intval( anemos_eutf_option( 'post_anchor_menu_height', 120 ) ) . "px;
}

#eut-post-anchor .eut-anchor-wrapper {
	line-height: " . anemos_eutf_option( 'post_anchor_menu_height' ) . "px;
}

#eut-post-anchor.eut-anchor-menu .eut-anchor-btn {
	width: " . anemos_eutf_option( 'page_anchor_menu_height' ) . "px;
}

";


/* Breadcrumbs
============================================================================= */
$css .= "
.eut-breadcrumbs {
	background-color: " . anemos_eutf_option( 'page_breadcrumbs_background_color' ) . ";
	border-color: " . anemos_eutf_option( 'page_breadcrumbs_border_color' ) . ";
}

.eut-breadcrumbs ul li {
	color: " . anemos_eutf_option( 'page_breadcrumbs_divider_color' ) . ";
}

.eut-breadcrumbs ul li a {
	color: " . anemos_eutf_option( 'page_breadcrumbs_text_color' ) . ";
}

.eut-breadcrumbs ul li a:hover {
	color: " . anemos_eutf_option( 'page_breadcrumbs_text_hover_color' ) . ";
}

";

// Page Breadcrumbs Size
$css .= "

#eut-page-breadcrumbs {
	line-height: " . anemos_eutf_option( 'page_breadcrumbs_height' ) . "px;
}

";

// Post Breadcrumbs Size
$css .= "

#eut-post-breadcrumbs {
	line-height: " . anemos_eutf_option( 'post_breadcrumbs_height' ) . "px;
}

";

// Portfolio Breadcrumbs Size
$css .= "

#eut-portfolio-breadcrumbs {
	line-height: " . anemos_eutf_option( 'portfolio_breadcrumbs_height' ) . "px;
}

";

// Product Breadcrumbs Size
$css .= "

#eut-product-breadcrumbs {
	line-height: " . anemos_eutf_option( 'product_breadcrumbs_height' ) . "px;
}

";

/* Main Content
============================================================================= */
$css .= "

#eut-content,
.eut-single-wrapper,
#eut-main-content .eut-section,
.eut-anchor-menu {
	background-color: " . anemos_eutf_option( 'main_content_background_color' ) . ";
	color: " . anemos_eutf_option( 'body_text_color' ) . ";
}

body,
.eut-text-content,
.eut-text-content a,
#eut-content form,
#eut-content form p,
#eut-content form div,
#eut-content form span,
table,
.eut-blog.eut-with-shadow .eut-blog-item:not(.eut-style-2) .eut-post-content,
.eut-blog.eut-with-shadow .eut-blog-item:not(.eut-style-2) .eut-post-meta-wrapper {
	color: " . anemos_eutf_option( 'body_text_color' ) . ";
}

";
	/* - Main Content Borders
	========================================================================= */
	$css .= "
	#eut-theme-wrapper .eut-border,
	a.eut-border,
	#eut-content table,
	#eut-content tr,
	#eut-content td,
	#eut-content th,
	#eut-theme-wrapper form,
	#eut-theme-wrapper .wpcf7-form-control-wrap,
	#eut-theme-wrapper label,
	hr,
	.eut-hr.eut-element div,
	.eut-title-double-line span:before,
	.eut-title-double-line span:after,
	.eut-title-double-bottom-line span:after,
	.vc_tta-tabs-position-top .vc_tta-tabs-list,
	#reply-title,
	.eut-pagination ul li,
	.vc_tta.vc_general .vc_tta-panel-title,
	#eut-single-post-tags .eut-tags li a,
	#eut-single-post-categories .eut-categories li a,
	#eut-theme-wrapper .widget.widget_tag_cloud a {
		border-color: " . anemos_eutf_option( 'body_border_color' ) . ";
	}

	#eut-single-post-categories .eut-categories li a {
		background-color: " . anemos_eutf_option( 'body_border_color' ) . ";
	}

	";

	/* Primary Border */
	$css .= "
	.eut-border-primary-1,
	#eut-content .eut-blog-large .eut-blog-item.sticky ul.eut-post-meta,
	.eut-carousel-pagination-2 .eut-carousel .owl-controls .owl-page.active span,
	.eut-carousel-pagination-2 .eut-carousel .owl-controls.clickable .owl-page:hover span,
	.eut-carousel-pagination-2.eut-testimonial .owl-controls .owl-page.active span,
	.eut-carousel-pagination-2.eut-testimonial .owl-controls.clickable .owl-page:hover span,
	.eut-carousel-pagination-2 .eut-flexible-carousel .owl-controls .owl-page.active span,
	.eut-carousel-pagination-2 .eut-flexible-carousel .owl-controls.clickable .owl-page:hover span,
	#eut-content .eut-read-more:after,
	#eut-content .more-link:after,
	.eut-blog-large .eut-blog-item.sticky .eut-blog-item-inner:after {
		border-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
	}
	";

	/* - Widget Colors
	========================================================================= */
	$css .= "
	#eut-content .widget .eut-widget-title,
	#reply-title {
		color: " . anemos_eutf_option( 'widget_title_color' ) . ";
		background-color: " . anemos_eutf_option( 'widget_title_background_color' ) . ";
	}

	.widget {
		color: " . anemos_eutf_option( 'body_text_color' ) . ";
	}

	.widget,
	.widget ul,
	.widget li,
	.widget div {
		border-color: " . anemos_eutf_option( 'body_border_color' ) . ";
	}

	.widget a:not(.eut-outline):not(.eut-btn) {
		color: " . anemos_eutf_option( 'body_text_color' ) . ";
	}

	.widget:not(.eut-social) a:not(.eut-outline):not(.eut-btn):hover,
	.widget.widget_nav_menu li.open > a {
		color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
	}
	";

/* Bottom Bar Colors
============================================================================= */
$css .= "

#eut-bottom-bar {
	background-color: " . anemos_eutf_option( 'bottom_bar_background_color' ) . ";
	color: " . anemos_eutf_option( 'bottom_bar_text_color' ) . ";
}

#eut-bottom-bar .eut-bottom-bar-title{
	color: " . anemos_eutf_option( 'bottom_bar_text_color' ) . ";
}

#eut-bottom-bar form,
#eut-bottom-bar .wpcf7-form-control-wrap,
#eut-bottom-bar .eut-social li a {
	border-color: " . anemos_eutf_option( 'bottom_bar_border_color' ) . ";
}

#eut-bottom-bar .eut-social li a {
	color: " . anemos_eutf_option( 'bottom_bar_link_color' ) . ";
}

#eut-bottom-bar .eut-social li a:hover {
	background-color: " . anemos_eutf_option( 'bottom_bar_link_hover_color' ) . ";
	border-color: " . anemos_eutf_option( 'bottom_bar_link_hover_color' ) . ";
	color: #ffffff;
}

.eut-bottom-bar-social-wrapper span:after {
	border-color: " . anemos_eutf_option( 'bottom_bar_link_hover_color' ) . ";
}

";


/* Footer
============================================================================= */

	/* - Widget Area
	========================================================================= */
	$css .= "
	#eut-footer .eut-widget-area {
		background-color: " . anemos_eutf_option( 'footer_widgets_bg_color' ) . ";
	}
	";
	/* - Footer Widget Colors
	========================================================================= */
	$css .= "
	#eut-footer .widget .eut-widget-title,
	#eut-footer h1,
	#eut-footer h2,
	#eut-footer h3,
	#eut-footer h4,
	#eut-footer h5,
	#eut-footer h6 {
		color: " . anemos_eutf_option( 'footer_widgets_headings_color' ) . ";
	}

	#eut-footer .widget,
	#eut-footer form,
	#eut-footer form p,
	#eut-footer form div,
	#eut-footer form span {
		color: " . anemos_eutf_option( 'footer_widgets_font_color' ) . ";
	}

	#eut-footer .widget,
	#eut-footer .widget a:not(.eut-outline):not(.eut-btn),
	#eut-footer .widget ul,
	#eut-footer .widget li,
	#eut-footer .widget div,
	#eut-footer table,
	#eut-footer tr,
	#eut-footer td,
	#eut-footer th,
	#eut-footer form,
	#eut-footer .wpcf7-form-control-wrap,
	#eut-footer label,
	#eut-footer .eut-border,
	#eut-footer form,
	#eut-footer form p,
	#eut-footer form div,
	#eut-footer form span,
	#eut-footer .eut-widget-area {
		border-color: " . anemos_eutf_option( 'footer_widgets_border_color' ) . ";
	}

	#eut-footer .widget a:not(.eut-outline):not(.eut-btn) {
		color: " . anemos_eutf_option( 'footer_widgets_link_color' ) . ";
	}

	#eut-footer .widget:not(.widget_tag_cloud) a:not(.eut-outline):not(.eut-btn):hover,
	#eut-footer .widget.widget_nav_menu li.open > a {
		color: " . anemos_eutf_option( 'footer_widgets_hover_color' ) . ";
	}

	#eut-footer .widget.widget_tag_cloud a:hover {
		background-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
		border-color: " . anemos_eutf_option( 'body_primary_1_color' ) . ";
		color: #ffffff;
	}

	";
	/* - Footer Bar Colors
	========================================================================= */
	$anemos_eutf_footer_bar_background_color = anemos_eutf_option( 'footer_bar_bg_color', '#000000' );
	$css .= "
	#eut-footer .eut-footer-bar {
		color: " . anemos_eutf_option( 'footer_bar_font_color' ) . ";
		background-color: rgba(" . anemos_eutf_hex2rgb( $anemos_eutf_footer_bar_background_color ) . "," . anemos_eutf_option( 'footer_bar_bg_color_opacity', '1') . ");
	}

	#eut-footer .eut-footer-bar a {
		color: " . anemos_eutf_option( 'footer_bar_link_color' ) . ";
	}

	#eut-footer .eut-footer-bar a:hover {
		color: " . anemos_eutf_option( 'footer_bar_hover_color' ) . ";
	}
	";



/* Composer Front End Fix*/
$css .= "

.compose-mode .vc_element .eut-row {
    margin-top: 30px;
}

.compose-mode .vc_vc_column .wpb_column {
    width: 100% !important;
    margin-bottom: 30px;
    border: 1px dashed rgba(125, 125, 125, 0.4);
}

.compose-mode .vc_controls > .vc_controls-out-tl {
    left: 15px;
}

.compose-mode .vc_controls > .vc_controls-bc {
    bottom: 15px;
}

.compose-mode .vc_welcome .vc_buttons {
    margin-top: 60px;
}

.compose-mode .eut-image img {
    opacity: 1;
}

.compose-mode .vc_controls > div {
    z-index: 9;
}
.compose-mode .eut-bg-image {
    opacity: 1;
}

.compose-mode #eut-theme-wrapper .eut-section.eut-fullwidth-background,
.compose-mode #eut-theme-wrapper .eut-section.eut-fullwidth-element {
	visibility: visible;
}

.compose-mode .eut-animated-item {
	opacity: 1;
}

";

$anemos_eutf_gap_size = array (
	array(
		'gap' => '5',
	),
	array(
		'gap' => '10',
	),
	array(
		'gap' => '15',
	),
	array(
		'gap' => '20',
	),
	array(
		'gap' => '25',
	),
	array(
		'gap' => '30',
	),
	array(
		'gap' => '35',
	),
	array(
		'gap' => '40',
	),
	array(
		'gap' => '45',
	),
	array(
		'gap' => '50',
	),
	array(
		'gap' => '55',
	),
	array(
		'gap' => '60',
	),
);

function anemos_eutf_print_gap_size( $anemos_eutf_gap_size = array()) {

	$css = '';

	foreach ( $anemos_eutf_gap_size as $size ) {

		$anemos_eutf_gap_size = $size['gap'];
		$anemos_eutf_gap_half_size = $size['gap'] * 0.5;

		$css .= "

			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " {
				margin-top: -" . esc_attr( $anemos_eutf_gap_size ) . "px;
				margin-left: -" . esc_attr( $anemos_eutf_gap_half_size ) . "px;
				margin-right: -" . esc_attr( $anemos_eutf_gap_half_size ) . "px;
			}
			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-column {
				margin-top: " . esc_attr( $anemos_eutf_gap_size ) . "px;
				padding-left: " . esc_attr( $anemos_eutf_gap_half_size ) . "px;
				padding-right: " . esc_attr( $anemos_eutf_gap_half_size ) . "px;
			}
			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-element + .eut-element {
				margin-top: " . esc_attr( $anemos_eutf_gap_size ) . "px;
			}
			.eut-section.eut-fullwidth .eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " {
				padding-left: " . esc_attr( $anemos_eutf_gap_half_size ) . "px;
				padding-right: " . esc_attr( $anemos_eutf_gap_half_size ) . "px;
			}

			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-row-inner {
				margin-left: -" . esc_attr( $anemos_eutf_gap_half_size ) . "px;
				margin-right: -" . esc_attr( $anemos_eutf_gap_half_size ) . "px;
			}

			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-row-inner + .eut-row-inner {
				margin-top: " . esc_attr( $anemos_eutf_gap_size ) . "px;
			}

			.eut-row.eut-columns-gap-" . esc_attr( $size['gap'] ) . " .eut-column-inner {
				padding-left: " . esc_attr( $anemos_eutf_gap_half_size ) . "px;
				padding-right: " . esc_attr( $anemos_eutf_gap_half_size ) . "px;
			}

		";

	}

	return $css;
}

$css .= anemos_eutf_print_gap_size( $anemos_eutf_gap_size );


$anemos_eutf_space_size = array (
	array(
		'id' => '1x',
		'percentage' => 1,
	),
	array(
		'id' => '2x',
		'percentage' => 2,
	),
	array(
		'id' => '3x',
		'percentage' => 3,
	),
	array(
		'id' => '4x',
		'percentage' => 4,
	),
	array(
		'id' => '5x',
		'percentage' => 5,
	),
	array(
		'id' => '6x',
		'percentage' => 6,
	),
);

function anemos_eutf_print_space_size( $anemos_eutf_space_size = array() , $ratio = 1) {
	$default_space_size = 30;
	$css = '';

	foreach ( $anemos_eutf_space_size as $size ) {

		$anemos_eutf_space_size = ( $default_space_size * $size['percentage'] ) * $ratio;
		if ( $anemos_eutf_space_size <= $default_space_size ) {
			$anemos_eutf_space_size = $default_space_size;
		}
		$css .= "
			#eut-theme-wrapper .eut-padding-top-" . esc_attr( $size['id'] ) . "{ padding-top: " . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-padding-bottom-" . esc_attr( $size['id'] ) . "{ padding-bottom: " . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-margin-top-" . esc_attr( $size['id'] ) . "{ margin-top: " . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-margin-bottom-" . esc_attr( $size['id'] ) . "{ margin-bottom: " . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-height-" . esc_attr( $size['id'] ) . "{ height: " . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-top-" . esc_attr( $size['id'] ) . "{ top: " . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-left-" . esc_attr( $size['id'] ) . "{ left: " . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-right-" . esc_attr( $size['id'] ) . "{ right: " . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-bottom-" . esc_attr( $size['id'] ) . "{ bottom: " . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-top-minus-" . esc_attr( $size['id'] ) . "{ top: -" . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-left-minus-" . esc_attr( $size['id'] ) . "{ left: -" . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-right-minus-" . esc_attr( $size['id'] ) . "{ right: -" . esc_attr( $anemos_eutf_space_size ) . "px; }
			#eut-theme-wrapper .eut-bottom-minus-" . esc_attr( $size['id'] ) . "{ bottom: -" . esc_attr( $anemos_eutf_space_size ) . "px; }
		";

	}


	if ( is_page_template('page-templates/template-content-only.php') || is_404() ) {
	   $default_space_size = 0;
	}

	$css .= "
	#eut-main-content .eut-main-content-wrapper,
	#eut-sidebar {
		padding-top: " . intval( $default_space_size * 3  * $ratio ) . "px;
		padding-bottom: " . intval( $default_space_size * 3  * $ratio ) . "px;
	}
	";

	return $css;
}

$css .= anemos_eutf_print_space_size( $anemos_eutf_space_size, 1 );

$css .= "
	@media only screen and (max-width: 2000px) {
		" . anemos_eutf_print_space_size( $anemos_eutf_space_size, 0.8 ). "
	}
	@media only screen and (max-width: 1200px) {
		" . anemos_eutf_print_space_size( $anemos_eutf_space_size, 0.5 ). "
	}
";

if ( is_singular() ) {

	$anemos_eutf_padding_top = anemos_eutf_post_meta( '_anemos_eutf_padding_top' );
	$anemos_eutf_padding_bottom = anemos_eutf_post_meta( '_anemos_eutf_padding_bottom' );
	if( '' != $anemos_eutf_padding_top || '' != $anemos_eutf_padding_bottom ) {
		$css .= "#eut-main-content .eut-main-content-wrapper, #eut-sidebar {";
		if( '' != $anemos_eutf_padding_top ) {
			$css .= 'padding-top: '. ( preg_match('/(px|em|\%|pt|cm)$/', $anemos_eutf_padding_top) ? esc_attr( $anemos_eutf_padding_top ) : esc_attr( $anemos_eutf_padding_top ) . 'px').';';
		}
		if( '' != $anemos_eutf_padding_bottom  ) {
			$css .= 'padding-bottom: '.( preg_match('/(px|em|\%|pt|cm)$/', $anemos_eutf_padding_bottom) ? esc_attr( $anemos_eutf_padding_bottom ) : esc_attr(  $anemos_eutf_padding_bottom ) .'px').';';
		}
		$css .= "}";
	}

}

echo anemos_eutf_get_css_output( $css );

//Omit closing PHP tag to avoid accidental whitespace output errors.
