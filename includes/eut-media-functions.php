<?php

/*
 *	Media functions
 *
 * 	@version	1.0
 * 	@author		Euthemians Team
 * 	@URI		http://euthemians.com
 */


 /**
 * Generic function that prints a slider/carousel navigation
 */
function anemos_eutf_element_navigation( $navigation_type = 0, $navigation_color = 'dark' ) {

	$output = '';

	if ( 0 != $navigation_type ) {

		switch( $navigation_type ) {

			case '2':
				$icon_nav_prev = 'eut-icon-nav-left';
				$icon_nav_next = 'eut-icon-nav-right';
				break;
			case '3':
				$icon_nav_prev = 'eut-icon-nav-left';
				$icon_nav_next = 'eut-icon-nav-right';
				break;
			case '4':
				$icon_nav_prev = 'eut-icon-nav-left';
				$icon_nav_next = 'eut-icon-nav-right';
				break;
			default:
				$navigation_type = '1';
				$icon_nav_prev = 'eut-icon-nav-left';
				$icon_nav_next = 'eut-icon-nav-right';
				break;
		}

		$output .= '<div class="eut-carousel-navigation eut-' . esc_attr( $navigation_color ) . ' eut-navigation-' . esc_attr( $navigation_type ) . '">';
		$output .= '	<div class="eut-carousel-buttons">';
		$output .= '		<div class="eut-carousel-prev">';
		$output .= '			<i class="' . esc_attr( $icon_nav_prev ) . '"></i>';
		$output .= '		</div>';
		$output .= '		<div class="eut-carousel-next">';
		$output .= '			<i class="' . esc_attr( $icon_nav_next ) . '"></i>';
		$output .= '		</div>';
		$output .= '	</div>';
		$output .= '</div>';
	}

	return 	$output;

}

/**
 * Generic function that prints a slider or gallery
 */
function anemos_eutf_print_gallery_slider( $gallery_mode, $slider_items , $image_size_slider = 'anemos-eutf-large-rect-horizontal', $extra_class = "") {

	if ( empty( $slider_items ) ) {
		return;
	}
	$image_size_gallery_thumb = 'anemos-eutf-small-rect-horizontal';
	if( 'gallery-vertical' == $gallery_mode ) {
		$image_size_gallery_thumb = $image_size_slider;
	}

	$start_block = $end_block = $item_class = '';


	if ( 'gallery' == $gallery_mode || '' == $gallery_mode || 'gallery-vertical' == $gallery_mode ) {

		$gallery_index = 0;
		
		$image_popup_title_caption = "none";
		if ( is_singular( 'post' ) ) {
			$image_popup_title_caption = anemos_eutf_post_meta( '_anemos_eutf_post_type_gallery_image_popup_title_caption' );
		}
		if ( empty( $image_popup_title_caption ) ) {
			$image_popup_title_caption = "none";
		}

?>
		<div class="eut-media">
			<ul class="eut-post-gallery eut-post-gallery-popup <?php echo esc_attr( $extra_class ); ?>">
<?php

		foreach ( $slider_items as $slider_item ) {

			$media_id = $slider_item['id'];
			$full_src = wp_get_attachment_image_src( $media_id, 'anemos-eutf-fullscreen' );
			$image_full_url = $full_src[0];

			$thumb_src = wp_get_attachment_image_src( $media_id, $image_size_gallery_thumb );
			$image_thumb_url = $thumb_src[0];

			$image_title = get_post_field( 'post_title', $media_id );
			$image_caption = get_post_field( 'post_excerpt', $media_id );
			$data = "";
			if ( !empty( $image_title ) && 'none' != $image_popup_title_caption && 'caption-only' != $image_popup_title_caption ) {
				$data .= ' data-title="' . esc_attr( $image_title ) . '"';
			}
			if ( !empty( $image_caption ) && 'none' != $image_popup_title_caption && 'title-only' != $image_popup_title_caption ) {
				$data .= ' data-desc="' . esc_attr( $image_caption ) . '"';
			}
			
			echo '<li class="eut-image-hover">';
			echo '<a href="' . esc_url( $image_full_url ) . '"' . $data . '>';
			echo wp_get_attachment_image( $media_id, $image_size_gallery_thumb );
			echo '</a>';
			echo '</li>';
		}
?>
			</ul>
		</div>
<?php

	} else {

		$slider_settings = array();
		if ( is_singular( 'post' ) || is_singular( 'portfolio' ) ) {
			if ( is_singular( 'post' ) ) {
				$slider_settings = anemos_eutf_post_meta( '_anemos_eutf_post_slider_settings' );
			} else {
				$slider_settings = anemos_eutf_post_meta( '_anemos_eutf_portfolio_slider_settings' );
			}
		}
		$slider_speed = anemos_eutf_array_value( $slider_settings, 'slideshow_speed', '2500' );
		$slider_dir_nav = anemos_eutf_array_value( $slider_settings, 'direction_nav', '1' );
		$slider_dir_nav_color = anemos_eutf_array_value( $slider_settings, 'direction_nav_color', 'dark' );
		$blog_style = anemos_eutf_option( 'blog_style', 'large' );

?>
		<div class="eut-media clearfix">
			<div class="eut-element eut-carousel-wrapper">

			<?php echo anemos_eutf_element_navigation( $slider_dir_nav, $slider_dir_nav_color ); ?>

				<div class="eut-slider eut-carousel-element " data-slider-speed="<?php echo esc_attr( $slider_speed ); ?>" data-slider-pause="yes" data-slider-autoheight="no">
<?php
					foreach ( $slider_items as $slider_item ) {
						$media_id = $slider_item['id'];
						$full_src = wp_get_attachment_image_src( $media_id, $image_size_slider );
						$image_url = $full_src[0];
						echo '<div class="eut-slider-item">';
						echo wp_get_attachment_image( $media_id, $image_size_slider );
						echo '</div>';

					}
?>
				</div>

			</div>
		</div>
<?php
	}
}

/**
 * Generic function that prints video settings ( HTML5 )
 */

if ( !function_exists( 'anemos_eutf_print_media_video_settings' ) ) {
	function anemos_eutf_print_media_video_settings( $video_settings ) {
		$video_attr = '';

		if ( !empty( $video_settings ) ) {

			$video_poster = anemos_eutf_array_value( $video_settings, 'poster' );
			$video_preload = anemos_eutf_array_value( $video_settings, 'preload', 'metadata' );

			if( 'yes' == anemos_eutf_array_value( $video_settings, 'controls' ) ) {
				$video_attr .= ' controls';
			}
			if( 'yes' == anemos_eutf_array_value( $video_settings, 'loop' ) ) {
				$video_attr .= ' loop="loop"';
			}
			if( 'yes' ==  anemos_eutf_array_value( $video_settings, 'muted' ) ) {
				$video_attr .= ' muted="muted"';
			}
			if( 'yes' == anemos_eutf_array_value( $video_settings, 'autoplay' ) ) {
				$video_attr .= ' autoplay="autoplay"';
			}
			if( !empty( $video_poster ) ) {
				$video_attr .= ' poster="' . esc_url( $video_poster ) . '"';
			}
			$video_attr .= ' preload="' . esc_attr( $video_preload ) . '"';

		}
		return $video_attr;
	}
}

/**
 * Generic function that prints a video ( Embed or HTML5 )
 */
function anemos_eutf_print_media_video( $video_mode, $video_webm, $video_mp4, $video_ogv, $video_embed, $video_poster = '' ) {
	global $wp_embed;
	$video_output = '';

	if( empty( $video_mode ) && !empty( $video_embed ) ) {
		$video_output .= '<div class="eut-media">';
		$video_output .= $wp_embed->run_shortcode( '[embed]' . $video_embed . '[/embed]' );
		$video_output .= '</div>';
	} else {


		if ( !empty( $video_webm ) || !empty( $video_mp4 ) || !empty( $video_ogv ) ) {

			$video_settings = array(
				'controls' => 'yes',
				'poster' => $video_poster,
			);
			$video_settings = apply_filters( 'anemos_eutf_media_video_settings', $video_settings );

			$video_output .= '<div class="eut-media">';
			$video_output .= '  <video ' . anemos_eutf_print_media_video_settings( $video_settings ) . ' >';

			if ( !empty( $video_webm ) ) {
				$video_output .= '<source src="' . esc_url( $video_webm ) . '" type="video/webm">';
			}
			if ( !empty( $video_mp4 ) ) {
				$video_output .= '<source src="' . esc_url( $video_mp4 ) . '" type="video/mp4">';
			}
			if ( !empty( $video_ogv ) ) {
				$video_output .= '<source src="' . esc_url( $video_ogv ) . '" type="video/ogg">';
			}
			$video_output .='  </video>';
			$video_output .= '</div>';

		}
	}

	echo  $video_output;

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
