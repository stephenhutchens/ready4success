<?php

/*
*	Woocommerce helper functions and configuration
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

/**
 * Helper function to check if woocommerce is enabled
 */
function anemos_eutf_woocommerce_enabled() {

	if ( class_exists( 'woocommerce' ) ) {
		return true;
	}
	return false;

}

function anemos_eutf_is_woo_shop() {
	if ( anemos_eutf_woocommerce_enabled() && is_shop() && !is_search() ) {
		return true;
	}
	return false;
}

function anemos_eutf_is_woo_tax() {
	if ( anemos_eutf_woocommerce_enabled() && is_product_taxonomy() ) {
		return true;
	}
	return false;
}

function anemos_eutf_is_woo_category() {
	if ( anemos_eutf_woocommerce_enabled() && is_product_category() ) {
		return true;
	}
	return false;
}

function anemos_eutf_is_woo_tag() {
	if ( anemos_eutf_woocommerce_enabled() && is_product_tag() ) {
		return true;
	}
	return false;
}




//If woocomerce plugin is not enabled return
if ( !anemos_eutf_woocommerce_enabled() ) {
	return false;
}

//Add Theme support for woocommerce
add_theme_support( 'woocommerce' );

/**
 * Helper function to get shop custom fields with fallback
 */
function anemos_eutf_post_meta_shop( $id, $fallback = false ) {
	$post_id = wc_get_page_id( 'shop' );
	if ( $fallback == false ) $fallback = '';
	$post_meta = get_post_meta( $post_id, $id, true );
	$output = ( $post_meta !== '' ) ? $post_meta : $fallback;
	return $output;
}

/**
 * Helper function to skin Product Search
 */
function anemos_eutf_woo_product_search( $form ) {
	$new_custom_id = uniqid( 'eut_product_search_' );
	$form =  '<form class="eut-search" method="get" action="' . esc_url( home_url( '/' ) ) . '" >';
	$form .= '  <button type="submit" class="eut-search-btn eut-custom-btn"><i class="eut-icon-search"></i></button>';
	$form .= '  <input type="text" class="eut-search-textfield" id="' . esc_attr( $new_custom_id ) . '" value="' . get_search_query() . '" name="s" placeholder="' . esc_attr__( 'Search for ...', 'anemos' ) . '" />';
	$form .= '  <input type="hidden" name="post_type" value="product" />';
	$form .= '</form>';
	return $form;
}


/**
 * Function to add before main woocommerce content
 */
function anemos_eutf_woo_before_main_content() {

	if ( is_shop() && !is_search() ) {
		anemos_eutf_print_header_title( 'page' );
		anemos_eutf_print_header_breadcrumbs( 'page' );
		anemos_eutf_print_anchor_menu( 'page' );
	}
?>

	<!-- CONTENT -->
	<div id="eut-content" class="clearfix <?php echo anemos_eutf_sidebar_class( 'shop' ); ?>">
		<div class="eut-content-wrapper">
			<!-- MAIN CONTENT -->
			<div id="eut-main-content" role="main">
				<div class="eut-main-content-wrapper clearfix">
<?php
		if( !is_product() ) {
?>
					<div class="eut-container">
<?php
		}

}

/**
 * Function to add after main woocommerce content
 */
function anemos_eutf_woo_after_main_content() {
		if( !is_product() ) {
?>
					</div>
<?php
		}
?>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<?php anemos_eutf_set_current_view( 'shop' ); ?>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<!-- END CONTENT -->
<?php
}


/**
 * Overwrite the WooCommerce actions and filters
 */

//Add Content Wrappers
add_action('woocommerce_before_main_content', 'anemos_eutf_woo_before_main_content', 10);
add_action('woocommerce_after_main_content', 'anemos_eutf_woo_after_main_content', 10);

//General Woo
add_filter( 'get_product_search_form', 'anemos_eutf_woo_product_search' );

//Omit closing PHP tag to avoid accidental whitespace output errors.
