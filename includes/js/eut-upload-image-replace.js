jQuery(document).ready(function($) {

	"use strict";

	var eutMediaImageReplaceFrame;
	var eutMediaImageReplaceContainer;
	var eutMediaImageReplaceMode;
	var eutMediaImageReplaceImage;
	var eutMediaImageFieldName;

	$('.eut-upload-replace-image').bind("click",(function(){
		$(this).bindUploadReplaceImage();
	}));
	
	$('.eut-upload-remove-image').bind("click",(function(e){
		$(this).bindUploadRemoveImage(e);
	}));

	$.fn.bindUploadRemoveImage = function(e){
		e.preventDefault();
		$(this).parent().find('.eut-upload-media-id').val('0');
		$(this).parent().removeClass('eut-visible');
	}

	$.fn.bindUploadReplaceImage = function(){

		eutMediaImageReplaceContainer = $(this).parent().find('.eut-thumb-container');
		eutMediaImageReplaceMode = eutMediaImageReplaceContainer.data('mode');
		eutMediaImageFieldName = eutMediaImageReplaceContainer.data('field-name');
		eutMediaImageReplaceImage = $(this).parent().find('.eut-thumb');

        if ( eutMediaImageReplaceFrame ) {
            eutMediaImageReplaceFrame.open();
            return;
        }


        eutMediaImageReplaceFrame = wp.media.frames.eutMediaImageReplaceFrame = wp.media({
            className: 'media-frame eut-media-replace-image-frame',
            frame: 'select',
            multiple: false,
            title: anemos_eutf_upload_image_replace_texts.modal_title,
            library: {
                type: 'image'
            },
            button: {
                text:  anemos_eutf_upload_image_replace_texts.modal_button_title
            }

        });

        eutMediaImageReplaceFrame.on('select', function(){
			var selection = eutMediaImageReplaceFrame.state().get('selection');
			var ids = selection.pluck('id');
			$('.eut-upload-replace-image').unbind("click").css({ 'cursor': 'wait' });
			eutMediaImageReplaceImage.remove();
			eutMediaImageReplaceContainer.addClass('eut-visible eut-loading');
			$.post( anemos_eutf_upload_image_replace_texts.ajaxurl, { action:'anemos_eutf_get_replaced_image', attachment_id: ids.toString(), attachment_mode: eutMediaImageReplaceMode, field_name: eutMediaImageFieldName } , function( mediaHtml ) {
				eutMediaImageReplaceContainer.html(mediaHtml).removeClass('eut-loading');
				$('.eut-upload-replace-image').bind("click",(function(){
					$(this).bindUploadReplaceImage();
				})).css({ 'cursor': 'pointer' });
				
				$('.eut-upload-remove-image').bind("click",(function(e){
					$(this).bindUploadRemoveImage(e);
				}));				
			});
        });

        eutMediaImageReplaceFrame.open();
    }


});