jQuery(document).ready(function($) {

	"use strict";

	var eutFeatureSliderFrame;
	var eutFeatureSliderContainer = $( "#eut-feature-slider-container" );
	eutFeatureSliderContainer.sortable();

	$('.eut-feature-slider-item-delete-button').click(function() {
		$(this).parent().remove();
	});

	$('.eut-upload-feature-slider-post-button').click(function() {

		var post_ids = $('#eut-upload-feature-slider-post-selection').val();
		if( '' != post_ids ) {
			$.post( anemos_eutf_upload_feature_slider_texts.ajaxurl, { action:'anemos_eutf_get_admin_feature_slider_media', post_ids: post_ids.toString() } , function( mediaHtml ) {
				eutFeatureSliderContainer.append(mediaHtml);
				$(this).bindFeatureSliderUpdatefunctions();
			});
		}

	});

	$('.eut-upload-feature-slider-button').click(function() {

        if ( eutFeatureSliderFrame ) {
            eutFeatureSliderFrame.open();
            return;
        }

        eutFeatureSliderFrame = wp.media.frames.eutFeatureSliderFrame = wp.media({
            className: 'media-frame eut-media-feature-slider-frame',
            frame: 'select',
            multiple: 'toggle',
            title: anemos_eutf_upload_feature_slider_texts.modal_title,
            library: {
                type: 'image'
            },
            button: {
                text:  anemos_eutf_upload_feature_slider_texts.modal_button_title
            }

        });
        eutFeatureSliderFrame.on('select', function(){
			var selection = eutFeatureSliderFrame.state().get('selection');
			var ids = selection.pluck('id');

			$('#eut-upload-feature-slider-button-spinner').show();

			$.post( anemos_eutf_upload_feature_slider_texts.ajaxurl, { action:'anemos_eutf_get_admin_feature_slider_media', attachment_ids: ids.toString() } , function( mediaHtml ) {
				eutFeatureSliderContainer.append(mediaHtml);
				$(this).bindFeatureSliderUpdatefunctions();
			});
        });
        eutFeatureSliderFrame.on('ready', function(){
			$( '.media-modal' ).addClass( 'eut-media-no-sidebar' );
        });


        eutFeatureSliderFrame.open();
    });

	$.fn.bindFeatureSliderUpdatefunctions = function(){
		$('.eut-feature-slider-item-delete-button.eut-item-new').click(function() {
			$(this).parent().remove();
		}).removeClass('eut-item-new');

		$('.eut-item-new .eut-upload-replace-image').bind("click",(function(){
			$(this).bindUploadReplaceImage();
		}));

		$('.eut-item-new .eut-upload-remove-image').bind("click",(function(e){
			$(this).bindUploadRemoveImage(e);
		}));

		$('.eut-open-slider-modal.eut-item-new').bind("click",(function(e){
			e.preventDefault();
			$(this).bindOpenSliderModal();
		})).removeClass('eut-item-new');

		$('.eut-tabs .eut-tab-links a').off("click").on("click", (function(e) {
			$(this).bindTabsMetaboxes(e);
		}));

		$('.eut-dependency-field').off("change").on("change",(function(){
			$(this).bindFieldsDependency();
		}));

		$('.postbox.eut-item-new .handlediv').on('click', function() {
			var p = $(this).parent('.postbox');

			p.removeClass('eut-item-new');
			p.toggleClass('closed');

		});

		$('.eut-slider-item.eut-item-new .wp-color-picker-field').wpColorPicker();


		$('.eut-slider-item.eut-item-new .eut-select-color-extra').change(function() {
			if( 'custom' == $(this).val() ) {
				$(this).parents('.eut-field-items-wrapper').find('.eut-wp-colorpicker').show();
			} else {
				$(this).parents('.eut-field-items-wrapper').find('.eut-wp-colorpicker').hide();
			}
		});

		$('.eut-slider-item.eut-item-new').removeClass('eut-item-new');

		$('#eut-upload-feature-slider-button-spinner').hide();

		$( "[data-dependency]" ).initFieldsDependency();

		$('.eut-admin-label-update').off("change").on("change",(function(){
			$(this).bindFieldsAdminLabelUpdate();
		}));
    }

	$('.eut-post-selector-select2').select2( {
		placeholder: 'Select a post',
		multiple: true,
		minimumInputLength: 3,
		ajax: {
			url: ajaxurl,
			dataType: 'json',
			data: function (term, page) {
				return {
					q: term,
					action: 'anemos_eutf_post_select_lookup',
				};
			},
			results: eutProcessPostSelectDataForSelect2
		},
		initSelection: function(element, callback) {
			var ids=$(element).val();
			if (ids!=="") {
				$.ajax(ajaxurl, {
					data: {
						action: 'anemos_eutf_get_post_titles',
						post_ids: ids
					},
					dataType: "json"
				}).done(function(data) {
					var processedData = eutProcessPostSelectDataForSelect2(data);
					callback(processedData.results); });
			}
		},
	});


});

function eutProcessPostSelectDataForSelect2( ajaxData, page, query ) {

	var items=[];
	var newItem=null;

	for (var thisId in ajaxData) {
		newItem = {
			'id': ajaxData[thisId]['id'],
			'text': ajaxData[thisId]['title']
		};
		items.push(newItem);
	}
	return { results: items };
}