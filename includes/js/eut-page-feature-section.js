jQuery(document).ready(function($) {

	"use strict";

	//Feature Map
	$('.eut-map-item-delete-button').click(function() {
		$(this).parent().remove();
	});

	$('#eut-upload-multi-map-point').click(function() {

		$('#eut-upload-multi-map-point').attr('disabled','disabled').addClass('disabled');
		$('#eut-upload-multi-map-button-spinner').show();


		$.post( anemos_eutf_feature_section_texts.ajaxurl, { action:'anemos_eutf_get_map_point', map_mode:'new' } , function( mediaHtml ) {
			$('#eut-feature-map-container').append(mediaHtml);

			$('.eut-map-item-delete-button.eut-item-new').click(function() {
				$(this).parent().remove();
			}).removeClass('eut-item-new');

			$('.eut-open-map-modal.eut-item-new').bind("click",(function(e){
				e.preventDefault();
				$(this).bindOpenMapModal();
			})).removeClass('eut-item-new');

			$('.eut-remove-simple-media-button.eut-item-new').click(function() {
				$(this).bindRemoveSimpleMedia();
			}).removeClass('eut-item-new');
			$('.eut-upload-simple-media-button.eut-item-new').click(function() {
				$(this).bindUploadSimpleMedia();
			}).removeClass('eut-item-new');

			$('.eut-tabs.eut-item-new .eut-tab-links a').bind("click", (function(e) {
				var currentAttrValue = $(this).attr('href');
				$('.eut-tabs ' + currentAttrValue).show().siblings().hide();
				$(this).parent('li').addClass('active').siblings().removeClass('active');
				e.preventDefault();
			})).removeClass('eut-item-new');

			$('.eut-admin-label-update').off("change").on("change",(function(){
				$(this).bindFieldsAdminLabelUpdate();
			}));

			$('.postbox.eut-item-new .handlediv').on('click', function() {
				var p = $(this).parent('.postbox');

				p.removeClass('eut-item-new');
				p.toggleClass('closed');

			});

			$('#eut-upload-multi-map-point').removeAttr('disabled').removeClass('disabled');
			$('#eut-upload-multi-map-button-spinner').hide();
		});
	});



	$('#eut-page-feature-element').change(function() {

		$('.eut-feature-section-item').hide();
		$('.eut-feature-required').hide();
		$('.eut-feature-options-wrapper').show();

		switch($(this).val())
		{
			case "title":
				$('#eut-feature-section-options').stop( true, true ).fadeIn(500);
				$('#eut-feature-single-tab-content-link').click();
				$('.eut-item-feature-content-settings').stop( true, true ).fadeIn(500);
				$('#eut-feature-single-container').stop( true, true ).fadeIn(500);
			break;
			case "image":
				$('#eut-feature-section-options').stop( true, true ).fadeIn(500);
				$('#eut-feature-single-tab-bg-link').click();
				$('.eut-item-feature-bg-settings').stop( true, true ).fadeIn(500);
				$('.eut-item-feature-content-settings').stop( true, true ).fadeIn(500);
				$('.eut-item-feature-image-settings').stop( true, true ).fadeIn(500);
				$('.eut-item-feature-overlay-settings').stop( true, true ).fadeIn(500);
				$('.eut-item-feature-button-settings').stop( true, true ).fadeIn(500);
				$('#eut-feature-single-container').stop( true, true ).fadeIn(500);

			break;
			 case "video":
				$('#eut-feature-section-options').stop( true, true ).fadeIn(500);
				$('#eut-feature-single-tab-video-link').click();
				$('.eut-item-feature-video-settings').stop( true, true ).fadeIn(500);
				$('.eut-item-feature-bg-settings').stop( true, true ).fadeIn(500);
				$('.eut-item-feature-content-settings').stop( true, true ).fadeIn(500);
				$('.eut-item-feature-overlay-settings').stop( true, true ).fadeIn(500);
				$('.eut-item-feature-button-settings').stop( true, true ).fadeIn(500);
				$('#eut-feature-single-container').stop( true, true ).fadeIn(500);
			break;
			case "slider":
				$('#eut-feature-section-options').stop( true, true ).fadeIn(500);
				$('#eut-feature-section-slider').stop( true, true ).fadeIn(500);
				$('#eut-feature-slider-container').stop( true, true ).fadeIn(500);
			break;
			case "map":
				$('#eut-feature-section-options').stop( true, true ).fadeIn(500);
				$('#eut-feature-section-map').stop( true, true ).fadeIn(500);
				$('#eut-feature-map-container').stop( true, true ).fadeIn(500);
			break;
			case "revslider":
				$('.eut-feature-options-wrapper').hide();
				$('#eut-feature-section-options').stop( true, true ).fadeIn(500);
				$('#eut-feature-single-tab-revslider-link').click();
				$('.eut-item-feature-revslider-settings').stop( true, true ).fadeIn(500);
				$('#eut-feature-single-container').stop( true, true ).fadeIn(500);
			break;
			default:
			break;
		}
	});

	$('#eut-page-feature-size').change(function() {

		if( 'custom' == $(this).val() ) {
			$('#eut-feature-section-height').stop( true, true ).fadeIn(500);
		} else {
			$('#eut-feature-section-height').hide();
		}

	});

	$('.eut-select-color-extra').change(function() {
		if( 'custom' == $(this).val() ) {
			$(this).parents('.eut-field-items-wrapper').find('.eut-wp-colorpicker').show();
		} else {
			$(this).parents('.eut-field-items-wrapper').find('.eut-wp-colorpicker').hide();
		}
	});

	$(window).load(function(){
		$('#eut-page-feature-element').change();
		$('#eut-page-feature-size').change();
		$('.eut-select-color-extra').change();
	});

	$('.wp-color-picker-field').wpColorPicker();


	// TABS METABOXES
	$('.eut-tabs .eut-tab-links a').on('click', function(e)  {
		$(this).bindTabsMetaboxes(e);
	});

	$.fn.bindTabsMetaboxes = function(e){
		var currentAttrValue = $(this).attr('href');

		$('.eut-tabs ' + currentAttrValue).show().siblings().hide();
		$(this).parent('li').addClass('active').siblings().removeClass('active');

		e.preventDefault();
	}

	$('.eut-dependency-field').on("change",(function(){
		$(this).bindFieldsDependency();
	}));

	// FIELDS DEPENDENCY
	$.fn.bindFieldsDependency = function(){

		var groupID = $(this).data( "group");

		$('#' + groupID + " [data-dependency] ").each(function() {
			var dataDependency = $(this).data( "dependency"),
				show = true;

			for (var i = 0; i < dataDependency.length; i++) {

				var depId = dataDependency[i].id,
					depValues = dataDependency[i].values,
					depVal = $('#' + depId ).val();

				if($.inArray( depVal, depValues ) == -1){
					show = false;
				}

			}

			if( show ) {
				$(this).fadeIn(500);
			} else {
				$(this).hide();
			}
		});
    }

	$.fn.initFieldsDependency = function(){

		$(this).each(function() {
			var dataDependency = $(this).data( "dependency"),
				show = true;

			for (var i = 0; i < dataDependency.length; i++) {

				var depId = dataDependency[i].id,
					depValues = dataDependency[i].values,
					depVal = $('#' + depId ).val();

				if($.inArray( depVal, depValues ) == -1){
					show = false;
				}

			}

			if( show ) {
				$(this).fadeIn(500);
			} else {
				$(this).hide();
			}

		});

	}
	$( "[data-dependency]" ).initFieldsDependency();

	$('.eut-admin-label-update').on("change",(function(){
		$(this).bindFieldsAdminLabelUpdate();
	}));

	$.fn.bindFieldsAdminLabelUpdate = function(){
		var itemID = $(this).attr('id') + '_admin_label';
		$('#' + itemID ).html($(this).val());
    }

});