<?php
/*
Template Name: Header and Feature Only
*/
?>
<?php get_header(); ?>

<?php the_post(); ?>

			<!-- SIDE AREA -->
			<?php
				$anemos_eutf_sidearea_data = anemos_eutf_get_sidearea_data();
				anemos_eutf_print_side_area( $anemos_eutf_sidearea_data );
			?>
			<!-- END SIDE AREA -->

			<!-- HIDDEN MENU -->
			<?php anemos_eutf_print_hidden_menu(); ?>
			<!-- END HIDDEN MENU -->

			<?php anemos_eutf_print_search_modal(); ?>
			<?php anemos_eutf_print_form_modals(); ?>
			<?php anemos_eutf_print_language_modal(); ?>
			<?php anemos_eutf_print_login_modal(); ?>
			<?php anemos_eutf_print_social_modal(); ?>

		</div> <!-- end #eut-theme-wrapper -->

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>