<?php
/*
Template Name: Scrolling Full Screen Sections
*/
?>
<?php get_header(); ?>

<?php the_post(); ?>

<?php $responsive_scrolling_page = anemos_eutf_post_meta( '_anemos_eutf_responsive_scrolling', 'yes' ); ?>

			<!-- CONTENT -->
			<div id="eut-content" class="clearfix">
				<div class="eut-content-wrapper">
					<!-- MAIN CONTENT -->
					<div id="eut-main-content">
						<div class="eut-main-content-wrapper clearfix" style="padding: 0;">

							<!-- PAGE CONTENT -->
							<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
								<div id="eut-fullpage" data-device-scrolling="<?php echo esc_attr( $responsive_scrolling_page ); ?>">
									<?php the_content(); ?>
								</div>
							</div>
							<!-- END PAGE CONTENT -->

						</div>
					</div>
					<!-- END MAIN CONTENT -->

				</div>
			</div>
			<!-- END CONTENT -->

			<!-- SIDE AREA -->
			<?php
				$anemos_eutf_sidearea_data = anemos_eutf_get_sidearea_data();
				anemos_eutf_print_side_area( $anemos_eutf_sidearea_data );
			?>
			<!-- END SIDE AREA -->

			<!-- HIDDEN MENU -->
			<?php anemos_eutf_print_hidden_menu(); ?>
			<!-- END HIDDEN MENU -->

			<?php anemos_eutf_print_search_modal(); ?>
			<?php anemos_eutf_print_form_modals(); ?>
			<?php anemos_eutf_print_language_modal(); ?>
			<?php anemos_eutf_print_login_modal(); ?>
			<?php anemos_eutf_print_social_modal(); ?>

		</div> <!-- end #eut-theme-wrapper -->

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>