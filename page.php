<?php get_header(); ?>

<?php the_post(); ?>

<?php anemos_eutf_print_header_title( 'page' ); ?>
<?php anemos_eutf_print_header_breadcrumbs( 'page' ); ?>
<?php anemos_eutf_print_anchor_menu( 'page' ); ?>

<?php
	if ( 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_content' ) ) {
		get_footer();
	} else {
?>
		<!-- CONTENT -->
		<div id="eut-content" class="clearfix <?php echo anemos_eutf_sidebar_class( 'page' ); ?>">
			<div class="eut-content-wrapper">
				<!-- MAIN CONTENT -->
				<div id="eut-main-content">
					<div class="eut-main-content-wrapper clearfix">

						<!-- PAGE CONTENT -->
						<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php the_content(); ?>
						</div>
						<!-- END PAGE CONTENT -->

						<?php if ( anemos_eutf_visibility( 'page_comments_visibility' ) ) { ?>
						<div class="eut-container">
							<?php comments_template(); ?>
						</div>
						<?php } ?>

					</div>
				</div>
				<!-- END MAIN CONTENT -->

				<?php anemos_eutf_set_current_view( 'page' ); ?>
				<?php get_sidebar(); ?>

			</div>
		</div>
		<!-- END CONTENT -->

	<?php get_footer(); ?>

<?php
	}

//Omit closing PHP tag to avoid accidental whitespace output errors.
