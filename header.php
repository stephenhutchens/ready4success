<!doctype html>
<!--[if lt IE 10]>
<html class="ie9 no-js" <?php language_attributes(); ?>>
<![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

	<head>
		<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) { ?>
		<!-- allow pinned sites -->
		<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
		<?php } ?>
		<?php wp_head(); ?>
	</head>

	<?php
		$anemos_eutf_theme_layout = 'eut-' . anemos_eutf_option( 'theme_layout', 'stretched' );
		$anemos_eutf_header_mode = anemos_eutf_option( 'header_mode', 'default' );

		$header_sticky_type = anemos_eutf_option( 'header_sticky_type', 'simple' );

		$anemos_eutf_header_fullwidth = anemos_eutf_option( 'header_fullwidth', '1' );

		$anemos_eutf_header_data = anemos_eutf_get_feature_header_data();

		$anemos_eutf_header_style = $anemos_eutf_header_data['header_style'];
		$anemos_eutf_header_overlapping = $anemos_eutf_header_data['data_overlap'];
		$anemos_eutf_responsive_header_overlapping = anemos_eutf_option( 'responsive_header_overlapping', 'no' );
		$anemos_eutf_header_position = $anemos_eutf_header_data['data_header_position'];

		$anemos_eutf_menu_open_type = anemos_eutf_option( 'header_menu_open_type', 'toggle' );

		//Sticky Header
		$anemos_eutf_header_sticky_type = anemos_eutf_option( 'header_sticky_type', 'simple' );
		$anemos_eutf_header_sticky_height = anemos_eutf_option( 'header_sticky_shrink_height', '0' );
		if( 'simple' == $anemos_eutf_header_sticky_type && 'default' == $anemos_eutf_header_mode) {
			$anemos_eutf_header_sticky_height = anemos_eutf_option( 'header_height' );
		}
		if( 'simple' == $anemos_eutf_header_sticky_type && 'logo-top' == $anemos_eutf_header_mode) {
			$anemos_eutf_header_sticky_height = anemos_eutf_option( 'header_bottom_height' );
		}

		$anemos_eutf_disable_sticky = '';
		if ( is_singular() ) {
			$anemos_eutf_disable_sticky = anemos_eutf_post_meta( '_anemos_eutf_disable_sticky', $anemos_eutf_disable_sticky );
			if ( 'yes' == $anemos_eutf_disable_sticky  ) {
				$anemos_eutf_header_sticky_type = 'none';
			} else {
				$anemos_eutf_header_sticky_type = anemos_eutf_post_meta( '_anemos_eutf_sticky_header_type', $anemos_eutf_header_sticky_type );
			}
		} else if ( anemos_eutf_is_woo_shop() ) {
			$anemos_eutf_disable_sticky = anemos_eutf_post_meta_shop( '_anemos_eutf_disable_sticky', $anemos_eutf_disable_sticky );
			if ( 'yes' == $anemos_eutf_disable_sticky  ) {
				$anemos_eutf_header_sticky_type = 'none';
			} else {
				$anemos_eutf_header_sticky_type = anemos_eutf_post_meta_shop( '_anemos_eutf_sticky_header_type', $anemos_eutf_header_sticky_type );
			}
		}
		$anemos_eutf_header_sticky_type = anemos_eutf_visibility( 'header_sticky_enabled' ) ? $anemos_eutf_header_sticky_type : 'none';

		if ( 'default' == $anemos_eutf_header_mode ) {
			$anemos_eutf_logo_align = 'left';
			$anemos_eutf_menu_align = anemos_eutf_option( 'menu_align', 'right' );
			$anemos_eutf_menu_type = anemos_eutf_option( 'menu_type', 'classic' );
			if ( is_singular() ) {
				$anemos_eutf_menu_type = anemos_eutf_post_meta( '_anemos_eutf_menu_type', $anemos_eutf_menu_type );
			} else if ( anemos_eutf_is_woo_shop() ) {
				$anemos_eutf_menu_type = anemos_eutf_post_meta_shop( '_anemos_eutf_menu_type', $anemos_eutf_menu_type );
			}
		} else if ( 'logo-top' == $anemos_eutf_header_mode ) {
			$anemos_eutf_logo_align = anemos_eutf_option( 'header_top_logo_align', 'center' );
			$anemos_eutf_menu_align = anemos_eutf_option( 'header_top_menu_align', 'center' );
			$anemos_eutf_menu_type = anemos_eutf_option( 'header_top_logo_menu_type', 'classic' );
			if ( is_singular() ) {
				$anemos_eutf_menu_type = anemos_eutf_post_meta( '_anemos_eutf_menu_type', $anemos_eutf_menu_type );
			} else if ( anemos_eutf_is_woo_shop() ) {
				$anemos_eutf_menu_type = anemos_eutf_post_meta_shop( '_anemos_eutf_menu_type', $anemos_eutf_menu_type );
			}
		} else {
			$anemos_eutf_header_fullwidth = 0;
			$anemos_eutf_header_overlapping = 'no';
			$anemos_eutf_header_sticky_type = 'none';
			$anemos_eutf_menu_align = anemos_eutf_option( 'header_side_menu_align', 'left' );
			$anemos_eutf_logo_align = anemos_eutf_option( 'header_side_logo_align', 'left' );
		}
		//Header Classes
		$anemos_eutf_header_classes = array();
		if ( 1 == $anemos_eutf_header_fullwidth ) {
			$anemos_eutf_header_classes[] = 'eut-fullwidth';
		}
		if ( 'yes' == $anemos_eutf_header_overlapping ) {
			$anemos_eutf_header_classes[] = 'eut-overlapping';
		}
		if ( 'yes' == $anemos_eutf_responsive_header_overlapping ) {
			$anemos_eutf_header_classes[] = 'eut-responsive-overlapping';
		}
		$anemos_eutf_header_class_string = implode( ' ', $anemos_eutf_header_classes );


		//Main Header Classes
		$anemos_eutf_main_header_classes = array();
		$anemos_eutf_main_header_classes[] = 'eut-header-' . $anemos_eutf_header_mode;
		if ( 'side' == $anemos_eutf_header_mode ) {
			$anemos_eutf_main_header_classes[] = 'eut-' . $anemos_eutf_menu_open_type . '-menu';
		} else {
			$anemos_eutf_main_header_classes[] = 'eut-' . $anemos_eutf_header_style;
		}
		if ( 'side' != $anemos_eutf_header_mode || 'none' != $anemos_eutf_header_sticky_type ) {
			$anemos_eutf_main_header_classes[] = 'eut-' . $anemos_eutf_header_sticky_type . '-sticky';
		}
		$anemos_eutf_header_main_class_string = implode( ' ', $anemos_eutf_main_header_classes );

		$anemos_eutf_menu_arrows = anemos_eutf_option( 'submenu_pointer', 'none' );

		// Main Menu Classes
		$anemos_eutf_main_menu_classes = array();
		if ( 'side' != $anemos_eutf_header_mode ) {
			$anemos_eutf_main_menu_classes[] = 'eut-horizontal-menu';
			$anemos_eutf_main_menu_classes[] = 'eut-position-' . $anemos_eutf_menu_align;
			if( 'none' != $anemos_eutf_menu_arrows ) {
				$anemos_eutf_main_menu_classes[] = 'eut-' . $anemos_eutf_menu_arrows;
			}
			if ( 'hidden' != $anemos_eutf_menu_type ){
				$anemos_eutf_main_menu_classes[] = 'eut-menu-type-' . $anemos_eutf_menu_type;
			}
		} else {
			$anemos_eutf_main_menu_classes[] = 'eut-vertical-menu';
			$anemos_eutf_main_menu_classes[] = 'eut-align-' . $anemos_eutf_menu_align;
		}
		$anemos_eutf_main_menu_classes[] = 'eut-main-menu';
		$anemos_eutf_main_menu_class_string = implode( ' ', $anemos_eutf_main_menu_classes );

		$anemos_eutf_main_menu = anemos_eutf_get_header_nav();
		$anemos_eutf_sidearea_data = anemos_eutf_get_sidearea_data();


		$anemos_eutf_header_sticky_devices_enabled = anemos_eutf_option( 'header_sticky_devices_enabled' );
		$anemos_eutf_header_sticky_devices = 'no';
		if ( '1' == $anemos_eutf_header_sticky_devices_enabled ) {
			$anemos_eutf_header_sticky_devices = 'yes';
		}

	?>

	<body id="eut-body" <?php body_class(); ?>>
		<?php do_action( 'anemos_eutf_body_top' ); ?>
		<?php if ( anemos_eutf_check_theme_loader_visibility() ) { ?>
		<!-- LOADER -->
		<div id="eut-loader-overflow">
			<div class="eut-spinner"></div>
		</div>
		<?php } ?>

		<?php
			// Theme Wrapper Classes
			$anemos_eutf_theme_wrapper_classes = array();
			if ( 'side' == $anemos_eutf_header_mode ) {
				$anemos_eutf_theme_wrapper_classes[] = 'eut-header-side';
			}
			if( 'below' == $anemos_eutf_header_position && 'yes' == $anemos_eutf_header_overlapping ) {
				$anemos_eutf_theme_wrapper_classes[] = 'eut-feature-below';
			}
			$anemos_eutf_theme_wrapper_class_string = implode( ' ', $anemos_eutf_theme_wrapper_classes );
		?>

		<!-- Theme Wrapper -->
		<div id="eut-theme-wrapper" class="<?php echo esc_attr( $anemos_eutf_theme_wrapper_class_string ); ?>">

			<?php
				//Top Bar
				if ( !is_page_template( 'page-templates/template-full-page.php' ) ) {
					anemos_eutf_print_header_top_bar();
				}
				//FEATURE Header Below
				if( 'below' == $anemos_eutf_header_position ) {
					anemos_eutf_print_header_feature();
				}
			?>

			<!-- HEADER -->
			<header id="eut-header" class="<?php echo esc_attr( $anemos_eutf_header_class_string ); ?>" data-sticky="<?php echo esc_attr( $anemos_eutf_header_sticky_type ); ?>" data-sticky-height="<?php echo esc_attr( $anemos_eutf_header_sticky_height ); ?>" data-devices-sticky="<?php echo esc_attr( $anemos_eutf_header_sticky_devices ); ?>">
				<div class="eut-wrapper clearfix">

					<!-- Header -->
					<div id="eut-main-header" class="<?php echo esc_attr( $anemos_eutf_header_main_class_string ); ?>">
					<?php
						if ( 'side' == $anemos_eutf_header_mode ) {
					?>
						<div class="eut-main-header-wrapper clearfix">
							<div class="eut-content">
								<?php anemos_eutf_print_logo( 'side', $anemos_eutf_logo_align ); ?>
								<?php if ( $anemos_eutf_main_menu != 'disabled' ) { ?>
								<!-- Main Menu -->
								<nav id="eut-main-menu" class="<?php echo esc_attr( $anemos_eutf_main_menu_class_string ); ?>">
									<div class="eut-wrapper">
										<?php anemos_eutf_header_nav( $anemos_eutf_main_menu ); ?>
									</div>
								</nav>
								<!-- End Main Menu -->
								<?php } ?>
							</div>
						</div>
						<div class="eut-header-elements-wrapper eut-align-<?php echo esc_attr( $anemos_eutf_menu_align); ?>">
							<?php anemos_eutf_print_header_elements( $anemos_eutf_sidearea_data ); ?>
						</div>
						<?php anemos_eutf_print_side_header_bg_image(); ?>
					<?php
						} else if ( 'logo-top' == $anemos_eutf_header_mode ) {
						//Log on Top Header
					?>
						<div id="eut-top-header">
							<div class="eut-wrapper clearfix">
								<div class="eut-container">
									<?php anemos_eutf_print_logo( 'logo-top', $anemos_eutf_logo_align ); ?>
								</div>
							</div>
						</div>
						<div id="eut-bottom-header">
							<div class="eut-wrapper clearfix">
								<div class="eut-container">
									<div class="eut-header-elements-wrapper eut-position-right">
								<?php
									if ( 'hidden' == $anemos_eutf_menu_type && 'disabled' != $anemos_eutf_main_menu ) {
										anemos_eutf_print_header_hiddenarea_button();
									}
									anemos_eutf_print_header_elements();
									anemos_eutf_print_header_sidearea_button( $anemos_eutf_sidearea_data );
								?>
									</div>
								<?php
									if ( 'hidden' != $anemos_eutf_menu_type && $anemos_eutf_main_menu != 'disabled' ) {
								?>
										<!-- Main Menu -->
										<nav id="eut-main-menu" class="<?php echo esc_attr( $anemos_eutf_main_menu_class_string ); ?>">
											<div class="eut-wrapper">
												<?php anemos_eutf_header_nav( $anemos_eutf_main_menu ); ?>
											</div>
										</nav>
										<!-- End Main Menu -->
								<?php
									}
								?>
								</div>
							</div>
						</div>
					<?php
						} else {
						//Default Header
					?>
						<div class="eut-wrapper clearfix">
							<div class="eut-container">
								<?php anemos_eutf_print_logo( 'default', $anemos_eutf_logo_align ); ?>
								<div class="eut-header-elements-wrapper eut-position-right">
							<?php
								if ( 'hidden' == $anemos_eutf_menu_type && 'disabled' != $anemos_eutf_main_menu ) {
									anemos_eutf_print_header_hiddenarea_button();
								}
								anemos_eutf_print_header_elements();
								anemos_eutf_print_header_sidearea_button( $anemos_eutf_sidearea_data );
							?>
								</div>
							<?php
								if ( 'hidden' != $anemos_eutf_menu_type && $anemos_eutf_main_menu != 'disabled' ) {
							?>
									<!-- Main Menu -->
									<nav id="eut-main-menu" class="<?php echo esc_attr( $anemos_eutf_main_menu_class_string ); ?>">
										<div class="eut-wrapper">
											<?php anemos_eutf_header_nav( $anemos_eutf_main_menu ); ?>
										</div>
									</nav>
									<!-- End Main Menu -->
							<?php
								}
							?>
							</div>
						</div>
					<?php
						}
					?>

					</div>
					<!-- End Header -->

					<!-- Responsive Header -->
					<div id="eut-responsive-header">
						<div class="eut-wrapper clearfix">
							<div class="eut-container">
							<?php anemos_eutf_print_logo( 'responsive' , 'left' ); ?>
								<div class="eut-header-elements-wrapper eut-position-right">
								<!-- Hidden Menu & Side Area Button -->
								<?php
									if ( 'disabled' != $anemos_eutf_main_menu || anemos_eutf_check_header_elements_visibility_any() ){
										anemos_eutf_print_header_hiddenarea_button();
									}
								?>
								<?php anemos_eutf_print_login_responsive_button(); ?>
								<?php
									$anemos_eutf_responsive_sidearea_button = anemos_eutf_option( 'responsive_sidearea_button_visibility', 'yes');
									if ( 'yes' == $anemos_eutf_responsive_sidearea_button ) {
										anemos_eutf_print_header_sidearea_button( $anemos_eutf_sidearea_data );
									}
								?>
								<!-- End Hidden Menu & Side Area Button -->
								</div>
							</div>
						</div>
					</div>
					<!-- End Responsive Header -->
				</div>

				<!-- Anemos Sticky Header -->
			<?php
				if ( 'side' != $anemos_eutf_header_mode && 'anemos' == $anemos_eutf_header_sticky_type ) {
				$header_data = anemos_eutf_header_title();
				$header_title = isset( $header_data['title'] ) ? $header_data['title'] : '';
			?>
				<div id="eut-anemos-sticky-header" class="eut-fullwidth">
					<div class="eut-wrapper clearfix">
						<div class="eut-container">

							<?php anemos_eutf_print_logo( 'anemos-sticky' , 'left' ); ?>

							<div class="eut-page-title-wrapper eut-position-left">
								<span class="eut-title"><?php echo esc_html( $header_title ); ?></span>
							</div>

							<div class="eut-header-elements-wrapper eut-position-right">
								<?php
									anemos_eutf_print_header_elements();
									anemos_eutf_print_header_sidearea_button( $anemos_eutf_sidearea_data );
								 ?>
							</div>

						<?php
							if ( $anemos_eutf_main_menu != 'disabled' ) {
						?>
							<div class="eut-hidden-menu-btn eut-position-right">
								<div class="eut-header-element">
									<a href="#" class="eut-toggle-anemos-sticky-menu">
										<span class="eut-item">
											<i class="eut-icon-menu"></i>
											<i class="eut-icon-close"></i>
										</span>
									</a>
								</div>
							</div>
						<?php
							}
						?>

						<?php anemos_eutf_print_header_back_top(); ?>

						<?php
							if ( 'hidden' != $anemos_eutf_menu_type && $anemos_eutf_main_menu != 'disabled' ) {
						?>
							<!-- Main Menu -->
							<nav id="eut-anemos-sticky-menu" class="<?php echo esc_attr( $anemos_eutf_main_menu_class_string ); ?>">
								<div class="eut-wrapper">
									<?php anemos_eutf_header_nav( $anemos_eutf_main_menu ); ?>
								</div>
							</nav>
							<!-- End Main Menu -->
						<?php
							}
						?>

						</div>
					</div>
				</div>
			<?php
				}
			?>
				<!-- End Anemos Sticky Header -->

			</header>
			<!-- END HEADER -->

			<?php
				//FEATURE Header Above
				if( 'above' == $anemos_eutf_header_position ) {
					anemos_eutf_print_header_feature();
				}

//Omit closing PHP tag to avoid accidental whitespace output errors.
