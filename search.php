<?php get_header(); ?>


<?php
	anemos_eutf_print_header_title('search_page');

	$search_style = anemos_eutf_option( 'search_page_style', 'masonry' );
	if ( 'grid' == $search_style) {
		$search_style = 'fitRows';
	}
	$columns = anemos_eutf_option( 'search_page_columns', '3' );
	$columns_tablet_large_screen  = anemos_eutf_option( 'search_page_columns_large_screen', '3' );
	$columns_tablet_landscape  = anemos_eutf_option( 'search_page_columns_tablet_landscape', '3' );
	$columns_tablet_portrait  = anemos_eutf_option( 'search_page_columns_tablet_portrait', '2' );
	$columns_mobile  = anemos_eutf_option( 'search_page_columns_mobile', '1' );
	$search_mode = anemos_eutf_option( 'search_page_mode', 'shadow-mode' );
	$gutter_size = 40;

	$search_extra_classes = '';
	if ( 'shadow-mode' == $search_mode ) {
		$search_extra_classes .= ' eut-with-shadow';
	}
?>

<!-- CONTENT -->
<div id="eut-content" class="clearfix">
	<div class="eut-content-wrapper">
		<!-- MAIN CONTENT -->
		<div id="eut-main-content">
			<div class="eut-main-content-wrapper clearfix">

				<div class="eut-section" style="margin-bottom: 0px;">

					<div class="eut-container">
						<!-- ROW -->
						<div class="eut-row">

							<!-- COLUMN 1 -->
							<div class="wpb_column eut-column-1">
							<?php
								if ( have_posts() ) :
							?>
								<div class="eut-column-wrapper">
									<div class="eut-blog eut-blog-columns eut-isotope eut-with-gap<?php echo esc_attr( $search_extra_classes ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" data-columns-large-screen="<?php echo esc_attr( $columns_tablet_large_screen ); ?>" data-columns-tablet-landscape="<?php echo esc_attr( $columns_tablet_landscape ); ?>" data-columns-tablet-portrait="<?php echo esc_attr( $columns_tablet_portrait ); ?>" data-columns-mobile="<?php echo esc_attr( $columns_mobile ); ?>" data-layout="<?php echo esc_attr( $search_style ); ?>"  data-gutter-size="<?php echo esc_attr( $gutter_size ); ?>" data-spinner="no">
										<?php
											$anemos_eutf_post_items = $anemos_eutf_page_items = $anemos_eutf_portfolio_items = $anemos_eutf_other_post_items = 0;
											$anemos_eutf_has_post_items = $anemos_eutf_has_page_items = $anemos_eutf_has_portfolio_items = 0;

											while ( have_posts() ) : the_post();
												$post_type = get_post_type();
												switch( $post_type ) {
													case 'post':
														 $anemos_eutf_post_items++;
														 $anemos_eutf_has_post_items = 1;
													break;
													case 'page':
														 $anemos_eutf_page_items++;
														 $anemos_eutf_has_page_items = 1;
													break;
													case 'portfolio':
														 $anemos_eutf_portfolio_items++;
														 $anemos_eutf_has_portfolio_items = 1;
													break;
													default:
														$anemos_eutf_other_post_items++;
													break;
												}
											endwhile;
											$anemos_eutf_item_types = $anemos_eutf_has_post_items + $anemos_eutf_has_page_items + $anemos_eutf_has_portfolio_items;

											if ( $anemos_eutf_item_types > 1 ) {
										?>
										<div class="eut-filter eut-link-text eut-list-divider eut-align-left">
											<ul>
												<li data-filter="*" class="selected"><?php _e( "All", 'anemos' ); ?></li>
												<?php if ( $anemos_eutf_has_post_items ) { ?>
												<li data-filter=".post"><?php _e( "Post", 'anemos' ); ?></li>
												<?php } ?>
												<?php if ( $anemos_eutf_has_page_items ) { ?>
												<li data-filter=".page"><?php _e( "Page", 'anemos' ); ?></li>
												<?php } ?>
												<?php if ( $anemos_eutf_has_portfolio_items ) { ?>
												<li data-filter=".portfolio"><?php _e( "Portfolio", 'anemos' ); ?></li>
												<?php } ?>
											</ul>
										</div>
										<?php
											}

										?>
													<div class="eut-isotope-container">
												<?php
													// Start the Loop.
													while ( have_posts() ) : the_post();
														//Get post template
														get_template_part( 'templates/search', 'masonry' );

													endwhile;
												?>
													</div>
										<?php
												// Previous/next post navigation.
												anemos_eutf_paginate_links();
										?>
									</div>
								</div>
								<?php
									else :
										// If no content, include the "No posts found" template.
										get_template_part( 'content', 'none' );
									endif;
								?>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
		<!-- End Content -->
	</div>
</div>
<?php get_footer();

//Omit closing PHP tag to avoid accidental whitespace output errors.
