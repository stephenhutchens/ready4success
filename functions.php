<?php

/*
*	Main theme functions and definitions
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/

/**
 * Theme Definitions
 * Please leave these settings unchanged
 */

define( 'ANEMOS_EUTF_THEME_MAJOR_VERSION', 1 );
define( 'ANEMOS_EUTF_THEME_MINOR_VERSION', 3 );
define( 'ANEMOS_EUTF_THEME_HOTFIX_VERSION', 9 );
define( 'ANEMOS_EUTF_THEME_REDUX_CUSTOM_PANEL', false);

/**
 * Set up the content width value based on the theme's design.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1080;
}

/**
 * Theme textdomain - must be loaded before redux
 */
load_theme_textdomain( 'anemos', get_template_directory() . '/languages' );

/**
 * Include Global helper files
 */
require_once get_template_directory() . '/includes/eut-global.php';
require_once get_template_directory() . '/includes/eut-meta-tags.php';
require_once get_template_directory() . '/includes/eut-woocommerce-functions.php';

/**
 * Register Plugins Libraries
 */
if ( is_admin() ) {
	require_once get_template_directory() . '/includes/plugins/tgm-plugin-activation/register-plugins.php';
	require_once get_template_directory() . '/includes/plugins/envato-wordpress-toolkit-library/class-envato-wordpress-theme-upgrader.php';
}

/**
 * ReduxFramework
 */
require_once get_template_directory() . '/includes/admin/eut-redux-extension-loader.php';

if ( !class_exists( 'ReduxFramework' ) && file_exists( get_template_directory() . '/includes/framework/framework.php' ) ) {
    require_once get_template_directory() . '/includes/framework/framework.php';
}

if ( !isset( $redux_demo ) ) {
	require_once get_template_directory() . '/includes/admin/eut-redux-framework-config.php';
}

function anemos_eutf_remove_redux_demo_link() {
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
    }
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
    }
}
add_action('init', 'anemos_eutf_remove_redux_demo_link');

/**
 * Custom Nav Menus
 */
require_once get_template_directory() . '/includes/custom-menu/eut-custom-nav-menu.php';

/**
 * Visual Composer Extentions
 */
if ( class_exists( 'WPBakeryVisualComposerAbstract' ) ) {

	function anemos_eutf_add_vc_extentions() {
		require_once get_template_directory() . '/vc_extend/eut-shortcodes-vc-helper.php';
		require_once get_template_directory() . '/vc_extend/eut-shortcodes-vc-remove.php';
		require_once get_template_directory() . '/vc_extend/eut-shortcodes-vc-add.php';
	}
	add_action( 'init', 'anemos_eutf_add_vc_extentions', 5 );

}

/**
 * Include admin helper files
 */
require_once get_template_directory() . '/includes/admin/eut-admin-functions.php';
require_once get_template_directory() . '/includes/admin/eut-admin-custom-sidebars.php';
require_once get_template_directory() . '/includes/admin/eut-admin-option-functions.php';
require_once get_template_directory() . '/includes/admin/eut-admin-feature-functions.php';

require_once get_template_directory() . '/includes/admin/eut-update-functions.php';
require_once get_template_directory() . '/includes/admin/eut-meta-functions.php';
require_once get_template_directory() . '/includes/admin/eut-category-meta.php';
require_once get_template_directory() . '/includes/admin/eut-post-meta.php';

require_once get_template_directory() . '/includes/admin/eut-testimonial-meta.php';
require_once get_template_directory() . '/includes/eut-wp-gallery.php';

/**
 * Include Dynamic css
 */
require_once get_template_directory() . '/includes/eut-dynamic-css-loader.php';

/**
 * Include helper files
 */
require_once get_template_directory() . '/includes/eut-breadcrumbs.php';
require_once get_template_directory() . '/includes/eut-excerpt.php';
require_once get_template_directory() . '/includes/eut-vce-functions.php';
require_once get_template_directory() . '/includes/eut-header-functions.php';
require_once get_template_directory() . '/includes/eut-feature-functions.php';
require_once get_template_directory() . '/includes/eut-layout-functions.php';
require_once get_template_directory() . '/includes/eut-blog-functions.php';
require_once get_template_directory() . '/includes/eut-media-functions.php';
require_once get_template_directory() . '/includes/eut-footer-functions.php';
require_once get_template_directory() . '/includes/eut-login-functions.php';

/**
 * Include Theme Widgets
 */
require_once get_template_directory() . '/includes/widgets/eut-widget-social.php';
require_once get_template_directory() . '/includes/widgets/eut-widget-latest-posts.php';
require_once get_template_directory() . '/includes/widgets/eut-widget-promote-post.php';
require_once get_template_directory() . '/includes/widgets/eut-widget-latest-comments.php';
require_once get_template_directory() . '/includes/widgets/eut-widget-contact-info.php';
require_once get_template_directory() . '/includes/widgets/eut-widget-instagram-feed.php';
require_once get_template_directory() . '/includes/widgets/eut-widget-image-banner.php';
require_once get_template_directory() . '/includes/widgets/eut-widget-sticky.php';

add_action( 'after_switch_theme', 'anemos_eutf_theme_activate' );
add_action( 'after_setup_theme', 'anemos_eutf_theme_setup' );
add_action( 'widgets_init', 'anemos_eutf_register_sidebars' );

/**
 * Theme activation function
 * Used whe activating the theme
 */
function anemos_eutf_theme_activate() {

	$eut_version = array (
		"major" => ANEMOS_EUTF_THEME_MAJOR_VERSION,
		"minor" => ANEMOS_EUTF_THEME_MINOR_VERSION,
		"hotfix" => ANEMOS_EUTF_THEME_HOTFIX_VERSION,
	);
	update_option( 'anemos_eutf_theme_version', $eut_version );

	flush_rewrite_rules();
}

/**
 * Theme setup function
 * Theme support
 */
function anemos_eutf_theme_setup() {

	add_theme_support( 'menus' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array( 'gallery', 'link', 'quote', 'video', 'audio' ) );
	add_theme_support( 'title-tag' );

	$size_large_landscape_wide = anemos_eutf_option( 'size_large_landscape_wide', array( 'width'   => '1170', 'height'  => '658') );
	$size_small_square = anemos_eutf_option( 'size_small_square', array( 'width'   => '600', 'height'  => '600') );
	$size_small_landscape = anemos_eutf_option( 'size_small_landscape', array( 'width'   => '800', 'height'  => '600') );
	$size_small_landscape_wide = anemos_eutf_option( 'size_small_landscape_wide', array( 'width'   => '800', 'height'  => '450') );
	$size_small_portrait = anemos_eutf_option( 'size_small_portrait', array( 'width'   => '600', 'height'  => '800') );
	$size_medium_portrait = anemos_eutf_option( 'size_medium_portrait', array( 'width'   => '560', 'height'  => '1120') );
	$size_medium_square = anemos_eutf_option( 'size_medium_square', array( 'width'   => '1120', 'height'  => '1120') );
	$size_fullscreen = anemos_eutf_option( 'size_fullscreen', array( 'width'   => '1920', 'height'  => '1920') );

	add_image_size( 'anemos-eutf-large-rect-horizontal', $size_large_landscape_wide['width'], $size_large_landscape_wide['height'], true );
	add_image_size( 'anemos-eutf-small-square', $size_small_square['width'], $size_small_square['height'], true );
	add_image_size( 'anemos-eutf-small-rect-horizontal', $size_small_landscape['width'], $size_small_landscape['height'], true );
	add_image_size( 'anemos-eutf-small-rect-horizontal-wide', $size_small_landscape_wide['width'], $size_small_landscape_wide['height'], true );
	add_image_size( 'anemos-eutf-small-rect-vertical', $size_small_portrait['width'], $size_small_portrait['height'], true );
	add_image_size( 'anemos-eutf-medium-rect-vertical', $size_medium_portrait['width'], $size_medium_portrait['height'], true );
	add_image_size( 'anemos-eutf-medium-square', $size_medium_square['width'], $size_medium_square['height'], true );
	add_image_size( 'anemos-eutf-fullscreen', $size_fullscreen['width'], $size_fullscreen['height'], false );

	register_nav_menus(
		array(
			'anemos_header_nav' => esc_html__( 'Header Menu', 'anemos' ),
			'anemos_top_left_nav' => esc_html__( 'Top Left Menu', 'anemos' ),
			'anemos_top_right_nav' => esc_html__( 'Top Right Menu', 'anemos' ),
			'anemos_footer_nav' => esc_html__( 'Footer Menu', 'anemos' ),
		)
	);

}

function anemos_eutf_add_excerpt_support_for_pages() {
    add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'anemos_eutf_add_excerpt_support_for_pages' );

/**
 * Navigation Menus
 */
function anemos_eutf_get_header_nav() {

	$anemos_eutf_main_menu = '';

	if ( 'default' == anemos_eutf_option( 'menu_header_integration', 'default' ) ) {

		if ( is_singular() ) {
			if ( 'yes' == anemos_eutf_post_meta( '_anemos_eutf_disable_menu' ) ) {
				return 'disabled';
			} else {
				$anemos_eutf_main_menu	= anemos_eutf_post_meta( '_anemos_eutf_main_navigation_menu' );
			}
		} else if ( anemos_eutf_is_woo_shop() ) {
			if ( 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_disable_menu' ) ) {
				return 'disabled';
			} else {
				$anemos_eutf_main_menu	= anemos_eutf_post_meta_shop( '_anemos_eutf_main_navigation_menu' );
			}
		}
	} else {
		$anemos_eutf_main_menu = 'disabled';
	}

	return $anemos_eutf_main_menu;
}

function anemos_eutf_header_nav( $anemos_eutf_main_menu = '') {

	if ( empty( $anemos_eutf_main_menu ) ) {
		wp_nav_menu(
			array(
				'menu_class' => 'eut-menu', /* menu class */
				'theme_location' => 'anemos_header_nav', /* where in the theme it's assigned */
				'container' => false,
				'fallback_cb' => 'anemos_eutf_fallback_menu',
				'link_before' => '<span class="eut-item">',
				'link_after' => '</span>',
				'walker' => new Anemos_Eutf_Main_Navigation_Walker(),
			)
		);
	} else {
		//Custom Alternative Menu
		wp_nav_menu(
			array(
				'menu_class' => 'eut-menu', /* menu class */
				'menu' => $anemos_eutf_main_menu, /* menu name */
				'container' => false,
				'fallback_cb' => 'anemos_eutf_fallback_menu',
				'link_before' => '<span class="eut-item">',
				'link_after' => '</span>',
				'walker' => new Anemos_Eutf_Main_Navigation_Walker(),
			)
		);
	}
}

/**
 * Main Navigation FallBack Menu
 */
if ( ! function_exists( 'anemos_eutf_fallback_menu' ) ) {
	function anemos_eutf_fallback_menu(){

		if( current_user_can( 'administrator' ) ) {
			echo '<span class="eut-no-assigned-menu eut-small-text">';
			echo esc_html__( 'Header Menu is not assigned!', 'anemos'  ) . " " .
			"<a href='" . esc_url( admin_url() ) . "nav-menus.php?action=locations' target='_blank'>" . esc_html__( "Manage Locations", 'anemos' ) . "</a>";
			echo '</span>';
		}
	}
}

function anemos_eutf_footer_nav() {

	wp_nav_menu(
		array(
			'theme_location' => 'anemos_footer_nav',
			'container' => false, /* no container */
			'depth' => '1',
			'fallback_cb' => false,
			'walker' => new Anemos_Eutf_Simple_Navigation_Walker(),
		)
	);

}

function anemos_eutf_top_left_nav() {

	wp_nav_menu(
		array(
			'theme_location' => 'anemos_top_left_nav',
			'container' => false, /* no container */
			'depth' => '2',
			'fallback_cb' => false,
			'walker' => new Anemos_Eutf_Simple_Navigation_Walker(),
		)
	);

}

function anemos_eutf_top_right_nav() {

	wp_nav_menu(
		array(
			'theme_location' => 'anemos_top_right_nav',
			'container' => false, /* no container */
			'depth' => '2',
			'fallback_cb' => false,
			'walker' => new Anemos_Eutf_Simple_Navigation_Walker(),
		)
	);

}

/**
 * Sidebars & Widgetized Areas
 */
function anemos_eutf_register_sidebars() {

	$sidebar_heading_tag = anemos_eutf_option( 'sidebar_heading_tag', 'div' );
	$footer_heading_tag = anemos_eutf_option( 'footer_heading_tag', 'div' );

	register_sidebar( array(
		'id' => 'eut-default-sidebar',
		'name' => esc_html__( 'Main Sidebar', 'anemos' ),
		'description' => esc_html__( 'Main Sidebar Widget Area', 'anemos' ),
		'before_widget' => '<div id="%1$s" class="eut-widget widget eut-margin-bottom-2x %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<' . tag_escape( $sidebar_heading_tag ) . ' class="eut-widget-title">',
		'after_title' => '</' . tag_escape( $sidebar_heading_tag ) . '>',
	));

	register_sidebar( array(
		'id' => 'eut-footer-1-sidebar',
		'name' => esc_html__( 'Footer 1', 'anemos' ),
		'description' => esc_html__( 'Footer 1 Widget Area', 'anemos' ),
		'before_widget' => '<div id="%1$s" class="eut-widget widget eut-margin-bottom-2x %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<' . tag_escape( $footer_heading_tag ) . ' class="eut-widget-title">',
		'after_title' => '</' . tag_escape( $footer_heading_tag ) . '>',
	));
	register_sidebar( array(
		'id' => 'eut-footer-2-sidebar',
		'name' => esc_html__( 'Footer 2', 'anemos' ),
		'description' => esc_html__( 'Footer 2 Widget Area', 'anemos' ),
		'before_widget' => '<div id="%1$s" class="eut-widget widget eut-margin-bottom-2x %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<' . tag_escape( $footer_heading_tag ) . ' class="eut-widget-title">',
		'after_title' => '</' . tag_escape( $footer_heading_tag ) . '>',
	));
	register_sidebar( array(
		'id' => 'eut-footer-3-sidebar',
		'name' => esc_html__( 'Footer 3', 'anemos' ),
		'description' => esc_html__( 'Footer 3 Widget Area', 'anemos' ),
		'before_widget' => '<div id="%1$s" class="eut-widget widget eut-margin-bottom-2x %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<' . tag_escape( $footer_heading_tag ) . ' class="eut-widget-title">',
		'after_title' => '</' . tag_escape( $footer_heading_tag ) . '>',
	));
	register_sidebar( array(
		'id' => 'eut-footer-4-sidebar',
		'name' => esc_html__( 'Footer 4', 'anemos' ),
		'description' => esc_html__( 'Footer 4 Widget Area', 'anemos' ),
		'before_widget' => '<div id="%1$s" class="eut-widget widget eut-margin-bottom-2x %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<' . tag_escape( $footer_heading_tag ) . ' class="eut-widget-title">',
		'after_title' => '</' . tag_escape( $footer_heading_tag ) . '>',
	));

	$anemos_eutf_custom_sidebars = get_option( '_anemos_eutf_custom_sidebars' );
	if ( ! empty( $anemos_eutf_custom_sidebars ) ) {
		foreach ( $anemos_eutf_custom_sidebars as $anemos_eutf_custom_sidebar ) {
			register_sidebar( array(
				'id' => $anemos_eutf_custom_sidebar['id'],
				'name' => esc_html__( 'Custom Sidebar', 'anemos' ) . ': ' . esc_html( $anemos_eutf_custom_sidebar['name'] ),
				'description' => '',
				'before_widget' => '<div id="%1$s" class="eut-widget widget eut-margin-bottom-2x %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<' . tag_escape( $sidebar_heading_tag ) . ' class="eut-widget-title">',
				'after_title' => '</' . tag_escape( $sidebar_heading_tag ) . '>',
			));
		}
	}

}

/**
 * Custom Search Form
 */
 function anemos_eutf_modal_wpsearch( $form ) {
	$form = '';
	$form .= '<form class="eut-search eut-search-modal" method="get" action="' . esc_url( home_url( '/' ) ) . '" >';
	$form .= '  <button type="submit" class="eut-search-btn eut-custom-btn eut-bg-primary-1"><i class="eut-icon-search"></i></button>';
	$form .= '  <input type="text" class="eut-search-textfield eut-h1" value="' . get_search_query() . '" name="s" placeholder="' . esc_attr__( 'Search for ...', 'anemos' ) . '" autocomplete="off"/>';
	if ( defined( 'ICL_SITEPRESS_VERSION' ) && defined( 'ICL_LANGUAGE_CODE' ) ) {
		$form .= '<input type="hidden" name="lang" value="'. esc_attr( ICL_LANGUAGE_CODE ) .'"/>';
	}
	$form .= '</form>';
	return $form;
}

function anemos_eutf_wpsearch( $form ) {
	$form =  '<form class="eut-search" method="get" action="' . esc_url( home_url( '/' ) ) . '" >';
	$form .= '  <button type="submit" class="eut-search-btn eut-custom-btn"><i class="eut-icon-search"></i></button>';
	$form .= '  <input type="text" class="eut-search-textfield" value="' . get_search_query() . '" name="s" placeholder="' . esc_attr__( 'Search for ...', 'anemos' ) . '" />';
	$form .= '</form>';
	return $form;
}
//add_filter( 'get_search_form', 'anemos_eutf_wpsearch' );

/**
 * Enqueue scripts and styles for the front end.
 */
function anemos_eutf_frontend_scripts() {

	$anemos_version = ANEMOS_EUTF_THEME_MAJOR_VERSION . '.' . ANEMOS_EUTF_THEME_MINOR_VERSION . '.' . ANEMOS_EUTF_THEME_HOTFIX_VERSION;

	wp_register_style( 'anemos-eutf-style', get_stylesheet_directory_uri()."/style.css", array(), esc_attr( $anemos_version ), 'all' );
	wp_enqueue_style( 'anemos-eutf-awesome-fonts', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0' );

	wp_enqueue_style( 'anemos-eutf-theme-style', get_template_directory_uri() . '/css/theme-style.css', array(), esc_attr( $anemos_version ) );
	wp_enqueue_style( 'anemos-eutf-elements', get_template_directory_uri() . '/css/elements.css', array(), esc_attr( $anemos_version ) );

	if ( get_stylesheet_directory_uri() !=  get_template_directory_uri() ) {
		wp_enqueue_style( 'anemos-eutf-style');
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_style( 'anemos-eutf-responsive', get_template_directory_uri() . '/css/responsive.css', array(), esc_attr( $anemos_version ) );

	$gmap_api_key = anemos_eutf_option( 'gmap_api_key' );

	if ( !empty( $gmap_api_key ) ) {
		wp_register_script( 'google-maps', '//maps.googleapis.com/maps/api/js?v=3.24&key=' . esc_attr( $gmap_api_key ), NULL, NULL, true );
	} else {
		wp_register_script( 'google-maps', '//maps.googleapis.com/maps/api/js?v=3.24', NULL, NULL, true );
	}

	wp_register_script( 'anemos-eutf-maps-script', get_template_directory_uri() . '/js/maps.js', array( 'jquery', 'google-maps' ), esc_attr( $anemos_version ), true );
	$anemos_eutf_maps_data = array(
		'custom_enabled' => anemos_eutf_option( 'gmap_custom_enabled', '0' ) ,
		'water_color' => anemos_eutf_option( 'gmap_water_color', '#e9e9e9' ) ,
		'lanscape_color' => anemos_eutf_option( 'gmap_landscape_color', '#f1b144' ) ,
		'poi_color' => anemos_eutf_option( 'gmap_poi_color', '#f1b144' ) ,
		'road_color' => anemos_eutf_option( 'gmap_road_color', '#f2c77d' ) ,
		'label_color' => anemos_eutf_option( 'gmap_label_color', '#000000' ) ,
		'label_enabled' => anemos_eutf_option( 'gmap_label_enabled', '0' ) ,
		'country_color' => anemos_eutf_option( 'gmap_country_color', '#f1b144' ) ,
	);
	wp_localize_script( 'anemos-eutf-maps-script', 'anemos_eutf_maps_data', $anemos_eutf_maps_data );
	wp_enqueue_script( 'anemos-eutf-modernizr-script', get_template_directory_uri() . '/js/modernizr.custom.js', array( 'jquery' ), '2.8.3', false );
	if ( anemos_eutf_browser_webkit_check() ) {
		wp_enqueue_script( 'anemos-eutf-smoothscrolling-script', get_template_directory_uri() . '/js/smoothscrolling.js', array( 'jquery' ), esc_attr( $anemos_version ), true );
	}

	wp_enqueue_script( 'anemos-eutf-plugins', get_template_directory_uri() . '/js/plugins.js', array( 'jquery' ), esc_attr( $anemos_version ), true );
	$anemos_eutf_plugins_data = array(
		'retina_support' => anemos_eutf_option( 'retina_support', 'default' ),
	);
	wp_localize_script( 'anemos-eutf-plugins', 'anemos_eutf_plugins_data', $anemos_eutf_plugins_data );

	if ( is_page_template( 'page-templates/template-full-page.php' ) ) {
		wp_enqueue_script( 'anemos-eutf-full-page', get_template_directory_uri() . '/js/jquery.fullPage.js', array( 'jquery' ), '2.6.9', true );
	}

	wp_enqueue_script( 'anemos-eutf-main-script', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), esc_attr( $anemos_version ), true );

	$anemos_eutf_main_data = array(
		'siteurl' => get_template_directory_uri() ,
		'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
		'wp_gallery_popup' => anemos_eutf_option( 'wp_gallery_popup', '0' ),
		'back_top_top' => anemos_eutf_option( 'back_to_top_enabled', '1' ),
		'string_weeks' => esc_html__( 'Weeks', 'anemos' ),
		'string_days' => esc_html__( 'Days', 'anemos' ),
		'string_hours' => esc_html__( 'Hours', 'anemos' ),
		'string_minutes' => esc_html__( 'Min', 'anemos' ),
		'string_seconds' => esc_html__( 'Sec', 'anemos' ),
	);
	wp_localize_script( 'anemos-eutf-main-script', 'anemos_eutf_main_data', $anemos_eutf_main_data );



}
add_action( 'wp_enqueue_scripts', 'anemos_eutf_frontend_scripts' );

function anemos_eutf_remove_conflict_frontend_css() {

	//Deregister VC awesome fonts as it is already enqueued
	if ( wp_style_is( 'font-awesome', 'registered' ) ) {
		wp_deregister_style( 'font-awesome' );
	}

}
add_action( 'wp_head', 'anemos_eutf_remove_conflict_frontend_css', 2000 );

/**
 * Pagination functions
 */
function anemos_eutf_paginate_links() {
	global $wp_query;

	$paged = 1;
	if ( get_query_var( 'paged' ) ) {
		$paged = get_query_var( 'paged' );
	} elseif ( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	}

	$total = $wp_query->max_num_pages;
	$big = 999999999; // need an unlikely integer
	if( $total > 1 )  {
		 echo '<div class="eut-pagination eut-link-text">';

		 if( get_option('permalink_structure') ) {
			 $format = 'page/%#%/';
		 } else {
			 $format = '&paged=%#%';
		 }
		 echo paginate_links(array(
			'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format'		=> $format,
			'current'		=> max( 1, $paged ),
			'total'			=> $total,
			'mid_size'		=> 2,
			'type'			=> 'list',
			'prev_text'    => "<i class='eut-icon-nav-left'></i>",
			'next_text'    => "<i class='eut-icon-nav-right'></i>",
			'add_args' => false,
		 ));
		 echo '</div>';
	}
}

function anemos_eutf_wp_link_pages_args( $args ) {

	$args = array(
		'before'           => '<div class="eut-pagination eut-link-text"><ul><li>',
		'after'            => '</li></ul></div>',
		'link_before'      => '',
		'link_after'       => '',
		'next_or_number'   => 'number',
		'separator'        => '</li><li>',
		'nextpagelink'     => "<i class='eut-icon-nav-right'></i>",
		'previouspagelink' => "<i class='eut-icon-nav-left'></i>",
		'pagelink'         => '%',
		'echo'             => 1
	);

	return $args;
}
add_filter( 'wp_link_pages_args', 'anemos_eutf_wp_link_pages_args' );

/**
 * Comments
 */
function anemos_eutf_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	?>
	<li class="eut-comment-item eut-border">
		<!-- Comment -->
		<div id="comment-<?php comment_ID(); ?>"  <?php comment_class(); ?>>
			<div class="eut-comment-header">
				<div class="eut-author-image">
					<?php echo get_avatar( $comment, 50 ); ?>
				</div>
				<div class="eut-comment-title">
					<span class="eut-author eut-text-heading eut-h6"><?php comment_author(); ?></span>
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ) ?>" class="eut-small-text eut-comment-date eut-text-hover-primary-1"><?php printf( ' %1$s ' . esc_html__( 'at', 'anemos' ) . ' %2$s', get_comment_date(),  get_comment_time() ); ?></a>
				</div>
			</div>
			<div class="eut-comment-content">
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<p><?php esc_html_e( 'Your comment is awaiting moderation.', 'anemos' ); ?></p>
				<?php endif; ?>
				<div class="eut-comment-text"><?php comment_text(); ?></div>
				<div class="eut-reply-edit">
					<?php comment_reply_link( array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => esc_html__( 'Reply', 'anemos' ) ) ) ); ?>
					<?php edit_comment_link( esc_html__( 'Edit', 'anemos' ), '  ', '' ); ?>
				</div>
			</div>
		</div>

	<!-- </li> is added by WordPress automatically -->
<?php
}

/**
 * Navigation links for prev/next in comments
 */
function anemos_eutf_replace_reply_link_class( $output ) {
	$class = 'eut-comment-reply eut-link-text eut-text-primary-1 eut-text-dark-hover';
	return preg_replace( '/comment-reply-link/', 'comment-reply-link ' . $class, $output, 1 );
}
add_filter('comment_reply_link', 'anemos_eutf_replace_reply_link_class');

function anemos_eutf_replace_edit_link_class( $output ) {
	$class = 'eut-comment-edit eut-link-text eut-text-primary-1 eut-text-dark-hover';
	return preg_replace( '/comment-edit-link/', 'comment-edit-link ' . $class, $output, 1 );
}
add_filter('edit_comment_link', 'anemos_eutf_replace_edit_link_class');


/**
 * Title Render Fallback before WordPress 4.1
 */
 if ( ! function_exists( '_wp_render_title_tag' ) ) {
	function anemos_eutf_theme_render_title() {
?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php
	}
	add_action( 'wp_head', 'anemos_eutf_theme_render_title' );
}

/**
 * Theme identifier function
 * Used to get theme information
 */
function anemos_eutf_info() {

	$anemos_eutf_info = array (
		"major_version" => ANEMOS_EUTF_THEME_MAJOR_VERSION,
		"minor_version" => ANEMOS_EUTF_THEME_MINOR_VERSION,
		"hotfix_version" => ANEMOS_EUTF_THEME_HOTFIX_VERSION,
		"short_name" => 'anemos',
	);

	return $anemos_eutf_info;
}

/**
 * Add Container
 */
add_action('the_content','anemos_eutf_container_div');

function anemos_eutf_container_div( $content ){

	if( is_singular() && !has_shortcode( $content, 'vc_row') ) {
		return '<div class="eut-container">' . $content . '</div>';
	} else {
		return $content;
	}

}

/**
 * Add Body Class
 */
add_action('the_content','anemos_eutf_container_div');

function anemos_eutf_body_class( $classes ){
	$anemos_eutf_theme_layout = 'eut-' . anemos_eutf_option( 'theme_layout', 'stretched' );
	return array_merge( $classes, array( $anemos_eutf_theme_layout ) );
}
add_filter( 'body_class', 'anemos_eutf_body_class' );

/**
 * Max srcset filter
 */
function anemos_eutf_max_srcset_image_width( $max_image_width, $size_array ) {
	return 1920;
}
add_filter( 'max_srcset_image_width', 'anemos_eutf_max_srcset_image_width', 10 , 2 );

/**
 * VC Control Fix
 */
if ( ! function_exists( 'anemos_eutf_vc_control_scripts' ) ) {
	function anemos_eutf_vc_control_scripts() {
?>
	<script type="text/javascript">
	jQuery(document).on('click','.vc_ui-button[data-vc-ui-element="button-save"]', function(e){
		if ( vc !== undefined && vc.edit_form_callbacks !== undefined ) { vc.edit_form_callbacks=[]; }
	});
	</script>
<?php
	}
}
add_action('admin_print_footer_scripts', 'anemos_eutf_vc_control_scripts');

//Omit closing PHP tag to avoid accidental whitespace output errors.
