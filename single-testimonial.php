<?php get_header(); ?>

	<div id="eut-main-content">
		<?php
			the_post();
			$name =  anemos_eutf_post_meta( '_anemos_eutf_testimonial_name' );
			$identity =  anemos_eutf_post_meta( '_anemos_eutf_testimonial_identity' );
			if ( !empty( $name ) && !empty( $identity ) ) {
				$identity = ', ' . $identity;
			}
		?>
		<div class="eut-container">

			<div id="eut-post-area">
				<div class="eut-element eut-testimonial">
					<div class="eut-testimonial-element">
						<?php the_content(); ?>
						<?php if ( !empty( $name ) || !empty( $identity ) ) { ?>
						<div class="eut-testimonial-name"><?php echo esc_html( $name . $identity ); ?></div>
						<?php } ?>
					</div>
				</div>

				<?php wp_link_pages(); ?>

			</div>
		</div>
	</div>

<?php get_footer();

//Omit closing PHP tag to avoid accidental whitespace output errors.
