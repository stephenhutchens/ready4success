<?php
/**
 * The Quote Post Type Template
 */
?>

<?php
if ( is_singular() ) {

	$bg_color = anemos_eutf_post_meta( '_anemos_eutf_post_quote_bg_color', 'primary-1' );
	$bg_hover_color = anemos_eutf_post_meta( '_anemos_eutf_post_quote_bg_hover_color', 'black' );
	$bg_opacity = anemos_eutf_post_meta( '_anemos_eutf_post_quote_bg_opacity', '70' );
	$bg_options = array(
		'bg_color' => $bg_color,
		'bg_hover_color' => $bg_hover_color,
		'bg_opacity' => $bg_opacity,
	);
	$anemos_eutf_post_quote_name = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_quote_name' );
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'eut-single-post eut-post-quote' ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<div id="eut-single-quote">
			<div class="eut-container">
				<?php anemos_eutf_print_post_bg_image_container( $bg_options ); ?>
				<div class="eut-post-content">
					<i class="eut-post-icon eut-icon-quote"></i>
					<div>
						<?php anemos_eutf_print_post_excerpt('quote'); ?>
					</div>
					<?php if ( !empty( $anemos_eutf_post_quote_name ) ) { ?>
					<div class="eut-quote-writer eut-small-text eut-circle-arrow"><?php echo wp_kses_post(  $anemos_eutf_post_quote_name ); ?></div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div id="eut-post-content">
			<?php anemos_eutf_print_post_simple_title(); ?>
			<?php anemos_eutf_print_post_structured_data(); ?>
			<div itemprop="articleBody">
				<?php the_content(); ?>
			</div>
		</div>
	</article>

<?php
} else {
	$blog_style = anemos_eutf_option( 'blog_style', 'large' );
	$anemos_eutf_post_class = anemos_eutf_get_post_class( 'eut-style-2' );

	$bg_color = anemos_eutf_post_meta( '_anemos_eutf_post_quote_bg_color', 'primary-1' );
	$bg_hover_color = anemos_eutf_post_meta( '_anemos_eutf_post_quote_bg_hover_color', 'black' );
	$bg_opacity = anemos_eutf_post_meta( '_anemos_eutf_post_quote_bg_opacity', '70' );
	$bg_options = array(
		'bg_color' => $bg_color,
		'bg_hover_color' => $bg_hover_color,
		'bg_opacity' => $bg_opacity,
	);
	$anemos_eutf_post_quote_name = anemos_eutf_admin_post_meta( $post->ID, '_anemos_eutf_post_quote_name' );


?>

	<!-- Article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class( $anemos_eutf_post_class ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<?php do_action( 'anemos_eutf_inner_post_loop_item_before' ); ?>

		<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
			<?php anemos_eutf_print_post_bg_image_container( $bg_options ); ?>
			<div class="eut-post-content">
				<i class="eut-post-icon eut-icon-quote"></i>
				<?php do_action( 'anemos_eutf_inner_post_loop_item_title_hidden' ); ?>
				<?php anemos_eutf_print_post_structured_data(); ?>
				<div itemprop="articleBody">
				<?php anemos_eutf_print_post_excerpt('quote'); ?>
				</div>
				<?php if ( !empty( $anemos_eutf_post_quote_name ) ) { ?>
					<div class="eut-quote-writer eut-small-text eut-circle-arrow"><?php echo wp_kses_post(  $anemos_eutf_post_quote_name ); ?></div>
				<?php } ?>
			</div>

		</a>

		<?php do_action( 'anemos_eutf_inner_post_loop_item_after' ); ?>
	</article>
	<!-- End Article -->

<?php
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
