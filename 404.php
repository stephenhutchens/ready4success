<?php get_header( 'basic' ); ?>

			<div id="eut-content" class="eut-error-404 clearfix">
				<div class="eut-content-wrapper">
					<div id="eut-main-content">
						<div class="eut-main-content-wrapper clearfix">

							<div class="eut-section eut-fullheight eut-feature-header eut-feature-footer">
								<div class="eut-container">
									<div class="eut-row">
										<div class="wpb_column eut-column-1">
											<div class="eut-column-wrapper">
												<div class="eut-align-center">

													<div id="eut-content-area">
													<?php
														$anemos_eutf_404_search_box = anemos_eutf_option('page_404_search');
														$anemos_eutf_404_home_button = anemos_eutf_option('page_404_home_button');
														echo do_shortcode( anemos_eutf_option( 'page_404_content' ) );
													?>
													</div>

													<br/>

													<?php if ( $anemos_eutf_404_search_box ) { ?>
													<div class="eut-widget">
														<?php get_search_form(); ?>
													</div>
													<br/>
													<?php } ?>

													<?php if ( $anemos_eutf_404_home_button ) { ?>
													<div class="eut-element">
														<a class="eut-btn eut-btn-medium eut-round eut-bg-primary-1 eut-bg-hover-black" target="_self" href="<?php echo esc_url( home_url( '/' ) ); ?>">
															<span><?php echo esc_html( get_bloginfo( 'name' ) ); ?></span>
														</a>
													</div>
													<?php } ?>

												</div>

											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

<?php get_footer( 'basic' );

//Omit closing PHP tag to avoid accidental whitespace output errors.
