<!doctype html>
<!--[if lt IE 10]>
<html class="ie9 no-js" <?php language_attributes(); ?>>
<![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

	<head>
		<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) { ?>
		<!-- allow pinned sites -->
		<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
		<?php } ?>
		<?php wp_head(); ?>
	</head>

	<body id="eut-body" <?php body_class(); ?>>
		<?php do_action( 'anemos_eutf_body_top' ); ?>
		<?php if ( anemos_eutf_check_theme_loader_visibility() ) { ?>

		<!-- LOADER -->
		<div id="eut-loader-overflow">
			<div class="eut-loader"></div>
		</div>
		<?php } ?>

		<!-- Theme Wrapper -->
		<div id="eut-theme-wrapper">
