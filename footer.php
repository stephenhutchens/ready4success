
			<?php anemos_eutf_print_bottom_bar(); ?>

			<footer id="eut-footer" data-sticky-footer="no" class="eut-border eut-bookmark">

				<div class="eut-footer-wrapper">
				<?php anemos_eutf_print_footer_widgets(); ?>
				<?php anemos_eutf_print_footer_bar(); ?>
				<?php anemos_eutf_print_footer_bg_image(); ?>
				</div>

			</footer>

			<!-- SIDE AREA -->
			<?php
				$anemos_eutf_sidearea_data = anemos_eutf_get_sidearea_data();
				anemos_eutf_print_side_area( $anemos_eutf_sidearea_data );
			?>
			<!-- END SIDE AREA -->

			<!-- HIDDEN MENU -->
			<?php anemos_eutf_print_hidden_menu(); ?>
			<!-- END HIDDEN MENU -->

			<?php anemos_eutf_print_search_modal(); ?>
			<?php anemos_eutf_print_form_modals(); ?>
			<?php anemos_eutf_print_language_modal(); ?>
			<?php anemos_eutf_print_login_modal(); ?>
			<?php anemos_eutf_print_social_modal(); ?>

			<?php do_action( 'anemos_eutf_footer_modal_container' ); ?>

			<?php anemos_eutf_print_back_top(); ?>

		</div> <!-- end #eut-theme-wrapper -->

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>