<?php get_header(); ?>


<?php anemos_eutf_print_header_title( 'blog' ); ?>
<?php anemos_eutf_print_header_breadcrumbs( 'post' ); ?>

<!-- CONTENT -->
<div id="eut-content" class="clearfix <?php echo anemos_eutf_sidebar_class( 'blog' ); ?>">
	<div class="eut-content-wrapper">
		<!-- MAIN CONTENT -->
		<div id="eut-main-content">
			<div class="eut-main-content-wrapper clearfix">

				<div class="eut-section" style="margin-bottom: 0px;">

					<div class="eut-container">

						<!-- ROW -->
						<div class="eut-row">

							<!-- COLUMN 1 -->
							<div class="wpb_column eut-column-1">
								<div class="eut-column-wrapper">
									<!-- Blog FitRows -->
									<?php
										if( is_author() ){
											anemos_eutf_print_post_about_author('overview');
										}
										$anemos_eutf_blog_style = anemos_eutf_option( 'blog_style', 'large' );
										$anemos_eutf_blog_class = anemos_eutf_get_blog_class();
									?>
									<div class="<?php echo esc_attr( $anemos_eutf_blog_class ); ?>" <?php anemos_eutf_print_blog_data(); ?>>

										<?php
										if ( have_posts() ) :
											if ( 'large' == $anemos_eutf_blog_style || 'small' == $anemos_eutf_blog_style ) {
										?>
											<div class="eut-standard-container">
										<?php
											} else {
										?>
											<div class="eut-isotope-container">
										<?php
											}

										// Start the Loop.
										while ( have_posts() ) : the_post();
											//Get post template
											get_template_part( 'content', get_post_format() );
										endwhile;

										?>
										</div>
										<?php
											// Previous/next post navigation.
											anemos_eutf_paginate_links();
										else :
											// If no content, include the "No posts found" template.
											get_template_part( 'content', 'none' );
										endif;
										?>

									</div>
									<!-- End Element Blog -->
								</div>
							</div>
							<!-- END COLUMN 1 -->

						</div>
						<!-- END ROW -->

					</div>

				</div>

			</div>
		</div>
		<!-- End Content -->
		<?php
			anemos_eutf_set_current_view( 'blog' );
			if ( is_front_page() ) {
				//anemos_eutf_set_current_view( 'frontpage' );
			}
		?>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer();

//Omit closing PHP tag to avoid accidental whitespace output errors.
