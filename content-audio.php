<?php
/**
 * The Audio Post Type Template
 */
?>

<?php
if ( is_singular() ) {
	$anemos_eutf_disable_media = anemos_eutf_post_meta( '_anemos_eutf_disable_media' );
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'eut-single-post' ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<?php
			if ( 'yes' != $anemos_eutf_disable_media ) {
		?>
		<div id="eut-single-media">
			<div class="eut-container">
				<?php anemos_eutf_print_post_audio(); ?>
			</div>
		</div>
		<?php
			}
		?>
		<div id="eut-post-content">
			<?php anemos_eutf_print_post_simple_title(); ?>
			<?php anemos_eutf_print_post_structured_data(); ?>
			<div itemprop="articleBody">
				<?php the_content(); ?>
			</div>
		</div>

	</article>

<?php
} else {
	$blog_style = anemos_eutf_option( 'blog_style', 'large' );
	$anemos_eutf_post_class = anemos_eutf_get_post_class();
?>

	<!-- Article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class( $anemos_eutf_post_class ); ?> itemscope itemType="http://schema.org/BlogPosting">
		<?php do_action( 'anemos_eutf_inner_post_loop_item_before' ); ?>
		<?php anemos_eutf_print_post_feature_media( 'audio' ); ?>
		<div class="eut-post-content-wrapper">
			<div class="eut-post-content">
				<?php anemos_eutf_print_post_meta_top(); ?>
				<?php anemos_eutf_print_post_structured_data(); ?>
				<div itemprop="articleBody">
					<?php anemos_eutf_print_post_excerpt(); ?>
				</div>
			</div>
			<?php anemos_eutf_print_post_meta_bottom(); ?>
		</div>
		<?php do_action( 'anemos_eutf_inner_post_loop_item_after' ); ?>
	</article>
	<!-- End Article -->

<?php

}

//Omit closing PHP tag to avoid accidental whitespace output errors.
