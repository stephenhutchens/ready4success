<?php

	if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && 'comments.php' == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
		die ( esc_html__( 'This page cannot be opened directly!', 'anemos' ) );
	}

	if ( post_password_required() ) {
?>
		<div class="help">
			<p class="no-comments"><?php esc_html_e( 'This post is password protected. Enter the password to view comments.', 'anemos' ); ?></p>
		</div>
<?php
		return;
	}
?>

<?php if ( have_comments() ) : ?>

	<!-- Comments -->
	<div id="eut-comments" class="eut-padding-top-2x eut-margin-top-2x eut-border eut-border-top clearfix">
		<div class="eut-comments-header">
			<h5 class="eut-comments-number eut-text-heading">
			<?php comments_number( esc_html__( 'no comments', 'anemos' ), esc_html__( '1 comment', 'anemos' ), '% ' . esc_html__( 'comments', 'anemos' ) ); ?>
			</h5>
			<nav class="eut-comment-nav eut-small-text">
				<ul>
			  		<li><?php previous_comments_link(); ?></li>
			  		<li><?php next_comments_link(); ?></li>
			 	</ul>
			</nav>
		</div>
		<ul>
		<?php wp_list_comments( 'type=comment&callback=anemos_eutf_comments' ); ?>
		</ul>
	</div>
	<!-- End Comments -->

<?php endif; ?>


<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

	<div id="eut-comments" class="eut-padding-top-2x eut-margin-top-2x eut-border eut-border-top clearfix">
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'anemos' ); ?></p>
	</div>

<?php endif; ?>


<?php if ( comments_open() ) : ?>

<?php
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );

		$args = array(
			'id_form'           => 'commentform',
			'id_submit'         => 'eut-comment-submit-button',
			'title_reply'       => esc_html__( 'Leave a Reply', 'anemos' ),
			'title_reply_to'    => esc_html__( 'Leave a Reply to', 'anemos' ) . ' %s',
			'cancel_reply_link' => esc_html__( 'Cancel Reply', 'anemos' ),
			'label_submit'      => esc_html__( 'Submit Comment', 'anemos' ),
			'submit_button'     => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',

			'comment_field' =>
				'<div class="eut-form-textarea eut-border">'.
				'<textarea style="resize:none;" id="comment" name="comment" placeholder="' . esc_attr__( 'Your Comment Here...', 'anemos' ) . '" cols="45" rows="15" aria-required="true">' .
				'</textarea></div>',

			'must_log_in' =>
				'<p class="must-log-in">' . esc_html__( 'You must be', 'anemos' ) .
				'<a href="' .  wp_login_url( get_permalink() ) . '">' . esc_html__( 'logged in', 'anemos' ) . '</a> ' . esc_html__( 'to post a comment.', 'anemos' ) . '</p>',

			'logged_in_as' =>
				'<div class="logged-in-as eut-link-text">' .  esc_html__('Logged in as','anemos') .
				'<a class="eut-text-content eut-text-hover-primary-1" href="' . esc_url( admin_url( 'profile.php' ) ) . '"> ' . $user_identity . '</a>. ' .
				'<a class="eut-text-content eut-text-hover-primary-1" href="' . wp_logout_url( get_permalink() ) . '" title="' . esc_attr__( 'Log out of this account', 'anemos' ) . '"> ' . esc_html__( 'Log out', 'anemos' ) . '</a></div>',

			'comment_notes_before' => '',
			'comment_notes_after' => '' ,

			'fields' => apply_filters(
				'comment_form_default_fields',
				array(
					'author' =>
						'<div class="eut-form-input eut-border">' .
						'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '"' .
						' placeholder="' . esc_attr__( 'Name', 'anemos' ) . ' ' . ( $req ? esc_attr__( '(required)', 'anemos' ) : '' ) . '" />' .
						'</div>',

					'email' =>
						'<div class="eut-form-input eut-border">' .
						'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '"' .
						' placeholder="' . esc_attr__( 'E-mail', 'anemos' ) . ' ' . ( $req ? esc_attr__( '(required)', 'anemos' ) : '' ) . '" />' .
						'</div>',

					'url' =>
						'<div class="eut-form-input eut-border">' .
						'<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '"' .
						' placeholder="' . esc_attr__( 'Website', 'anemos' )  . '" />' .
						'</div>',
					)
				),
		);
?>
		<div id="eut-comment-form" class="eut-margin-top-2x clearfix">
			<?php
				//Use comment_form() with no parameters if you want the default form instead.
				comment_form( $args );
			?>
		</div>


<?php endif;

//Omit closing PHP tag to avoid accidental whitespace output errors.