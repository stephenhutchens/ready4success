<?php

/*
*	Main sidebar area
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/


if( is_search() ) {
	return;
}

$fixed = "";
$anemos_eutf_sidebar_extra_content = false;

$sidebar_view = anemos_eutf_get_current_view();
if ( 'shop' == $sidebar_view ) {
	if ( is_shop() ) {
		$anemos_eutf_sidebar_id = anemos_eutf_post_meta_shop( '_anemos_eutf_sidebar', anemos_eutf_option( 'page_sidebar' ) );
		$anemos_eutf_sidebar_layout = anemos_eutf_post_meta_shop( '_anemos_eutf_layout', anemos_eutf_option( 'page_layout', 'none' ) );
		if ( 'yes' == anemos_eutf_post_meta_shop( '_anemos_eutf_fixed_sidebar' ) ) {
			$fixed = " eut-fixed-sidebar";
		}			
	} else if( is_product() ) {
		$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidebar', anemos_eutf_option( 'product_sidebar' ) );
		$anemos_eutf_sidebar_layout = anemos_eutf_post_meta( '_anemos_eutf_layout', anemos_eutf_option( 'product_layout', 'none' ) );
		if ( 'yes' == anemos_eutf_post_meta( '_anemos_eutf_fixed_sidebar' ) ) {
			$fixed = " eut-fixed-sidebar";
		}
	} else {
		$anemos_eutf_sidebar_id = anemos_eutf_option( 'product_tax_sidebar' );
		$anemos_eutf_sidebar_layout = anemos_eutf_option( 'product_tax_layout', 'none' );
	}
} else if ( is_singular() ) {
	if ( 'yes' == anemos_eutf_post_meta( '_anemos_eutf_fixed_sidebar' ) ) {
		$fixed = " eut-fixed-sidebar";
	}
	if ( is_singular( 'post' ) ) {
		$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidebar', anemos_eutf_option( 'post_sidebar' ) );
		$anemos_eutf_sidebar_layout = anemos_eutf_post_meta( '_anemos_eutf_layout', anemos_eutf_option( 'post_layout', 'none' ) );
	} else if ( is_singular( 'portfolio' ) ) {
		$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidebar', anemos_eutf_option( 'portfolio_sidebar' ) );
		$anemos_eutf_sidebar_layout = anemos_eutf_post_meta( '_anemos_eutf_layout', anemos_eutf_option( 'portfolio_layout', 'none' ) );
		$anemos_eutf_sidebar_extra_content = anemos_eutf_check_portfolio_details();
		if( $anemos_eutf_sidebar_extra_content && 'none' == $anemos_eutf_sidebar_layout ) {
			$anemos_eutf_sidebar_layout = 'right';
		}
	} else {
		$anemos_eutf_sidebar_id = anemos_eutf_post_meta( '_anemos_eutf_sidebar', anemos_eutf_option( 'page_sidebar' ) );
		$anemos_eutf_sidebar_layout = anemos_eutf_post_meta( '_anemos_eutf_layout', anemos_eutf_option( 'page_layout', 'none' ) );
	}
} else {
	$anemos_eutf_sidebar_id = anemos_eutf_option( 'blog_sidebar' );
	$anemos_eutf_sidebar_layout = anemos_eutf_option( 'blog_layout', 'none' );
}

if ( 'none' != $anemos_eutf_sidebar_layout && ( is_active_sidebar( $anemos_eutf_sidebar_id ) || $anemos_eutf_sidebar_extra_content ) ) {
	if ( 'left' == $anemos_eutf_sidebar_layout || 'right' == $anemos_eutf_sidebar_layout ) {

		$anemos_eutf_sidebar_class = 'eut-sidebar' . $fixed;
?>
		<!-- Sidebar -->
		<aside id="eut-sidebar" class="<?php echo esc_attr( $anemos_eutf_sidebar_class ); ?>">
			<div class="eut-wrapper clearfix">
				<?php
					if ( $anemos_eutf_sidebar_extra_content ) {
						anemos_eutf_print_portfolio_details();
					}
				?>
				<?php dynamic_sidebar( $anemos_eutf_sidebar_id ); ?>
			</div>
		</aside>
		<!-- End Sidebar -->
<?php
	}
}

//Omit closing PHP tag to avoid accidental whitespace output errors.
