
<?php if( ! is_user_logged_in() ) { ?>
	<div class="eut-login-modal">
		<?php
			wp_enqueue_script( 'anemos-eutf-login', get_template_directory_uri() . '/js/ajax-login.js', array( 'jquery' ), '1.0.0', true );

			wp_localize_script( 'anemos-eutf-login', 'eut_form', array(
				'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
			));

			if( get_option('users_can_register') ){ ?>

				<!-- Register form -->
				<div class="eut-register-form eut-login-form-item eut-align-center">

					<div class="eut-login-form-title eut-h3 eut-with-line eut-align-center"><?php printf( esc_html__( 'Join %s', 'anemos' ), get_bloginfo('name') ); ?></div>

					<form id="eut_registration_form" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="POST">

						<div class="eut-form-field">
							<input class="eut-form-control required" name="eut_user_login" type="text" placeholder="<?php esc_html_e( 'Username *', 'anemos' ); ?>"/>
						</div>
						<div class="eut-form-field">
							<input class="eut-form-control required" name="eut_user_email" id="eut_user_email" type="email" placeholder="<?php esc_html_e( 'Email *', 'anemos' ); ?>"/>
						</div>
						<?php do_action( 'anemos_eutf_register_form' ); ?>
						<div class="eut-form-field">
							<input type="hidden" name="action" value="anemos_eutf_register_user"/>
							<button class="btn eut-fullwidth-btn" data-loading-text="<?php esc_attr_e( 'Loading...', 'anemos' ) ?>" type="submit"><?php esc_html_e('Sign up', 'anemos'); ?></button>
						</div>
						<?php wp_nonce_field( 'anemos_eutf_nonce', '_anemos_eutf_nonce_register' ); ?>
					</form>
					<div class="eut-form-errors eut-align-center eut-text-primary-1 eut-link-text"></div>
				</div>

			<?php } ?>

				<!-- Login form -->
				<div class="eut-login-form eut-login-form-item eut-align-center">

					<div class="eut-login-form-title eut-h3 eut-with-line eut-align-center"><?php printf( esc_html__('Login to %s', 'anemos'), get_bloginfo('name') ); ?></div>

					<form id="eut_login_form" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="post">

						<div class="eut-form-field">
							<input class="eut-form-control required" name="eut_user_login" type="text" placeholder="<?php esc_html_e( 'Username *', 'anemos' ); ?>"/>
						</div>
						<div class="eut-form-field">
							<input class="eut-form-control required" name="eut_user_pass" id="eut_user_pass" type="password" placeholder="<?php esc_html_e( 'Password *', 'anemos' ); ?>"/>
						</div>
						<?php do_action( 'anemos_eutf_login_form' ); ?>
						<div class="eut-form-field">
							<input type="hidden" name="action" value="anemos_eutf_login_user"/>
							<button class="btn eut-fullwidth-btn" data-loading-text="<?php esc_html_e('Loading...', 'anemos') ?>" type="submit"><?php _e('Login', 'anemos'); ?></button>
							<a class="eut-reset-password-form-btn eut-link-text eut-text-hover-primary-1" href="#"><?php _e('Lost Password?', 'anemos') ?></a>
						</div>
						<?php wp_nonce_field( 'anemos_eutf_nonce', '_anemos_eutf_nonce_login' ); ?>
					</form>
					<div class="eut-form-errors eut-align-center eut-text-primary-1 eut-link-text"></div>
				</div>

				<!-- Lost Password form -->
				<div class="eut-reset-password-form eut-login-form-item eut-align-center">

					<div class="eut-login-form-title eut-h3 eut-with-line eut-align-center"><?php esc_html_e('Reset Password', 'anemos'); ?></div>
					<span class="eut-login-form-description eut-link-text"><?php esc_html_e( 'Enter the username or e-mail you used in your profile. A password reset link will be sent to you by email.', 'anemos'); ?></span>

					<form id="eut_reset_password_form" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="post">
						<div class="eut-form-field">
							<input class="eut-form-control required" name="eut_user_or_email" id="eut_user_or_email" type="text" placeholder="<?php esc_html_e( 'Username or E-mail', 'anemos' ); ?>"/>
						</div>
						<?php do_action( 'anemos_eutf_reset_password_form' ); ?>
						<div class="eut-form-field">
							<input type="hidden" name="action" value="anemos_eutf_reset_password_user"/>
							<button class="btn eut-fullwidth-btn" data-loading-text="<?php esc_attr_e('Loading...', 'anemos') ?>" type="submit"><?php esc_html_e('Get new password', 'anemos'); ?></button>
						</div>
						<?php wp_nonce_field( 'anemos_eutf_nonce', '_anemos_eutf_nonce_password' ); ?>
					</form>
					<div class="eut-form-errors eut-align-center eut-text-primary-1 eut-link-text"></div>
				</div>
	</div>
	<div class="eut-login-modal-footer eut-align-center">
		<span class="eut-login-link eut-link-text"><?php esc_html_e( 'Already have an account?', 'anemos' ); ?> <a class="eut-text-hover-primary-1 eut-login-form-btn" href="#"><?php esc_html_e('Login', 'anemos'); ?></a></span>
		<?php if( get_option('users_can_register') ) { ?>
		<br/>
		<span class="eut-register-link eut-link-text"><?php esc_html_e('Don\'t have an account?', 'anemos' ); ?> <a class="eut-register-form-btn" href="#"><?php esc_html_e('Sign Up', 'anemos'); ?></a></span>
		<?php } ?>
		</div>
<?php } else { ?>
	<div class="eut-login-modal-footer">
		<div class="eut-alert eut-alert-info eut-align-center eut-link-text"><?php $current_user = wp_get_current_user(); printf( __( 'You have already logged in as %1$s. <a class="eut-text-hover-primary-1" href="%2$s">Logout?</a>', 'anemos' ), $current_user->user_login, wp_logout_url( home_url() ) ); ?></div>
	</div>
<?php } ?>
