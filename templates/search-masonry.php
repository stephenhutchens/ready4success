<?php
/*
*	Template Search Masonry
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/
?>

<?php
	$title_tag = anemos_eutf_option( 'search_page_heading_tag', 'h4' );
	$title_class = anemos_eutf_option( 'search_page_heading', 'h4' );
	$excerpt_length = anemos_eutf_option( 'search_page_excerpt_length_small' );
	$excerpt_more = anemos_eutf_option( 'search_page_excerpt_more' );
	$search_page_show_image = anemos_eutf_option( 'search_page_show_image', 'yes' );
	$search_page_style = anemos_eutf_option( 'search_page_style', 'masonry' );

	if ( 'grid' == $search_page_style ) {
		$image_size  = 'anemos-eutf-small-rect-horizontal';
	} else {
		$image_size  = 'large';
	}

?>

<article id="eut-search-<?php the_ID(); ?><?php echo uniqid('-'); ?>" <?php post_class( 'eut-blog-item eut-isotope-item' ); ?>>
	<div class="eut-blog-item-inner eut-isotope-item-inner">
	<?php if ( 'yes' == $search_page_show_image && has_post_thumbnail() ) { ?>
		<div class="eut-media clearfix">
			<a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_post_thumbnail( $image_size ); ?></a>
		</div>
	<?php } ?>
		<div class="eut-post-content">
			<?php the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><' . tag_escape( $title_tag ) . ' class="eut-post-title eut-text-hover-primary-1 eut-' . esc_attr( $title_class ) . '">', '</' . tag_escape( $title_tag ) . '></a>' ); ?>
			<div itemprop="articleBody">
				<?php echo anemos_eutf_excerpt( $excerpt_length, $excerpt_more  ); ?>
			</div>
		</div>
	</div>
</article>