��    <      �  S   �      (     )     6  
   C  	   N     X     \     l     u     y     �     �     �     �     �     �     �     �     �               #     ;     H     L  	   _     i     q     v     �     �     �  
   �     �  	   �     �     �     �          "     &  O   5  $   �  E   �     �                               -     A     M  $   b     �     �  	   �     �  	   �     �  �  �     �     �     �     �     �     �                    5      H     i     }     �     �     �     �     �     �     �          $     3     7     L     R     X     _  *   w  
   �  
   �     �     �     �     �       !   "  
   D     O     S  L   e  ,   �  `   �     @     Q  	   U     _     k     s     �  
   �     �  -   �     �                 	   $     .     "      &   +   
           $      !          (      *   3      -      7                     2                      /                    5             <                 8      	      :      ,                #       0                    6   ;   1   .       %       9   )          4                 '    %s (Invalid) %s (Pending) (required) 1 comment All All stories by: Archives By: CSS Classes (optional) Cancel Reply Comments are closed. Daily Archives : Days E-mail Edit Menu Item Hours Leave a Reply Leave a Reply to Link Relationship (XFN) Log out Log out of this account Logged in as Min Monthly Archives : Move down Move up Name Navigation Label Open link in a new window/tab Page %s Phone: Posts By : Posts Tagged : Read Next Read Previous Search Results for : Search Results for : %s Search for ... Sec Submit Comment The description will be displayed in the menu if the current theme supports it. This page cannot be opened directly! This post is password protected. Enter the password to view comments. Title Attribute URL Website Website: Weeks Yearly Archives : You might also like You must be Your Comment Here... Your comment is awaiting moderation. comments in logged in no comments read more to post a comment. Project-Id-Version: Anemos v1.0.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-07-13 12:47+0300
PO-Revision-Date: 2016-07-13 12:47+0300
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: woocommerce
X-Poedit-SearchPathExcluded-1: includes/framework
 %s (no válido) %s (Pendiente) (requerido) 1 comentario Todo Todos los relatos por: Archivos Por: Clases CSS (opcional) Cancelar Respuesta Los comentarios están cerrados. Archivo por Días : Días Correo electrónico Editar elemento del Menú Horas Dejar una Respuesta Dejar una Respuesta a Relación de links (XFN) Finalizar la sesión Salir de esta cuenta Conectado como Min Archivos Mensuales : Bajar Subir Nombre Etiqueta de Navegación Abrir enlace en una nueva ventana/pestaña Página %s Teléfono: Entradas Por : Entradas Etiquetadas : Lea a continuación Lectura anterior Resultados de Búsqueda para : Resultados de Búsqueda para : %s Buscar ... Seg Enviar Comentario La descripción será desplegada en el menú si el tema corriente lo soporta Esta página no se puede abrir directamente! Esta entrada está protegida con contraseña. Introduce la contraseña para ver los comentarios. Atributo Título URL Sitio web Pagina Web: Semanas Archivos Anuales : También te puede gustar Tienes que Entra tu Comentario Aquí... Tu comentario está pendiente de moderación. comentarios en iniciar sesión sin comentarios leer más para publicar un comentario. 