<?php
/*
*	Template Content None
*
* 	@version	1.0
* 	@author		Euthemians Team
* 	@URI		http://euthemians.com
*/
?>
<div class="eut-content-none">
	<div class="eut-post-content">
		<?php echo do_shortcode( anemos_eutf_option( 'search_page_not_found_text' ) ); ?>
		<div class="eut-widget">
			<?php get_search_form(); ?>
		</div>
	</div>
</div>